/*******************************************************
 ***  Header file containing typedefs for explictly  ***
 ***  single- and double-precision types.            ***
 ***  Used by low-level input routines.              ***
 *******************************************************/

#ifndef _SU3_PREC_H
#define _SU3_PREC_H

#include "su3.h"

typedef struct {
  float re;
  float im;
} single_complex;

typedef struct {
  double re;
  double im;
} double_complex;

typedef single_complex Su3_single[COLOUR][COLOUR];
typedef single_complex Short_Trans_Su3_single[COLOUR][COLOUR-1];
typedef single_complex Short_Su3_single[COLOUR-1][COLOUR];

typedef double_complex Su3_double[COLOUR][COLOUR];
typedef double_complex Short_Trans_Su3_double[COLOUR][COLOUR-1];
typedef double_complex Short_Su3_double[COLOUR-1][COLOUR];

#ifdef SPIN
typedef single_complex Spinor_single[SPIN][COLOUR];
typedef double_complex Spinor_double[SPIN][COLOUR];

typedef Su3_single Prop_single[SPIN][SPIN];
typedef Su3_double Prop_double[SPIN][SPIN];
#endif

#endif
