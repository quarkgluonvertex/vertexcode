#include "gauge.h"
#include <math.h>
#include <arralloc.h>

int compute_gluon(Gauge* gluon, Gauge* u)
{
  const char fnm[MAXNAME] = "compute_gluon";
  Lattice* lat = u->parm->lattice;
  Group_el** uu = u->u;
  Algebra_el** a;
  const Real g0inv = sqrt(0.5*u->parm->beta/COLOUR);
  int i,mu;

  if (GROUP!=SU2 && GROUP!=SU3) {
    fprintf(stderr,"%s: gauge group %d not implemented!\n",fnm,GROUP);
    return EVIL;
  }
  if (u->type==ALGEBRA || u->type==FALG) {
    fprintf(stderr,"%s: Warning: input is already a gluon field\n",fnm);
    fprintf(stderr,"%s: doing nothing\n",fnm);
    if (gluon!=u) *gluon = *u;
    return 0;
  }    
  if (u->type != GAUGE) {
    fprintf(stderr,"%s: computing gluon from invalid gauge field type %d\n",
	    fnm,u->type);
    return BADDATA;
  } /* TODO: compute gluon from links in momentum space? */

  if (gluon==u) {
    a = arralloc(sizeof(Algebra_el),2,u->npoint,u->ndir);
  } else {
    if (gluon->type != ALGEBRA) {
      fprintf(stderr,"%s: gluon field is not gluon type!\n",fnm);
      return BADDATA;
    }
    gluon->parm = u->parm;
    gluon->ndir = u->ndir;
    if (!gluon->u) {
      gluon->u = arralloc(sizeof(Algebra_el),2,lat->nsite,gluon->ndir);
      if (!gluon->u) {
	fprintf(stderr,"%s: cannot allocate space for gluon\n",fnm);
	return NOSPACE;
      }
    }
    a = gluon->u;
  }

  if (gluon->ndir<1 || gluon->ndir>DIR) {
    fprintf(stderr,"%s: Warning: setting no of gluon field components to %d\n",
	    fnm,lat->ndim);
    gluon->ndir = lat->ndim;
  }
    
  if (u->parm->action != WILSON) {
    fprintf(stderr,"%s: Warning: computing naive (unimproved) gluon\n",fnm);
  }
			
  for (i=0; i<lat->nsite; i++) {
    for (mu=0; mu<gluon->ndir; mu++) {
#ifdef SU2_GROUP
      a[i][mu][0] = 2*uu[i][mu][1]*g0inv;
      a[i][mu][1] = 2*uu[i][mu][2]*g0inv;
      a[i][mu][2] = 2*uu[i][mu][3]*g0inv;
#elif defined SU3_GROUP
      su3_to_su3alg(a[i][mu],uu[i][mu]);
      su3alg_by_real(a[i][mu],a[i][mu],g0inv);
#endif
    }
  }

  if (gluon==u) {
    free(u->u);
    u->u = a;
    u->type = ALGEBRA;
  }
  return 0;
}
