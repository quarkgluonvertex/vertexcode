#ifndef _SORT_H_
#define _SORT_H_

#include <stdio.h>
#include <stdlib.h>
#include "Real.h"

void keysort(Real *val, int length, int *key);

#endif
