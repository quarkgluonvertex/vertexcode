/* $Id: momenta.c,v 1.13 2013/09/23 14:28:24 jonivar Exp $ */

#include "momenta.h"
#include "sort.h"
#include <arralloc.h>
#include <math.h>

#define SWAP(a,b) { int tmp=(a);(a)=(b);(b)=tmp; }
#define SORT(a,b,c) {if ((a)>(b)) SWAP (a,b);if ((b)>(c)) SWAP(b,c);if ((a)>(b)) SWAP(a,b);}

static int check_input(Momenta *mom);
static int indx(int x, int y, int z, int L);
static int nequiv(int x, int y, int z, int ndim);
static void* talloc(int n1, int n2, int n3, int n4, int* ind);

/************************************************************************
 * set_default_momenta: assigns default values to a Momenta struct      *
 *   (no cuts, no sorting, no smearing, only z3 averaging)              *
 *   leaving the Momenta uninitialised.                                 *
 *   The lattice size, nmomenta and boundary conditions must be set     *
 *   separately.                                                        *
 ************************************************************************/

void set_default_momenta(Momenta* mom)
{
  int i;

  /* set invalid values for lattice size and nmom */
  for (i=0;i<DIR;i++) {
    mom->L[i] = 0;
    mom->nmom_in[i] = mom->nmom_out[i] = 0;
    mom->bc[i] = PERIODIC;
  }

  mom->cutoff = mom->dcut = -1.0;
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = mom->z4 = FALSE;
  mom->nvar = 1;             /* only naive momenta by default */
  mom->sort = FALSE;
  mom->sortvar = 0;
  mom->smear = NONE;
  mom->smearing = 0.0;
  mom->key = mom->index = mom->nequiv = 0;
  mom->val = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nval = 0;
  mom->initialised = FALSE;
}

/************************************************************************
 * setup_momenta_onaxis: selects the on-axis momenta up to nmom_out[i]  *
 *   from a 4-dimensional momentum array, sorting them into a           *
 *   1-dimensional array.  The three spatial directions are symmetrised.*
 *   If we have antiperiodic boundary conditions in time, momenta which *
 *   are on-axis in space and with pt = +-1/2 are also selected.        *
 ************************************************************************/

int setup_momenta_onaxis(Momenta *mom)
{
  const Real unit_sqr
    = (Real)(mom->L[IZ]*mom->L[IZ])/(Real)(mom->L[IT]*mom->L[IT]);
  const int bct = mom->bc[IT];
  const int npt = bct==PERIODIC ? mom->nmom_out[IT]+1 : mom->nmom_out[IT];
  const int nptin = bct==PERIODIC ? mom->nmom_in[IT]+1 : mom->nmom_in[IT];
  const int nall = nallmom(mom);
  const char fnm[MAXNAME] = "setup_momenta_onaxis";

  int i, p, pt, p_spatial, n_spatial;
  int status;
  Real p_tot;

/* Initialise control arrays */

  if (status=check_input(mom)) return status;
  if (mom->dcut > 0.0) {
    printf("%s: Warning: ignoring cylinder cut\n", fnm);
  }
  if (mom->nvar > 1) {
    printf("%s: Warning: only computing naive momentum values\n", fnm);
    printf("%s: resetting nvar to 1\n",fnm);
    mom->nvar=1;
  }
  if ( mom->smear ) {
    printf("%s: Warning: smearing not implemented (use smear_momenta)\n", fnm);
  }
  if (mom->L[IX]==1) {
    printf("%s: Warning: ndim < 3\n",fnm);
  }

  /*** Allocate space ***/
  if (mom->initialised) {
    printf("%s: Warning: reinitialising momentum structures\n", fnm);
    free_momenta(mom);
  }

  mom->index = malloc(sizeof(int)*nall);
  mom->val = arralloc(sizeof(Real), 2, mom->nvar, 
		      mom->nmom_out[IX]+mom->nmom_out[IT]+2);
  mom->nequiv = malloc(sizeof(int)*(mom->nmom_out[IX]+mom->nmom_out[IT]+2));

  for (i=0; i<nall; i++) mom->index[i] = DISCARDED;

  for (i=0,p=0; p <= mom->nmom_out[IZ]; p++,i++) {
    p_spatial = p*p;
    mom->index[p*nptin] = i;
    if (bct == PERIODIC) {
      mom->val[0][i] = 2.0*PI*mom->scale*p/(Real)mom->L[IZ];
      mom->nequiv[i] = p==0 ? 1 : 6;
    } else {
      mom->val[0][i] = 2.0*PI*mom->scale*sqrt(p_spatial+0.25*unit_sqr)/mom->L[IZ];
      mom->nequiv[i] = p==0 ? 2 : 12;
    }
    /* recheck and push back if the mom value is larger than the cutoff */
    if ( (mom->cutoff > 0.0) && (mom->val[0][i] > mom->cutoff) ) {
      mom->index[p*nptin] = DISCARDED;
      i--;
    }
  }
  n_spatial = i;
  
  for (pt=1; pt < npt; pt++,i++) {
    if (bct == PERIODIC)
      p_tot = pt*pt*unit_sqr;
    else
      p_tot = (pt+0.5)*(pt+0.5)*unit_sqr;
    mom->index[pt] = i;
    mom->val[0][i] = 2.0*PI*mom->scale*sqrt(p_tot)/mom->L[IZ];
    mom->nequiv[i] = 2;
    /* recheck and push back if the mom value is larger than the cutoff */
    if ( (mom->cutoff > 0.0) && (mom->val[0][i] > mom->cutoff) ) {
      mom->index[pt] = DISCARDED;
      i--;
    }
  }

  mom->nval = i;
  /* Perform sorting if required */
  if (mom->sort) {
    mom->key = malloc(sizeof(int)*mom->nval);
    keysort(mom->val[mom->sortvar],mom->nval,mom->key);
  }
  mom->initialised = TRUE;

  return 0;
}


/************************************************************************
 *                                                                      *
 * setup_momenta: orders momenta (px,py,pz,pt) into a one-dimensional   *
 *   array, symmetrising equivalent momenta and imposing cuts.          *
 *                                                                      *
 *   Input values:                                                      *
 *      nmom_in    highest momentum value in each direction             *
 *         p[i] = -nmom_in[i] ... nmom_in[i] (periodic boundaries)      *
 *                -nmom_in[i]+1/2 ... nmom_in[i]-1/2 (antiperiodic bc)  *
 *      nmom_out   highest momentum value to be kept in each direction  *
 *      L          lattice size                                         *
 *      bc         flags for boundary conditions                        *
 *                 (required to be periodic in space)                   *
 *      cutoff     maximum total momentum (in lattice units)            *
 *      dcut       maximum distance of (px,py,pz,pt) from the diagonal  *
 *                 (in units of spatial momentum)                       *
 *   cutoff or dcut < 0 means that no cut will be imposed               *
 *      scale      physical value of a^-1 (for instance)                *
 *      z3         Z3 symmetrisation of spatial momenta on/off          *
 *                 (required to be on, may be relaxed in the future)    *
 *      o3         O(3) symmetrisation of spatial momenta on/off        *
 *      z4         Z4 symmetrisation of spatial and time momenta on/off *
 *      nvar       number of momentum variables to compute (see below)  *
 *      sort       flag for sorting on magnitude of total momentum      *
 *      sortvar    variable to use for sorting or smearing (see below)  *
 *      smear      flag to smear (bin) nearby momentum values           *
 *      smearing   smearing distance / bin size                         *
 *                                                                      *
 *   Z4 symmetrisation will only be performed if z4 is set *and*        *
 *   the time length of the lattice is a multiple of the spatial        *
 *   length *and* we have periodic boundary conditions in time.         *
 *   The spatial directions are assumed to always be symmetric.         *
 *   If both o3 and z4 are set, O(4) symmetry is imposed.               *
 *   If neither is set, only Z3 symmetry will be imposed.               *
 *                                                                      *
 *   Return values:                                                     *
 *      nval         the total number of momenta assigned               *
 *      index        mapping of (pt,pz,py,px) -> ip                     *
 *      val          total momentum p=sqrt(p^2)                         *
 *   If nvar > 1, then val also returns the lattice momenta             *
 *   val[1...nvar-1] given by                                           *
 *      val[1]       q = 2 sqrt(sum_mu sin^2(p_mu/2))                   *
 *      val[2]       k = sqrt(sum_mu sin^2(p_mu))                       *
 *      val[3]       K = sqrt(sum_mu sin^2(2p_mu)) / 2                  *
 *      val[4]   kdotK = sqrt(sum_mu sin(p_mu)sin(2p_mu)/2)             *
 *                                                                      *
 *   Although this is written with a 4-dimensional problem in mind,     *
 *   it may easily be made purely spatial by setting L[IT]=1            *
 *   (along with nmom[IT]=0, bc[IT]=PERIODIC).  Note that because of    *
 *   the ordering of indices internally this is slightly less efficient *
 *   than a tailor-made 3-dim routine (at least if itab is used)        *
 *   2 or 1 spatial dimensions may be catered for by setting            *
 *   L[IX] resp. also L[IY] to 1.                                       *
 ************************************************************************/

int setup_momenta(Momenta *mom)
{
  int i, j, px, py, pz, pt, px1, py1, pz1, pt1;
  const int max_spatial =  mom->nmom_in[IX]*mom->nmom_in[IX]
                         + mom->nmom_in[IY]*mom->nmom_in[IY]
                         + mom->nmom_in[IZ]*mom->nmom_in[IZ];
  const int nall = nallmom(mom);
  const int bct = mom->bc[IT];
  const int npt = bct==PERIODIC ? mom->nmom_in[IT]+1 : mom->nmom_in[IT];
  const int nsdim = mom->L[IY]>1 ? (mom->L[IX]>1 ? 3 : 2) : 1;
  const int ndim = mom->L[IT]>1 ? nsdim+1 : nsdim;
  const Real vnorm = 1.0/sqrt((double)ndim); /* inverse length of unit vec */
  int ncontrol, ndata;
  Real pibyl[DIR];

  int *assigned, *jmom;
  int npi[DIR];
  int np[DIR];
  int m;
  int status;
#ifdef DEBUG
  int ii;
#endif

  const char fnm[MAXNAME] = "setup_momenta";

/* branch for smearing/binning (much simpler...) */
  if ( mom->smear ) {
    return setup_smeared_momenta(mom);
  }

/* Check validity of arguments */

  if (status=check_input(mom)) return status;

/*  z4 symmetrisation only if the lattice sizes allow it  */
  if ( mom->z4 && (mom->L[IT]%mom->L[IZ] == 0) ) {
    m = mom->L[IT]/mom->L[IZ];
  }
  else {
    if (mom->z4) {
      printf("%s: Warning: Z4 symmetry flag set but time length is not a\n", fnm);
      printf("                 multiple of spatial length.  Ignoring Z4 flag.\n");
    }
    m = 0;
  }

/*  nmom is the number of positive momentum values in each direction  *
 *  np   is the total number of distinct values (including zero)      */
  for (i=0; i < DIR; i++) {
    pibyl[i] = PI/(Real)mom->L[i];
    npi[i] = mom->nmom_in[i]+1;
    np[i] = mom->nmom_out[i]+1;
  }
  if (bct == ANTIPERIODIC)
    np[IT] = mom->nmom_out[IT];

/*  Allocate space  */
  if (mom->initialised) {
    printf("%s: Warning: reinitialising momentum structures\n", fnm);
    free_momenta(mom);
  }
  mom->index = malloc(sizeof(int)*nall);
  mom->itab  = talloc(npi[IX],npi[IY],npi[IZ],npt,mom->index);
  mom->itabexists = TRUE;

  for (i=0; i<nall; i++) mom->index[i] = DISCARDED;

/* the size of the control arrays depend on what kind of symmetry */
/* we are trying to impose -- allocate only what is needed        */
  if (mom->o3 && m>0) {
    ncontrol = m*m*max_spatial+np[IT]*np[IT];   /* O(4) symmetry -- expensive */
  } else if (mom->o3) {
    ncontrol = max_spatial;
  } else if (m>0) {       /* z4 symmetry */
    ncontrol = nall;
  } else {
    ncontrol = 1;
  }
  assigned = vecalloc(sizeof(int),ncontrol);
  jmom = vecalloc(sizeof(int),ncontrol);
  for (i=0; i<ncontrol; i++) {
    assigned[i] = FALSE;
  }

/* compute the number of out values before cuts */
  ndata = np[IX]*np[IY]*np[IZ] + np[IX]*(np[IX]-1)*(np[IX]-2)/6
    - np[IX]*(np[IY]*(np[IY]-1)+np[IZ]*(np[IX]-1))/2;
  ndata *= np[IT];

  mom->val = arralloc(sizeof(Real), 2, mom->nvar, ndata);
  mom->nequiv = vecalloc(sizeof(int), ndata);
  for (i=0;i<ndata;i++) mom->nequiv[i]=0;
  j=0;
  for (px=0; px < np[IX]; px++) {
    Real k[DIR], q[DIR], K[DIR];         /* lattice momenta */
    Real ptot, ksqr, qsqr, Ksqr, kdotK;  /* lattice momenta */
    int p_spatial, p_tot; /* integer total mom for O(3) and O(4) symmetry */
    Real rept, ptval, pabs, dist, cosdist;
    int newp, firstp;                    /* control flags   */

    q[IX] = 2.0*sin(pibyl[IX]*px);
    k[IX] = sin(2.0*pibyl[IX]*px);
    K[IX] = 0.5*sin(4.0*pibyl[IX]*px);
    for (py=px; py < np[IY]; py++) {
      q[IY] = 2.0*sin(pibyl[IY]*py);
      k[IY] = sin(2.0*pibyl[IY]*py);
      K[IY] = 0.5*sin(4.0*pibyl[IY]*py);
      for (pz=py; pz < np[IZ]; pz++) {
	q[IZ] = 2.0*sin(pibyl[IZ]*pz);
	k[IZ] = sin(2.0*pibyl[IZ]*pz);
	K[IZ] = 0.5*sin(4.0*pibyl[IZ]*pz);
	p_spatial = px*px + py*py + pz*pz;
	firstp = FALSE;
	for (pt=0; pt < np[IT]; pt++) {
	  if (bct == PERIODIC) {
	    rept = (Real)pt*mom->L[IZ]/mom->L[IT];
	    if ( mom->o3 && (m>0) ) {             /* impose O(4) symmetry */
	      p_tot = m*m*p_spatial + pt*pt;
	    }
	  } else {                    /* antiperiodic boundary conditions */
	    rept = (pt+0.5)*mom->L[IZ]/mom->L[IT];
	  }
	  ptval = 2.0*PI*rept/mom->L[IZ];
	  pabs = sqrt(p_spatial+rept*rept);
	  q[IT] = 2.0*sin(ptval/2.0);
	  k[IT] = sin(ptval);
	  K[IT] = 0.5*sin(2.0*ptval);
	  /* compute distance from 4-diagonal, in spatial momentum units  */
	  cosdist = (pabs > 0.0) ? vnorm*(px+py+pz+rept)/pabs : 0.0;
	  dist = pabs*sqrt(1.0-cosdist*cosdist);
	  ptot = 2.0*pabs*pibyl[IZ];
	  newp = TRUE;
	  /* check for diagonal and total momentum cuts */
	  if ( ((ptot <= mom->cutoff) || (mom->cutoff < 0.0)) &&
	       ((dist <= mom->dcut) || (mom->dcut < 0.0)) ) {
	    int nn = nequiv(px,py,pz,nsdim);
	    if (pt>0 || bct==ANTIPERIODIC) nn*=2;
	    qsqr = q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3];
	    ksqr = k[0]*k[0] + k[1]*k[1] + k[2]*k[2] + k[3]*k[3];
	    Ksqr = K[0]*K[0] + K[1]*K[1] + K[2]*K[2] + K[3]*K[3];
	    kdotK = k[0]*K[0] + k[1]*K[1] + k[2]*K[2] + k[3]*K[3];
	    if (mom->o3) {
	      if (m>0) {                          /* impose O(4) symmetry */
		if (assigned[p_tot]) {
		  mom->itab[px][py-px][pz-py][pt] = jmom[p_tot];
		  mom->nequiv[jmom[p_tot]] += nn;
#ifdef DEBUG
		  ii=jmom[p_tot];
#endif
		  newp = FALSE;
		} else {
		  jmom[p_tot] = j;
		  assigned[p_tot] = TRUE;
		} /* if assigned[p_tot] else */
	      } else {                            /* impose O(3) symmetry */
		if (assigned[p_spatial] && !firstp) {
		    mom->itab[px][py-px][pz-py][pt] = jmom[p_spatial]+pt;
		    mom->nequiv[jmom[p_spatial]+pt] += nn;
#ifdef DEBUG
		    ii=jmom[p_spatial]+pt;
#endif
		    newp = FALSE;
		} else {
		  if (pt==0) {                 /* we always start at pt=0 */
		    firstp = TRUE;
		    assigned[p_spatial] = TRUE;
		    jmom[p_spatial] = j;
#ifdef DEBUG
		    ii=j;
#endif
		    mom->nequiv[j] += nn;
		  }
		}
	      } /* if m>0 ... else */
	    } /* if o3 */
	    else if (m>0) {                       /* impose Z4 symmetry */
	      if (pt%m == 0) {
	      /* Reorder momenta to see if this corresponds to a spatial
		 momentum we have already encountered */
		pt1 = pt/m;
		pz1 = pz;
		py1 = py;
		px1 = px;
		if (pt1 < pz1) {
		  SWAP(pt1, pz1);
		  if (pz1 < py1) {
		    SWAP(pz1, py1);
		    if (py1 < px1) {
		      SWAP(py1, px1);
		    }
		  }
		}
		if ((i=mom->itab[px1][py1-px1][pz1-py1][m*pt1]) != DISCARDED) {
              /* yes, we have already encountered this */
		  mom->itab[px][py-px][pz-py][pt] = i;
		  mom->nequiv[i] += nn;
#ifdef DEBUG
		  ii=i;
#endif
		  newp = FALSE;
		}
	      } /* if pt%m == 0 */
	    } /* if o3 ... else if m>0 */
	    if (newp) {         /* always true if we are only imposing Z3 */
	      mom->itab[px][py-px][pz-py][pt] = j;
	      mom->nequiv[j] = nn;
	      switch (mom->nvar) {
	      case 7: mom->val[6][j] = 0.0; /* user-defined var    */
	      case 6: mom->val[5][j] = 0.0; /* TODO: more flexible */
	      case 5: mom->val[4][j] = mom->scale*mom->scale*kdotK;
	      case 4: mom->val[3][j] = mom->scale*sqrt(Ksqr);
	      case 3: mom->val[2][j] = mom->scale*sqrt(ksqr);
	      case 2: mom->val[1][j] = mom->scale*sqrt(qsqr);
	      case 1: mom->val[0][j] = mom->scale*ptot;
		break;
	      default: 
		printf("%s: illegal nvar %d. Exiting\n", fnm, mom->nvar);
		return BADPARAM;
	      }
#ifdef DEBUG
	      ii=j;
#endif
	      j++;
	    }
#ifdef DEBUG
	    printf("%d %d %d %2d  idx=%-3d  neq=%-2d (%d) \tptot=%g\n",
		   px, py, pz, pt, mom->itab[px][py-px][pz-py][pt], 
		   nn, mom->nequiv[ii], mom->val[0][ii]);
	    fflush(stdout);
#endif
	  } /* if ptot <= cutoff */
	} /* for pt */
      } /* for pz */
    } /* for py */
  } /* for px */
  mom->nval = j;

  /* Perform sorting if required */
  if (mom->sort) {
    mom->key = malloc(sizeof(int)*mom->nval);
    keysort(mom->val[mom->sortvar],mom->nval,mom->key);
  }
  mom->initialised = TRUE;
#ifdef DEBUG
  for (i=0; i < j; i++) {
    printf("i=%d  ptot=%10.6f  neq=%d\n", i, mom->val[0][i], mom->nequiv[i]);
    fflush(stdout);
  }
#endif
  free(assigned);
  free(jmom);
  return 0;
}

/* setup_momenta_st: as setup_momenta, but keeps space and time momenta *
 * as separate variables                                                */

int setup_momenta_st(Momenta *mom)
{
  int i, j, px, py, pz, pt;
  const int max_spatial =  mom->nmom_in[IX]*mom->nmom_in[IX]
                         + mom->nmom_in[IY]*mom->nmom_in[IY]
                         + mom->nmom_in[IZ]*mom->nmom_in[IZ];
  const int nall = nallmom(mom);
  const int bct = mom->bc[IT];
  const int npt = bct==PERIODIC ? mom->nmom_in[IT]+1 : mom->nmom_in[IT];
  const int nsdim = mom->L[IY]>1 ? (mom->L[IX]>1 ? 3 : 2) : 1;
  const Real vnorm = 1.0/sqrt((double)nsdim); /* inverse length of unit vec */
  int ncontrol, ndata;
  Real pibyl[DIR];

  int *assigned, *jmom;
  int npi[DIR];
  int np[DIR];
  int status;
#ifdef DEBUG
  int ii;
#endif

  const char fnm[MAXNAME] = "setup_momenta_st";

/* branch for smearing/binning (much simpler...) */
  if ( mom->smear ) {
    fprintf(stderr,
	    "%s: smearing not implemented for separate space/time mom\n",fnm);
    return BADPARAM;
  }

/* Check validity of arguments */

  if (status=check_input(mom)) return status;

/*  no z4 symmetrisation */
  if ( mom->z4 ) {
    fprintf(stderr,
	    "%s: No Z4 symmetrisation when space and time are separate!\n",
	    fnm);
    return BADPARAM;
  }

/*  nmom is the number of positive momentum values in each direction  *
 *  np   is the total number of distinct values (including zero)      */
  for (i=0; i < DIR; i++) {
    pibyl[i] = PI/(Real)mom->L[i];
    npi[i] = mom->nmom_in[i]+1;
    np[i] = mom->nmom_out[i]+1;
  }
  if (bct == ANTIPERIODIC)
    np[IT] = mom->nmom_out[IT];

/*  Allocate space  */
  if (mom->initialised) {
    printf("%s: Warning: reinitialising momentum structures\n", fnm);
    free_momenta(mom);
  }
  mom->index = malloc(sizeof(int)*nall);
  mom->itab  = talloc(npi[IX],npi[IY],npi[IZ],npt,mom->index);
  mom->itabexists = TRUE;

  for (i=0; i<nall; i++) mom->index[i] = DISCARDED;

/* the size of the control arrays depend on what kind of symmetry */
/* we are trying to impose -- allocate only what is needed        */
  if (mom->o3) {
    ncontrol = max_spatial;
  } else {
    ncontrol = 1;
  }
  assigned = vecalloc(sizeof(int),ncontrol);
  jmom = vecalloc(sizeof(int),ncontrol);
  for (i=0; i<ncontrol; i++) {
    assigned[i] = FALSE;
  }

/* compute the number of out values before cuts */
  ndata = np[IX]*np[IY]*np[IZ] + np[IX]*(np[IX]-1)*(np[IX]-2)/6
    - np[IX]*(np[IY]*(np[IY]-1)+np[IZ]*(np[IX]-1))/2;
  ndata *= np[IT];

  mom->val = arralloc(sizeof(Real), 2, mom->nvar, ndata);
  mom->nequiv = vecalloc(sizeof(int), ndata);
  for (i=0;i<ndata;i++) mom->nequiv[i]=0;
  j=0;
  for (px=0; px < np[IX]; px++) {
    Real k[DIR], q[DIR], ptot, ksqr, qsqr;     /* lattice momenta */
    int p_spatial;         /* integer total mom for O(3) symmetry */
    Real rept, ptval, pabs, dist, cosdist;
    int newp, firstp;                          /* control flags   */

    q[IX] = 2.0*sin(pibyl[IX]*px);
    k[IX] = sin(2.0*pibyl[IX]*px);
    for (py=px; py < np[IY]; py++) {
      q[IY] = 2.0*sin(pibyl[IY]*py);
      k[IY] = sin(2.0*pibyl[IY]*py);
      for (pz=py; pz < np[IZ]; pz++) {
	q[IZ] = 2.0*sin(pibyl[IZ]*pz);
	k[IZ] = sin(2.0*pibyl[IZ]*pz);
	p_spatial = px*px + py*py + pz*pz;
	firstp = FALSE;
	for (pt=0; pt < np[IT]; pt++) {
	  if (bct == PERIODIC) {
	    rept = (Real)pt*mom->L[IZ]/mom->L[IT];
	  } else {                    /* antiperiodic boundary conditions */
	    rept = (pt+0.5)*mom->L[IZ]/mom->L[IT];
	  }
	  ptval = 2.0*PI*rept/mom->L[IZ];
	  pabs = sqrt(p_spatial);
	  q[IT] = 2.0*sin(ptval/2.0);
	  k[IT] = sin(ptval);
	  /* compute distance from 3-diagonal, in spatial momentum units  */
	  cosdist = (pabs > 0.0) ? vnorm*(px+py+pz)/pabs : 0.0;
	  dist = pabs*sqrt(1.0-cosdist*cosdist);
	  ptot = 2.0*pabs*pibyl[IZ];
	  newp = TRUE;
	  /* check for diagonal and total momentum cuts */
	  if ( ((ptot <= mom->cutoff) || (mom->cutoff < 0.0)) &&
	       ((dist <= mom->dcut) || (mom->dcut < 0.0)) ) {
	    int nn = nequiv(px,py,pz,nsdim);
	    if (pt>0 || bct==ANTIPERIODIC) nn*=2;
	    qsqr = q[IX]*q[IX] + q[IY]*q[IY] + q[IZ]*q[IZ];
	    ksqr = k[IX]*k[IX] + k[IY]*k[IY] + k[IZ]*k[IZ];
	    if (mom->o3) {                        /* impose O(3) symmetry */
	      if (assigned[p_spatial] && !firstp) {
		mom->itab[px][py-px][pz-py][pt] = jmom[p_spatial]+pt;
		mom->nequiv[jmom[p_spatial]+pt] += nn;
#ifdef DEBUG
		ii=jmom[p_spatial]+pt;
#endif
		newp = FALSE;
	      } else {
		if (pt==0) {                 /* we always start at pt=0 */
		  firstp = TRUE;
		  assigned[p_spatial] = TRUE;
		  jmom[p_spatial] = j;
#ifdef DEBUG
		  ii=j;
#endif
		  mom->nequiv[j] += nn;
		}
	      }
	    } /* if o3 */
	    if (newp) {         /* always true if we are only imposing Z3 */
	      mom->itab[px][py-px][pz-py][pt] = j;
	      mom->nequiv[j] = nn;
	      switch (mom->nvar) {
	      case 7: mom->val[6][j] = 0.0; /* user-defined var    */
	      case 6: mom->val[5][j] = 0.0; /* TODO: more flexible */
	      case 5:
		mom->val[4][j] = mom->scale*k[IT];
		mom->val[3][j] = mom->scale*sqrt(ksqr);
	      case 3:
		mom->val[2][j] = mom->scale*q[IT];
		mom->val[1][j] = mom->scale*sqrt(qsqr);
		mom->val[0][j] = mom->scale*sqrt(qsqr+q[IT]*q[IT]);
		break;
	      default: 
		printf("%s: illegal nvar %d. Exiting\n", fnm, mom->nvar);
		return BADPARAM;
	      }
#ifdef DEBUG
	      ii=j;
#endif
	      j++;
	    }
#ifdef DEBUG
	    printf("%d %d %d %2d  idx=%-3d  neq=%-2d (%d) \tptot=%g\n",
		   px, py, pz, pt, mom->itab[px][py-px][pz-py][pt], 
		   nn, mom->nequiv[ii], mom->val[0][ii]);
	    fflush(stdout);
#endif
	  } /* if ptot <= cutoff */
	} /* for pt */
      } /* for pz */
    } /* for py */
  } /* for px */
  mom->nval = j;

  /* Perform sorting if required */
  if (mom->sort) {
    mom->key = malloc(sizeof(int)*mom->nval);
    keysort(mom->val[mom->sortvar],mom->nval,mom->key);
  }
  mom->initialised = TRUE;
#ifdef DEBUG
  for (i=0; i < j; i++) {
    printf("i=%d  ptot=%10.6f  neq=%d\n", i, mom->val[0][i], mom->nequiv[i]);
    fflush(stdout);
  }
#endif
  free(assigned);
  free(jmom);
  return 0;
}

/************************************************************************
 * setup_smeared_momenta: gathers momenta into bins, regarding all      *
 *   momenta in the same bin as equivalent.  Average (smeared)          *
 *   momentum values are computed for each bin.                         *
 *                                                                      *
 *   Two ways to control the smearing are recognised:                   *
 *     NBIN     The total (or, rather, maximum) number of smeared       *
 *              momenta is specified                                    *
 *     BINSIZE  The bin size (smearing radius) is specified             *
 *   A third codeword, NINBIN, is defined but not implemented.          *
 *   This would specify the maximum number of momenta to gather in one  *
 *   bin.  (Note that up to 192 momenta may be Z4-equivalent.)          *
 *                                                                      *
 *   The parameters are as in setup_momenta, although the o3 and z4     *
 *   flags are ignored (or 'on' by default).                            *
 *   The specific smearing parameters are:                              *
 *     smear     the type of smearing (NBIN, BINSIZE)                   *
 *     smearing  the number of smeared momenta (NBIN) or the smearing   *
 *               radius (BINSIZE)                                       *
 *     sortvar   the momentum variable to use as magnitude              *
 *   Note that kdotK (val[4]) is not permitted as sort key, since i     *
 *   cannot conceive of a circumstance where this would be useful.      *
 *   It is trivial to add this though.                                  *
 ************************************************************************/

int setup_smeared_momenta(Momenta *mom)
{
  const char fnm[MAXNAME] = "setup_smeared_momenta";
  const int nsdim = mom->L[IY]>1 ? (mom->L[IX]>1 ? 3 : 2) : 1;
  const int ndim = mom->L[IT]>1 ? nsdim+1 : nsdim;
  const Real vnorm = 1.0/sqrt((double)ndim); /* inverse length of unit vec */
  const Real maxpt
    = mom->bc[IT]==PERIODIC ? mom->nmom_out[IT] : mom->nmom_out[IT]+0.5;
  const Real unit_sqr
    = (Real)(mom->L[IZ]*mom->L[IZ])/(Real)(mom->L[IT]*mom->L[IT]);
  const Real cut = mom->cutoff*mom->cutoff;
  const int nall = nallmom(mom);
  Real max_mom;
  Real binsize;
  Real pibyl[DIR];
  int np[DIR], ip[DIR];
  const int bct = mom->bc[IT];
  const int npt = bct==PERIODIC ? mom->nmom_in[IT]+1 : mom->nmom_in[IT];
  int nbin;
  int status;

  Real p[DIR], q[DIR], k[DIR], K[DIR], ptot;
  Real *pp;
  int i, ii, j;
  int *jj;

  if (status=check_input(mom)) return status;
  if (mom->nmom_out[IX] != mom->nmom_out[IZ]) {
    printf("%s: Warning: unequal spatial nmomenta.\n",fnm);
    printf("\t\tAverage momentum per bin may be incorrect\n");
  }

  for (i=0; i < DIR; i++) {
    pibyl[i] = PI/(Real)mom->L[i];
    np[i] = mom->nmom_out[i]+1;
  }
  if (mom->bc[IT] == ANTIPERIODIC)
    np[IT] = mom->nmom_out[IT];
  
  /* compute maximum momentum value */
  if (mom->cutoff >= 0.0 ) max_mom=mom->cutoff;
  else {
    switch (mom->sortvar) {
    case 0:
      pp = &p[0];
      max_mom = mom->nmom_out[IX]*mom->nmom_out[IX]
	+ mom->nmom_out[IY]*mom->nmom_out[IY]
	+ mom->nmom_out[IZ]*mom->nmom_out[IZ] + maxpt*maxpt*unit_sqr;
      max_mom = 2.0*PI*sqrt(max_mom)/mom->L[IZ];
      break;
    case 2:
      pp = &k[0];
      p[IX]=sin(2.0*pibyl[IX]*mom->nmom_out[IX]);
      p[IY]=sin(2.0*pibyl[IY]*mom->nmom_out[IY]);
      p[IZ]=sin(2.0*pibyl[IZ]*mom->nmom_out[IZ]);
      p[IT]=sin(2.0*pibyl[IT]*maxpt);
      max_mom = sqrt(p[0]*p[0]+p[1]*p[1]+p[2]*p[2]+p[3]*p[3]);
      break;
    case 1:
      pp = &q[0];
      p[IX]=sin(pibyl[IX]*mom->nmom_out[IX]);
      p[IY]=sin(pibyl[IY]*mom->nmom_out[IY]);
      p[IZ]=sin(pibyl[IZ]*mom->nmom_out[IZ]);
      p[IT]=sin(pibyl[IT]*maxpt);
      max_mom = 2.0*sqrt(p[0]*p[0]+p[1]*p[1]+p[2]*p[2]+p[3]*p[3]);
      break;
    case 3:
      pp = &K[0];
      p[IX]=sin(4.0*pibyl[IX]*mom->nmom_out[IX]);
      p[IY]=sin(4.0*pibyl[IY]*mom->nmom_out[IY]);
      p[IZ]=sin(4.0*pibyl[IZ]*mom->nmom_out[IZ]);
      p[IT]=sin(4.0*pibyl[IT]*maxpt);
      max_mom = 0.5*sqrt(p[0]*p[0]+p[1]*p[1]+p[2]*p[2]+p[3]*p[3]);
      break;
    default:
      printf("%s: Error: Illegal sorting variable %d\n",fnm,mom->sortvar);
      return BADPARAM;
    }
  }
  switch (mom->smear) {
  case BINSIZE:
    binsize = mom->smearing;
    nbin = (int)(max_mom/mom->smearing)+1;
    break;
  case NBIN:
    nbin = INT(mom->smearing);
    binsize = max_mom/nbin;
    break;
  case NINBIN:
    printf("%s: NInBin smearing not implemented\n",fnm);
    return BADPARAM;
  default:
    printf("%s: Error: Unknown smearing type %d\n",fnm,mom->smear);
    return BADPARAM;
  }

/*  Allocate space  */
  if (mom->initialised) {
    printf("%s: Warning: reinitialising momentum structures\n", fnm);
    free_momenta(mom);
  }
  mom->nval = nbin;
  mom->index = malloc(sizeof(int)*nall);
  mom->itab  = talloc(mom->nmom_in[IX]+1,mom->nmom_in[IY]+1,
		      mom->nmom_in[IZ]+1,npt,mom->index);
  mom->itabexists = TRUE;

  for (i=0; i<nall; i++) mom->index[i] = DISCARDED;

  mom->nequiv = malloc(sizeof(int)*nbin);
  mom->val    = arralloc(sizeof(Real), 2, mom->nvar, nbin);
  mom->sort = FALSE; /* smeared momenta are already sorted */
  mom->key  = 0;     /* to be safe */
  for (i=0;i<nbin;i++) mom->nequiv[i]=0;
  arrf_set_zero(mom->val[0],sizeof(Real),sizeof(Real),mom->nvar*nbin);

  /* Loop over momenta ... */
  for (ip[IX]=0; ip[IX]<np[IX]; ip[IX]++) {
    p[IX] = 2.0*pibyl[IX]*ip[IX];
    if (mom->nvar > 1) {  /* compute lattice momenta */
      k[IX] = sin(p[IX]);
      q[IX] = 2.0*sin(0.5*p[IX]);
      K[IX] = 0.5*sin(2.0*p[IX]);
    }
    for (ip[IY]=ip[IX]; ip[IY]<np[IY]; ip[IY]++) {
      p[IY] = 2.0*pibyl[IY]*ip[IY];
      if (mom->nvar > 1) {
	k[IY] = sin(p[IY]);
	q[IY] = 2.0*sin(0.5*p[IY]);
	K[IY] = 0.5*sin(2.0*p[IY]);
      }
      for (ip[IZ]=ip[IY]; ip[IZ]<np[IZ]; ip[IZ]++) {
	p[IZ] = 2.0*pibyl[IZ]*ip[IZ];
	if (mom->nvar > 1) {
	  k[IZ] = sin(p[IZ]);
	  q[IZ] = 2.0*sin(0.5*p[IZ]);
	  K[IZ] = 0.5*sin(2.0*p[IZ]);
	}
	for (ip[IT]=0; ip[IT]<np[IT]; ip[IT]++) {
	  Real pabs, cosdist, dist;
	  p[IT] = bct==PERIODIC ? 2.0*pibyl[IT]*ip[IT]
	             : 2.0*pibyl[IT]*(ip[IT]+0.5);
	  if (mom->nvar > 1) {
	    k[IT] = sin(p[IT]);
	    q[IT] = 2.0*sin(0.5*p[IT]);
	    K[IT] = 0.5*sin(2.0*p[IT]);
	  }
	  /* compute total momentum and distance from d-diagonal (for cuts) */
	  pabs = sqrt(p[0]*p[0]+p[1]*p[1]+p[2]*p[2]+p[3]*p[3]);
	  cosdist = (pabs > 0.0) ? vnorm*(p[IX]+p[IY]+p[IZ]+p[IT])/pabs : 0.0;
	  dist = 0.5*pabs*sqrt(1.0-cosdist*cosdist)/pibyl[IZ];
	  ptot = pp[0]*pp[0]+pp[1]*pp[1]+pp[2]*pp[2]+pp[3]*pp[3];
	  if ( ((ptot <= cut) || (mom->cutoff < 0.0)) &&
	       ((dist <= mom->dcut) || (mom->dcut < 0.0)) ) {
	    int nn;
	    i = (int)(sqrt(ptot)/binsize);
	    nn = nequiv(ip[IX],ip[IY],ip[IZ],nsdim);
	    if (bct==ANTIPERIODIC || p[IT]>0) nn *= 2;
	    mom->nequiv[i] += nn;
	    mom->itab[ip[IX]][ip[IY]-ip[IX]][ip[IZ]-ip[IY]][ip[IT]] = i;
	    switch (mom->nvar) {
	    case 7: mom->val[6][i] = 0.0; /* user-defined var    */
	    case 6: mom->val[5][i] = 0.0; /* TODO: more flexible */
	    case 5: 
	      mom->val[4][i] += nn*mom->scale*mom->scale
		               *(K[0]*k[0]+K[1]*k[1]+K[2]*k[2]+K[3]*k[3]);
	    case 4: 
	      mom->val[3][i] += nn*mom->scale
                               *sqrt(K[0]*K[0]+K[1]*K[1]+K[2]*K[2]+K[3]*K[3]);
	    case 3: 
	      mom->val[2][i] += nn*mom->scale
                               *sqrt(k[0]*k[0]+k[1]*k[1]+k[2]*k[2]+k[3]*k[3]);
	    case 2: 
	      mom->val[1][i] += nn*mom->scale
                               *sqrt(q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3]);
	    case 1: mom->val[0][i] += nn*mom->scale*pabs;
	      break;
	    default: 
	      printf("%s: illegal nvar %d. Exiting\n", fnm, mom->nvar);
	      return BADPARAM;
	    }
#ifdef DEBUG
	    printf("%d %d %d %2d  idx=%-3d  neq=%-3d (%d) \tptot=%g\t(%10.6g %10.6g)\n", 
		   ip[IX], ip[IY], ip[IZ], ip[IT],
		   mom->itab[ip[IX]][ip[IY]-ip[IX]][ip[IZ]-ip[IY]][ip[IT]],
		   nn,  mom->nequiv[i],
		   sqrt(ptot), mom->val[0][i], mom->val[mom->sortvar][i]);
	    fflush(stdout);
#endif
	  }
	}
      }
    }
  }

  /* Average over values in bins and compress to remove empty bins */
  jj = malloc(sizeof(int)*mom->nval); /* uncompressed to compressed index */
  for (i=0,ii=0; i<mom->nval; i++) {
    if (mom->nequiv[i]!=0) { /* bin is non-empty */
      jj[i]=ii;
      mom->nequiv[ii] = mom->nequiv[i];
      for (j=0; j<mom->nvar; j++) {
	mom->val[j][ii] = mom->val[j][i]/mom->nequiv[i];
      }
      ii++;
    }
  }
  for (i=0;i<nall;i++) 
    if (mom->index[i]!=DISCARDED) mom->index[i] = jj[mom->index[i]];
  free(jj);

  mom->nval = ii;
  mom->initialised = TRUE;
#ifdef DEBUG
  for (i=0; i<nall; i++) printf("mom no %3d  bin no %2d\n",i,mom->index[i]);
  for (i=0; i<mom->nval; i++) {
    printf("%2d p=%f\tneq=%d\n", i, mom->val[mom->sortvar][i], mom->nequiv[i]);
  }
#endif
  return 0;
}

/* smear_momenta: takes a Momenta that has already been initialised by  *
 *  setup_momenta and applies smearing to it.                           *
 *  Only binsize smearing is implemented (so far)                       */

int smear_momenta(Momenta *mom, Real smearing, int sort_key)
{
  const char fnm[MAXNAME] = "smear_momenta";
  const int nall = nallmom(mom);
  Real **v;
  int *idx, *neq, *jj;
  int nbin;
  Real max_mom;
  int sorted;
  int i, j, k;

  if (smearing==0.0) {
    printf("%s: bin size set to zero.  doing nothing\n",fnm);
    return NOP;
  }
  if (sort_key >= mom->nvar) {
    printf("%s: Error: invalid sort key %d > nvar (%d)\n", 
	   fnm, sort_key, mom->nvar);
    return BADPARAM;
  }
  if (mom->nmom_out[IX] != mom->nmom_out[IZ]) {
    printf("%s: Warning: unequal spatial nmomenta.\n",fnm);
    printf("\t\tAverage momentum per bin may be incorrect\n");
  }
  if (mom->smear) {
    printf("%s: Warning: momenta are already smeared\n",fnm);
  }
  /* check if the data are already sorted */
  sorted=FALSE;
  if (mom->sort) {
    if (mom->sortvar==sort_key) sorted=TRUE;
    else free(mom->key);  /* sorting is of no use to us */
  }
  mom->sortvar  = sort_key;
  mom->smear    = BINSIZE;
  mom->smearing = smearing;

  if (!mom->initialised) {
    printf("%s: Warning: initialising momenta\n",fnm);
    return setup_smeared_momenta(mom);
  }

  /* compute maximum momentum value and number of bins */
  if (sorted) max_mom=mom->val[sort_key][mom->key[mom->nval-1]];
  else {
    max_mom=0.0;
    for (i=mom->nval-1;i>=0;i--) 
      if (mom->val[sort_key][i]>max_mom) max_mom=mom->val[sort_key][i];
  }

  nbin = (int)(max_mom/mom->smearing)+1;
  if (nbin>mom->nval) {
    printf("%s: Warning: number of bins greater than number of momenta\n",fnm);
  }

  /* allocate temporary arrays */
  v = arralloc(sizeof(Real),2,mom->nvar,nbin);  /* binned momentum value   */
  idx = malloc(sizeof(int)*mom->nval); /* new index */
  neq = malloc(sizeof(int)*nbin);      /* number of momenta per bin        */
  jj  = malloc(sizeof(int)*nbin);      /* uncompressed to compressed index */
  for (i=0;i<nbin;i++) {
    neq[i]=0;
    for (k=0;k<mom->nvar;k++) v[k][i]=0.0;
  }

  /* put momenta in bins */
  for (i=0; i<mom->nval; i++) {
    j = (int)(mom->val[sort_key][i]/smearing);
    idx[i] = j;
    for (k=0; k<mom->nvar; k++) {
      v[k][j]   += mom->nequiv[i]*mom->val[k][i];
    }
    neq[j] += mom->nequiv[i];
#ifdef DEBUG
    printf("%3d  v=%-10.6f %2d  neq=%-3d (%d)\n",
	   i, mom->val[sort_key][i], j, mom->nequiv[i], neq[j]);
#endif
  }

  /* purge empty bins */
  for (j=0,k=0;j<nbin;j++) if (neq[j]) jj[j]=k++;
  mom->nval=k;

  /* reallocate space for momentum values and equivalence numbers */
  free(mom->val);
  mom->val = arralloc(sizeof(Real),2,mom->nvar,mom->nval);
  mom->nequiv = realloc(mom->nequiv,sizeof(int)*mom->nval);
  /* free and null the old sort key */
  if (sorted) free(mom->key);
  mom->sort=FALSE;
  mom->key=0;

  /* finally, assign the new values and indices */
  for (j=0; j<nbin; j++) {
    if (neq[j]) {
      for (k=0;k<mom->nvar;k++) {
	mom->val[k][jj[j]] = v[k][j]/neq[j];
      }
      mom->nequiv[jj[j]] = neq[j];
    }
  }
  for (i=0;i<nall;i++)
    if (mom->index[i]!=DISCARDED) mom->index[i] = jj[idx[mom->index[i]]];

  free(jj);
  free(neq);
  free(idx);
  free(v);
  return 0;
}

int sort_momenta(Momenta* mom, int sort_key)
{
  const char fnm[MAXNAME] = "sort_momenta";

  if (sort_key >= mom->nvar) {
    printf("%s: sort key too large %d>%d\n", fnm, sort_key, mom->nvar-1);
    return BADPARAM;
  }
  if (sort_key < 0) {
    printf("%s: illegal sort key %d\n", fnm, sort_key);
    return BADPARAM;
  }

  if (!mom->initialised) {
    printf("%s: Warning: momenta not initialised\n",fnm);
    printf("%s: Initialising momenta now\n",fnm);
    mom->sort=TRUE;
    mom->sortvar=sort_key;
    return setup_momenta(mom);
  }

  if (mom->smear) {
    printf("%s: Error: trying to sort smeared momenta\n",fnm);
    return BADPARAM;
  }

  if (!mom->sort) {
    mom->key = malloc(sizeof(int)*mom->nval);
    mom->sort = TRUE;
  }
  mom->sortvar = sort_key;
  keysort(mom->val[sort_key],mom->nval,mom->key);

  return 0;
}

/************************************************************************
 * reorder_momenta: orders a 4-dimensional array of momentum space data *
 *  into a 1-dimensional array which has been set up by setup_momenta   *
 *  or setup_momenta_onaxis.                                            *
 *  The 4-d array is data_4d[pt][pz][py][px] where                      *
 *   pi=-nmom_in[i],..,nmom_in[i] (ie the first element is -nmom_in)    *
 ************************************************************************/

void reorder_momenta(Real *data_1d, Real ****data_4d, Momenta *mom)
{
  int i, px, py, pz, pt, iz, iy, ix, it;
  const int bct = mom->bc[IT];

  int *nvalues = vecalloc(sizeof(int), mom->nval);
  const char fnm[MAXNAME] = "reorder_momenta";
  if (!data_1d) {
    printf("%s: Error: output data array does not exist\n",fnm);
    return;
  }
  if ( mom->nmom_in[IX]!=mom->nmom_in[IY] ||
       mom->nmom_in[IY]!=mom->nmom_in[IZ] ) {
    printf("%s: Warning: spatial nmomenta not equal\n",fnm);
    printf("         This may lead to undefined behaviour\n");
  }

  /* set up table of 4-d pointers to momentum index array */
  if (!mom->itabexists) {
    mom->itab = talloc(mom->nmom_in[IX]+1,mom->nmom_in[IY]+1,
		       mom->nmom_in[IZ]+1,
		       bct==PERIODIC ? mom->nmom_in[IT]+1 : mom->nmom_in[IT],
		       mom->index);
    mom->itabexists = TRUE;
  }

  /* We use nvalues to count the number of actual momenta we average    *
   * over.  Normally this will be equal to mom->nequiv but is more      *
   * robust eg when spatial nmomenta are unequal                        */
  for (i=0; i < mom->nval; i++) {
    nvalues[i] = 0;
    data_1d[i] = 0.0;
  }

  for (px=0; px <= 2*mom->nmom_in[IX]; px++) {
    for (py=0; py <= 2*mom->nmom_in[IY]; py++) {
      for (pz=0; pz <= 2*mom->nmom_in[IZ]; pz++) {
	ix=abs(px-mom->nmom_in[IX]);
	iy=abs(py-mom->nmom_in[IY]);
	iz=abs(pz-mom->nmom_in[IZ]);
	SORT(ix,iy,iz);  /* now ix<=iy<=iz */
	if (bct == PERIODIC) {
	  for (pt=0; pt <= 2*mom->nmom_in[IT]; pt++) {
	    it = abs(pt-mom->nmom_in[IT]);
	    if ( ( i=mom->itab[ix][iy-ix][iz-iy][it]) >= 0)  {
	      nvalues[i]++;
	      if (mom->sort) data_1d[mom->key[i]] += data_4d[pt][pz][py][px];
	      else data_1d[i] += data_4d[pt][pz][py][px];
#ifdef DEBUG
	      printf("%2d %d %d %d  %2d %d %d %d\t%3d\t%12.6g %12.6g\n", 
		     pt, pz, py, px, it, iz, iy, ix, i,
		     data_4d[pt][pz][py][px], data_1d[i]);
	      fflush(stdout);
#endif
	    }
	  }
	} else {  /* antiperiodic bcs, half-integer pt */
	  for (pt=0; pt < 2*mom->nmom_in[IT]; pt++) {
	    if (pt < mom->nmom_in[IT]) 
	      it = mom->nmom_in[IT]-pt-1;
	    else
	      it = pt-mom->nmom_in[IT];
	    if ( ( i=mom->itab[ix][iy-ix][iz-iy][it]) >= 0)  {
	      nvalues[i]++;
	      if (mom->sort) data_1d[mom->key[i]] += data_4d[pt][pz][py][px];
	      else data_1d[i] += data_4d[pt][pz][py][px];
#ifdef DEBUG
	      printf("%2d %d %d %d  %2d %d %d %d\t%3d\t%12.6g %12.6g\n", 
		     pt, pz, py, px, it, iz, iy, ix, i,
		     data_4d[pt][pz][py][px], data_1d[i]);
	      fflush(stdout);
#endif
	    }
	  } /* for pt */
	} /* if bcs */
      }
    }
  }

#ifdef DEBUG
  printf("done reordering\n");
  fflush(stdout);
#endif

  for (i=0; i < mom->nval; i++) {
    data_1d[i] /= nvalues[i];
#ifdef DEBUG
    printf("%4d nval=%-4d (%d)\t %f \n", i, nvalues[i], mom->nequiv[i], data_1d[i]);
    fflush(stdout);
#endif
  }

  free(nvalues);
}

/************************************************************************
 * reorder_momenta_ptslice: orders a momentum space data array for      *
 *  a single value of pt into a 1-dimensional array, averaging over     *
 *  +-pt and equivalent spatial momenta (as specified by setup_momenta) *
 *  pt must be > 0 for this to work, but pt=0 can be handled by the     *
 *  normal reorder_momenta.                                             *
 ************************************************************************/

void reorder_momenta_ptslice(Real *data_1d, Real ****data_4d, 
			     Momenta *mom)
{
  int momdiff[SPATIAL_DIR];
  int i, px, py, pz, pt, iz, iy, ix;

  int *nvalues = vecalloc(sizeof(int), mom->nval);
  const char fnm[MAXNAME] = "reorder_momenta_ptslice";
  if (!data_1d) {
    printf("%s: Error: output data array does not exist\n",fnm);
    return;
  }
  if (mom->nmom_in[IT]>1) {
    printf("%s: Error: number of time momenta greater than 1\n",fnm);
    return;
  }
  if ( mom->nmom_in[IX]!=mom->nmom_in[IY] ||
       mom->nmom_in[IY]!=mom->nmom_in[IZ] ||
       mom->nmom_out[IX]!=mom->nmom_out[IY] ||
       mom->nmom_out[IY]!=mom->nmom_out[IZ] ) {
    printf("%s: Warning: spatial nmomenta not equal\n",fnm);
    printf("         This may lead to undefined behaviour\n");
  }
  if (mom->sort) {
    printf("%s: Warning: sorting not implemented\n",fnm);
  }

  for (i=0; i<SPATIAL_DIR; i++) momdiff[i]=mom->nmom_in[i]-mom->nmom_out[i];
  if (!mom->itabexists) {
    mom->itab = talloc(mom->nmom_in[IX]+1,mom->nmom_in[IY]+1,
		       mom->nmom_in[IZ]+1, 2, mom->index);
    mom->itabexists = TRUE;
  }

  for (i=0; i < mom->nval; i++) {
    nvalues[i] = 0;
    data_1d[i] = 0.0;
  }

  for (px=0; px <= 2*mom->nmom_out[IX]; px++) {
    for (py=0; py <= 2*mom->nmom_out[IY]; py++) {
      for (pz=0; pz <= 2*mom->nmom_out[IZ]; pz++) {
	ix=abs(px-mom->nmom_out[IX]);
	iy=abs(py-mom->nmom_out[IY]);
	iz=abs(pz-mom->nmom_out[IZ]);
	SORT(ix,iy,iz);
	for (pt=0; pt < 2; pt++) {
	  if ( ( i=mom->itab[ix][iy-ix][iz-iy][0]) >= 0)  {
	    nvalues[i]++;
	    data_1d[i] += 
	      data_4d[pt][pz+momdiff[IZ]][py+momdiff[IY]][px+momdiff[IX]];
#ifdef DEBUG
	    printf("%d %d %d %d  %d %d %d \t%3d  %g  %g\n", 
		   pt, pz, py, px, iz, iy, ix, i,
		   data_4d[pt][pz+momdiff[IZ]][py+momdiff[IY]][px+momdiff[IX]],
		   data_1d[i]);
	    fflush(stdout);
#endif
	  }
	}
      }
    }
  }

#ifdef DEBUG
  printf("done reordering\n");
  fflush(stdout);
#endif

  for (i=0; i < mom->nval; i++) {
    data_1d[i] /= nvalues[i];
#ifdef DEBUG
    printf("%d nval=%d  %g \n", i, nvalues[i], data_1d[i]);
    fflush(stdout);
#endif
  }

  free(nvalues);
}

/***********************************************************************
 * setup_1d_momenta_list: reads a list of momenta (px,py,pz,pt)        *
 *  from a file and finds these values in an array of Z3-symmetrised   *
 *  data as produced by setup_momenta, creating a 1-d array consisting *
 *     only of the (Z3-equivalent) momentum values in the list.        *
 ***********************************************************************/

int setup_1d_momenta_list(char *filename, Momenta *mom)
{
  int i, j, px, py, pz, pt;
  const int nall = nallmom(mom);
  Real p_tot;
  const Real unit_sqr = (Real)mom->L[IZ]*mom->L[IZ]/(Real)(mom->L[IT]*mom->L[IT]);
  const int npt 
    = mom->bc[IT]==PERIODIC ? mom->nmom_in[IT]+1 : mom->nmom_in[IT];
  const char fnm[MAXNAME] = "setup_1d_momenta_list";
  FILE *input;
  char line[MAXLINE];
  int status;

/* Check validity of arguments */

  if (status=check_input(mom)) return status;

/* indx() assumes that the nmom_in are equal */
  if ( (mom->nmom_in[IX] != mom->nmom_in[IY])
       || (mom->nmom_in[IY] != mom->nmom_in[IZ]) ) {
    printf("%s: Error: spatial nmomenta not equal\n", fnm);
    return BADPARAM;
  }
  
  if (mom->nvar > 1) {
    printf("%s: Warning: only computing naive momentum values\n", fnm);
    printf("%s: resetting nvar to 1\n",fnm);
    mom->nvar=1;
  }

/*  Allocate space  */
  if (mom->initialised) {
    printf("%s: Warning: reinitialising momentum structures\n", fnm);
    free_momenta(mom);
  }
  mom->index = malloc(sizeof(int)*nall);
/* allocating more than enough space for momentum values   *
 * alternatively, find the length of the file and use that */
  mom->val   = arralloc(sizeof(Real),2,1,nall);
  for (i=0; i < nall; i++) {
    mom->index[i] = DISCARDED;
  }

  if ((input=fopen(filename, "r")) == NULL) {
    printf("Could not open file <%s>\n", filename);
    return BADFILE;
  }
    
  j=0;
  while (fgets(line,MAXLINE,input)) {
    if (sscanf(line, "%d %d %d %d", &px, &py, &pz, &pt) != 4) {
      printf("Bad data on file <%s>\n", filename);
      printf("Read line as <%s>\n", line);
      return BADDATA;
      exit (-1);
    }
    i = pt + npt*indx(px,py,pz,mom->nmom_in[IZ]+1);
    if (mom->bc[IT] == PERIODIC)
      p_tot = (Real)(px*px+py*py+pz*pz)+pt*pt*unit_sqr;
    else
      p_tot = (Real)(px*px+py*py+pz*pz)+(pt+0.5)*(pt+0.5)*unit_sqr;
    mom->index[i] = j;
    mom->val[0][j] = 2.0*PI*mom->scale*sqrt(p_tot)/mom->L[IZ];
    j++;
  }
  mom->nval = j;
#ifdef DEBUG
  for (i=0; i < j; i++) {
    printf("i=%d  p_tot=%g\n", i, mom->val[0][i]);
    fflush(stdout);
  }
#endif
  fclose(input);
  mom->initialised = TRUE;
  return 0;
}

/***********************************************************************
 * reorder_1d_momenta: takes as input Z3-symmetrised data as generated *
 *  by reorder_momenta (with no cuts, sort or O(3)/Z4) and selects     *
 *  and averages data as specified by setup_momenta                    *
 *  or setup_1d_momenta_list                                           *
 ***********************************************************************/

void reorder_1d_momenta(Real *data_out, Real *data_in, Momenta *mom)
{
  int i, j;
  int *nvalues = vecalloc(sizeof(int), mom->nval);
  const int nin = nallmom(mom);

  for (i=0; i < mom->nval; i++) {
    nvalues[i] = 0;
    data_out[i] = 0.0;
  }

  for (j=0; j < nin; j++) {
    if ( ( i=mom->index[j]) >= 0) {
      nvalues[i]++;
      if (mom->sort) data_out[mom->key[i]] += data_in[j];
      else data_out[i] += data_in[j];
    }
  }
  
  for (i=0; i < mom->nval; i++) {
    data_out[i] /= nvalues[i];
#ifdef DEBUG
    printf("%d  %g \n", i, data_out[i]);
    fflush(stdout);
#endif
  }

  free(nvalues);
}

/***********************************************************************
 * re_reorder_momenta: takes as input Z3-symmetrised data as generated *
 *  by reorder_momenta with mom_in and averages data using mom_out     *
 ***********************************************************************/

int re_reorder_momenta(Real *data_out, Real *data_in, 
			Momenta *mom_in, Momenta *mom_out)
{
  const int nall = nallmom(mom_in);
  const char fnm[MAXNAME] = "re_reorder_momenta";
  int *nvalues = vecalloc(sizeof(int), mom_out->nval);
  int i, j;

  if ( nall != nallmom(mom_out) ) {
    printf("%s: momenta do not match\n", fnm);
    return BADPARAM;
  }
  if (mom_in->sort && mom_in->sortvar != mom_out->sortvar) {
    printf("%s: incompatible sort keys %d != %d\n", fnm, 
	   mom_in->sortvar, mom_out->sortvar);
    return BADPARAM;
  }
  for (i=0; i<DIR; i++) {
    if ( mom_in->nmom_in[i] < mom_out->nmom_in[i] 
	 || mom_in->nmom_out[i] < mom_out->nmom_out[i] ) {
      printf("%s: mom_out too big in direction %d:\n", fnm, i);
      printf("%s: %d > %d or %d > %d\n", fnm, 
	     mom_in->nmom_in[i], mom_out->nmom_in[i],
	     mom_in->nmom_out[i], mom_out->nmom_out[i]);
      return BADPARAM;
    }
    if ( mom_out->bc[i] != mom_in->bc[i] ) {
      printf("%s: boundary conditions do not match in direction %d\n", fnm, i);
      return BADPARAM;
    }
  }

  for (i=0; i<mom_out->nval; i++) {
    nvalues[i] = 0;
    data_out[i] = 0.0;
  }

  for (j=0; j<nall; j++) {
    if ( (i=mom_out->index[j]) >= 0) {
      int k = mom_in->index[j];
      if (k<0) {
	printf("%s: Fatal: nonexistent input mom value no. %d\n", fnm, j);
	return EVIL;
      }
      nvalues[i]++;
#if 0 // sorting the data here stuffs up thing later: use key when needed
      if (!mom_out->sort || mom_in->sort) data_out[i] += data_in[k];
      else data_out[mom_out->key[i]] += data_in[k];
#else
      data_out[i] += data_in[k];
#endif
    }
  }
  
  for (i=0; i<mom_out->nval; i++) {
    data_out[i] /= nvalues[i];
#ifdef DEBUG
    printf("%d  %g \n", i, data_out[i]);
    fflush(stdout);
#endif
  }

  free(nvalues);
  return 0;
}

/*   check_input: check the validity of the parameters                  */

static int check_input(Momenta *mom)
{
  const char fnm[MAXNAME] = "check_input";

  if ( (mom->L[IX] != mom->L[IY] && mom->L[IX] > 1) ||
       (mom->L[IY] != mom->L[IZ] && mom->L[IY] > 1)) {
    printf("%s: spatial lengths of lattice not equal (or 1)\n", fnm);
    return BADPARAM;
  }
  if ( (mom->bc[IX] != PERIODIC) || (mom->bc[IY] != PERIODIC) || 
       (mom->bc[IZ] != PERIODIC) ) {
    printf("%s: non-periodic boundaries in space not implemented\n", fnm);
    return BADPARAM;
  }
  if ( (mom->nmom_in[IX] < mom->nmom_out[IX])
       || (mom->nmom_in[IY] < mom->nmom_out[IY])
       || (mom->nmom_in[IZ] < mom->nmom_out[IZ])
       || (mom->nmom_in[IT] < mom->nmom_out[IT]) ) {
    printf("%s: outgoing nmomenta greater than incoming\n", fnm);
    return BADPARAM;
  }
  if (mom->nmom_in[IX]>mom->nmom_in[IY]
      || mom->nmom_in[IY]>mom->nmom_in[IZ]) {
    printf("%s: nmom=(%d,%d,%d) (must have nmomx<=nmomy<=nmomz)\n", fnm,
	   mom->nmom_in[IX], mom->nmom_in[IY], mom->nmom_in[IZ]);
    return BADPARAM;
  }
  if (mom->nmom_out[IX]>mom->nmom_out[IY]
      || mom->nmom_out[IY]>mom->nmom_out[IZ]) {
    printf("%s: nmom=(%d,%d,%d) (must have nmomx<=nmomy<=nmomz)\n", fnm,
	   mom->nmom_out[IX], mom->nmom_out[IY], mom->nmom_out[IZ]);
    return BADPARAM;
  }
  if ( mom->nmom_in[IX]>mom->L[IX]/2 || mom->nmom_in[IY]>mom->L[IY]/2 ||
       mom->nmom_in[IZ]>mom->L[IZ]/2 || mom->nmom_in[IT]>mom->L[IT]/2 ) {
    printf("%s: Warning: momenta greater than L/2\n", fnm);
  }

  if ( mom->sortvar >= mom->nvar ) {
    printf("%s: sort variable %d does not exist\n",fnm,mom->sortvar);
    return BADPARAM;
  }

  if ( !mom->z3 ) {
    printf("%s: Z3 symmetrisation is compulsory\n", fnm);
    return BADPARAM;
  }

  /* branch for equivalence averaging or smearing */
  if (mom->smear) {
    if (mom->smearing==0.0) {
      printf("%s: Error: smearing set to zero\n",fnm);
      return BADPARAM;
    }
  } else {  /* symmetrising not smearing */
/* Its no use treating time as equivalent to space if the bcs are different */
   if ( mom->z4 && (mom->bc[IT] == ANTIPERIODIC)) {
     printf("%s: Warning: trying to impose Z4 symmetry when time\n", fnm);
     printf("                        is antiperiodic.\n");
     printf("               Ignoring Z4 symmetry flag.\n");
   } 
   if  (mom->bc[IT] == ANTIPERIODIC ) mom->z4=FALSE;
/*  O(3) symmetrising is programmed to depend on pt=0 being the first *
 *  time momentum encountered.  With diagonal cuts this may not work  *
 *  so well.                                                          */
   if ( mom->o3 && (mom->dcut > 0.0) ) {
     printf("%s: Warning: trying to impose O(3) symmetry with", fnm);
     printf("diagonal cuts.\n");
     printf("                        This may lead to undefined behaviour\n");
   }
  }

  return 0;
}

void scale_momenta(Momenta* mom, Real scale)
{
  int i,v;
  Real s;

  for (v=0; v<mom->nvar; v++) {
    s = (v==MOMKDK) ? scale*scale : scale;
    for (i=0; i<mom->nval; i++) mom->val[v][i] *= s;
  }
  mom->scale *= scale;
}

/* free_momenta: frees up all the dynamically allocated storage         *
 *   but keeps the static data intact                                   */
void free_momenta(Momenta *mom)
{
  if (!mom->initialised) return;
  free(mom->index);
  free(mom->val);
  free(mom->nequiv);
  if (mom->itabexists) free(mom->itab);
  mom->itabexists = FALSE;
  if (mom->sort) free(mom->key);

  mom->initialised = FALSE;
}

/* nallmom is the total number of distinct Z3-averaged momenta available *
 * when p[i] = 0...nmom_in[i] (or 1/2...nmom_in[i]-1/2)                  */

int nallmom(Momenta *mom)
{
  const int npt = mom->bc[IT]==PERIODIC ? mom->nmom_in[IT]+1
                                        : mom->nmom_in[IT];
  int n = (mom->nmom_in[IX]+1)*(mom->nmom_in[IY]+1)*(mom->nmom_in[IZ]+1)
    + (mom->nmom_in[IX]+1)*mom->nmom_in[IX]*(mom->nmom_in[IX]-1)/6
    - (mom->nmom_in[IX]+1)*((mom->nmom_in[IY]+1)*mom->nmom_in[IY]
			    +(mom->nmom_in[IZ]+1)*mom->nmom_in[IX])/2;
  n *= npt;
  return n;
}

/* The index function returns the map of the unsorted (and signed) x to ip */
/* In momindex_p, x is assumed to take values -nmom,...,nmom               *
 * In momindex, x is assumed to take values 0,...,ntotmom                  */
int momindex_p(const int x[DIR], Momenta* mom)
{
  const int Nt = mom->bc[IT]==PERIODIC ? mom->nmom_in[IT]+1
                                        : mom->nmom_in[IT];
  const int Nz=mom->nmom_in[IZ]+1;
  const int Ny=mom->nmom_in[IY]+1;
  int ix=abs(x[IX]);
  int iy=abs(x[IY]);
  int iz=abs(x[IZ]);
  int it = mom->bc[IT]==PERIODIC ? abs(x[IT]) : x[IT]<0 ? -x[IT]-1 : x[IT];
  int n;

  SORT(ix,iy,iz);  /* now ix<=iy<=iz */

  if (mom->itabexists) return mom->itab[ix][iy-ix][iz-iy][it];

  if ( Ny==Nz ) return mom->index[it + Nt*indx(ix,iy,iz,Nz)];

  n = (iz-iy) + (2*Nz-iy-ix+1)*(iy-ix)/2;
  n += ix*Ny*Nz + ix*(ix-1)*(ix-2)/6 - ix*(Ny*(Ny-1)+Nz*(ix-1))/2;
  
  return mom->index[it + Nt*n];
}

static int indx(int x, int y, int z, int L)
{
  /* index = (z-y) + T1'(y-x-1,L-x) + T2'(x-1,L)        *
   *    T1'(n,N) = (2*N-n)*(n+1)/2                      *
   *    T2'(n,N) = N*(n+1)*(N-n+1)/2 + n*(n+1)*(n-1)/6  */

  int T1 = (2*L-y-x+1)*(y-x)/2;
  int T2 = L*x*(L-x+2)/2 + x*(x-1)*(x-2)/6;

#ifdef DEBUG
  printf("index: %d %d %d  i=%d\n", x, y, z, z-y+T1+T2);
#endif
  
  return(z-y+T1+T2);

}

/*  ~nequiv returns the number of vectors which are Z3-equivalent to *
 *     (x,y,z).  It is assumed that x<=y<=z.                         */
static int nequiv(int x, int y, int z, int ndim)
{
  if (z==0) return 1;
  switch (ndim) {
  case 1:
    if (x || y) {
      printf("nequiv: ndim=1 but x or y nonzero\n");
      return 0;
    }
    return 2;
  case 2:
    if (x) {
      printf("nequiv: ndim=2 but x nonzero\n");
      return 0;
    }
    if (y==z) return 4;
    else if (y) return 8;
    else return 4;
  case 3:
    if (y) {
      if (x==z) {
	return 8;
      }
      else if ((x == y) || (y == z)) {
	if (x) return 24;
	else return 12;
      }
      else {
	if (x) return 48;
	else return 24;
      }
    }
    else return 6;
  default:
    printf("nequiv: ndim=%d not implemented\n",ndim);
    return 0;
  }
}

#undef SORT
#undef SWAP

/*  talloc sets up a 4-d reference table to the 1-d index vector ind    *
 *  ind is assumed to refer to Z3-symmetrised momenta with the spatial  *
 *  momenta in the order                                                *
 *   (0,0,0),(0,0,1),...,(0,0,nz-1),(0,1,1),...,(0,1,nz-1),(0,1,2),...  */

static void* talloc(int n1, int n2, int n3, int n4, int* ind)
{
  const int Nx = n1;
  const int Ny = n1*n2-n1*(n1-1)/2;
  const int Nz = n1*n2*n3 - n1*(n2*(n2-1)+n3*(n1-1))/2
                + n1*(n1-1)*(n1-2)/6;

  void** arr = malloc(sizeof(void*)*(Nx+Ny+Nz));
  int* dp = ind;
  void **x=arr, **y=arr+Nx, **z=arr+Nx+Ny;
  int ix,iy,iz;

  for (ix=0;ix<n1;ix++) {
    *x++ = y;
    for (iy=0;iy<n2-ix;iy++) {
      *y++ = z;
      for (iz=0;iz<n3-iy-ix;iz++) {
	*z++ = dp;
	dp += n4;
      }
    }
  }

  return arr;
}
