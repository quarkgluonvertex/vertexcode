#ifndef _XDR_GAUGE_H_
#define _XDR_GAUGE_H_
#include "xdr.h"
#include "gauge.h"

bool_t xdr_parm(XDR* xdrs, Gauge_parameters* par);

#ifdef SU2_GROUP
bool_t xdr_su2(XDR* xdrs, Su2* u);
#define xdr_grpel xdr_su2
#elif defined SU3_GROUP
bool_t xdr_su3(XDR* xdrs, Su3* u);
bool_t xdr_su3_arr(XDR* xdrs, Su3** su3, u_int* n, u_int len);
#define xdr_grpel xdr_su3
#endif

bool_t xdr_algebra_c(XDR* xdrs, Algebra_c* a);
bool_t xdr_algebra(XDR* xdrs, Algebra_el* a);
#endif
