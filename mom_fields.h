/* $Id: mom_fields.h,v 1.6 2009/08/25 10:24:54 jonivar Exp $ */

/************************************************************************
 *                                                                      *
 *   functions to compute propagators, vertices, etc in momentum space  *
 *                                                                      *
 ************************************************************************/

#ifndef _MOM_FIELDS_H_
#define _MOM_FIELDS_H_

#include "gauge.h"
#include "momenta.h"
#ifndef GAUGE_ONLY
#include "fermion.h"
#endif

/*** gluon propagator functions ***/
Real** gluon_propagator(Gauge* g, Momenta* mom, int* status);
Real** gluon_prop_elmag(Gauge* g, Momenta* mom, int* status);
int compute_one_gluon_prop_diag(Real*,Algebra_c*,int);
int compute_one_gluon_prop_tl(Real* d, Algebra_c* g, int q[DIR], int L[DIR]);

#ifndef GAUGE_ONLY
/*** quark propagator functions ***/
int quark_propagator(Real* a, Real* b, Propagator* s, Momenta* mom);
int quark_propagator_cleanup();
int gorkov_propagator(Real**,Propagator*,Momenta*);
#endif

#endif
