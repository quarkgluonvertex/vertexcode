#include "matrixfunc.h"
#include "matrix.h"
#include "filename.h"
#include <assert.h>

int bootstrap_matrix(MatrixData* data, BootMatrixFuncs* fit, char* filename)
{
  const char fnm[MAXNAME] = "bootstrap_matrix.c";
  const int ndata  = data->ndata;
  const int nval = fit->yfunc(0,0,0,0,0,0);
  const int nboot = fit->nboot;
  const int do_fit = fit->model ? TRUE : FALSE;
  int i, j, k, writestat;
  Real dummy;
  Boot** ydata = init_bootarr(nboot,2,ndata,nval);
  Real** ytmpdata = arralloc(sizeof(Real),2,ndata,nval);
  MatrixData bootdata = *data;

  bootdata.mdata = arralloc(sizeof(Matrix*),2,data->nmatrix,data->nconfig);
  bootdata.realdata = arralloc(sizeof(Real*),2,data->nreal,data->nconfig);

  /*** initialise the matrix function ***/
  fit->yfunc(0,0,0,data->rank,0,0);

  matrixfunc_average(data,fit->yfunc,ytmpdata,0,0);
  for (j=0;j<ndata;j++) for (k=0;k<nval;k++) ydata[j][k].best = ytmpdata[j][k];

  if (do_fit) {
    fprintf(stderr,"%s: warning: fitting not implemented\n",fnm);
  }
  for (i=0;i<nboot;i++) {
    get_boot_matrixdata(&bootdata,data);
    matrixfunc_average(&bootdata,fit->yfunc,ytmpdata,0,fit->subset);
    for (j=0;j<ndata;j++) for (k=0;k<nval;k++)
      ydata[j][k].boot[i] = ytmpdata[j][k];
  }

  if (fit->save) {
    writestat = write_boot(*ydata,ndata*nval,filename);
  }

  /*** Free up space. The dummy call to datafunc frees up internal space ***/
  fit->yfunc(&dummy,0,0,0,0,0);

  free(bootdata.mdata);
  free(bootdata.realdata);
  free(ydata);
  free(ytmpdata);

  printf("\n");
  return (fit->save) ? writestat : 0;
}

int matrix_graph(MatrixData* this, Real* xdata, Matrixfunction* func,
		 char* name, PlotOptions* opt)
{
  const char fnm[MAXNAME] = "matrix_graph";
  const int ncfg = this->nconfig;
  const int ndata = this->ndata;
  const int msz = this->rank;
  const int nval = func ? func(0,0,0,0,0,0) : 0;
  Real** average;
  Real*** cov;
  int i,j,k,v;
 
  FILE* out;

  char filename[MAXLINE];
  char basename[MAXLINE];
  const Graph_t gtype = opt ? opt->graph_type : RAW;

  if (!func) { /* there is no obvious meaning to a null matrixfunction */
    fprintf(stderr,"%s: null datafunction - bailing out\n",fnm);
    return BADPARAM;
  }

  /*** initialise the matrix function ***/
  func(0,0,0,this->rank,0,0);

/* Set up the plot options */
  if (opt) {
    opt->nset = nval;
    if (!opt->errorbars) opt->errorbars = malloc(sizeof(Errorbar_t)*nval);
    for (i=0;i<nval;i++) opt->errorbars[i] = (ncfg>1) ? DY : NOERR;
    if (!opt->symbol) {
      opt->symbol = malloc(sizeof(int)*nval);
      for (i=0;i<nval;i++) opt->symbol[i]=i+1;
    }
    if (!opt->size) {
      opt->size = malloc(sizeof(float)*nval);
      for (i=0;i<nval;i++) opt->size[i]=DEFAULT_SIZE;
    }
    if (!opt->line) {
      opt->line = malloc(sizeof(int)*nval);
      for (i=0;i<nval;i++) opt->line[i]=0;
    }
  }

/* Make a name for file, checking if file already exists */
  if (name)
    sprintf(basename,"%s",name);
  else {
    if (this->name)
      sprintf(basename,"%s",this->name);
    else
      sprintf(basename,"latest_plot");
  }
  get_filename(filename,basename,suffix[gtype]);	

/* Write data to plot file */
  out = fopen(filename, "w");
  if (!out) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
    return BADFILE;
  }
  write_preamble(out,opt);

  average = arralloc(sizeof(Real),2,ndata,nval);
  if (ncfg>1)	{
    cov = arralloc(sizeof(Real),3,ndata,ndata,nval);
    /* Get covariance matrix and central values */
    matrixfunc_cov_ave(this,func,average,cov,0,0);
    for (v=0;v<nval;v++) {
      for (i=0;i<ndata;i++) {
	fprintf(out, "%12.6g %12.6g %12.6g\n",
		xdata[i],average[i][v],sqrt(cov[i][i][v]));
      }
      write_separator(out,opt,v+1);
    }
    free(cov);
  } else {
    /* evaluate the datafunction for the single config */
    const int nmat = this->nmatrix + this->nmconst + this->nmparam;;
    const int nreal = this->nreal + this->nrconst + this->nparam;
    Complex*** mat = arralloc(sizeof(Complex),3,nmat,msz,msz);
    Real rdata[nreal];
    int set;
    for (set=0;set<this->nmparam;set++) {
      for (j=0;j<msz;j++) for (k=0;k<msz;k++) {
	mat[set+this->nmatrix+this->nmconst][j][k] = this->mparam[set][j][k];
      }
    }
    for (set=0;set<this->nparam;set++) {
      rdata[set+this->nreal+this->nrconst] = this->param[set];
    }
    for (i=0;i<ndata;i++) {
      for (set=0;set<this->nmatrix;set++) {
	for (j=0;j<msz;j++) for (k=0;k<msz;k++) {
	  mat[set][j][k] = this->mdata[set][0][i][j][k];
	}
      }
      for (;set<this->nmatrix+this->nmconst;set++) {
	for (j=0;j<msz;j++) for (k=0;k<msz;k++) {
	  mat[set][j][k] = this->mdata[set-this->nmatrix][0][i][j][k];
	}
      }
      for (set=0;set<this->nreal;set++) {
	rdata[set] = this->realdata[set][0][i];
      }
      for (;set<this->nreal+this->nrconst;set++) {
	rdata[set] = this->rconst[set-this->nreal][i];
      }
      func(average[i],mat,rdata,msz,nmat,nreal);
    }
    free(mat);
    for (v=0;v<nval;v++) {
      for (i=0;i<ndata;i++) 
	fprintf(out, "%12.6g %12.6g\n", xdata[i],average[i][v]);
      write_separator(out,opt,v+1);
    }
  }

  fclose(out);
  /*** Free up space. The dummy call to datafunc frees up internal space ***/
  func(average[0],0,0,0,0,0);
  free(average);

  return 0;
}

void matrixfunc_average(MatrixData* data, Matrixfunction* func, Real** ave,
			int* cmask, int* xmask)
{
  const int len   = ntrue(xmask,data->ndata);
  const int nreal = data->nreal;
  const int nmatrix = data->nmatrix;
  const int nrvar = nreal + data->nrconst;
  const int nmvar = nmatrix + data->nmconst;
  const int nrall = nrvar + data->nparam;
  const int nmall = nmvar + data->nmparam;
  const int rank  = data->rank;
  int m,i;

  Complex**** matrixbundle
    = arralloc(sizeof(Complex),4,nmatrix,len,rank,rank);
  Real** realbundle = arralloc(sizeof(Real),2,nreal,len);
  Matrix* matrixdata = vecalloc(sizeof(Matrix),nmall);
  Real*   realdata   = vecalloc(sizeof(Real),nrall);

  /** average the config-dependent data **/
  matrix_average(data,cmask,xmask,matrixbundle,realbundle);

  for (i=0;i<len;i++) {
    /** assign the matrix data **/
    for (m=0;m<nmatrix;m++) {
      matrixdata[m] = matrixbundle[m][i];
    }
    /** the configuration-independent matrix data **/
    for (m=0;m<data->nmconst;m++) {
      matrixdata[nmatrix+m] = data->mconst[m][i];
    }
    /** and the constant matrices **/
    for (m=0;m<data->nmparam;m++) {
      matrixdata[nmvar+m] = data->mparam[m];
    }

    /** repeat for the real (scalar) data **/
    for (m=0;m<nreal;m++) {
      realdata[m] = realbundle[m][i];
    }
    for (m=0;m<data->nrconst;m++) {
      realdata[nreal+m] = data->rconst[m][i];
    }
    for (m=0;m<data->nparam;m++) {
      realdata[nrvar+m] = data->param[m];
    }

    (*func)(ave[i],matrixdata,realdata,rank,nmall,nrall);
  }

  free(matrixbundle);
  free(realbundle);
  free(matrixdata);
  free(realdata);
}

void matrixfunc_cov_ave(MatrixData* this, Matrixfunction* func,
			Real** ave, Real*** cov, int* cmask, int* xmask)
{
/* assume cov and ave are already allocated...? */
  const int len=ntrue(xmask,this->ndata);
  const int ncf=ntrue(cmask,this->nconfig);
  const int nval=func(0,0,0,0,0,0);
  Real*** jack_ave = arralloc(sizeof(Real),3,ncf,len,nval);
  int i,j,k,m;
  int* msk = cmask ? cmask : malloc(sizeof(int)*this->nconfig);
  assert(func); /* never call this with null func */

  /* get central values */
  matrixfunc_average(this,func,ave,cmask,xmask);

/* get the jackknifed averages */
  if (!cmask) for (i=0;i<ncf;i++) msk[i]=true;
  for (i=j=0;i<this->nconfig;i++) {
    if (msk[i]) {
      msk[i]=false;
      matrixfunc_average(this,func,jack_ave[j],msk,xmask);
      msk[i]=true;
      j++;
    }
  }
/* now we have to get the jackknife covariance... */
  for (k=0;k<nval;k++) for(i=0;i<len;i++) for(j=i;j<len;j++) {
    Real cij = 0.0;
    for(m=0;m<ncf;m++)
      cij += ((jack_ave[m][i][k]-ave[i][k])*(jack_ave[m][j][k]-ave[j][k]));
    cov[i][j][k] = cov[j][i][k] = cij;
  }
  free(jack_ave);
  if (!cmask) free(msk);
}

void matrix_average(MatrixData* this, int* cmask, int* xmask,
		    Complex**** mave, Real** rave)
{
  const int ncfg = ntrue(cmask,this->nconfig);
  const int rank = this->rank;
  int m,i,j,k,l,cfg;

  for (i=j=0;i<this->ndata;i++) if (MASK(xmask,i)) {
    for (m=0;m<this->nmatrix;m++) for (k=0;k<rank;k++) for (l=0;l<rank;l++) {
      Complex csum = {0.0,0.0};
      for (cfg=0;cfg<this->nconfig;cfg++) if (MASK(cmask,cfg)) {
	csum.re += this->mdata[m][cfg][i][k][l].re;
	csum.im += this->mdata[m][cfg][i][k][l].im;
      }
      mave[m][j][k][l].re = csum.re/ncfg;
      mave[m][j][k][l].im = csum.im/ncfg;
    }
    for (m=0;m<this->nreal;m++) {
      Real sum = 0.0;
      for (cfg=0;cfg<this->nconfig;cfg++) if (MASK(cmask,cfg)) {
	sum += this->realdata[m][cfg][i];
      }
      rave[m][j] = sum/ncfg;
    }
    j++;
  }
}

void get_boot_matrixdata(MatrixData* bootdata, MatrixData* data)
{
  int* bootlist = vecalloc(sizeof(int),data->nconfig);
  int i, j;

  /* we assume that bootdata and data are compatible, so no checks */  
  get_boot_subset(bootlist,data->nconfig);

  /* we assume that config-independent data are already assigned */
  for (j=0;j<data->nconfig;j++) {
    for (i=0;i<data->nmatrix;i++) {   
      bootdata->mdata[i][j] = data->mdata[i][bootlist[j]];
    }
    for (i=0;i<data->nreal;i++) {   
      bootdata->realdata[i][j] = data->realdata[i][bootlist[j]];
    }
  }

  free(bootlist);
}
