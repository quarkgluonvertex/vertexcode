/*** declaration of the byte-swap routine ***/

#include <stddef.h>

int byte_swap(void* strip, size_t size, size_t length);
