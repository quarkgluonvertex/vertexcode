/* $Id: assign_params_fermion.c,v 1.12 2015/03/31 14:40:59 jonivar Exp $ */

#include "gauge.h"
#include "fermion.h"
#include "ftrans.h"
#include "momenta.h"
#include <producers.h>
#include <parm.h>

#define MAXACT 7
#define MAXPRO 8
#define MAXSMR 4
#define MAXGAU 4
#define UNSET_KAPPA -99.9999

static PaCode action[MAXACT]
 = {"WILSON","CLOVER","STAGGERED","GAUGEHIGGS","SYMANZIK","TSI31","FWCHW31"};
static PaCode gauge[MAXGAU]  = {"RANDOM","COULOMB","LANDAU","COVARIANT"};
static PaCode producer[MAXPRO]
 = {"INTERNAL","GC_OSU","UKQCD","SCIDAC","ROMA","KRASNITZ","SJH_HMC","TRINLAT"};
static PaCode smearing[MAXSMR] = {"NONE","BINSIZE","NBIN","NINBIN"};

int assign_params_fprop(char* filename, Propagator_parameters* ppar, 
			Ftrans_params* run, Momenta* mom)
{
  const char fnm[MAXNAME] = "assign_params_fprop";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));
  Gauge_parameters* gpar = malloc(sizeof(Gauge_parameters));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gact[MAXLINE]  = "WILSON";
  char fact[MAXLINE]  = "WILSON";
  char gau[MAXLINE] = "LANDAU";
  char pprod[MAXLINE] = "";
  char gprod[MAXLINE] = "INTERNAL";
  char tprod[MAXLINE] = "";
  char basename[MAXLINE]  = "";
  char directory[MAXLINE] = "";
  char smear[MAXLINE] = "NONE";
  char infostr[MAXLINE];

  double csw = 0.0, rotation = 0.0;
  double mass[4] = {0.0,0.0,0.0,0.0};
  double umu=0.0, uj=0.0;
  int prop_swap_in = FALSE;
  int prop_is_double = TRUE;
  int trans_swap_in = FALSE;
  int trans_is_double = TRUE;
  int ismear = 0;
  int id;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat || !gpar) {
    printf("%s: could not alloc space for gauge params and lattice!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=1;
  gpar->lattice = lat;
  ppar->gpar = gpar;

  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = TRUE;
  ppar->bcs[IX] = ppar->bcs[IY] = ppar->bcs[IZ] = ppar->bcs[IT] = FALSE;

  gpar->action = WILSON;
  gpar->nf     = 0;
  gpar->beta   = 6.0;
  gpar->sweep  = 0;
  gpar->producer_id = UNKNOWN;

  ppar->kappa    = UNSET_KAPPA;
  ppar->kappat   = UNSET_KAPPA;
  ppar->xi       = 1.0;
  ppar->mass     = 0.0;
  ppar->ncoeff   = 0;
  ppar->imp      = 0;
  ppar->chempot  = 0.0;
  ppar->j = ppar->jbar = 0.0; /* diquark and antidiquark source */
  ppar->chempot  = FALSE;
  ppar->source_type = 0;
  ppar->source_address[IX] = ppar->source_address[IY]
    = ppar->source_address[IZ] = ppar->source_address[IT] = 0;
  ppar->id = 0;
  memset(ppar->info,0,MAXLINE);

  run->nmomenta[IX] = run->nmomenta[IY] = run->nmomenta[IZ] = 0;
  run->nmomenta[IT] = 0;
  run->gauge_type = RANDOM_G;
  run->gauge_param = 0.0;
  run->gfix_precision = 1.0e-7;
  run->gfix_iterations = 0;
  run->trans_origin = UNKNOWN;

  run->spin_min = 0;
  run->spin_max = SPIN-1;
  run->colour_min = 0;
  run->colour_max = COLOUR-1;
  run->do_gauge_transform = TRUE;
  run->save = 0;
  run->save_propagator = FALSE;
  strcpy(run->control_dir,".");

  mom->cutoff = -1.0;
  mom->dcut = -1.0;
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = FALSE;
  mom->z4 = FALSE;
  mom->nvar = 3;
  mom->sort = FALSE;
  mom->sortvar = MOMK;
  mom->smear = NONE;
  mom->smearing = 0.05;

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,DEFAULT,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&run->nmomenta[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&run->nmomenta[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&run->nmomenta[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_momenta",&run->nmomenta[IT]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_x",&ppar->bcs[IX]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_y",&ppar->bcs[IY]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_z",&ppar->bcs[IZ]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_t",&ppar->bcs[IT]);
  add_parm(list,MUSTSET,STRING,"gauge_action",gact);
  add_parm(list,DEFAULT,STRING,"gauge_producer",gprod);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,DEFAULT,INTEGER,"nflavor",&gpar->nf);
  add_parm(list,DEFAULT,DOUBLE,"mass1",&mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"mass2",&mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"mass3",&mass[2]);
  add_parm(list,DEFAULT,DOUBLE,"mass4",&mass[3]);
  add_parm(list,DEFAULT,DOUBLE,"gauge_chempot",&umu);
  add_parm(list,DEFAULT,DOUBLE,"gauge_diquark_src",&uj);
  add_parm(list,MUSTSET,INTEGER,"sweep_number",&gpar->sweep);
  add_parm(list,MUSTSET,STRING,"fermion_action",fact);
  add_parm(list,MUSTSET,STRING,"prop_producer",pprod);
  add_parm(list,DEFAULT,STRING,"prop_basename",basename);
  add_parm(list,DEFAULT,STRING,"prop_dir",directory);
  add_parm(list,DEFAULT,BOOL,"prop_swap_in",&prop_swap_in);
  add_parm(list,DEFAULT,BOOL,"prop_is_double",&prop_is_double);
  add_parm(list,MUSTSET,DOUBLE,"kappa",&ppar->kappa);
  add_parm(list,DEFAULT,DOUBLE,"kappat",&ppar->kappat);
  add_parm(list,DEFAULT,DOUBLE,"anisotropy",&ppar->xi);
  add_parm(list,DEFAULT,DOUBLE,"chempot",&ppar->chempot);
  add_parm(list,DEFAULT,DOUBLE,"diquark_src",&ppar->j);
  add_parm(list,DEFAULT,DOUBLE,"antidiquark_src",&ppar->jbar);
  add_parm(list,DEFAULT,DOUBLE,"csw",&csw);
  add_parm(list,DEFAULT,DOUBLE,"rotation",&rotation);
  add_parm(list,DEFAULT,INTEGER,"source_type",&ppar->source_type);
  add_parm(list,DEFAULT,INTEGER,"source_x",&ppar->source_address[IX]);
  add_parm(list,DEFAULT,INTEGER,"source_y",&ppar->source_address[IY]);
  add_parm(list,DEFAULT,INTEGER,"source_z",&ppar->source_address[IZ]);
  add_parm(list,DEFAULT,INTEGER,"source_t",&ppar->source_address[IT]);
  add_parm(list,DEFAULT,BOOL,"do_gauge_transform",&run->do_gauge_transform);
  add_parm(list,MUSTSET,STRING,"gauge_type",gau);
  add_parm(list,MUSTSET,DOUBLE,"gauge_param",&run->gauge_param);
  add_parm(list,MUSTSET,STRING,"trans_producer",tprod);
  add_parm(list,DEFAULT,BOOL,"trans_swap_in",&trans_swap_in);
  add_parm(list,DEFAULT,BOOL,"trans_is_double",&trans_is_double);
  add_parm(list,DEFAULT,DOUBLE,"trans_precision",&run->gfix_precision);
  add_parm(list,DEFAULT,INTEGER,"gfix_iterations",&run->gfix_iterations);
  add_parm(list,DEFAULT,INTEGER,"colour_min",&run->colour_min);
  add_parm(list,DEFAULT,INTEGER,"colour_max",&run->colour_max);
  add_parm(list,DEFAULT,INTEGER,"spin_min",&run->spin_min);
  add_parm(list,DEFAULT,INTEGER,"spin_max",&run->spin_max);
  add_parm(list,DEFAULT,INTEGER,"save",&run->save);
  add_parm(list,DEFAULT,BOOL,"save_propagator",&run->save_propagator);
  add_parm(list,DEFAULT,STRING,"control_directory",run->control_dir);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smear);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(gpar->lattice);
  gpar->lattice->has_ntab = FALSE;

/*** Determine the gauge action ***/
  status += parm_from_string(&gpar->action,gact,action,MAXACT,"gauge_action");
  status += parm_from_string(&gpar->producer_id,gprod,producer,MAXPRO,"gauge_producer");
  if (gpar->producer_id < _PROD_ST && gpar->producer_id != COLD)
    gpar->producer_id += _PROD_ST-1;

/*** Put additional action parameters in the "mass" array if applicable ***/
  if (gpar->nf) {
    if (umu==0.0 && uj==0.0) {
      gpar->mass = malloc(sizeof(double)*gpar->nf);
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
    } else {
      gpar->mass = malloc(sizeof(double)*(gpar->nf+2));
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
      gpar->mass[gpar->nf] = umu;
      gpar->mass[gpar->nf+1] = uj;
      gpar->has_chempot = TRUE;
    }
  }
/*** Scale the diquark source with kappa ***/
  ppar->j *= ppar->kappa;
  ppar->jbar *= ppar->kappa;

/*** Determine gauge transformation type ***/
  status += parm_from_string(&run->gauge_type,gau,gauge,MAXGAU,"gauge_type");

/***  If we are not performing a gauge transformation these go  ***
 ***   directly into the Gauge_parameters                       ***/
  if (!run->do_gauge_transform) {
    gpar->gauge = run->gauge_type;
    gpar->gauge_param = run->gauge_param;
  } else {
    gpar->gauge = RANDOM_G;
    gpar->gauge_param = 0.0;
  }

/***  Assign remaining (unused) parameters to defaults ***/
  gpar->time  = 0;
  gpar->start = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace = 0.0;
  for (i=0; i<MAXPREC; i++)
    gpar->gfix_precision[i] = 100.0;  /* a ridiculous number... */
  gpar->gfix_iterations = 0;
  gpar->checksum = 0;

/*** Determine the fermion action ***/
  status += parm_from_string(&ppar->action,fact,action,MAXACT,"fermion_action");
  status += parm_from_string(&ppar->producer_id,pprod,producer,MAXPRO,"prop_producer");
  if (ppar->producer_id < _PROD_ST) ppar->producer_id += _PROD_ST;
  if (ppar->action != WILSON && ppar->action != CLOVER) {
    fprintf(stderr,"%s: illegal fermion action - only Wilson and clover implemented\n",
	    fnm);
    result = BADPARAM;
  }

/*** Determine anisotropy parameters if applicable ***/
  if (ppar->kappat != UNSET_KAPPA && ppar->xi != 1.0) {
    fprintf(stderr,"%s: Warning: both anisotropy and temporal kappa set\n",fnm);
    fprintf(stderr,"%s: Warning: taking anisotropy from temporal kappa\n",fnm);
  }
  if (ppar->kappat != UNSET_KAPPA) {
    ppar->xi = ppar->kappat/ppar->kappa;
  } else {
    ppar->kappat = ppar->kappa*ppar->xi;
  }

/*** Compute the "id" code from input formats ***/
  id=0;
  if (prop_swap_in) id += 2;
  if (!prop_is_double) id++;
  ppar->id = id;  /* UKQCD input -- other producers may have other ids */

  if (directory[0]) {
    sprintf(infostr,"dir:%s ",directory);
    strcat(ppar->info,infostr);
  }
  if (basename[0]) {
    sprintf(infostr,"base:%s ",basename);
    strcat(ppar->info,infostr);
  }



/***  Assign remaining (unused) parameters to defaults ***/
  ppar->kappa_scalar = 0.0;
  ppar->spin = ppar->colour = -1;
  ppar->nev = 0;

/***  Assign the elements of Ftrans_params  ***/

/*** Allow default nmomenta = L/4 ***/
  for (i=0;i<DIR;i++) {
    if (run->nmomenta[i]==-1) run->nmomenta[i] = gpar->lattice->length[i]/4;
  }

/*** determine the trans producer_id ***/
  status += parm_from_string(&run->trans_origin,tprod,producer,MAXPRO,"trans_producer");
  if (run->trans_origin < _PROD_ST) run->trans_origin += _PROD_ST;

/*** Compute the "id" code from input formats ***/
  id=0;
  if (trans_swap_in) id += 2;
  if (!trans_is_double) id++;
  run->trans_id = id;
  if (gpar->id > 1023) run->trans_id += 1024;

/*** set the remainder of run to default values ***/
  run->mu_min = run->mu_max = 0;
  run->partial_save = FALSE;
  run->id = 0;

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_in[i]  = run->nmomenta[i];
    mom->nmom_out[i] = run->nmomenta[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = ppar->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smear,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->key = 0;
  mom->nval = 0;
  mom->initialised = FALSE;

/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}
