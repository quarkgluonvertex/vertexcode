#include "fermion.h"
#include <producers.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*****************************************************************************
 *                                                                           *
 *  Routines to construct filenames for fermion propagators                  *
 *  The filename is put into the Propagator.                                 *
 *  No checks are performed on the parameters (other than producer_id).      *
 *                                                                           *
 *****************************************************************************/

/*** The location of all binary data.                                    ***
 *** We assume a universal directory structure                           ***
 ***  DATA_ROOT/BETAbb/VxxXtt/{gauge|prop|trans|...}/<other_params>/...  ***
 *** This should preferably be set from the environment variable         ***
 *** $DATA_ROOT                                                          ***/

#define DATA_ROOT "/scratch"
#define TRINLAT_ROOT "/data/trinlat"
#define TRINLAT_BASENAME "small";

static char data_root[MAX_FILENAME] = DATA_ROOT;
static char trinlat_root[MAX_FILENAME] = TRINLAT_ROOT;
static int  initialised = FALSE;
static int  trinlat_ini = FALSE;
static char prop_dir[MAX_FILENAME] = "";
static char basename[MAX_FILENAME] = TRINLAT_BASENAME;

static int get_data_root(void) {
  char* str = getenv("DATA_ROOT");
  if (str) strcpy(data_root,str); else return 1;
  initialised = TRUE;
  return 0;
}

static int get_trinlat_root(void) {
  char* str = getenv("TRINLAT_ROOT");
  if (str) strcpy(trinlat_root,str); else return 1;
  trinlat_ini = TRUE;
  return 0;
}

/* get basename for propagator from the info string *
 * returns 1 on success, 0 on failure               */
static int get_basename(char* info) {
  char* sptr;
  if (sptr=strstr(info,"base:")) {
    if (sscanf(sptr,"base:%s ",basename)!=1) return 0;
    else return 1;
  }
  return 0;
}

/* get the data directory from the info string      */
static int get_prop_dir(char* info) {
  char* sptr;
  if (sptr=strstr(info,"dir:")) {
    if (sscanf(sptr,"dir:%s ",prop_dir)!=1) return 0;
  } else return 0;
  return 1;
}

#define MAXSRC 4
#define MAXACT 3
#define MAX_TYP_CODE 6
#define MAX_GAU_CODE 6

static int construct_prop_root(Propagator*);
static int construct_prop_root_trinlat(Propagator*);
static int construct_tfprop_root(Propagator*);
static int construct_fprop_filename(Propagator*);
static int construct_gfprop_filename(Propagator*);

static int set_prop_dir(Propagator_parameters* parm)
{
  const char fnm[MAXNAME] = "set_prop_dir";
  Lattice* lat = parm->gpar->lattice;
  Gauge_parameters* gpar = parm->gpar;
  if (get_prop_dir(parm->info)) return 0;

  switch(parm->producer_id) {
  case PROD_INTERNAL:
  case UKQCD:
    switch (gpar->nf) {
    case 0:
      sprintf(prop_dir,"%s/B%02d/V%02dX%02d/prop",
	      data_root,INT(gpar->beta*10),lat->length[IX],lat->length[IT]);
      break;
    case 2: // assuming dynamical wilson with chemical potential
      sprintf(prop_dir,"%s/B%03d/K%04d/V%02dX%02d/MU%04d",
	      data_root,INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      lat->length[IX],lat->length[IT],INT(gpar->mass[2]*1000));
      break;
    default:
      fprintf(stderr,"%s: Wrong Nf %d; only 0 and 2 implemented\n",fnm,
	      gpar->nf);
      return BADPARAM;
    }
    break;
  case TRINLAT:
    switch (parm->id) {
    case 0: /* quenched isotropic */
      sprintf(prop_dir,"%s/B%02d_%dx%dx%dx%d/CFG_%d",
	      trinlat_root,INT(gpar->beta*10),
	      lat->length[IX],lat->length[IY],lat->length[IZ],lat->length[IT],
	      gpar->sweep);
      break;
    case 1: /* quenched anisotropic */
      sprintf(prop_dir,"%s/B%03d/X%03d/V%02dX%02d",data_root,
	      INT(gpar->beta*100),INT(gpar->mass[0]*100),
	      lat->length[IX],lat->length[IT]);
      break;
    default:
      fprintf(stderr,"%s: unknown id %d for trinlat config\n",fnm,parm->id);
      return BADPARAM;
    }
    break;
  default:
    fprintf(stderr,"%s: unknown producer id %d\n",fnm,parm->producer_id);
    return BADDATA;
  }
  return 0;
}

int construct_prop_filename(Propagator* prop)
{
  const char fnm[MAXNAME] = "construct_prop_filename";
  if (!initialised) get_data_root();
  set_prop_dir(prop->parm);

  memset(prop->filename,0,MAXLINE);
  if (S_ISGORKOV(prop->type)) return construct_gorkov_filename(prop);
  switch (prop->space) {
  case XSPACE:
  case TSLICE_X:
    return construct_prop_root(prop);
  case TSLICE_P:
    return construct_tfprop_root(prop);
  case FOURIER:
  case FOURIERSLICE:
    return construct_fprop_filename(prop);
  default:
    fprintf(stderr,"%s: unknown space for propagator %d\n",fnm,prop->space);
    return BADDATA;
  }
  return 0;
}

static int construct_prop_root_ukqcd(Propagator* p)
{
  const char fnm[MAXNAME] = "construct_prop_root_ukqcd";
  Gauge_parameters* gpar = p->parm->gpar;
  const char srccode[MAXSRC] = "LRFS";
  const char actcode[MAXACT] = "WCS";
  char typecode[MAX_TYP_CODE] = "\0\0\0\0\0\0";

  switch (p->space) {
  case XSPACE:
  case TSLICE_X:
    typecode[0] = (gpar->nf) ? 'S' : 'Q';  break;
  case FOURIER:
  case TSLICE_P:
  case FOURIERSLICE:
    strcpy(typecode,"SK"); break;
  default:
    fprintf(stderr,"%s: unknown space for propagator %d\n",fnm,p->space);
    return BADDATA;
  }

  switch (gpar->nf) {
  case 0:
    sprintf(p->filename,"%s/%s%02dU%06d%c%04d%c",
	    prop_dir,typecode,INT(gpar->beta*10),gpar->sweep,
	    srccode[p->parm->source_type],INT(100000*(p->parm->kappa-0.1)),
	    actcode[p->parm->action]);
    break;
  case 2: // assuming dynamical wilson with chemical potential
    if (p->parm->xi==1.0) {
      sprintf(p->filename,"%s/%s%c%03dK%04dU%06d%c%04dX",
	      prop_dir,typecode,actcode[p->parm->action],
	      INT(gpar->beta*100), INT(gpar->mass[0]*10000),
	      gpar->sweep, srccode[p->parm->source_type],
	      INT(10000*p->parm->kappa));
    } else {
      sprintf(p->filename,"%s/%s%c%03dK%04dU%06d%c%04dKT%04dX",
	      prop_dir,typecode,actcode[p->parm->action],
	      INT(gpar->beta*100), INT(gpar->mass[0]*10000),
	      gpar->sweep, srccode[p->parm->source_type],
	      INT(10000*p->parm->kappa),INT(10000*p->parm->kappat));
    }
    break;
  default:
    fprintf(stderr,"%s: Wrong Nf %d; only 0 and 2 implemented\n",fnm,
	    gpar->nf);
    return BADPARAM;
  }

  return 0;
}

static int construct_prop_root_trinlat(Propagator* s)
{
  const char fnm[MAXNAME] = "construct_prop_root_trinlat";
  if (!trinlat_ini) get_trinlat_root();
  get_basename(s->parm->info);

  sprintf(s->filename,"%s/%s.T%03d",prop_dir,basename,
	  s->parm->source_address[IT]);

  return 0;
}

static int construct_prop_root(Propagator* s)
{
  const char fnm[MAXNAME] = "construct_prop_root";
  const int prodid = s->parm->producer_id;
  char tmp_filename[MAXLINE];
  int status;
  
  if (prodid != TRINLAT && get_basename(s->parm->info)) {
    sprintf(s->filename,"%s/%s.%05d.prop",prop_dir,basename,
	    s->parm->gpar->sweep);
    return 0;
  }

  switch (prodid) {
  case TRINLAT:
    return construct_prop_root_trinlat(s);
  case UKQCD:
    return construct_prop_root_ukqcd(s);
  case PROD_INTERNAL:
    status = construct_prop_root_ukqcd(s);
    if (status) return status;
    strcpy(tmp_filename,s->filename);
    sprintf(s->filename,"%s.%d.%d.%d.%d",tmp_filename,
	    s->parm->source_address[IX],s->parm->source_address[IY],
	    s->parm->source_address[IZ],s->parm->source_address[IT]);
    return 0;
  default: /* Assume this is the full filename, include sweep number */
    get_basename(s->parm->info);
    sprintf(s->filename,"%s/%s.%05d.prop",prop_dir,basename,
	    s->parm->gpar->sweep);
  }
  return 0;
}

static int construct_tfprop_root(Propagator* s)
{
  const char fnm[MAXNAME] = "construct_tfprop_root";
#if 0 // avoid annoying warning messages....
  fprintf(stderr,"%s: Not defined! Exiting\n",fnm);
#endif
  return NOCODE;
}

static int construct_fprop_filename(Propagator* s)
{
  const char fnm[MAXNAME] = "construct_fprop_filename";
  char gauge_code[MAX_GAU_CODE] = "\0\0\0\0\0\0";
  int status;

  if (status=construct_prop_root(s)) return status;

/*** code for gauge type and parameter ***/
  switch (s->parm->gpar->gauge) {
  case LANDAU: gauge_code[0]='L'; break;
  case COVARIANT: 
    sprintf(gauge_code,"X%02d",INT(10*s->parm->gpar->gauge_param+0.01));
    break;
  case COULOMB: gauge_code[0]='C'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   s->parm->gpar->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='L';
  }
  strcat(s->filename,gauge_code);

  return 0;
}

/*** Gorkov propagator routines ***/
static int construct_gorkov_root(Propagator* p);

int construct_gorkov_filename(Propagator* prop)
{
  const char fnm[MAXNAME] = "construct_gorkov_filename";
  if (!initialised) get_data_root();
  set_prop_dir(prop->parm);

  switch (prop->space) {
  case XSPACE:
  case TSLICE_X:
    return construct_gorkov_root(prop);
  case TSLICE_P:
    return NOCODE; /* not defined */
  case FOURIER:
    return construct_gfprop_filename(prop);
  default:
    fprintf(stderr,"%s: unknown space type %d\n",fnm,prop->space);
    return BADDATA;
  }
  return 0;
}

static int construct_gorkov_root(Propagator* p)
{
  const char fnm[MAXNAME] = "construct_gorkov_root";
  Gauge_parameters* gpar = p->parm->gpar;
  const int prodid = p->parm->producer_id;
  const char srccode[MAXSRC] = "LRFS";
  const char actcode[MAXACT] = "WCS";

  if (prodid!=PROD_INTERNAL) {
    fprintf(stderr,"%s: unknown producer id %d\n",fnm,prodid);
    return BADDATA;
  }

  switch (gpar->nf) {
  case 0:
    sprintf(p->filename,"%s/Q%02dU%06d%c%04d%c",
	    prop_dir,INT(gpar->beta*10),gpar->sweep,
	    srccode[p->parm->source_type],INT(100000*(p->parm->kappa-0.1)),
	    actcode[p->parm->action]);
    break;
  case 2: // assuming dynamical wilson
    sprintf(p->filename,
	    "%s/G%c%03dK%04dU%06d%c%04dM%04dJ%03dX",
	    prop_dir, actcode[p->parm->action],
	    INT(gpar->beta*100), INT(gpar->mass[0]*10000),
	    gpar->sweep, srccode[p->parm->source_type],
	    INT(10000*p->parm->kappa),
	    INT(1000*p->parm->chempot), INT(1000*p->parm->j/p->parm->kappa));
    break;
  default:
    fprintf(stderr,"%s: Wrong Nf %d; only 0 and 2 implemented\n",fnm,
	    gpar->nf);
    return BADPARAM;
  }

  return 0;
}

static int construct_gfprop_filename(Propagator* s)
{
  const char fnm[MAXNAME] = "construct_gfprop_filename";
  char gauge_code[MAX_GAU_CODE] = "\0\0\0\0\0\0";
  int status;

  if (status=construct_gorkov_root(s)) return status;

/*** code for gauge type and parameter ***/
  switch (s->parm->gpar->gauge) {
  case LANDAU: gauge_code[0]='L'; break;
  case COVARIANT: 
    sprintf(gauge_code,"X%02d",INT(10*s->parm->gpar->gauge_param+0.01));
    break;
  case COULOMB: gauge_code[0]='C'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   s->parm->gpar->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='L';
  }
  strcat(s->filename,gauge_code);

  return 0;
}

