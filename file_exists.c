/* $Id: file_exists.c,v 1.2 2003/09/21 20:43:22 jonivar Exp $ */
/*
 * Routines to check if a file exists and generate a proper filename.
 */	

#include "filename.h"

int file_exists(char *name)
{
  FILE *fp;
  int exists;

  if((fp=fopen(name,"r")) == NULL) {
    exists = 0;
  } else {
    exists = 1;
    fclose(fp);
  }
  return exists;
}

void get_filename(char *name, const char *base, const char *suffix)
{
	
  int counter = 0;
	
  do {
    if(counter<100)
      sprintf(name, "%s%02d%s", base, counter, suffix);
    else
      sprintf(name, "%s%d%s", base, counter, suffix);
    counter++;
  } while(file_exists(name));
}
