#include "gauge.h"
#include <array.h>
#include <string.h>
#include <assert.h>

void gauge_trans(Gauge u, Gauge_transform g)
{
  Lattice *lat = u.parm->lattice;
  int x[DIR], site, i;

/*** check arguments ***/
  assert(GROUP==SU2 || GROUP==SU3);
  assert(u.type==GAUGE || u.type==ALGEBRA);

/*** check compatibility of u and g ***/
  assert(lat==g.parm->lattice);

  if (u.type==GAUGE) {
    Group_el** uu = u.u;
    Group_el omega;
    for (site=0,x[3]=0; x[3]<lat->length[3]; x[3]++) {
      for (x[2]=0; x[2]<lat->length[2]; x[2]++) {
	for (x[1]=0; x[1]<lat->length[1]; x[1]++) {
	  for (x[0]=0; x[0]<lat->length[0]; x[0]++,site++) {
	    for (i=0;i<u.ndir;i++) {
	      const int nsite = site+lat->up[x[i]][i];
#ifdef SU2_GROUP
	      su2_mult(omega, g.g[site], uu[site][i]);
	      su2_mult_dag(uu[site][i], omega, g.g[nsite]);
#else
	      su3_mult(omega, g.g[site], uu[site][i]);
	      su3_mult_dag(uu[site][i], omega, g.g[nsite]);
#endif
	    }
	  }
	}
      }
    }
  } else { /* algebra field */
    Algebra_el** aa = u.u;
    Algebra_el tmp;
    assert(GROUP==SU2);
    /* TODO: warning/check for site/link field (eg F vs A)? */
    for (site=0; site<lat->nsite; site++) {
      for (i=0; i<u.ndir; i++) {
#ifdef SU2_GROUP
	su2alg_gagdag(tmp, aa[site][i], g.g[site]);
	su2alg_copy(aa[site][i], tmp);
#endif
    }
  }
  }
/*** Assign gauge fixing parameters to u ***/
  u.parm->gauge           = g.parm->gauge;
  u.parm->gauge_param     = g.parm->gauge_param;
  u.parm->rtrace          = g.parm->rtrace;
  u.parm->gfix_iterations = g.parm->gfix_iterations;
  for (i=0; i<MAXPREC; i++)
    u.parm->gfix_precision[i] = g.parm->gfix_precision[i];
}

/*** Gauge transform an algebra-valued covariant field object (eg E_i) ***/

void gauge_trans_field(Algebra_field e, Gauge_transform g)
{
  Lattice *lat = e.parm->lattice;
  int x, i;
  Algebra_el tmp;

  /** check that the lattices match **/
  assert(lat == g.parm->lattice);
  assert(GROUP==SU2);

  for (x=0; x<lat->nsite; x++) {
    for (i=0; i<e.ndir; i++) {
#ifdef SU2_GROUP
      su2alg_gagdag(tmp, e.e[x][i], g.g[x]);
      su2alg_copy(e.e[x][i], tmp);
#endif
    }
  }

/*** Assign gauge fixing parameters to e ***/
  e.parm->gauge           = g.parm->gauge;
  e.parm->gauge_param     = g.parm->gauge_param;
  e.parm->rtrace          = g.parm->rtrace;
  e.parm->gfix_iterations = g.parm->gfix_iterations;
  for (i=0; i<MAXPREC; i++)
    e.parm->gfix_precision[i] = g.parm->gfix_precision[i];
}

/************************************************************************
 * gauge_trans_temporal: fixes all links except on the last timeslice   *
 *   to temporal gauge A_0 = 0 (U_t = 1).                               *
 *   The last timeslice will then contain the polyakov loops.           *
 ************************************************************************/
void gauge_trans_temporal(Gauge u)
{
  Lattice* lat = u.parm->lattice;
  Group_el** uu = u.u;
  Group_el** ufix = arralloc(sizeof(Group_el),2,u.npoint,u.ndir);
  int x[DIR], site, mu;

  /** check arguments **/
  assert(u.type==GAUGE);
  assert(u.ndir==DIR);
  assert(ufix);

  memcpy(*ufix,*uu,sizeof(Group_el)*u.npoint*u.ndir);
  for (site=lat->nspatial,x[IT]=1;x[IT]<lat->length[IT];x[IT]++)
    for (x[IZ]=0;x[IZ]<lat->length[IZ];x[IZ]++)
      for (x[IY]=0;x[IY]<lat->length[IY];x[IY]++)
	for (x[IX]=0;x[IX]<lat->length[IX];x[IX]++,site++) {
	  /** multiply "next" link with U_t^dag and "previous" with U_t **/
	  int tsite = site+lat->dn[x[IT]][IT];
	  for (mu=0;mu<DIR;mu++) {
	    int dsite = site+lat->dn[x[mu]][mu];
	    if (x[mu]==0) {
	      grp_mult(ufix[site][mu],ufix[tsite][IT],uu[site][mu]);
	      grp_mult_dag(ufix[dsite][mu],uu[dsite][mu],ufix[tsite][IT]);
	    } else if (x[mu]==lat->length[mu]-1) {
	      grp_mult(uu[site][mu],ufix[tsite][IT],ufix[site][mu]);
	      grp_mult_dag(uu[dsite][mu],ufix[dsite][mu],ufix[tsite][IT]);
	    } else {
	      grp_mult(ufix[site][mu],ufix[tsite][IT],uu[site][mu]);
	      grp_mult_dag(uu[dsite][mu],ufix[dsite][mu],ufix[tsite][IT]);
	    }
	  }
	}

  free(ufix);

  /** set gauge parameters **/
  u.parm->gauge           = AXIAL;
  u.parm->gauge_param     = IT;
  u.parm->rtrace          = 1.0; /* dummy value */
  u.parm->gfix_iterations = 0;
  for (mu=0;mu<MAXPREC;mu++) u.parm->gfix_precision[mu] = 0.0;
}

void get_trans_temporal(Gauge u, Gauge_transform g)
{
  const char fnm[MAXNAME] = "get_trans_temporal";
  Lattice* lat = u.parm->lattice;
  Group_el** uu = u.u;
  int x[DIR], site, mu;

  /** check arguments **/
  assert(u.type==GAUGE);
  assert(u.ndir==DIR);
  assert(g.parm->lattice==lat);
  assert(g.g);

  for (site=0;site<lat->nspatial;site++) grp_unit(g.g[site]);

  for (x[IT]=1;x[IT]<lat->length[IT];x[IT]++)
    for (x[IZ]=0;x[IZ]<lat->length[IZ];x[IZ]++)
      for (x[IY]=0;x[IY]<lat->length[IY];x[IY]++)
	for (x[IX]=0;x[IX]<lat->length[IX];x[IX]++,site++) {
	  int tsite = site+lat->dn[x[IT]][IT];
	  grp_mult(g.g[site],g.g[tsite],uu[tsite][IT]);
	}

  /** set gauge parameters **/
  if (g.parm==u.parm) {
    fprintf(stderr,"%s: warning: overwriting gauge parameters\n",fnm);
  }
  g.parm->gauge           = AXIAL;
  g.parm->gauge_param     = IT;
  g.parm->rtrace          = 1.0; /* dummy value */
  g.parm->gfix_iterations = 0;
  for (mu=0;mu<MAXPREC;mu++) g.parm->gfix_precision[mu] = 0.0;
}
