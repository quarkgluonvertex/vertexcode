/* $Id: Real.h,v 1.3 2004/04/10 11:47:27 jonivar Exp $ */
#include <float.h>

/*** definition of default precision */ 
#ifndef _REAL_H_
#define _REAL_H_

#define UNSET_DBL DBL_MAX
#define UNSET_FLT FLT_MAX

#if 1
#define REAL_IS_DOUBLE
typedef double Real;
#define UNSET_REAL UNSET_DBL
#elif 0
#define REAL_IS_FLOAT
typedef float Real;
#define UNSET_REAL UNSET_FLT
#else
#define REAL_IS_LONGDOUBLE
typedef long double Real;
#define UNSET_REAL LDBL_MAX
#endif

#endif
