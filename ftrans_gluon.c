#include "gauge.h"
#include "gfix.h"
#include "ftrans.h"
#include "momenta.h"
#include "mom_fields.h"

static int set_gauge_params(Gauge_parameters*,Gfix_params*);
static int construct_gluon_filename(char* name, Gauge* u, char* dir);

int main(int argc, char** argv) {
  Gauge_parameters parm;
  Gauge u;
  Gauge_transform g;
  Gfix_params gfix;  /* dummy variable */

  Ftrans_params run;
  Momenta mom;
  Real** gluon_prop;
  int i;
  char filename[MAXLINE];
  int status;

/*** Read run parameters from file ***/
  if (argc < 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename> [gauge_filename]\n", argv[0]);
    return BADPARAM;
  }
  sprintf(filename, "%s",argv[1]);
  if (assign_params_gftrans(filename, &parm, &gfix, &run, &mom)) {
    return BADPARAM;
  }

/*** Initialise the lattice ***/
  compute_neighbours(parm.lattice);

/*** Initialise and allocate space ***/
  if ( (status = set_gauge_params(&parm,&gfix)) ) return status;
  if ( (status = init_gauge(&u,&parm,GAUGE)) ) return status;
  if (argc > 2) gauge_set_filename(&u,argv[2]);

/*** Read configuration data, including consistency checks ***/
  if ( (status = read_gauge(u)) ) return status;

/*** init_trans will give g the gauge params from run not parm ***/
  if (run.do_gauge_transform) {
    if ( (status = init_trans_ftrans(&g,&parm,&run)) ) return status;
    if ( (status = read_trans(g)) ) return status;

/*** Perform gauge transformation and throw away transform ***/
    gauge_trans(u,g);
    trans_delete(g);
    printf("ReTrU     = %10.6f\n",average_trace(u));
    printf("plaquette = %10.6f\n",average_plaquette(u));
  }

/* It is best to compute the gluon field BEFORE the fourier transform  */
  compute_gluon(&u,&u);
  printf("div A     = %10.6g\n",average_divergence(u));

/*** Allocate space for momentup space field and fourier transform ***/
  set_gauge_momenta(&u,&run);
  fourier_gauge(&u,&u);
  printf("div A     = %10.6g\n",average_divergence(u));

  if (run.save) {
/*** Dump momentum space field to file ***/
  /* TODO: call construct_filename from write? */
    if ( (status = construct_gauge_filename(&u)) ) return status;
    if ( (status = dump_gauge(u)) ) return status;
  }

/*** Now we can calculate the gluon propagator if we want to ***/

  if (run.save_propagator) {
    FILE* out;
    if ( (status = setup_momenta(&mom)) ) return status;
    gluon_prop = gluon_propagator(&u,&mom,&status);
    if (!gluon_prop) return status;

    status = construct_gluon_filename(filename,&u,run.control_dir);
    if (status) return status;
    if ( !(out=fopen(filename,"a")) ) {
      fprintf(stderr,"Cannot open <%s>\n",filename);
      return BADFILE;
    }
    fprintf(out,"Sweep number %d\n",u.parm->sweep);
    if (u.parm->gauge==LANDAU || u.parm->gauge==COULOMB) {
      for (i=0; i<mom.nval; i++) {
	fprintf(out,"%10.6f  %16.12f\n", mom.val[MOMQ][i], gluon_prop[0][i]);
      }
    } else {
      for (i=0; i<mom.nval; i++) {
	fprintf(out,"%10.6f   %16.12f  %16.12f\n", mom.val[MOMQ][i],
	       gluon_prop[0][i], gluon_prop[1][i]);
      }
    }
    fclose(out);
  }
  return 0;

}

static int set_gauge_params(Gauge_parameters* parm, Gfix_params* g)
{
  const char fnm[MAXNAME] = "set_gauge_params";

  parm->gauge = g->gauge;
  parm->gauge_param = g->param;
  parm->gfix_precision[0] = g->precision;
  parm->gfix_iterations = g->maxiter; /* this one is dubious */

  return 0;
}

static int construct_gluon_filename(char* name, Gauge* u, char* dir)
{
  const char fnm[MAXNAME] = "construct_gluon_filename";
  char gauge_code[6] = "\0\0\0\0\0\0";
  char cutstr[12] = "";

  switch (u->type) {
  case FALG: break;
  default:
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u->type);
    return BADPARAM;
  }

/*** code for gauge type and parameter ***/
  switch (u->parm->gauge) {
  case LANDAU: gauge_code[0]='l'; break;
  case COVARIANT: 
    sprintf(gauge_code,"x%02d",(int)(10*u->parm->gauge_param+0.01));
    break;
  case COULOMB: gauge_code[0]='c'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   u->parm->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='L';
  }
/*** create filename ***/
  switch (u->parm->nf) {
  case 0:
    sprintf(name,"%s/gluon_b%03ds%02dt%02d%s_p%d%d%d%02d%s.dat",
	    dir,(int)(u->parm->beta*100+0.001),
	    u->parm->lattice->length[IX],u->parm->lattice->length[IT],
	    gauge_code,
	    u->nmom[IX],u->nmom[IY],u->nmom[IZ],u->nmom[IT],cutstr);
    break;
  case 2: /* assuming wilson-type fermions here */
    sprintf(name,"%s/gluon_b%03dk%05ds%02dt%02d%s_p%02d%02d%02d%02d%s.dat",
	    dir,(int)(u->parm->beta*100+0.001),
	    (int)(u->parm->mass[0]*100000+0.001),
	    u->parm->lattice->length[IX],u->parm->lattice->length[IT],
	    gauge_code,
	    u->nmom[IX],u->nmom[IY],u->nmom[IZ],u->nmom[IT],cutstr);
    break;
  default:
    fprintf(stderr,"%s: invalid nf=%d; only 0 and 2 implemented\n",
	    fnm,u->parm->nf);
    return BADPARAM;
  }
  return 0;
}
