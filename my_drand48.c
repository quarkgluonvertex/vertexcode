#include "rand.h"
#include "RNG.h"
#include <stdio.h>

#define RANSEED 238976392L  /* default seed -- should not be used! */
#define RAN_FILENAME "rngseed.dat"

static char seed_filename[FILENAME_MAX] = RAN_FILENAME;
static long seed = 0;
static void set_seed(long s) { seed=s; }

long rand_seed(long s) { if (s) {seed=s; return 0;} else return seed; }

int rand_isinitialised()
{
  return uni_is_ini();
}

void rand_initialise(long s)
{
  if (!s) {
    if (!seed) {
      fprintf(stderr, "rand_initialise: seed is not set!\n");
      fprintf(stderr, "rand_initialise: Warning: using default seed\n");
      set_seed(RANSEED);
    }
    while (seed>SEED_MAX) seed/=4;
    my_drand48(seed);
  } else if (s==READ_SEED) {
    FILE* sf = fopen(seed_filename,"rb");
    if (!sf) {
      fprintf(stderr, "rand_initialise: cannot open seed file %s\n",
	      seed_filename);
      fprintf(stderr, "rand_initialise: Warning: using internal seed\n");
      rand_initialise(0);
      return;
    }
    if (uni_read(sf)) {
      fprintf(stderr, "rand_initialise: corrupted seed file %s\n",
	      seed_filename);
      fprintf(stderr, "rand_initialise: Warning: using internal seed\n");
      rand_initialise(0);
    }
    fclose(sf);
  } else {
    while (s>SEED_MAX) s/=4;
    my_drand48(s);
  }
}

double my_drand48(init_flag)
long int init_flag;
{
  double r;

  if(init_flag > 0)
  {
    seed_uni((int) init_flag);
    set_seed(init_flag);
  }
  else if(init_flag < 0)
  {
    do
    {
      init_flag += 100000;
    }
    while(init_flag < 0);

    seed_uni((int) init_flag);
    set_seed(init_flag);
  }

  r = (double) uni();

  return r;
}

int rand_save()
{
  const char fnm[MAXNAME] = "rand_save";
  FILE* fp = fopen(seed_filename,"wb");
  if (!fp) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,seed_filename);
    return BADFILE;
  }
  if (uni_write(fp)) {
    fprintf(stderr,"%s: error on writing to %s\n",fnm,seed_filename);
    fclose(fp);
    return BADFILE;
  }
  fclose(fp);
  return 0;
}
