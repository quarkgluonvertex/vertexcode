/* $Id: assign_params_gftrans.c,v 1.20 2015/06/17 13:37:16 jonivar Exp $ */

#include "gauge.h"
#include "gfix.h"
#include "ftrans.h"
#include "momenta.h"
#include <producers.h>
#include <parm.h>

#define HOT_VALUE 2.0
#define MAXACT 5
#define MAXGAU 6
#define MAXPRO 8
#define MAXSMR 4

static PaCode action[MAXACT]
            = {"WILSON","CLOVER","STAGGERED","GAUGEHIGGS","SYMANZIK"};
static PaCode gauge[MAXGAU]
            = {"RANDOM","COULOMB","LANDAU","COVARIANT","AXIAL","UNITARY"};
static PaCode producer[MAXPRO]
            = {"INTERNAL","GC_OSU","UKQCD","SCIDAC","ROMA","KRASNITZ","SJH_HMC","TRINLAT"};
static PaCode smearing[MAXSMR] = {"NONE","BINSIZE","NBIN","NINBIN"};

/********************************************************************
 *  assign_params_fgauge: assigns all the parameters required for a *
 *  		gauge field gauge and fourier transform.            *
 ********************************************************************/

int assign_params_gftrans(char* filename, Gauge_parameters* parm, 
			  Gfix_params* gparm, Ftrans_params* fparm, 
			  Momenta* mom)
{
  const char fnm[MAXNAME] = "assign_params_gftrans";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char act[MAXLINE] = "WILSON";
  char gau[MAXLINE] = "RANDOM";
  char gprod[MAXLINE] = "";
  char tprod[MAXLINE] = "INTERNAL";
  char basename[MAXLINE]  = "";
  char directory[MAXLINE] = "";
  char smear[MAXLINE] = "NONE";
  char infostr[MAXLINE];

  double dt=0.0, mu=0.0, j=0.0;
  double mass[4] = {0.0,0.0,0.0,0.0};
  int gauge_swap_in = FALSE;
  int gauge_is_double = TRUE;
  int gauge_is_ascii  = FALSE;
  int nproc = 1;
  int trans_swap_in = FALSE;
  int trans_is_double = FALSE;
  int seed = 0;
  int ismear = 0;
  int id;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat) {
    printf("%s: could not alloc space for lattice!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=1;
  parm->lattice = lat;

  fparm->nmomenta[IX] = fparm->nmomenta[IY] = fparm->nmomenta[IZ] = 0;
  fparm->nmomenta[IT] = 0;
  parm->bcs[IX] = parm->bcs[IY] = parm->bcs[IZ] = parm->bcs[IT] = TRUE;

  parm->lattice = lat;
  parm->action = WILSON;
  parm->nf = 0;
  parm->has_chempot = FALSE;
  parm->beta = 6.0;
  parm->sweep = 0;
  parm->time = 0;
  parm->producer_id = UNKNOWN;
  parm->id = 0;
  memset(parm->info,0,MAXLINE);

  gparm->producer_id = PROD_INTERNAL;
  gparm->gauge = RANDOM_G;
  gparm->param = 0.0;
  gparm->start = RANDOM;
  gparm->startval = HOT_VALUE;
  gparm->precision = 1.0e-7;
  gparm->criterion = DMUAMU;
  gparm->algorithm = OVERRELAX;
  gparm->maxiter = 1000;
  for (i=0; i<MAX_GFIXPAR; i++) gparm->algparm[i] = 0.0;

  fparm->do_gauge_transform = TRUE;
  fparm->write_trans = TRUE;
  fparm->save = 0;
  fparm->save_propagator = TRUE;
  fparm->medium_prop = FALSE;

  mom->cutoff = -1.0;
  mom->dcut = -1.0;
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = FALSE;
  mom->z4 = FALSE;
  mom->nvar = 3;
  mom->sort = FALSE;
  mom->sortvar = MOMQ;
  mom->smear = NONE;
  mom->smearing = 0.05;

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,DEFAULT,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&fparm->nmomenta[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&fparm->nmomenta[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&fparm->nmomenta[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_momenta",&fparm->nmomenta[IT]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_x",&parm->bcs[IX]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_y",&parm->bcs[IY]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_z",&parm->bcs[IZ]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_t",&parm->bcs[IT]);
  add_parm(list,MUSTSET,STRING,"action",act);
  add_parm(list,DEFAULT,INTEGER,"nflavor",&parm->nf);
  add_parm(list,MUSTSET,STRING,"gauge_producer",gprod);
  add_parm(list,DEFAULT,STRING,"gauge_basename",basename);
  add_parm(list,DEFAULT,STRING,"gauge_dir",directory);
  add_parm(list,MUSTSET,DOUBLE,"beta",&parm->beta);
  add_parm(list,DEFAULT,DOUBLE,"anisotropy",&dt);
  add_parm(list,DEFAULT,DOUBLE,"chempot",&mu);
  add_parm(list,DEFAULT,DOUBLE,"diquark_src",&j);
  add_parm(list,DEFAULT,DOUBLE,"mass1",&mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"mass2",&mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"mass3",&mass[2]);
  add_parm(list,DEFAULT,DOUBLE,"mass4",&mass[3]);
  add_parm(list,MUSTSET,INTEGER,"sweep_number", &parm->sweep);
  add_parm(list,DEFAULT,INTEGER,"time_step", &parm->time);
  add_parm(list,MUSTSET,STRING,"gauge_type",gau);
  add_parm(list,MUSTSET,DOUBLE,"gauge_param",&gparm->param);
  add_parm(list,MUSTSET,DOUBLE,"trans_precision",&gparm->precision);
  add_parm(list,DEFAULT,INTEGER,"fix_start",&gparm->start);
  add_parm(list,DEFAULT,DOUBLE,"start_value",&gparm->startval);
  add_parm(list,DEFAULT,STRING,"trans_producer",tprod);
  add_parm(list,DEFAULT,INTEGER,"fix_variable",&gparm->criterion);
  add_parm(list,DEFAULT,INTEGER,"algorithm",&gparm->algorithm);
  add_parm(list,MUSTSET,INTEGER,"max_iterations",&gparm->maxiter);
  add_parm(list,MUSTSET,DOUBLE,"gfix_param_1",&gparm->algparm[0]);
  add_parm(list,DEFAULT,DOUBLE,"gfix_param_2",&gparm->algparm[1]);
  add_parm(list,DEFAULT,DOUBLE,"gfix_param_3",&gparm->algparm[2]);
  add_parm(list,DEFAULT,DOUBLE,"gfix_param_4",&gparm->algparm[3]);
  add_parm(list,DEFAULT,BOOL,"gauge_swap_in",&gauge_swap_in);
  add_parm(list,DEFAULT,BOOL,"gauge_is_double",&gauge_is_double);
  add_parm(list,DEFAULT,BOOL,"gauge_is_ascii",&gauge_is_ascii);
  add_parm(list,DEFAULT,INTEGER,"gauge_nproc",&nproc);
  add_parm(list,DEFAULT,INTEGER,"id",&gparm->id);
  add_parm(list,DEFAULT,BOOL,"trans_swap_in",&trans_swap_in);
  add_parm(list,DEFAULT,BOOL,"trans_is_double",&trans_is_double);
  add_parm(list,MUSTSET,INTEGER,"seed",&seed);
  add_parm(list,DEFAULT,BOOL,"do_gauge_transform",&fparm->do_gauge_transform);
  add_parm(list,DEFAULT,BOOL,"write_trans",&fparm->write_trans);
  add_parm(list,DEFAULT,INTEGER,"save_fourier_fields",&fparm->save);
  add_parm(list,DEFAULT,BOOL,"save_propagator",&fparm->save_propagator);
  add_parm(list,DEFAULT,BOOL,"elmag",&fparm->medium_prop);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smear);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(parm->lattice);
  parm->lattice->has_ntab = FALSE;

/*** Determine the action ***/
  status += parm_from_string(&parm->action,act,action,MAXACT,"action");
  status += parm_from_string(&parm->producer_id,gprod,producer,MAXPRO,"gauge_producer");
  if (parm->producer_id < _PROD_ST) parm->producer_id += _PROD_ST;

/*** Put additional action parameters in the "mass" array if applicable ***/
  if (parm->action==GAUGEHIGGS) {
    parm->mass = malloc(sizeof(double)*4);
    parm->mass[0] = dt;
    for (i=0;i<3;i++) parm->mass[i+1]=mass[i];
  } else if (parm->action==TSI31) {
    parm->mass = malloc(sizeof(double)*(parm->nf+1));
    parm->mass[0] = dt;
    for (i=0;i<parm->nf;i++) parm->mass[i+1]=mass[i];
  } else if (parm->nf) {
    if (mu==0.0 && j==0.0) {
      parm->mass = malloc(sizeof(double)*parm->nf);
      for (i=0;i<parm->nf;i++) parm->mass[i]=mass[i];
    } else {
      parm->has_chempot = TRUE;
      parm->mass = malloc(sizeof(double)*(parm->nf+2));
      for (i=0;i<parm->nf;i++) parm->mass[i]=mass[i];
      parm->mass[parm->nf] = mu;
      parm->mass[parm->nf+1] = j;
    }
  } else {
/*** Put the "anisotropy" in the "mass" array if applicable ***/
    if (dt==0.0) parm->mass = NULL;
    else {
      parm->mass = malloc(sizeof(double));
      parm->mass[0] = dt;
    }
  }

/*** Compute the "id" code from input formats ***/
  if (parm->producer_id!=TRINLAT) {
  id=0;
  if (gauge_swap_in) id += 2;
  if (!gauge_is_double) id++;
  id += 4*(nproc-1);
  if (gauge_is_ascii) id += 4;
 /* note that ascii and nproc dont mix */
  if (gauge_is_ascii && nproc>1) {
    printf("%s: warning: ascii data on multiple processors may not work!!\n",
	   fnm);
    printf("%s: proceed at your own risk!\n",fnm);
  }
  if (gauge_is_ascii) id += 4;
  parm->id = id;
  }
  if (directory[0]) {
    sprintf(infostr,"dir:%s ",directory);
    strcat(parm->info,infostr);
  }
  if (basename[0]) {
    sprintf(infostr,"base:%s ",basename);
    strcat(parm->info,infostr);
  }

/***  Assign remaining (unused) parameters to defaults ***/
  parm->start = 0;
  parm->plaquette = 0.0;
  parm->gauge = RANDOM_G;
  parm->gauge_param = 0.0;
  parm->rtrace = 0.0;
  for (i=0; i<MAXPREC; i++)
    parm->gfix_precision[i] = 100.0;  /* a ridiculous number... */
  parm->gfix_iterations = 0;
  parm->checksum = 0;
  
/*** Determine the gauge type ***/
  status += parm_from_string(&gparm->gauge,gau,gauge,MAXGAU,"gauge_type");

/*** seed is long so must be handled separately ***/
  gparm->seed = seed;
  status += parm_from_string(&gparm->producer_id,tprod,producer,MAXPRO,"trans_producer");
  if (gparm->producer_id < _PROD_ST) gparm->producer_id += _PROD_ST;

/*** Compute the "id" code from input formats ***/
  id=0;
  if (trans_swap_in) id += 2;
  if (!trans_is_double) id++;
  gparm->id = id;
  if (parm->has_chempot) gparm->id += 1024; // KLUDGE

/*** copy ftrans_params from gfix_params ***/
  fparm->gauge_type = gparm->gauge;
  fparm->gauge_param = gparm->param;
  fparm->gfix_precision = gparm->precision;
  fparm->gfix_iterations = gparm->maxiter;
  fparm->trans_origin = gparm->producer_id;
  fparm->trans_id = gparm->id;

/*** set the remainder of fparm to default values ***/
  fparm->mu_min = 0;
  fparm->mu_max = 2;
  fparm->spin_min = fparm->spin_max = 0;
  fparm->colour_min = fparm->colour_max = 0;
  fparm->partial_save = FALSE;
  memset(fparm->control_dir,0,MAXLINE);
  fparm->control_dir[0] = '.';
  fparm->id = 0;

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_in[i]  = fparm->nmomenta[i];
    mom->nmom_out[i] = fparm->nmomenta[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = parm->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smear,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->key = 0;
  mom->nval = 0;
  mom->initialised = FALSE;


/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}
/********************************************************************
 *  assign_params_higgs: assigns all the parameters required for a  *
 *  		higgs field gauge and fourier transform.            *
 ********************************************************************/

int assign_params_higgs(char* filename, Higgs_parameters* parm, 
			Ftrans_params* fparm, Momenta* mom)
{
  const char fnm[MAXNAME] = "assign_params_higgs";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gprod[MAXLINE] = "KRASNITZ";
  char tprod[MAXLINE] = "INTERNAL";
  char smear[MAXLINE] = "NONE";

  int higgs_swap_in = FALSE;
  int higgs_is_double = TRUE;
  int higgs_is_ascii  = FALSE;
  int trans_swap_in = FALSE;
  int trans_is_double = FALSE;
  int ismear = 0;
  int id;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat) {
    printf("%s: could not alloc space for lattice!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=1;
  parm->lattice = lat;

  fparm->nmomenta[IX] = fparm->nmomenta[IY] = fparm->nmomenta[IZ] = 0;
  fparm->nmomenta[IT] = 0;

  parm->lattice = lat;
  parm->beta   = 9.0;
  parm->mu     = 1.0;
  parm->lambda = 1.0;
  parm->vev    = 0.0;
  parm->dt     = 0.0;
  parm->sweep = 0;
  parm->producer_id = UNKNOWN;
  parm->id = 0;

  fparm->do_gauge_transform = FALSE;
  fparm->save = FALSE;

  mom->cutoff = -1.0;
  mom->dcut = -1.0;
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = FALSE;
  mom->z4 = FALSE;
  mom->nvar = 3;
  mom->sort = FALSE;
  mom->sortvar = MOMQ;
  mom->smear = NONE;
  mom->smearing = 0.05;

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,DEFAULT,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&fparm->nmomenta[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&fparm->nmomenta[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&fparm->nmomenta[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_momenta",&fparm->nmomenta[IT]);
  add_parm(list,DEFAULT,STRING,"higgs_producer",gprod);
  add_parm(list,MUSTSET,DOUBLE,"beta",&parm->beta);
  add_parm(list,MUSTSET,DOUBLE,"lambda",&parm->lambda);
  add_parm(list,MUSTSET,DOUBLE,"mu",&parm->mu);
  add_parm(list,MUSTSET,DOUBLE,"temperature",&parm->T);
  add_parm(list,DEFAULT,DOUBLE,"anisotropy",&parm->dt);
  add_parm(list,MUSTSET,INTEGER,"sweep_number", &parm->sweep);
  add_parm(list,MUSTSET,INTEGER,"time_step", &parm->time);
  add_parm(list,DEFAULT,BOOL,"higgs_swap_in",&higgs_swap_in);
  add_parm(list,DEFAULT,BOOL,"higgs_is_double",&higgs_is_double);
  add_parm(list,DEFAULT,BOOL,"higgs_is_ascii",&higgs_is_ascii);
  add_parm(list,DEFAULT,BOOL,"do_gauge_transform",&fparm->do_gauge_transform);
  add_parm(list,DEFAULT,INTEGER,"gauge_type",&fparm->gauge_type);
  add_parm(list,DEFAULT,DOUBLE,"gauge_param",&fparm->gauge_param);
  add_parm(list,DEFAULT,STRING,"trans_producer",tprod);
  add_parm(list,DEFAULT,BOOL,"trans_swap_in",&trans_swap_in);
  add_parm(list,DEFAULT,BOOL,"trans_is_double",&trans_is_double);
  add_parm(list,DEFAULT,DOUBLE,"trans_precision",&fparm->gfix_precision);
  add_parm(list,DEFAULT,INTEGER,"trans_iterations",&fparm->gfix_iterations);
  add_parm(list,DEFAULT,BOOL,"save_fourier_fields",&fparm->save);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smear);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(parm->lattice);
  parm->lattice->has_ntab = FALSE;

/*** Determine the producer ids ***/
  status += parm_from_string(&parm->producer_id,gprod,producer,MAXPRO,"higgs_producer");
  if (parm->producer_id < _PROD_ST) parm->producer_id += _PROD_ST;

/*** Compute the "id" code from input formats ***/
  id=0;
  if (higgs_swap_in) id += 2;
  if (!higgs_is_double) id++;
  if (higgs_is_ascii) id += 4;
  parm->id = id;  /* UKQCD input -- other producers may have other ids */

  status += parm_from_string(&fparm->trans_origin,tprod,producer,MAXPRO,"trans_producer");
  if (fparm->trans_origin < _PROD_ST) fparm->trans_origin += _PROD_ST;

/*** Compute the "id" code from input formats ***/
  id=0;
  if (trans_swap_in) id += 2;
  if (!trans_is_double) id++;
  fparm->trans_id = id;

/*** set the remainder of fparm to default values ***/
  fparm->mu_min = 0;
  fparm->mu_max = 2;
  fparm->spin_min = fparm->spin_max = 0;
  fparm->colour_min = fparm->colour_max = 0;
  fparm->partial_save = FALSE;
  strcpy(fparm->control_dir,".");
  fparm->id = 0;

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_in[i]  = fparm->nmomenta[i];
    mom->nmom_out[i] = fparm->nmomenta[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = PERIODIC;
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smear,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;
/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->nval = 0;
  mom->initialised = FALSE;


/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}
