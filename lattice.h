/* $Id: lattice.h,v 1.2 2004/03/01 03:55:47 jonivar Exp $ */

/*
 * Definitions of lattice dimensions, boundary conditions etc
 */

#ifndef _LATTICE_H_
#define _LATTICE_H_

/* Lattice directions and boundary conditions */
#define DIR 4
#define SPATIAL_DIR 3		/* number of spatial dimensions */
#define IX 0
#define IY 1
#define IZ 2
#define IT 3
#define SPC_ST  0
#define SPC_END 3
#define PERIODIC     1
#define ANTIPERIODIC 0
#define	PARITY	2

/**  Structure containing lattice sizes **/

typedef int i4vec[DIR];

typedef struct {
  int length[DIR];	         /* Global size of lattice                  */
  int ndim;                      /* Actual number of dimensions             */
  int nsite;                     /* Total number of sites on the lattice    */
  int nspatial;                  /* Total number of sites on one time slice */
  i4vec *up;                     /* Nearest neighbours                      */
  i4vec *dn;
  int has_ntab;                  /* Are nearest neighbour tables set up?    */
} Lattice;

void compute_sizes(Lattice *lat);
void compute_neighbours(Lattice *lat);

#define XYZT2SITE(x,y,z,t,lat) ((x)+\
(lat)->length[IX]*((y)+(lat)->length[IY]*((z)+(lat)->length[IZ]*(t))))
#define X2SITE(x,lat) XYZT2SITE((x)[IX],(x)[IY],(x)[IZ],(x)[IT],lat)

#endif
