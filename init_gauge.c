#include "gauge.h"
#include <string.h>
#include <producers.h>
#include <arralloc.h>

/******************************************************************************
 *                                                                            *
 *  Routines to initialise and allocate space for gauge structures, including *
 *  gauge transformations, as well as to free space (delete structures)       *
 *                                                                            *
 ******************************************************************************/

/*** Bare-bones "constructor" for Gauge_transform ***
 *** The lattice data are NOT initialised         ***/
int init_trans(Gauge_transform* g, Gauge_parameters *parm, int has_file)
{
  const char fnm[MAXNAME] = "init_trans";
/*** Make space for a new parameter struct ***/
  g->parm=malloc(sizeof(Gauge_parameters));
  if (!(g->parm)) {
    fprintf(stderr,"%s: cannot allocate space for parm\n",fnm);
    return NOSPACE;
  }
/*** Copy the gauge parameters ***/
  *(g->parm) = *parm;
  
/*** Allocate space for data ***/
  g->g = malloc(sizeof(Group_el)*parm->lattice->nsite);
  if (!(g->g)) {
    fprintf(stderr,"%s: cannot allocate space\n",fnm);
    return NOSPACE;
  }

/*** Construct the filename and return ***/
  if (has_file) return construct_trans_filename(g);
  return 0;
}


/*** "Destructor" for Gauge_transform ***/
void trans_delete(Gauge_transform g)
{
/*** First free the data space ***/
  free(g.g);
  g.g = NULL;

/*** Then delete the gauge parameters ***/
  free(g.parm);
  g.parm = NULL;
}

/*** "Constructor" for Gauge ***/
int init_gauge(Gauge* u,Gauge_parameters* parm,Gauge_t type)
{
  const char fnm[MAXNAME] = "init_gauge";
  Lattice* lat = parm->lattice;
  int i;

  u->parm = parm;
  u->ndir = lat->ndim;  /* TODO: generalise! */
  u->type = type;

  if (type==FGAUGE || type==FALG) {
    fprintf(stderr,"%s: Warning: initialising nmom from defaults\n",fnm);
    u->npoint=1;
    for (i=0;i<lat->ndim;i++) {
      u->nmom[i] = lat->length[i]/4;
      if (parm->bcs[i]==PERIODIC) u->ntotmom[i]=2*u->nmom[i]+1;
      else u->ntotmom[i]=2*u->nmom[i];
      u->npoint *= u->ntotmom[i];
    }
  } else {
    /* Set dummy values for momenta */
    for (i=0;i<DIR;i++) {
      u->nmom[i] = -1;
      u->ntotmom[i] = 0;
    }
    u->npoint=lat->nsite;
  }

  switch (type) {
  case GAUGE:
    u->u = arralloc(sizeof(Group_el),2,u->npoint,u->ndir);
    break;
  case GAUGEMAT:
    u->u = arralloc(sizeof(Group_mat),2,u->npoint,u->ndir);
    break;
  case ALGEBRA:
    u->u = arralloc(sizeof(Algebra_el),2,u->npoint,u->ndir);
    break;
  case FGAUGE:
    u->u = arralloc(sizeof(Group_mat),2,u->npoint,u->ndir);
    break;
  case FALG:
    u->u = arralloc(sizeof(Algebra_c),2,u->npoint,u->ndir);
    break;
  default:
    fprintf(stderr,"%s: unknown gauge type %d\n",fnm,type);
    return BADPARAM;
  }

  if (!u->u) {
    fprintf(stderr,"%s: cannot allocate space\n",fnm);
    return NOSPACE;
  }

/*** Construct the filename and return ***/
  return (construct_gauge_filename(u));
}

void gauge_set_filename(Gauge* u, char* name)
{
  strcpy(u->filename,name);
}

/*** "Destructors" for gauge structures ***/
void gauge_delete(Gauge u)
{
  free(u.u);
  u.u = NULL;
}
