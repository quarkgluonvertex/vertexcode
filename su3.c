#include "su3.h"
#include <math.h>

/* $Id: su3.c,v 1.3 2006/09/06 13:15:33 jonivar Exp $ */

/* -----------------------------------------------------------*/
/* Routines to perform operations on Su3 matrices             */
/* Taken mainly from the qcd code.                            */
/* -----------------------------------------------------------*/

/* Matrix routine to perform matrix multiplication */
/* i.e., res = u1 * u2 */
void su3_mult(Su3 res, Su3 u1, Su3 u2)
{
#ifdef LOOPCOL
  int i ,j ,k ;
  su3_make_zero(res);
  for (i=0; i<COLOUR; ++i) {
    for(j=0; j<COLOUR; ++j ){
      for (k=0; k<COLOUR; ++k) {
	res[i][j].re += u1[i][k].re*u2[k][j].re - u1[i][k].im*u2[k][j].im;
	res[i][j].im += u1[i][k].re*u2[k][j].im + u1[i][k].im*u2[k][j].re;
      }
    }
  }
#else
  res[0][0].re
    = u1[0][0].re*u2[0][0].re - u1[0][0].im*u2[0][0].im
    + u1[0][1].re*u2[1][0].re - u1[0][1].im*u2[1][0].im
    + u1[0][2].re*u2[2][0].re - u1[0][2].im*u2[2][0].im;
  res[0][0].im
    = u1[0][0].re*u2[0][0].im + u1[0][0].im*u2[0][0].re
    + u1[0][1].re*u2[1][0].im + u1[0][1].im*u2[1][0].re
    + u1[0][2].re*u2[2][0].im + u1[0][2].im*u2[2][0].re;

  res[0][1].re
    = u1[0][0].re*u2[0][1].re - u1[0][0].im*u2[0][1].im
    + u1[0][1].re*u2[1][1].re - u1[0][1].im*u2[1][1].im
    + u1[0][2].re*u2[2][1].re - u1[0][2].im*u2[2][1].im;
  res[0][1].im
    = u1[0][0].re*u2[0][1].im + u1[0][0].im*u2[0][1].re
    + u1[0][1].re*u2[1][1].im + u1[0][1].im*u2[1][1].re
    + u1[0][2].re*u2[2][1].im + u1[0][2].im*u2[2][1].re;

  res[0][2].re
    = u1[0][0].re*u2[0][2].re - u1[0][0].im*u2[0][2].im
    + u1[0][1].re*u2[1][2].re - u1[0][1].im*u2[1][2].im
    + u1[0][2].re*u2[2][2].re - u1[0][2].im*u2[2][2].im;
  res[0][2].im
    = u1[0][0].re*u2[0][2].im + u1[0][0].im*u2[0][2].re
    + u1[0][1].re*u2[1][2].im + u1[0][1].im*u2[1][2].re
    + u1[0][2].re*u2[2][2].im + u1[0][2].im*u2[2][2].re;

  res[1][0].re
    = u1[1][0].re*u2[0][0].re - u1[1][0].im*u2[0][0].im
    + u1[1][1].re*u2[1][0].re - u1[1][1].im*u2[1][0].im
    + u1[1][2].re*u2[2][0].re - u1[1][2].im*u2[2][0].im;
  res[1][0].im
    = u1[1][0].re*u2[0][0].im + u1[1][0].im*u2[0][0].re
    + u1[1][1].re*u2[1][0].im + u1[1][1].im*u2[1][0].re
    + u1[1][2].re*u2[2][0].im + u1[1][2].im*u2[2][0].re;

  res[1][1].re
    = u1[1][0].re*u2[0][1].re - u1[1][0].im*u2[0][1].im
    + u1[1][1].re*u2[1][1].re - u1[1][1].im*u2[1][1].im
    + u1[1][2].re*u2[2][1].re - u1[1][2].im*u2[2][1].im;
  res[1][1].im
    = u1[1][0].re*u2[0][1].im + u1[1][0].im*u2[0][1].re
    + u1[1][1].re*u2[1][1].im + u1[1][1].im*u2[1][1].re
    + u1[1][2].re*u2[2][1].im + u1[1][2].im*u2[2][1].re;

  res[1][2].re
    = u1[1][0].re*u2[0][2].re - u1[1][0].im*u2[0][2].im
    + u1[1][1].re*u2[1][2].re - u1[1][1].im*u2[1][2].im
    + u1[1][2].re*u2[2][2].re - u1[1][2].im*u2[2][2].im;
  res[1][2].im
    = u1[1][0].re*u2[0][2].im + u1[1][0].im*u2[0][2].re
    + u1[1][1].re*u2[1][2].im + u1[1][1].im*u2[1][2].re
    + u1[1][2].re*u2[2][2].im + u1[1][2].im*u2[2][2].re;

  res[2][0].re
    = u1[2][0].re*u2[0][0].re - u1[2][0].im*u2[0][0].im
    + u1[2][1].re*u2[1][0].re - u1[2][1].im*u2[1][0].im
    + u1[2][2].re*u2[2][0].re - u1[2][2].im*u2[2][0].im;
  res[2][0].im
    = u1[2][0].re*u2[0][0].im + u1[2][0].im*u2[0][0].re
    + u1[2][1].re*u2[1][0].im + u1[2][1].im*u2[1][0].re
    + u1[2][2].re*u2[2][0].im + u1[2][2].im*u2[2][0].re;

  res[2][1].re
    = u1[2][0].re*u2[0][1].re - u1[2][0].im*u2[0][1].im
    + u1[2][1].re*u2[1][1].re - u1[2][1].im*u2[1][1].im
    + u1[2][2].re*u2[2][1].re - u1[2][2].im*u2[2][1].im;
  res[2][1].im
    = u1[2][0].re*u2[0][1].im + u1[2][0].im*u2[0][1].re
    + u1[2][1].re*u2[1][1].im + u1[2][1].im*u2[1][1].re
    + u1[2][2].re*u2[2][1].im + u1[2][2].im*u2[2][1].re;

  res[2][2].re
    = u1[2][0].re*u2[0][2].re - u1[2][0].im*u2[0][2].im
    + u1[2][1].re*u2[1][2].re - u1[2][1].im*u2[1][2].im
    + u1[2][2].re*u2[2][2].re - u1[2][2].im*u2[2][2].im;
  res[2][2].im
    = u1[2][0].re*u2[0][2].im + u1[2][0].im*u2[0][2].re
    + u1[2][1].re*u2[1][2].im + u1[2][1].im*u2[1][2].re
    + u1[2][2].re*u2[2][2].im + u1[2][2].im*u2[2][2].re;
#endif
}

/* multiply a matrix by the conjugate of another */
void su3_mult_dag(Su3 res, Su3 u1, Su3 u2)
{
  int i ,j ,k ;

  su3_make_zero(res);

  for (i=0; i<COLOUR; ++i) {
    for (j=0; j<COLOUR; ++j) {
      for (k=0; k<COLOUR; ++k) {
	res[i][j].re += u1[i][k].re*u2[j][k].re + u1[i][k].im*u2[j][k].im;
	res[i][j].im += u1[i][k].im*u2[j][k].re - u1[i][k].re*u2[j][k].im;
      }
    }
  }
}

/* multiply the conjugate of a matrix by another matrix */
void su3_dag_mult(Su3 res, Su3 u1, Su3 u2)
{
  int i ,j ,k ;

  su3_make_zero(res);
  for (i=0; i<COLOUR; ++i) {
    for (j=0; j<COLOUR; ++j) {
      for (k=0; k<COLOUR; ++k) {
	res[i][j].re += u1[k][i].re*u2[k][j].re + u1[k][i].im*u2[k][j].im;
	res[i][j].im += u1[k][i].re*u2[k][j].im - u1[k][i].im*u2[k][j].re;
      }
    }
  }
}

/* multiply the conjugates of two matrices by each other */
void su3_dag_mult_dag(Su3 res, Su3 u1, Su3 u2)
{
  int i ,j ,k ;

  su3_make_zero(res);
  for (i=0; i<COLOUR; ++i) {
    for (j=0; j<COLOUR; ++j) {
      for (k=0; k<COLOUR; ++k) {
	res[i][j].re += u1[k][i].re*u2[j][k].re - u1[k][i].im*u2[j][k].im;
	res[i][j].im += -u1[k][i].re*u2[j][k].im - u1[k][i].im*u2[j][k].re;
      }
    }
  }
}

/* Raise a complex matrix to the power n and store in res */
void su3_power(Su3 res, Su3 u, int n)
{
  Su3 work;

  if (n==0) {
    su3_make_unit(res);
    return;
  }

/* initialise matrices */
  su3_copy(res, u);
  su3_copy(work, u);
        
  for (; n>1; n--) {
/* update res */
    su3_mult(res, work, u);

/* update workspace */
    su3_copy(work, res);
  }

}

/* Add two complex matrices together res = u1 + u2 */
void su3_add(Su3 res, Su3 u1, Su3 u2)
{
  int i, j;

  for (i=0; i<COLOUR; i++) {
    for (j=0; j<COLOUR; j++) {
      res[i][j].re = u1[i][j].re + u2[i][j].re;
      res[i][j].im = u1[i][j].im + u2[i][j].im;
    }
  }
}

void su3_addto(Su3 res, Su3 u)
{
  res[0][0].re += u[0][0].re;  res[0][0].im += u[0][0].im;
  res[0][1].re += u[0][1].re;  res[0][1].im += u[0][1].im;
  res[0][2].re += u[0][2].re;  res[0][2].im += u[0][2].im;

  res[1][0].re += u[1][0].re;  res[1][0].im += u[1][0].im;
  res[1][1].re += u[1][1].re;  res[1][1].im += u[1][1].im;
  res[1][2].re += u[1][2].re;  res[1][2].im += u[1][2].im;

  res[2][0].re += u[2][0].re;  res[2][0].im += u[2][0].im;
  res[2][1].re += u[2][1].re;  res[2][1].im += u[2][1].im;
  res[2][2].re += u[2][2].re;  res[2][2].im += u[2][2].im;
}


/* Matrix subtraction :  res = u1 - u2   */
void su3_sub(Su3 res, Su3 u1, Su3 u2)
{
  int i, j;

  for (i=0; i<COLOUR; i++) {
    for (j=0; j<COLOUR; j++) {
      res[i][j].re = u1[i][j].re - u2[i][j].re;
      res[i][j].im = u1[i][j].im - u2[i][j].im;
    }
  }
}

void su3_subto(Su3 res, Su3 u)
{
  res[0][0].re -= u[0][0].re;  res[0][0].im -= u[0][0].im;
  res[0][1].re -= u[0][1].re;  res[0][1].im -= u[0][1].im;
  res[0][2].re -= u[0][2].re;  res[0][2].im -= u[0][2].im;

  res[1][0].re -= u[1][0].re;  res[1][0].im -= u[1][0].im;
  res[1][1].re -= u[1][1].re;  res[1][1].im -= u[1][1].im;
  res[1][2].re -= u[1][2].re;  res[1][2].im -= u[1][2].im;

  res[2][0].re -= u[2][0].re;  res[2][0].im -= u[2][0].im;
  res[2][1].re -= u[2][1].re;  res[2][1].im -= u[2][1].im;
  res[2][2].re -= u[2][2].re;  res[2][2].im -= u[2][2].im;
}


/* Multiply a matrix by a complex number : res = z * matrix */
void su3_by_complex(Su3 res, Su3 u, Complex z)
{
  int i, j;
  Real temp;

  for (i=0; i<COLOUR; i++) {
    for (j=0; j<COLOUR; j++) {
      temp = (z.re * u[i][j].re) -
	(z.im * u[i][j].im);
      res[i][j].im = (z.re * u[i][j].im) +
	(z.im * u[i][j].re);
      res[i][j].re = temp;
    }
  }
}

/* Multiply a matrix by a real number : res = r * u */
void su3_by_real(Su3 res, Su3 u, Real r)
{
  int i, j;

  for (i=0; i<COLOUR; i++) {
    for (j=0; j<COLOUR; j++) {
      res[i][j].re = r*u[i][j].re;
      res[i][j].im = r*u[i][j].im;
    }
  }
}

void su3_scale(Su3 u, Real r)
{
  u[0][0].re *= r; u[0][0].im *= r;
  u[0][1].re *= r; u[0][1].im *= r;
  u[0][2].re *= r; u[0][2].im *= r;

  u[1][0].re *= r; u[1][0].im *= r;
  u[1][1].re *= r; u[1][1].im *= r;
  u[1][2].re *= r; u[1][2].im *= r;

  u[2][0].re *= r; u[2][0].im *= r;
  u[2][1].re *= r; u[2][1].im *= r;
  u[2][2].re *= r; u[2][2].im *= r;

}

/* Set all elements of a complex matrix to zero        */
void su3_make_zero(Su3 u)
{
  u[0][0].re = u[0][1].re = u[0][2].re = 0.0;
  u[0][0].im = u[0][1].im = u[0][2].im = 0.0;

  u[1][0].re = u[1][1].re = u[1][2].re = 0.0;
  u[1][0].im = u[1][1].im = u[1][2].im = 0.0;

  u[2][0].re = u[2][1].re = u[2][2].re = 0.0;
  u[2][0].im = u[2][1].im = u[2][2].im = 0.0;

}


/* Set the complex matrix equal to the unit matrix     */
void su3_make_unit(Su3 u)
{
  u[0][0].re = u[1][1].re = u[2][2].re = 1.0;
  u[0][0].im = u[1][1].im = u[2][2].im = 0.0;

  u[0][1].re = u[0][1].im = u[0][2].re = u[0][2].im = 0.0;
  u[1][0].re = u[1][0].im = u[1][2].re = u[1][2].im = 0.0;
  u[2][0].re = u[2][0].im = u[2][1].re = u[2][1].im = 0.0;
}


/* Function to find the adjoint of a matrix  */
/*                   +                       */
/*            res = u                        */
void su3_dagger(Su3 res, Su3 u)
{
  res[0][0].re = u[0][0].re; res[0][0].im = -u[0][0].im;
  res[0][1].re = u[1][0].re; res[0][1].im = -u[1][0].im;
  res[0][2].re = u[2][0].re; res[0][2].im = -u[2][0].im;

  res[1][0].re = u[0][1].re; res[1][0].im = -u[0][1].im;
  res[1][1].re = u[1][1].re; res[1][1].im = -u[1][1].im;
  res[1][2].re = u[2][1].re; res[1][2].im = -u[2][1].im;

  res[2][0].re = u[0][2].re; res[2][0].im = -u[0][2].im;
  res[2][1].re = u[1][2].re; res[2][1].im = -u[1][2].im;
  res[2][2].re = u[2][2].re; res[2][2].im = -u[2][2].im;
}

/* function to take transpose of a matrix */
void su3_transpose(Su3 res, Su3 u)
{
  res[0][0] = u[0][0]; res[0][1] = u[1][0]; res[0][2] = u[2][0];
  res[1][0] = u[0][1]; res[1][1] = u[1][1]; res[1][2] = u[2][1];
  res[2][0] = u[0][2]; res[2][1] = u[1][2]; res[2][2] = u[2][2];
}

/* function to extract the traceless part of a matrix */
void su3_make_traceless(Su3 u)
{
  Real rtr = (u[0][0].re+u[1][1].re+u[2][2].re)/COLOUR;
  Real itr = (u[0][0].im+u[1][1].im+u[2][2].im)/COLOUR;

  u[0][0].re -= rtr; u[0][0].im -= itr;
  u[1][1].re -= rtr; u[1][1].im -= itr;
  u[2][2].re -= rtr; u[2][2].im -= itr;
}

/* function to take the hermitean part of a matrix */
void su3_hermitean(Su3 res, Su3 mat)
{
  res[0][0].re = mat[0][0].re;
  res[1][1].re = mat[1][1].re;
  res[2][2].re = mat[2][2].re;
  res[0][0].im = res[1][1].im = res[2][2].im = 0.0;
  res[0][1].re = 0.5*(mat[0][1].re+mat[1][0].re);
  res[0][1].im = 0.5*(mat[0][1].im-mat[1][0].im);
  res[0][2].re = 0.5*(mat[0][2].re+mat[2][0].re);
  res[0][2].im = 0.5*(mat[0][2].im-mat[2][0].im);
  res[1][2].re = 0.5*(mat[1][2].re+mat[2][1].re);
  res[1][2].im = 0.5*(mat[1][2].im-mat[2][1].im);
  res[1][0].re = res[0][1].re; res[1][0].im = -res[0][1].im;
  res[2][0].re = res[0][2].re; res[2][0].im = -res[0][2].im;
  res[2][1].re = res[1][2].re; res[2][1].im = -res[1][2].im;
}
  

/* function to take the antihermitean part of a matrix */
void su3_antihermitean(Su3 res, Su3 mat)
{
  res[0][0].re = res[1][1].re = res[2][2].re = 0.0;
  res[0][0].im = mat[0][0].im;
  res[1][1].im = mat[1][1].im;
  res[2][2].im = mat[2][2].im;
  res[0][1].re = 0.5*(mat[0][1].re-mat[1][0].re);
  res[0][1].im = 0.5*(mat[0][1].im+mat[1][0].im);
  res[0][2].re = 0.5*(mat[0][2].re-mat[2][0].re);
  res[0][2].im = 0.5*(mat[0][2].im+mat[2][0].im);
  res[1][2].re = 0.5*(mat[1][2].re-mat[2][1].re);
  res[1][2].im = 0.5*(mat[1][2].im+mat[2][1].im);
  res[1][0].re = -res[0][1].re; res[1][0].im = res[0][1].im;
  res[2][0].re = -res[0][2].re; res[2][0].im = res[0][2].im;
  res[2][1].re = -res[1][2].re; res[2][1].im = res[1][2].im;
}

/* compute the "norm square" of M,  M^dag M */
Real su3_norm(Su3 mat)
{
  return (mat[0][0].re*mat[0][0].re + mat[0][0].im*mat[0][0].im
	  + mat[0][1].re*mat[0][1].re + mat[0][1].im*mat[0][1].im
	  + mat[0][2].re*mat[0][2].re + mat[0][2].im*mat[0][2].im
	  + mat[1][0].re*mat[1][0].re + mat[1][0].im*mat[1][0].im
	  + mat[1][1].re*mat[1][1].re + mat[1][1].im*mat[1][1].im
	  + mat[1][2].re*mat[1][2].re + mat[1][2].im*mat[1][2].im
	  + mat[2][0].re*mat[2][0].re + mat[2][0].im*mat[2][0].im
	  + mat[2][1].re*mat[2][1].re + mat[2][1].im*mat[2][1].im
	  + mat[2][2].re*mat[2][2].re + mat[2][2].im*mat[2][2].im);
}

/* project a 3x3 complex matrix onto SU(3) */
void su3_reunitarise(Su3 mat)
{
  Real r, c;

  /* normalise the first column */
  r = mat[0][0].re*mat[0][0].re + mat[0][0].im*mat[0][0].im
    + mat[1][0].re*mat[1][0].re + mat[1][0].im*mat[1][0].im
    + mat[2][0].re*mat[2][0].re + mat[2][0].im*mat[2][0].im;
  r = 1.0/sqrt(r);
  mat[0][0].re*=r; mat[0][0].im*=r;
  mat[1][0].re*=r; mat[1][0].im*=r;
  mat[2][0].re*=r; mat[2][0].im*=r;

  /* orthogonalise the second column wrt the first */
  r = mat[0][0].re*mat[0][1].re + mat[0][0].im*mat[0][1].im
    + mat[1][0].re*mat[1][1].re + mat[1][0].im*mat[1][1].im
    + mat[2][0].re*mat[2][1].re + mat[2][0].im*mat[2][1].im;
  c = mat[0][0].re*mat[0][1].im - mat[0][0].im*mat[0][1].re
    + mat[1][0].re*mat[1][1].im - mat[1][0].im*mat[1][1].re
    + mat[2][0].re*mat[2][1].im - mat[2][0].im*mat[2][1].re;
  mat[0][1].re -= r*mat[0][0].re - c*mat[0][0].im;
  mat[0][1].im -= r*mat[0][0].im + c*mat[0][0].re;
  mat[1][1].re -= r*mat[1][0].re - c*mat[1][0].im;
  mat[1][1].im -= r*mat[1][0].im + c*mat[1][0].re;
  mat[2][1].re -= r*mat[2][0].re - c*mat[2][0].im;
  mat[2][1].im -= r*mat[2][0].im + c*mat[2][0].re;

  /* normalise the second column */
  r = mat[0][1].re*mat[0][1].re + mat[0][1].im*mat[0][1].im
    + mat[1][1].re*mat[1][1].re + mat[1][1].im*mat[1][1].im
    + mat[2][1].re*mat[2][1].re + mat[2][1].im*mat[2][1].im;
  r = 1.0/sqrt(r);
  mat[0][1].re*=r; mat[0][1].im*=r;
  mat[1][1].re*=r; mat[1][1].im*=r;
  mat[2][1].re*=r; mat[2][1].im*=r;

  /* reconstruct the third columns from the first two */
  reconstruct_su3(mat);
}

/*** Reconstruct an SU(3) matrix from the first two columns ***/
void reconstruct_su3(Su3 mat)
{
  int i, j, k;

  for (i=0;i<COLOUR;i++) {
    j = (i+1)%COLOUR;
    k = (i+2)%COLOUR;

    mat[i][2].re = mat[j][0].re*mat[k][1].re - mat[j][0].im*mat[k][1].im
      - mat[k][0].re*mat[j][1].re + mat[k][0].im*mat[j][1].im;

    mat[i][2].im = - mat[j][0].re*mat[k][1].im - mat[j][0].im*mat[k][1].re
      + mat[k][0].re*mat[j][1].im + mat[k][0].im*mat[j][1].re;
  }
}
