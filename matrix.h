#ifndef _MATRIX_H_
#define _MATRIX_H_
#include "Complex.h"

static inline void matrix_copy(Complex** res, Complex** m, int sz) {
  int i,j;
  for (i=0;i<sz;i++) for (j=0;j<sz;j++) res[i][j]=m[i][j];
}

static inline void matrix_zero(Complex** m, int sz) {
  int i,j;
  for (i=0;i<sz;i++) for (j=0;j<sz;j++) m[i][j].re=m[i][j].im=0.0;
}

void matrix_mult(Complex**,Complex**,Complex**,int);
void matrix_dag_mult(Complex**,Complex**,Complex**,int);
void matrix_mult_dag(Complex**,Complex**,Complex**,int);
void matrix_dag_mult_dag(Complex**,Complex**,Complex**,int);
void matrix_dagger(Complex**,Complex**,int);
Complex matrix_trace(Complex**,int);
void matrix_scale(Complex**,Real,int);
void matrix_add_real(Complex**,Real,int);

void matrix_inverse(Complex**,Complex**,int);

#endif
