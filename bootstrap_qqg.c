/************************************************************
 *                                                          *
 *    Program to compute and plot the quark-gluon vertex,   *
 *    as a function of the time component of the outgoing   *
 *    quark leg (with all other parameters kept constant).  *
 *                                                          *
 *    Bootstrap samples of the vertex data are written      *
 *    to files, to allow for subsequent analysis.           *
 *                                                          *
 ************************************************************/

/* $Id: bootstrap_qqg.c,v 1.11 2014/09/16 14:43:59 jonivar Exp $ */

#include "gauge.h"
#include "fermion.h"
#include "vertex.h"
#include "mom_fields.h"
#include "analysis.h"
#include "matrixfunc.h"
#include "matrix.h"
#include "rand.h"

#define MAXGAMMA 16
#define MAXCODELEN 16
#define FOURPISQR 39.478418
#define ALLMU -1

Matrixfunction vertexfunc_full;
Matrixfunction vertexfunc_gluon;
Matrixfunction vertexfunc_quark;
Matrixfunction vertexfunc_proj_amputate;
Matrixfunction vertexfunc_proj_amp_allmu;
Matrixfunction vertexfunc_q0_amp_allmu;
Matrixfunction vertexfunc_amputate;
Matrixfunction vertexfunc_amp_pequal;

static int init_matrixdata(MatrixData*,int,int,int,int,int); 
static int init_mfitdata(BootMatrixFuncs*,Vertex_params*,int,Matrixfunction*);
static int create_vertex_fileroot(char*, Vertex_params*);

const int msz = SPIN*COLOUR;
static int Lattice_vol = 0;
const int Nquark = 2;  /* the number of quark propagator arguments */

int main(int argc, char** argv)
{
  Propagator quarkp;
  Propagator quarkk;
  Algebra_c gluon[DIR];
  Real* gprop;
  MatrixData data;
  Matrixfunction* vfunc;
  BootMatrixFuncs fit;
  Vertex_params run;
  Lattice* lat;
  Complex*** threept;
  int* config;
  int nallconfig;
  Real pspatial;
  int k[DIR], nmom_vals[DIR], pmin, pmax, pt, ndata;
  int ngprop, nreal, nvertex, i, j, mu, cfg, myrank;
  int q_is_zero;
  int status;

  char filename[MAXLINE];
  char datastr[MAXLINE];

#ifdef MPI
  MPI_Init(&argc, &argv);
#endif

  /***  Read the name of parameter file from the command line ***/
  if (argc < 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename> [configlist_filename]\n", argv[0]);
    return BADPARAM;
  }

#ifdef MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  sprintf(filename, "%s.%d", argv[1], myrank);
#else
  myrank = 0;
  sprintf(filename, "%s",argv[1]);
#endif

  if (assign_params_vertex(filename,&run)) return BADPARAM;
  if (run.gauge->gauge!=LANDAU) {
    printf("Wrong gauge -- only landau gauge implemented!\n");
    return BADPARAM;
  }

  if (run.skip<1) { /* code for manually assigned config numbers */
    if (argc>2)
      status=setup_configs(&config,&nallconfig,argv[2]);
    else
      status=setup_configs(&config,&nallconfig,"config.list");
    if (status==BADPARAM) {
      printf("Warning: using config numbers 0,1,2,...\n");
      nallconfig = run.nconfig+run.start_cfg;
      config = vecalloc(sizeof(int),nallconfig);
      for (i=0;i<nallconfig;i++) config[i]=i;
    } else if (status) return status;
    if (nallconfig != run.nconfig) {
      printf("Warning: nconfig in list differs from param file %d != %d\n",
	     nallconfig,run.nconfig);
    }
  } else {
    nallconfig = run.nconfig;
    config = vecalloc(sizeof(int),nallconfig);
    for (i=0;i<nallconfig;i++) config[i]=run.start_cfg+run.skip*i;
  }
  if ( (status=create_vertex_fileroot(datastr,&run)) ) return status;

  /*** set up momentum variables ***/
  lat = run.gauge->lattice;
  Lattice_vol = lat->nsite;
  q_is_zero = (run.q[IX]==0 && run.q[IY]==0 && run.q[IZ]==0 && run.q[IT]==0);
  for (i=0; i<DIR; i++) {
    nmom_vals[i] = 2*run.nmom[i]+1;
    k[i] = run.p[i]+run.q[i];
  }
  pspatial = 0.0;
  for (i=0; i<SPATIAL_DIR; i++) {
    pspatial += FOURPISQR*run.p[i]*run.p[i]/(lat->length[i]*lat->length[i]);
  }

  /*** Determine the allowed momentum values:     ***
   *** both p and k=p+q must be in [-nmomt,nmomt] ***/
  if (run.q[IT]>0) {
    pmin = 0;
    pmax = 2*run.nmom[IT]-run.q[IT];
  } else {
    pmin = -run.q[IT];
    pmax = 2*run.nmom[IT];
  }
  ndata = pmax-pmin;

  /*** Determine the datafunction to use ***/
  if (run.mu==ALLMU) {
    vfunc = (q_is_zero) ? vertexfunc_q0_amp_allmu : vertexfunc_proj_amp_allmu;
  } else if (run.kinematics==VTX_ZEROMOM) {
    vfunc = vertexfunc_amp_pequal;
  } else if (run.gauge->gauge==LANDAU) {
    vfunc = vertexfunc_proj_amputate;
  } else {
    vfunc = vertexfunc_amputate;
  }

  /** the number of gluon prop components depends on gauge and kinematics **/
  if (q_is_zero) ngprop=DIR;
  else if (run.gauge->gauge==LANDAU) ngprop=1; else ngprop=2;
  if (run.mu==ALLMU && run.gauge->gauge==LANDAU) {
    nreal = (q_is_zero) ? DIR : 1;
    nvertex=DIR*NALG;
  } else if (q_is_zero || run.gauge->gauge==LANDAU) {
    nreal=1; nvertex=NALG;
  } else {
    nreal=2; nvertex=DIR*NALG;
  }
  gprop = vecalloc(sizeof(Real),ngprop);

  if (status=init_fpropslice(&quarkp,run.prop,run.nmom,run.p)) return status;
  if (status=init_fpropslice(&quarkk,run.prop,run.nmom,k)) return status;

  /*** Set up the structures containing data to be analysed ***/
#if 0 // the following is required by matrixfunc_quark
  status=init_matrixdata(&data,ndata,run.nconfig,nreal,nvertex+2,4);
#else
  status=init_matrixdata(&data,ndata,run.nconfig,nreal,nvertex+2,0);
#endif
  if (status) return status;
  threept = arralloc(sizeof(Complex),3,nvertex,msz,msz);
  
#if 0 
  /*** Assign momentum values as constant real data,  ***
   *** used to compute kslash in matrixfunc_quark     ***/
  for (pt=0;pt<ndata;pt++) {
    for (i=0;i<SPATIAL_DIR;i++) {
      data.rconst[i][pt] = sin(2.0*PI*run.p[i]/lat->length[i]);
    }
    data.rconst[IT][pt] = sin(2.0*PI*(pt+pmin-run.nmom[IT]+0.5)/lat->length[i]);
  }
#endif

  /*** Loop over configurations and assign config data ***/
  for (cfg=0;cfg<run.nconfig;cfg++) {
    Prop* sp = quarkp.prop;
    Prop* sk = quarkk.prop;
    run.gauge->sweep = config[cfg];
    construct_prop_filename(&quarkp);
    construct_prop_filename(&quarkk);
    if (status=read_prop(quarkp)) return status;
    if (status=read_prop(quarkk)) return status;
    if (status=pick_gluon(gluon,run.gauge,run.nmom,run.q,FALG)) return status;
    if (q_is_zero || run.gauge->gauge==LANDAU)
      compute_one_gluon_prop_diag(gprop,gluon,ngprop);
    else compute_one_gluon_prop_tl(gprop,gluon,run.nmom,run.q);
    printf("."); fflush(stdout);
    for (pt=0;pt<ndata;pt++) {
      int kt=pt+pmin+run.q[IT];
      if (q_is_zero && run.mu!=ALLMU) data.realdata[0][cfg][pt] = gprop[run.mu];
      else if (ngprop==1) data.realdata[0][cfg][pt] = gprop[0]*DIR/(DIR-1);
      else for (i=0;i<ngprop;i++) data.realdata[i][cfg][pt] = gprop[i];

      if (run.mu==ALLMU) {
	for (j=Nquark,mu=0;mu<DIR;mu++) {
	  compute_3pt(threept,sp[pt+pmin],gluon[mu],&run);
	  for (i=0;i<NALG;i++,j++) {
	    matrix_copy(data.mdata[j][cfg][pt],threept[i],msz); 
	  }
	}
      } else {
      /*** mdata[2...NALG+1] = V_mu^a(p,q); a=1,...,NALG ***/
	compute_3pt(threept,sp[pt+pmin],gluon[run.mu],&run);
	for (i=0;i<NALG;i++) {
	  matrix_copy(data.mdata[i+Nquark][cfg][pt],threept[i],msz); 
	}
      }

      /*** mdata[0,1] = S(k), S(p) including improvement ***/
      prop_to_matrix(data.mdata[0][cfg][pt],sk[pt+pmin+run.q[IT]]);
      prop_to_matrix(data.mdata[1][cfg][pt],sp[pt+pmin]);

      /* For "additive" S_I improvement as in the 2002-2007 papers    *
       * we need to add the improvement term to the quark propagator. *
       * With rotated quarks the propagators are already improved and *
       * we only have a multiplicative constant which can be handled  *
       * at the analysis stage                                        */
      if (run.prop->action==CLOVER && run.prop->ncoeff>2) {
	Real z=1.0, l=0.0;
	z += run.prop->imp[2]*run.prop->mass;
	if (run.prop->ncoeff>3) l = run.prop->imp[3];
	matrix_scale(data.mdata[0][cfg][pt],z,msz);
	matrix_add_real(data.mdata[0][cfg][pt],-l,msz);
	matrix_scale(data.mdata[1][cfg][pt],z,msz);
	matrix_add_real(data.mdata[1][cfg][pt],-l,msz);
      }
    }
  }
  prop_delete(&quarkp);
  prop_delete(&quarkk);
  free(threept);

  /*** Compute and write bootstrap samples of the vertex function ***/
  status=0;
  init_mfitdata(&fit,&run,ndata,vfunc);
  if (run.nboot) {
    sprintf(filename,"%s/vertex_%s_U%d_%dcfg_%dbt_",
	    run.boot_dir,datastr,run.start_cfg,run.nconfig,run.nboot);
    rand_initialise(run.seed);
    status = bootstrap_matrix(&data,&fit,filename);
  }

  if(run.plot) {
    Real* xdata = vecalloc(sizeof(Real),ndata);
    for (pt=0;pt<ndata;pt++) {
      Real ptval = 2.0*PI*((Real)(pt+pmin-run.nmom[IT]+0.5))/lat->length[IT];
      xdata[pt] = sqrt(pspatial+ptval*ptval);
    }
    sprintf(filename,"%s/vertex_%s_%dcfg_",run.graph_dir,datastr,run.nconfig);
    matrix_graph(&data,xdata,vfunc,filename,0);
  }

  if (status) {
    printf("Returning with error code %d\n",status);
    return status;
  }
  return 0;
}

/************************************************************************
 * compute_3pt: computes the 3-point function V^a(p,q)                  *
 *  from the quark propagator S(p) and the algebra-valued boson A^a(q)  *
 *  V^a(p,q) [a=0...NALG-1] is returned as a NxN matrix, N=Nspin x Ncol *
 *  additive and/or multiplicative improvement of the quark propagator  *
 *  is included as specified by the parameters                          *
 ************************************************************************/
void compute_3pt(Complex*** threept, Prop quark, Su3alg_c boson,
		 Vertex_params* run)
{
  int is, js, ic, jc, i, j, a;
  double z=1.0, l=0.0;
  int add=FALSE;

  /*** set up add and mult improvement terms for clover fermions ***/
  if (run->prop->action==CLOVER && run->prop->ncoeff>2) {
    add = (run->prop->ncoeff>3);
    z += run->prop->imp[2]*run->prop->mass;
    if (add) l = run->prop->imp[3];
  }
  for (i=is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++,i++) {
    for (j=js=0;js<SPIN;js++) for (jc=0;jc<COLOUR;jc++,j++) {
      for (a=0;a<NALG;a++) {
	if (add && i==j) {
	  threept[a][i][j].re = 
	    z*((quark[is][js][ic][jc].re-l)*boson[a].re
	       - quark[is][js][ic][jc].im*boson[a].im);
	  threept[a][i][j].im = 
	    z*((quark[is][js][ic][jc].re-l)*boson[a].im
	       + quark[is][js][ic][jc].im*boson[a].re);
	} else {
	  threept[a][i][j].re = 
	    z*((quark[is][js][ic][jc].re)*boson[a].re
	       - quark[is][js][ic][jc].im*boson[a].im);
	  threept[a][i][j].im =
	    z*((quark[is][js][ic][jc].re)*boson[a].im
	       + quark[is][js][ic][jc].im*boson[a].re);
	}
      }
    }
  }
}



/************************************************************************
 ***                                                                  ***
 ***           Matrix datafunctions begin here                        ***
 ***                                                                  ***
 ************************************************************************/

static Complex** tmp1    = 0;
static Complex** tmp2    = 0;
static Complex** invtmp1 = 0;
static Complex** invtmp2 = 0;
static int tmp_is_initialised = FALSE;

static void free_tmp_matrices()
{
  free(tmp1); free(tmp2);
  free(invtmp1); free(invtmp2);
  tmp1 = tmp2 = invtmp1 = invtmp2 = 0;
}  

static void init_tmp_matrices(int size)
{
  if (tmp_is_initialised) {
    fprintf(stderr,"Warning: re-initialising temporary matrix space\n");
    free_tmp_matrices();
  }
  tmp1 = arralloc(sizeof(Complex),2,size,size);
  tmp2 = arralloc(sizeof(Complex),2,size,size);  
  invtmp1 = arralloc(sizeof(Complex),2,size,size);
  invtmp2 = arralloc(sizeof(Complex),2,size,size);
}

/*** vertexfunc_quark computes traces of the quark propagator ***
 *** or its inverse - can be useful for diagnostics purposes  ***/
int vertexfunc_quark(Real* y, Complex*** matrix, Real* realarg, 
		     int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_quark";
  Prop tmp;
  Dirac s1, s2, pslash;
  Real tr = 0.0;
  Real psqr;
  int i;

  if (!y && !msize) return 2;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix<Nquark) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
  if (nreal<1+DIR) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }

  matrix_inverse(invtmp1,matrix[0],msize);

// this computes tr[kslash S(p)]/ksqr, tr[S(p)]
// or the equivalent traces of S^-1(p)
  psqr = realarg[1]*realarg[1] + realarg[2]*realarg[2]
    + realarg[3]*realarg[3] + realarg[4]*realarg[4];
#if 0
  matrix_to_prop(tmp,matrix[NALG]);
#else
  matrix_to_prop(tmp,invtmp1);
#endif
  colour_trace_prop(s1,tmp);
  aslash(pslash,&realarg[1]);
  dirac_mult(s2,pslash,s1);
  y[0] = dirac_trace(s2).im/(psqr*msize);
  y[1] = dirac_trace(s1).re/msize;

  return 0;
}

/*** vertexfunc_gluon computes the gluon propagator ***/
int vertexfunc_gluon(Real* y, Complex*** matrix, Real* realarg, 
			    int msize, int nmatrix, int nreal)
{
  if (!y && !msize) return 1;
  y[0] = realarg[0];
  return 0;
}

/*** vertexfunc_full: the unamputated vertex ***/
int vertexfunc_full(Real* y, Complex*** matrix, Real* realarg, 
			  int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_full";
#define Ngamma 15
  const int G[Ngamma] = {IDENTITY,GAMMA_X,GAMMA_Y,GAMMA_Z,GAMMA_T,
			 SIGMA_XY,SIGMA_XZ,SIGMA_XT,SIGMA_YZ,SIGMA_YT,SIGMA_ZT,
			 GAMMA_5X, GAMMA_5Y, GAMMA_5Z, GAMMA_5T};
  const int reim[Ngamma] = {0,1,1,1,1,0,0,0,0,0,0,1,1,1,1};
  Dirac lietmp[NALG];
  Dirac dtmp;
  Complex ctmp;
  int i, g;

  if (!y && !msize) return Ngamma;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix<NALG+2) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
#if 0
  if (nreal<1) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }
#endif
  for (g=0;g<Ngamma;g++) y[g]=0.0;

  for (i=0;i<NALG;i++) {
    qmatrix_to_algdirac(lietmp,matrix[i+Nquark]);
    for (g=0;g<Ngamma;g++) {
      gamma_by_dirac(dtmp,lietmp[i],G[g]);
      ctmp = dirac_trace(dtmp);
      y[g] += reim[g] ? -ctmp.re : ctmp.im;
    }
  }

  for (g=0;g<Ngamma;g++) y[g] /= NALG*SPIN;
  return 0;
#undef Ngamma
}

/*** vertexfunc_amp_pequal: the amputated vertex with p=k,           ***
 ***  ie the two quark legs are the same                             ***
 ***  only the traces with the identity and gamma_mu are computed    ***
 ***  since the form factors involving other traces are zero here    ***/
int vertexfunc_amp_pequal(Real* y, Complex*** matrix, Real* realarg, 
			  int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_amp_pequal";
#define Ngamma 5
  const int G[Ngamma] = {IDENTITY,GAMMA_X,GAMMA_Y,GAMMA_Z,GAMMA_T};
  const int reim[Ngamma] = {0,1,1,1,1};
  Dirac lietmp[NALG];
  Dirac dtmp;
  Complex ctmp;
  int i, g;

  if (!y && !msize) return Ngamma;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix!=NALG+2) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
  if (nreal<1) {
    fprintf(stderr,"%s: Too few real arguments %d\n",fnm,nreal);
  }
#if 0 // allow additional dummy real arguments to be passed
  if (nreal!=1) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }
#endif
  for (g=0;g<Ngamma;g++) y[g]=0.0;

  /* Both quark legs are equal, so we amputate with the same *
   * inverse propagator on both sides                        */
  matrix_inverse(invtmp1,matrix[0],msize);
  for (i=0;i<NALG;i++) {
    matrix_mult(tmp1,invtmp1,matrix[i+Nquark],msize);
    matrix_mult(tmp2,tmp1,invtmp1,msize);
    /* trace out colour indices: (Lambda_mu^a)_ij -> -i(t^a)_ij Gamma_mu */
    /* specifically: lietmp[b] = -i tr_c(lambda_b Lambda_mu^a) */
    qmatrix_to_algdirac(lietmp,tmp2);
    /* trace with all dirac matrices with appropriate re/im part */
    for (g=0;g<Ngamma;g++) {
      gamma_by_dirac(dtmp,lietmp[i],G[g]);
      ctmp = dirac_trace(dtmp);
      y[g] += reim[g] ? -ctmp.re : ctmp.im;
    }
  }

  for (g=0;g<Ngamma;g++)
    y[g] *= sqrt((Real)Lattice_vol)/(realarg[0]*NALG*SPIN);

  return 0;
#undef Ngamma
}


/*** vertexfunc_proj_amputate: the transverse-projected amputated    ***
 ***   vertex for general p, q, k=p+q                                ***
 ***  the traces with all gamma matrices except gamma5 are computed  ***/
int vertexfunc_proj_amputate(Real* y, Complex*** matrix, Real* realarg, 
			     int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_proj_amputate";
#define Ngamma 15
  const int G[Ngamma] = {IDENTITY,GAMMA_X,GAMMA_Y,GAMMA_Z,GAMMA_T,
			 SIGMA_XY,SIGMA_XZ,SIGMA_XT,SIGMA_YZ,SIGMA_YT,SIGMA_ZT,
			 GAMMA_5X,GAMMA_5Y,GAMMA_5Z,GAMMA_5T};
#ifdef HERMITEAN_GAMMA
  const int reim[Ngamma] = {0,1,1,1,1,1,1,1,1,1,1,0,0,0,0};
#else
  const int reim[Ngamma] = {0,1,1,1,1,0,0,0,0,0,0,1,1,1,1};
#endif
  Dirac lietmp[NALG];
  Dirac dtmp;
  Complex ctmp;
  int i, g;

  if (!y && !msize) return Ngamma;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix!=NALG+2) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
  if (nreal<1) {
    fprintf(stderr,"%s: Too few real arguments %d\n",fnm,nreal);
  }
#if 0 // allow additional dummy real arguments to be passed
  if (nreal!=1) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }
#endif

  for (g=0;g<Ngamma;g++) y[g]=0.0;

  /* S(k) = matrix[0]; S(p) = matrix[1] */
  matrix_inverse(invtmp1,matrix[0],msize);
  matrix_inverse(invtmp2,matrix[1],msize);
  for (i=0;i<NALG;i++) {
    /* Lambda_mu^a(p,q,k) = S(p)^-1 V_mu^a(p,q) S(k)^-1/D(q) */
    matrix_mult(tmp1,invtmp2,matrix[i+Nquark],msize);
    matrix_mult(tmp2,tmp1,invtmp1,msize); 
   /* trace out colour indices: (Lambda_mu^a)_ij -> -i(t^a)_ij Gamma_mu */
   /* specifically: lietmp[b] = -i tr_c(lambda_b Lambda_mu^a) */
    qmatrix_to_algdirac(lietmp,tmp2);
    /* trace with all dirac matrices with appropriate re/im part */
    for (g=0;g<Ngamma;g++) {
      gamma_by_dirac(dtmp,lietmp[i],G[g]);
      ctmp = dirac_trace(dtmp);
      y[g] += reim[g] ? -ctmp.re : ctmp.im;
    }
  }

  for (g=0;g<Ngamma;g++)
    y[g] *= sqrt((Real)Lattice_vol)/(realarg[0]*NALG*SPIN);

  return 0;
#undef Ngamma
}

/*** vertexfunc_q0_amp_allmu: the amputated vertex for q=0 (p=k),    ***
 ***   all lorentz components (gluon prop inverted componentwise)    ***
 ***  the traces with all gamma matrices except gamma5 are computed  ***/
int vertexfunc_q0_amp_allmu(Real* y, Complex*** matrix, Real* realarg, 
			    int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_proj_amp_allmu";
#define Ngamma 15
  const int G[Ngamma] = {IDENTITY,GAMMA_X,GAMMA_Y,GAMMA_Z,GAMMA_T,
			 SIGMA_XY,SIGMA_XZ,SIGMA_XT,SIGMA_YZ,SIGMA_YT,SIGMA_ZT,
			 GAMMA_5X,GAMMA_5Y,GAMMA_5Z,GAMMA_5T};
#ifdef HERMITEAN_GAMMA
  const int reim[Ngamma] = {0,1,1,1,1,1,1,1,1,1,1,0,0,0,0};
#else
  const int reim[Ngamma] = {0,1,1,1,1,0,0,0,0,0,0,1,1,1,1};
#endif
  Dirac lietmp[NALG];
  Dirac dtmp;
  Complex ctmp;
  int i, j, mu, g;

  if (!y && !msize) return DIR*Ngamma;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix!=DIR*NALG+2) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
  if (nreal<DIR) {
    fprintf(stderr,"%s: Too few real arguments %d\n",fnm,nreal);
  }
#if 0 // allow additional dummy real arguments to be passed
  if (nreal!=1) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }
#endif

  for (g=0;g<DIR*Ngamma;g++) y[g]=0.0;

  /* S(k) = matrix[0]; S(p) = matrix[1] */
  matrix_inverse(invtmp1,matrix[0],msize);
  for (j=Nquark,mu=0;mu<DIR;mu++) {
    for (i=0;i<NALG;i++,j++) {
    /* Lambda_mu^a(p,q,k) = S(p)^-1 V_mu^a(p,q) S(k)^-1/D(q) */
      matrix_mult(tmp1,invtmp1,matrix[j],msize);
      matrix_mult(tmp2,tmp1,invtmp1,msize); 
      /* trace out colour indices: (Lambda_mu^a)_ij -> -i(t^a)_ij Gamma_mu */
      /* specifically: lietmp[b] = -i tr_c(lambda_b Lambda_mu^a) */
      qmatrix_to_algdirac(lietmp,tmp2);
      /* trace with all dirac matrices with appropriate re/im part */
      for (g=0;g<Ngamma;g++) {
	gamma_by_dirac(dtmp,lietmp[i],G[g]);
	ctmp = dirac_trace(dtmp);
	y[mu*Ngamma+g] += reim[g] ? -ctmp.re/realarg[mu] : ctmp.im/realarg[mu];
      }
    }
  }

  for (g=0;g<DIR*Ngamma;g++)
    y[g] *= sqrt((Real)Lattice_vol)/(NALG*SPIN);

  return 0;
#undef Ngamma
}

/*** vertexfunc_proj_amp_allmu: the transverse-projected amputated   ***
 ***   vertex for general p, q, k=p+q, all lorentz components        ***
 ***  the traces with all gamma matrices except gamma5 are computed  ***/
int vertexfunc_proj_amp_allmu(Real* y, Complex*** matrix, Real* realarg, 
			      int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_proj_amp_allmu";
#define Ngamma 15
  const int G[Ngamma] = {IDENTITY,GAMMA_X,GAMMA_Y,GAMMA_Z,GAMMA_T,
			 SIGMA_XY,SIGMA_XZ,SIGMA_XT,SIGMA_YZ,SIGMA_YT,SIGMA_ZT,
			 GAMMA_5X,GAMMA_5Y,GAMMA_5Z,GAMMA_5T};
#ifdef HERMITEAN_GAMMA
  const int reim[Ngamma] = {0,1,1,1,1,1,1,1,1,1,1,0,0,0,0};
#else
  const int reim[Ngamma] = {0,1,1,1,1,0,0,0,0,0,0,1,1,1,1};
#endif
  Dirac lietmp[NALG];
  Dirac dtmp;
  Complex ctmp;
  int i, j, mu, g;

  if (!y && !msize) return DIR*Ngamma;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix!=DIR*NALG+2) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
  if (nreal<1) {
    fprintf(stderr,"%s: Too few real arguments %d\n",fnm,nreal);
  }
#if 0 // allow additional dummy real arguments to be passed
  if (nreal!=1) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }
#endif

  for (g=0;g<DIR*Ngamma;g++) y[g]=0.0;

  /* S(k) = matrix[0]; S(p) = matrix[1] */
  matrix_inverse(invtmp1,matrix[0],msize);
  matrix_inverse(invtmp2,matrix[1],msize);
  for (j=Nquark,mu=0;mu<DIR;mu++) {
    for (i=0;i<NALG;i++,j++) {
    /* Lambda_mu^a(p,q,k) = S(p)^-1 V_mu^a(p,q) S(k)^-1/D(q) */
      matrix_mult(tmp1,invtmp2,matrix[j],msize);
      matrix_mult(tmp2,tmp1,invtmp1,msize); 
      /* trace out colour indices: (Lambda_mu^a)_ij -> -i(t^a)_ij Gamma_mu */
      /* specifically: lietmp[b] = -i tr_c(lambda_b Lambda_mu^a) */
      qmatrix_to_algdirac(lietmp,tmp2);
      /* trace with all dirac matrices with appropriate re/im part */
      for (g=0;g<Ngamma;g++) {
	gamma_by_dirac(dtmp,lietmp[i],G[g]);
	ctmp = dirac_trace(dtmp);
	y[mu*Ngamma+g] += reim[g] ? -ctmp.re : ctmp.im;
      }
    }
  }

  for (g=0;g<DIR*Ngamma;g++)
    y[g] *= sqrt((Real)Lattice_vol)/(realarg[0]*NALG*SPIN);

  return 0;
#undef Ngamma
}

/*** vertexfunc_amputate: computes the 1PI vertex in a general gauge, ***
 *** ie where the gluon propagator is invertible                      ***/
int vertexfunc_amputate(Real* y, Complex*** matrix, Real* realarg, 
			int msize, int nmatrix, int nreal)
{
  const char fnm[MAXNAME] = "vertexfunc_amputate";
#define Ngamma 15
  const int G[Ngamma] = {IDENTITY,GAMMA_X,GAMMA_Y,GAMMA_Z,GAMMA_T,
			 SIGMA_XY,SIGMA_XZ,SIGMA_XT,SIGMA_YZ,SIGMA_YT,SIGMA_ZT,
			 GAMMA_5X,GAMMA_5Y,GAMMA_5Z,GAMMA_5T};
#ifdef HERMITEAN_GAMMA
  const int reim[Ngamma] = {1,0,0,0,0,0,0,0,0,0,0,1,1,1,1};
#else
  const int reim[Ngamma] = {1,0,0,0,0,1,1,1,1,1,1,0,0,0,0};
#endif
  Dirac lietmp[NALG];
  Dirac dtmp;
  Complex ctmp;
  int i, g;

  fprintf(stderr,"%s: not implemented!!\n",fnm);
  exit(EVIL);

  if (!y && !msize) return Ngamma;
  if (!y) {
    init_tmp_matrices(msize);
    return 0;
  }
  if (!msize) {
    free_tmp_matrices();
    return 0;
  }
  if (msize!=SPIN*COLOUR) {
    fprintf(stderr,"%s: Wrong matrix size %d\n",fnm,msize);
    exit(-1);
  }
  if (nmatrix!=DIR*NALG+2) {
    fprintf(stderr,"%s: WARNING: Wrong number of matrix arguments %d\n",
	    fnm,nmatrix);
  }
  if (nreal!=2) {
    fprintf(stderr,"%s: WARNING: Wrong number of real arguments %d\n",fnm,nreal);
  }

  for (g=0;g<Ngamma;g++) y[g]=0.0;

  matrix_inverse(invtmp1,matrix[0],msize);
  matrix_inverse(invtmp2,matrix[1],msize);

  // TO BE WRITTEN....

  return 0;
#undef Ngamma
}

/*** Initialisation functions for data structures:           ***
 ***  init_matrixdata, init_mfitdata                         ***/
static int init_matrixdata(MatrixData* data, int ndata, int ncfg,
			   int nreal, int nmatrix, int nrconst)
{
  const char fnm[MAXNAME] = "init_matrixdata";

  data->type = FERMION;
  data->ndata   = ndata;
  data->nconfig = ncfg;
  data->nparam  = 0; data->param  = 0;
  data->nmconst = 0; data->mconst = 0;
  data->nmparam = 0; data->mparam = 0;
  data->nrconst = nrconst;
  data->nreal = nreal;
  data->nmatrix = nmatrix;
  data->rank = msz;

  data->mdata = arralloc(sizeof(Complex),5, data->nmatrix,
			 data->nconfig,data->ndata,data->rank,data->rank);
  data->rconst = arralloc(sizeof(Real),2,data->nrconst,data->ndata);
  data->realdata = arralloc(sizeof(Real),3,
			    data->nreal,data->nconfig,data->ndata);
  if (!data->mdata || !data->realdata) {
    fprintf(stderr,"%s: could not allocate space for config data\n",fnm);
    return NOSPACE;
  }
  memset(data->basename,0,MAXNAME);
  memset(data->name,0,MAXLINE);
  memset(data->filename,0,MAXLINE);

  return 0;
}

static int init_mfitdata(BootMatrixFuncs* fit, Vertex_params* run, int ndata,
			 Matrixfunction* func)
{
  const char fnm[MAXNAME] = "init_mfitdata";

  fit->ndata = ndata;
  fit->nboot = run->nboot;
  fit->seed  = run->seed;
  fit->yfunc = func;
  fit->save  = run->write_boot;

  /* we do not perform any fit, so all the following are null */
  fit->nparam = 0;
  fit->param = 0; fit->paramcov = 0;
  fit->freeze = 0; fit->ndof = 0; fit->correlate = 0;
  fit->model = 0; fit->guess = 0;
  fit->subset = 0;

  /* no offset or fancy functions */
  fit->xfunc = 0; /* xdata are passed explicitly... */
  fit->xoffset = fit->xsoffset = 0;
  fit->yoffset = fit->ysoffset = 0;

  return 0;
}

/*** create_vertex_fileroot: creates the string that forms the basis ***
 ***   of the filenames for the files created by this program        ***/
static int create_vertex_fileroot(char* str, Vertex_params* run)
{
  const char fnm[MAXNAME] = "create_vertex_fileroot";
  char actstr[MAXCODELEN] = "";
  char impstr[MAXCODELEN] = "";
  char gaugestr[MAXCODELEN] = "";

  switch (run->gauge->gauge) {
  case LANDAU:
    strcpy(gaugestr,"L"); break;
  case COVARIANT:
    sprintf(gaugestr,"X%02d",INT(10*run->gauge->gauge_param));
    break;
  default:
    fprintf(stderr,"%s: unknown gauge type %d\n",fnm,run->gauge->gauge);
    return BADPARAM;
  }

  /** check that all necessary improvement factors are present for clover **/
  if (run->prop->action==CLOVER && run->prop->ncoeff<2) {
    fprintf(stderr,"%s: not enough improvement terms in clover action\n",fnm);
    return BADPARAM;
  }

  switch (run->prop->action) {
  case WILSON:
    strcpy(actstr,"W"); break;
  case CLOVER:
    sprintf(actstr,"C%03d",INT(100*run->prop->imp[0])); break;
  default:
    fprintf(stderr,"%s: unknown action type %d\n",fnm,run->prop->action);
    return BADPARAM;
  }

  /** string code for improvement terms: only for clover (improved) action **/
  if (run->prop->action==CLOVER) {
    if (run->prop->source_type==ROTATED) {
      /** rotated clover: imp[1] = rotation coeff               **
       **                 imp[2] = multiplicative (mass) factor **
       **                 imp[3] = additive term (if present)   **/
      switch (run->prop->ncoeff) {
      case 2: /* only rotation */
	sprintf(impstr,"r%03db00",INT(1000*run->prop->imp[1]));
	break;
      case 3:
	sprintf(impstr,"r%03db%03d",
		INT(1000*run->prop->imp[1]),INT(100*run->prop->imp[2]));
	break;
      case 4:
	sprintf(impstr,"r%03db%03dl%02d",INT(1000*run->prop->imp[1]),
		INT(100*run->prop->imp[2]),INT(100*run->prop->imp[3]));
	break;
      default:
	fprintf(stderr,"%s: invalid number of improvement coeffs %d\n",
		fnm,run->prop->ncoeff);
	return BADPARAM;
      }
    } else {
      /** additive improved clover:                                 **
       **   imp[3] = additive term;  imp[2] = multiplicative factor **/
      sprintf(impstr,"l%02db%03d",
	      INT(100*run->prop->imp[3]),INT(100*run->prop->imp[2]));
    }
  }

  sprintf(str,"B%d%s%sK%04d%s_mu%d_q%d_%d_%d_%d_p%d_%d_%dt", 
	  INT(run->gauge->beta*100),gaugestr,actstr,
	  INT(run->prop->kappa*10000),impstr,
	  run->mu+1,run->q[IX],run->q[IY],run->q[IZ],run->q[IT],
	  run->p[IX],run->p[IY],run->p[IZ]);

 return 0;
}

/***** Dummy functions *****/
void random_gauge(Gauge u, float r) { }
