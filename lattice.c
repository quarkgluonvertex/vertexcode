#include "lattice.h"
#include <stdio.h>
#include <magic.h>
#include <stdio.h>
#include <stdlib.h>

/** compute the lattice volume and actual number of dimensions       **
 **  Note: this only works for purely spatial and 3+1-dim lattices   **/
/* TODO: Implement also for 2+1, 1+1 and add checks on ordering */
void compute_sizes(Lattice *lat)
{
  int i;
  lat->nspatial = lat->length[IX]*lat->length[IY]*lat->length[IZ];
  lat->nsite    = lat->nspatial*lat->length[IT];
  for (i=0; i<DIR; i++) {
    if (lat->length[i]<1) {
      fprintf(stderr,"compute_sizes: illegal L[%d] %d -- set to 1\n",
	      i,lat->length[i]);
      lat->length[i]=1;
      break;
    }
    if (lat->length[i]==1) break;
  }
  lat->ndim=i;
}

void compute_neighbours(Lattice *lat)
{
  int stride[DIR+1];
  int i, mu, maxlen;

  stride[0]=1;
  maxlen=lat->length[0];
  for (mu=0; mu<DIR; mu++) {
    stride[mu+1] = stride[mu]*lat->length[mu];
    if (lat->length[mu]>maxlen) maxlen=lat->length[mu];
  }
  /* lattice volume in case it wasnt done yet */
  lat->nsite = stride[DIR];
  lat->nspatial = lat->nsite/lat->length[IT];

  lat->up = malloc(sizeof(i4vec)*maxlen);
  lat->dn = malloc(sizeof(i4vec)*maxlen);

  for (mu=0; mu<DIR; mu++) {
    for (i=0; i<maxlen; i++) {
      lat->up[i][mu] =  stride[mu];
      lat->dn[i][mu] = -stride[mu];
    }
    lat->up[lat->length[mu]-1][mu] -= stride[mu]*lat->length[mu];
    lat->dn[0][mu] += stride[mu]*lat->length[mu];
  }
  lat->has_ntab = TRUE;
}
