#ifndef _FERMION_H_
#define _FERMION_H_
#include "Quark.h"
#include "field_params.h"
#include "gauge.h"
#include <magic.h>

/** This file contains structures and prototypes for manipulating fermions **
 ** The typedefs and sizes of the basic fermion types are in Quark.h       **/

/* Propagator types */
typedef enum {XSPACE,TSLICE_X,TSLICE_P,FOURIER,FOURIERSLICE} Space_t;
typedef enum {PROP,SPINOR,COLMAT,GORKOVPROP,GORKOV} Prop_t;

#define S_ISGORKOV(t) ((t)==GORKOVPROP || (t)==GORKOV)

typedef struct {
  Propagator_parameters* parm;
  Space_t space;
  Prop_t type;
  size_t elsz;       /* size of a site variable (Spinor, Prop, Gorkov,...) */
  int nmom[DIR];     /* for momentum-space fields */
  int ntotmom[DIR];  /* in position space these are dummy members */
  int tslice;        /* for timesliced props (dummy if not timesliced) */
  int x[DIR];        /* for space-time (or momentum) slices */
  int s1, s2;        /* spin indices (for component data)   */
  int c1, c2;        /* colour indices (for component data) */
  int npoint;        /* total number of points (lattice sites or momenta) */
  void* prop;        /* Spinor or Prop (or Gorkov or ???) */
  char filename[MAX_FILENAME];
} Propagator;

void spinor_zero(Spinor psi);
void spinor_copy(Spinor res, Spinor psi);
void spinor_add(Spinor res, Spinor psi1, Spinor psi2);
void spinor_sub(Spinor res, Spinor psi1, Spinor psi2);
void spinor_addto(Spinor res, Spinor psi);
void spinor_subto(Spinor res, Spinor psi);
void spinor_by_real(Spinor res, Spinor psi1, Real r);
void spinor_by_complex(Spinor res, Spinor psi, Complex z);
void spinor_scale(Spinor res, Real r);

void prop_zero(Prop psi);
void prop_copy(Prop res, Prop psi);
void prop_add(Prop res, Prop psi1, Prop psi2);
void prop_sub(Prop res, Prop psi1, Prop psi2);
void prop_addto(Prop res, Prop psi);
void prop_subto(Prop res, Prop psi);
void prop_by_real(Prop res, Prop psi1, Real r);
void prop_by_complex(Prop res, Prop psi, Real z);
void prop_scale(Prop res, Real r);

void dirac_zero(Dirac psi);
void dirac_constant(Dirac psi, Real r);
void dirac_constant_z(Dirac psi, Real re, Real im);
void dirac_copy(Dirac res, Dirac psi);
void dirac_add(Dirac res, Dirac psi1, Dirac psi2);
void dirac_sub(Dirac res, Dirac psi1, Dirac psi2);
void dirac_addto(Dirac res, Dirac psi);
void dirac_subto(Dirac res, Dirac psi);
void dirac_mult(Dirac res, Dirac psi1, Dirac psi2);
void dirac_trans_mult(Dirac res, Dirac psi1, Dirac psi2);
void dirac_mult_trans(Dirac res, Dirac psi1, Dirac psi2);
void dirac_trans_mult_trans(Dirac res, Dirac psi1, Dirac psi2);
void dirac_by_real(Dirac res, Dirac psi1, Real r);
void dirac_by_complex(Dirac res, Dirac psi, Real z);
void dirac_scale(Dirac res, Real r);
void dirac_scale_z(Dirac, Complex);
void dirac_transpose(Dirac res, Dirac psi);
Complex dirac_trace(Dirac psi);

Complex prop_trace(Prop s);
void colour_trace_prop(Dirac res, Prop);

void dirac_by_gamma(Dirac result, Dirac corr, int gamma_code);
void gamma_by_dirac(Dirac result, Dirac corr, int gamma_code);
void gammatrans_by_dirac(Dirac result, Dirac corr, int gamma_code);
void gamma_by_spinor(Spinor result, Spinor quark, int gamma_code);
void spinor_by_gamma(Spinor result, Spinor quark, int gamma_code);
void gamma_by_prop(Prop result, Prop quark, int gamma_code);
void prop_by_gamma(Prop result, Prop quark, int gamma_code);
void gammatrans_by_prop(Prop result, Prop quark, int gamma_code);
void prop_by_gammatrans(Prop result, Prop quark, int gamma_code);
void aslash(Dirac res, Real* a);

void prop_to_matrix(Complex**,Prop);
void qmatrix_to_algdirac(Dirac*,Complex**);

#ifdef SU2_GROUP
void su2_by_spinor(Spinor res, Group_mat u, Spinor psi);
void su2dag_by_spinor(Spinor res, Group_mat u, Spinor psi);
void tau2Stau2(Prop res, Prop s);
void KpropK(Prop res, Prop s);
void colour_trace_prop_tau2(Dirac res, Prop);
#define grp_by_spinor su2_by_spinor
#define grpdag_by_spinor su2dag_by_spinor
#endif
#ifdef SU3_GROUP
void su3_by_spinor(Spinor res, Group_mat u, Spinor psi);
void su3dag_by_spinor(Spinor res, Group_mat u, Spinor psi);
#define grp_by_spinor su3_by_spinor
#define grpdag_by_spinor su3dag_by_spinor
#endif

/*** initialisation functions and destructors ***/
int init_prop(Propagator*, Propagator_parameters*, Space_t, Prop_t);
int init_fpropslice(Propagator*,Propagator_parameters*,int*,int*);
int construct_prop_filename(Propagator* prop);
int construct_gorkov_filename(Propagator* prop);
void prop_delete(Propagator* p);

/*** i/o routines ***/
int reorder_prop_xyzt(Propagator*);
int reorder_prop_tzyx(Propagator*);
int write_prop_cpt(Propagator prop, int spin, int col);
int write_prop(Propagator);
int dump_prop(Propagator);
int write_eigenvectors(Spinor**,double*,int,Propagator_parameters*,char*);
int read_prop(Propagator prop);
int read_prop_tslice(Propagator prop);

void insert_spinor_in_prop(Prop* p, Spinor* psi, int spin, int col, int n);

/*** miscellaneous ***/
int gauge_trans_prop(Propagator p, Gauge_transform g);

#endif
