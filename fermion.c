#include "fermion.h"

void spinor_zero(Spinor psi)
{
  int spin,col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) psi[spin][col].re = psi[spin][col].im = 0.0;

}

void spinor_copy(Spinor res, Spinor psi)
{
  int spin,col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) res[spin][col] = psi[spin][col];

}

void spinor_add(Spinor res, Spinor psi1, Spinor psi2)
{
  int spin, col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) {
      res[spin][col].re = psi1[spin][col].re + psi2[spin][col].re;
      res[spin][col].im = psi1[spin][col].im + psi2[spin][col].im;
    }
}

void spinor_sub(Spinor res, Spinor psi1, Spinor psi2)
{
  int spin, col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) {
      res[spin][col].re = psi1[spin][col].re - psi2[spin][col].re;
      res[spin][col].im = psi1[spin][col].im - psi2[spin][col].im;
    }
}

void spinor_addto(Spinor res, Spinor psi)
{
  int spin, col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) {
      res[spin][col].re += psi[spin][col].re;
      res[spin][col].im += psi[spin][col].im;
    }
}

void spinor_subto(Spinor res, Spinor psi)
{
  int spin, col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) {
      res[spin][col].re -= psi[spin][col].re;
      res[spin][col].im -= psi[spin][col].im;
    }
}

void spinor_scale(Spinor res, Real r)
{
  int spin, col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) {
      res[spin][col].re *= r;
      res[spin][col].im *= r;
    }
}

void spinor_by_complex(Spinor res, Spinor psi, Complex z)
{
  int spin, col;
  for (spin=0;spin<SPIN;spin++)
    for (col=0;col<COLOUR;col++) {
      res[spin][col].re = psi[spin][col].re*z.re - psi[spin][col].im*z.im;
      res[spin][col].im = psi[spin][col].re*z.im + psi[spin][col].im*z.re;
    }
}

void dirac_copy(Dirac res, Dirac psi)
{
  int is,js;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) 
    res[is][js] = psi[is][js];
}

void dirac_zero(Dirac res)
{
  int is,js;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) 
    res[is][js].re = res[is][js].im = 0.0;
}

void dirac_constant(Dirac res, Real r)
{
  int is;
  dirac_zero(res);
  for (is=0;is<SPIN;is++) res[is][is].re = r;
}

void dirac_constant_z(Dirac res, Real re, Real im)
{
  int is;
  dirac_zero(res);
  for (is=0;is<SPIN;is++) {
    res[is][is].re = re;
    res[is][is].im = im;
  }
}

void dirac_scale(Dirac res, Real r)
{
  int is, js;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    res[is][js].re *= r; 
    res[is][js].im *= r;
  }
}

void dirac_scale_z(Dirac res, Complex z)
{
  int is, js;
  Real tmp;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    tmp = res[is][js].re;
    res[is][js].re = tmp*z.re - res[is][js].im*z.im; 
    res[is][js].im = tmp*z.im + res[is][js].im*z.re;
  }
}

void dirac_addto(Dirac res, Dirac psi)
{
  int is,js;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    res[is][js].re += psi[is][js].re;
    res[is][js].im += psi[is][js].im;
  }
}

void dirac_mult(Dirac res, Dirac psi1, Dirac psi2)
{
  int i,j,k;

  for (i=0;i<SPIN;i++) for (j=0;j<SPIN;j++) {
    res[i][j].re = res[i][j].im = 0.0;
    for (k=0;k<SPIN;k++) {
      res[i][j].re += psi1[i][k].re*psi2[k][j].re-psi1[i][k].im*psi2[k][j].im;
      res[i][j].im += psi1[i][k].re*psi2[k][j].im+psi1[i][k].im*psi2[k][j].re;
    }
  }
}


void dirac_trans_mult(Dirac res, Dirac psi1, Dirac psi2)
{
  int i,j,k;

  for (i=0;i<SPIN;i++) for (j=0;j<SPIN;j++) {
    res[i][j].re = res[i][j].im = 0.0;
    for (k=0;k<SPIN;k++) {
      res[i][j].re += psi1[k][i].re*psi2[k][j].re-psi1[k][i].im*psi2[k][j].im;
      res[i][j].im += psi1[k][i].re*psi2[k][j].im+psi1[k][i].im*psi2[k][j].re;
    }
  }
}

void dirac_mult_trans(Dirac res, Dirac psi1, Dirac psi2)
{
  int i,j,k;

  for (i=0;i<SPIN;i++) for (j=0;j<SPIN;j++) {
    res[i][j].re = res[i][j].im = 0.0;
    for (k=0;k<SPIN;k++) {
      res[i][j].re += psi1[i][k].re*psi2[j][k].re-psi1[i][k].im*psi2[j][k].im;
      res[i][j].im += psi1[i][k].re*psi2[j][k].im+psi1[i][k].im*psi2[j][k].re;
    }
  }
}

Complex dirac_trace(Dirac psi)
{
  Complex tr = {0.0,0.0};
  int is;
  for (is=0;is<SPIN;is++) {
    tr.re += psi[is][is].re;
    tr.im += psi[is][is].im;
  }
  return tr;
}

void dirac_transpose(Dirac res, Dirac psi)
{
  int is,js;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) res[is][js] = psi[js][is];
}

void prop_copy(Prop res, Prop s)
{
  int is,js,ic,jc;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++)
      res[is][js][ic][jc] = s[is][js][ic][jc];
}

void prop_add(Prop res, Prop s1, Prop s2)
{
  int is,js,ic,jc;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
      res[is][js][ic][jc].re = s1[is][js][ic][jc].re+s2[is][js][ic][jc].re;
      res[is][js][ic][jc].im = s1[is][js][ic][jc].im+s2[is][js][ic][jc].im;
    }
}

void prop_addto(Prop res, Prop s)
{
  int is,js,ic,jc;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
      res[is][js][ic][jc].re += s[is][js][ic][jc].re;
      res[is][js][ic][jc].im += s[is][js][ic][jc].im;
    }
}

void prop_sub(Prop res, Prop s1, Prop s2)
{
  int is,js,ic,jc;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
      res[is][js][ic][jc].re = s1[is][js][ic][jc].re-s2[is][js][ic][jc].re;
      res[is][js][ic][jc].im = s1[is][js][ic][jc].im-s2[is][js][ic][jc].im;
    }
}

void prop_subto(Prop res, Prop s)
{
  int is,js,ic,jc;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
      res[is][js][ic][jc].re -= s[is][js][ic][jc].re;
      res[is][js][ic][jc].im -= s[is][js][ic][jc].im;
    }
}

Complex prop_trace(Prop s)
{
  Complex z = {0.0,0.0};
  int ic,is;
  for (is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++) {
    z.re += s[is][is][ic][ic].re;
    z.im += s[is][is][ic][ic].im;
  }
  return z;
}

void colour_trace_prop(Dirac res, Prop s)
{
  int is,js,ic;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    res[is][js].re = res[is][js].im = 0.0;
    for (ic=0;ic<COLOUR;ic++) {
      res[is][js].re += s[is][js][ic][ic].re;
      res[is][js].im += s[is][js][ic][ic].im;
    }
  }
}

#ifdef SU2_GROUP
/* take the colour-antisymmetric trace of the propagator */
void colour_trace_prop_tau2(Dirac res, Prop s)
{
  int is,js,ic;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    res[is][js].re = res[is][js].im = 0.0;
    for (ic=0;ic<COLOUR;ic++) {
      res[is][js].re = s[is][js][1][0].im - s[is][js][0][1].im;
      res[is][js].im = s[is][js][0][1].re - s[is][js][1][0].re;
    }
  }
}
#endif

void prop_to_matrix(Complex** res, Prop s)
{
  int is, js, ic, jc, i, j;

  for (i=is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++,i++)
    for (j=js=0;js<SPIN;js++) for (jc=0;jc<COLOUR;jc++,j++) {
      res[i][j] = s[is][js][ic][jc];
    }
}

void matrix_to_prop(Prop res, Complex** m)
{
  int is, js, ic, jc, i, j;

  for (i=is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++,i++)
    for (j=js=0;js<SPIN;js++) for (jc=0;jc<COLOUR;jc++,j++) {
      res[is][js][ic][jc] = m[i][j];
    }
}

void qmatrix_to_algdirac(Dirac* res, Complex** s)
{
  Group_mat mat;
  Algebra_c lie;
  int is, js, ic, jc, i, j;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
      i = is*COLOUR+ic;
      j = js*COLOUR+jc;
      mat[ic][jc] = s[i][j];
    }
    grpmat_to_algc(lie,mat);
    for (i=0;i<NALG;i++) res[i][is][js] = lie[i];
  }
}

#ifdef SU2_GROUP
void su2_by_spinor(Spinor res, Su2mat u, Spinor psi)
{
  int s;
  for (s=0; s<SPIN; s++) {
    res[s][0].re =  u[0][0].re*psi[s][0].re - u[0][0].im*psi[s][0].im
                  + u[0][1].re*psi[s][1].re - u[0][1].im*psi[s][1].im;
    res[s][0].im =  u[0][0].re*psi[s][0].im + u[0][0].im*psi[s][0].re
                  + u[0][1].re*psi[s][1].im + u[0][1].im*psi[s][1].re;
    res[s][1].re =  u[1][0].re*psi[s][0].re - u[1][0].im*psi[s][0].im
                  + u[1][1].re*psi[s][1].re - u[1][1].im*psi[s][1].im;
    res[s][1].im =  u[1][0].re*psi[s][0].im + u[1][0].im*psi[s][0].re
                  + u[1][1].re*psi[s][1].im + u[1][1].im*psi[s][1].re;
  }
}

void su2dag_by_spinor(Spinor res, Su2mat u, Spinor psi)
{
  int s;
  for (s=0; s<SPIN; s++) {
    res[s][0].re =  u[0][0].re*psi[s][0].re + u[0][0].im*psi[s][0].im
                  + u[1][0].re*psi[s][1].re + u[1][0].im*psi[s][1].im;
    res[s][0].im =  u[0][0].re*psi[s][0].im - u[0][0].im*psi[s][0].re
                  + u[1][0].re*psi[s][1].im - u[1][0].im*psi[s][1].re;
    res[s][1].re =  u[0][1].re*psi[s][0].re + u[0][1].im*psi[s][0].im
                  + u[1][1].re*psi[s][1].re + u[1][1].im*psi[s][1].im;
    res[s][1].im =  u[0][1].re*psi[s][0].im - u[0][1].im*psi[s][0].re
                  + u[1][1].re*psi[s][1].im - u[1][1].im*psi[s][1].re;
  }
}

/*** Compute tau_2 S tau_2 ***/
void tau2Stau2(Prop res, Prop s)
{
  int is,js;
  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
    res[is][js][0][0].re =  s[is][js][1][1].re;
    res[is][js][0][0].im =  s[is][js][1][1].im;

    res[is][js][0][1].re = -s[is][js][1][0].re;
    res[is][js][0][1].im = -s[is][js][1][0].im;

    res[is][js][1][0].re = -s[is][js][0][1].re;
    res[is][js][1][0].im = -s[is][js][0][1].im;

    res[is][js][1][1].re =  s[is][js][0][0].re;
    res[is][js][1][1].im =  s[is][js][0][0].im;
  }
}

/*** Compute (C g5 tau2) S (C g5 tau2) ***/
void KpropK(Prop res, Prop s)
{
#ifdef CHIRAL_GAMMA
  res[0][0][0][0].re = -s[1][1][1][1].re;
  res[0][0][0][0].im = -s[1][1][1][1].im;
  res[0][0][0][1].re =  s[1][1][1][0].re;
  res[0][0][0][1].im =  s[1][1][1][0].im;
  res[0][0][1][0].re =  s[1][1][0][1].re;
  res[0][0][1][0].im =  s[1][1][0][1].im;
  res[0][0][1][1].re = -s[1][1][0][0].re;
  res[0][0][1][1].im = -s[1][1][0][0].im;

  res[0][1][0][0].re =  s[1][0][1][1].re;
  res[0][1][0][0].im =  s[1][0][1][1].im;
  res[0][1][0][1].re = -s[1][0][1][0].re;
  res[0][1][0][1].im = -s[1][0][1][0].im;
  res[0][1][1][0].re = -s[1][0][0][1].re;
  res[0][1][1][0].im = -s[1][0][0][1].im;
  res[0][1][1][1].re =  s[1][0][0][0].re;
  res[0][1][1][1].im =  s[1][0][0][0].im;

  res[0][2][0][0].re = -s[1][3][1][1].re;
  res[0][2][0][0].im = -s[1][3][1][1].im;
  res[0][2][0][1].re =  s[1][3][1][0].re;
  res[0][2][0][1].im =  s[1][3][1][0].im;
  res[0][2][1][0].re =  s[1][3][0][1].re;
  res[0][2][1][0].im =  s[1][3][0][1].im;
  res[0][2][1][1].re = -s[1][3][0][0].re;
  res[0][2][1][1].im = -s[1][3][0][0].im;

  res[0][3][0][0].re =  s[1][2][1][1].re;
  res[0][3][0][0].im =  s[1][2][1][1].im;
  res[0][3][0][1].re = -s[1][2][1][0].re;
  res[0][3][0][1].im = -s[1][2][1][0].im;
  res[0][3][1][0].re = -s[1][2][0][1].re;
  res[0][3][1][0].im = -s[1][2][0][1].im;
  res[0][3][1][1].re =  s[1][2][0][0].re;
  res[0][3][1][1].im =  s[1][2][0][0].im;

  res[1][0][0][0].re =  s[0][1][1][1].re;
  res[1][0][0][0].im =  s[0][1][1][1].im;
  res[1][0][0][1].re = -s[0][1][1][0].re;
  res[1][0][0][1].im = -s[0][1][1][0].im;
  res[1][0][1][0].re = -s[0][1][0][1].re;
  res[1][0][1][0].im = -s[0][1][0][1].im;
  res[1][0][1][1].re =  s[0][1][0][0].re;
  res[1][0][1][1].im =  s[0][1][0][0].im;

  res[1][1][0][0].re = -s[0][0][1][1].re;
  res[1][1][0][0].im = -s[0][0][1][1].im;
  res[1][1][0][1].re =  s[0][0][1][0].re;
  res[1][1][0][1].im =  s[0][0][1][0].im;
  res[1][1][1][0].re =  s[0][0][0][1].re;
  res[1][1][1][0].im =  s[0][0][0][1].im;
  res[1][1][1][1].re = -s[0][0][0][0].re;
  res[1][1][1][1].im = -s[0][0][0][0].im;

  res[1][2][0][0].re =  s[0][3][1][1].re;
  res[1][2][0][0].im =  s[0][3][1][1].im;
  res[1][2][0][1].re = -s[0][3][1][0].re;
  res[1][2][0][1].im = -s[0][3][1][0].im;
  res[1][2][1][0].re = -s[0][3][0][1].re;
  res[1][2][1][0].im = -s[0][3][0][1].im;
  res[1][2][1][1].re =  s[0][3][0][0].re;
  res[1][2][1][1].im =  s[0][3][0][0].im;

  res[1][3][0][0].re = -s[0][2][1][1].re;
  res[1][3][0][0].im = -s[0][2][1][1].im;
  res[1][3][0][1].re =  s[0][2][1][0].re;
  res[1][3][0][1].im =  s[0][2][1][0].im;
  res[1][3][1][0].re =  s[0][2][0][1].re;
  res[1][3][1][0].im =  s[0][2][0][1].im;
  res[1][3][1][1].re = -s[0][2][0][0].re;
  res[1][3][1][1].im = -s[0][2][0][0].im;

  res[2][0][0][0].re = -s[3][1][1][1].re;
  res[2][0][0][0].im = -s[3][1][1][1].im;
  res[2][0][0][1].re =  s[3][1][1][0].re;
  res[2][0][0][1].im =  s[3][1][1][0].im;
  res[2][0][1][0].re =  s[3][1][0][1].re;
  res[2][0][1][0].im =  s[3][1][0][1].im;
  res[2][0][1][1].re = -s[3][1][0][0].re;
  res[2][0][1][1].im = -s[3][1][0][0].im;

  res[2][1][0][0].re =  s[3][0][1][1].re;
  res[2][1][0][0].im =  s[3][0][1][1].im;
  res[2][1][0][1].re = -s[3][0][1][0].re;
  res[2][1][0][1].im = -s[3][0][1][0].im;
  res[2][1][1][0].re = -s[3][0][0][1].re;
  res[2][1][1][0].im = -s[3][0][0][1].im;
  res[2][1][1][1].re =  s[3][0][0][0].re;
  res[2][1][1][1].im =  s[3][0][0][0].im;

  res[2][2][0][0].re = -s[3][3][1][1].re;
  res[2][2][0][0].im = -s[3][3][1][1].im;
  res[2][2][0][1].re =  s[3][3][1][0].re;
  res[2][2][0][1].im =  s[3][3][1][0].im;
  res[2][2][1][0].re =  s[3][3][0][1].re;
  res[2][2][1][0].im =  s[3][3][0][1].im;
  res[2][2][1][1].re = -s[3][3][0][0].re;
  res[2][2][1][1].im = -s[3][3][0][0].im;

  res[2][3][0][0].re =  s[3][2][1][1].re;
  res[2][3][0][0].im =  s[3][2][1][1].im;
  res[2][3][0][1].re = -s[3][2][1][0].re;
  res[2][3][0][1].im = -s[3][2][1][0].im;
  res[2][3][1][0].re = -s[3][2][0][1].re;
  res[2][3][1][0].im = -s[3][2][0][1].im;
  res[2][3][1][1].re =  s[3][2][0][0].re;
  res[2][3][1][1].im =  s[3][2][0][0].im;

  res[3][0][0][0].re =  s[2][1][1][1].re;
  res[3][0][0][0].im =  s[2][1][1][1].im;
  res[3][0][0][1].re = -s[2][1][1][0].re;
  res[3][0][0][1].im = -s[2][1][1][0].im;
  res[3][0][1][0].re = -s[2][1][0][1].re;
  res[3][0][1][0].im = -s[2][1][0][1].im;
  res[3][0][1][1].re =  s[2][1][0][0].re;
  res[3][0][1][1].im =  s[2][1][0][0].im;

  res[3][1][0][0].re = -s[2][0][1][1].re;
  res[3][1][0][0].im = -s[2][0][1][1].im;
  res[3][1][0][1].re =  s[2][0][1][0].re;
  res[3][1][0][1].im =  s[2][0][1][0].im;
  res[3][1][1][0].re =  s[2][0][0][1].re;
  res[3][1][1][0].im =  s[2][0][0][1].im;
  res[3][1][1][1].re = -s[2][0][0][0].re;
  res[3][1][1][1].im = -s[2][0][0][0].im;

  res[3][2][0][0].re =  s[2][3][1][1].re;
  res[3][2][0][0].im =  s[2][3][1][1].im;
  res[3][2][0][1].re = -s[2][3][1][0].re;
  res[3][2][0][1].im = -s[2][3][1][0].im;
  res[3][2][1][0].re = -s[2][3][0][1].re;
  res[3][2][1][0].im = -s[2][3][0][1].im;
  res[3][2][1][1].re =  s[2][3][0][0].re;
  res[3][2][1][1].im =  s[2][3][0][0].im;

  res[3][3][0][0].re = -s[2][2][1][1].re;
  res[3][3][0][0].im = -s[2][2][1][1].im;
  res[3][3][0][1].re =  s[2][2][1][0].re;
  res[3][3][0][1].im =  s[2][2][1][0].im;
  res[3][3][1][0].re =  s[2][2][0][1].re;
  res[3][3][1][0].im =  s[2][2][0][1].im;
  res[3][3][1][1].re = -s[2][2][0][0].re;
  res[3][3][1][1].im = -s[2][2][0][0].im;
#else
  printf("KpropK: not implemented for this gamma matrix rep!\n");
  exit(EVIL);
#endif
}
#endif

#ifdef SU3_GROUP
void su3_by_spinor(Spinor res, Su3mat u, Spinor psi)
{
  int s;
  for (s=0; s<SPIN; s++) {
    res[s][0].re =  u[0][0].re*psi[s][0].re - u[0][0].im*psi[s][0].im
                  + u[0][1].re*psi[s][1].re - u[0][1].im*psi[s][1].im
                  + u[0][2].re*psi[s][2].re - u[0][2].im*psi[s][2].im;
    res[s][0].im =  u[0][0].re*psi[s][0].im + u[0][0].im*psi[s][0].re
                  + u[0][1].re*psi[s][1].im + u[0][1].im*psi[s][1].re
                  + u[0][2].re*psi[s][2].im + u[0][2].im*psi[s][2].re;
    res[s][1].re =  u[1][0].re*psi[s][0].re - u[1][0].im*psi[s][0].im
                  + u[1][1].re*psi[s][1].re - u[1][1].im*psi[s][1].im
                  + u[1][2].re*psi[s][2].re - u[1][2].im*psi[s][2].im;
    res[s][1].im =  u[1][0].re*psi[s][0].im + u[1][0].im*psi[s][0].re
                  + u[1][1].re*psi[s][1].im + u[1][1].im*psi[s][1].re
                  + u[1][2].re*psi[s][2].im + u[1][2].im*psi[s][2].re;
    res[s][2].re =  u[2][0].re*psi[s][0].re - u[2][0].im*psi[s][0].im
                  + u[2][1].re*psi[s][1].re - u[2][1].im*psi[s][1].im
                  + u[2][2].re*psi[s][2].re - u[2][2].im*psi[s][2].im;
    res[s][2].im =  u[2][0].re*psi[s][0].im + u[2][0].im*psi[s][0].re
                  + u[2][1].re*psi[s][1].im + u[2][1].im*psi[s][1].re
                  + u[2][2].re*psi[s][2].im + u[2][2].im*psi[s][2].re;
  }
}

void su3dag_by_spinor(Spinor res, Su3mat u, Spinor psi)
{
  int s;
  for (s=0; s<SPIN; s++) {
    res[s][0].re =  u[0][0].re*psi[s][0].re + u[0][0].im*psi[s][0].im
                  + u[1][0].re*psi[s][1].re + u[1][0].im*psi[s][1].im
                  + u[2][0].re*psi[s][2].re + u[2][0].im*psi[s][2].im;
    res[s][0].im =  u[0][0].re*psi[s][0].im - u[0][0].im*psi[s][0].re
                  + u[1][0].re*psi[s][1].im - u[1][0].im*psi[s][1].re
                  + u[2][0].re*psi[s][2].im - u[2][0].im*psi[s][2].re;
    res[s][1].re =  u[0][1].re*psi[s][0].re + u[0][1].im*psi[s][0].im
                  + u[1][1].re*psi[s][1].re + u[1][1].im*psi[s][1].im
                  + u[2][1].re*psi[s][2].re + u[2][1].im*psi[s][2].im;
    res[s][1].im =  u[0][1].re*psi[s][0].im - u[0][1].im*psi[s][0].re
                  + u[1][1].re*psi[s][1].im - u[1][1].im*psi[s][1].re
                  + u[2][1].re*psi[s][2].im - u[2][1].im*psi[s][2].re;
    res[s][2].re =  u[0][2].re*psi[s][0].re + u[0][2].im*psi[s][0].im
                  + u[1][2].re*psi[s][1].re + u[1][2].im*psi[s][1].im
                  + u[2][2].re*psi[s][2].re + u[2][2].im*psi[s][2].im;
    res[s][2].im =  u[0][2].re*psi[s][0].im - u[0][2].im*psi[s][0].re
                  + u[1][2].re*psi[s][1].im - u[1][2].im*psi[s][1].re
                  + u[2][2].re*psi[s][2].im - u[2][2].im*psi[s][2].re;
  }
}
#endif
