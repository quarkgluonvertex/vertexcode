/* $Id: matrixfunc.h,v 1.1 2006/12/04 13:48:47 jonivar Exp $ */

/************************************************************************
 *                                                                      *
 *  Prototypes and typedefs for matrix statistics functions             *
 *                                                                      *
 ************************************************************************/

#ifndef _MATRIXFUNC_H_
#define _MATRIXFUNC_H_
#include "Complex.h"
#include "analysis.h"

typedef Complex** Matrix; /* TODO: change to struct including size? */

/** A Matrixfunction takes nmatrix matrix arguments and nreal scalar args **
 ** and returns one or more values in y.                                  **
 ** If called with y=0 it returns the number of return values.            **/
typedef int Matrixfunction(Real *y, Matrix *matrix_arg, Real *real_arg,
			   int matrix_size, int nmatrix, int nreal);
typedef struct {
  Data_t type;
  void* parm;
  int nconfig; /* number of data sets (configurations) */
  int ndata;   /* number of data points (eg timeslices, momenta) */
  Real* param; int nparam;  /* parameters, eg beta or mass, that are       *
			     * passed to the data function but do not vary */
  Real**  rconst; int nrconst; /* configuration-independent data */
  Real*** realdata; int nreal;
  Matrix*  mparam; int nmparam;
  Matrix** mconst; int nmconst;
  Matrix*** mdata; int nmatrix;
  int rank;
  char basename[MAXNAME];
  char name[MAXLINE];
  char filename[MAXLINE];
} MatrixData;

/* structure for bootstrap fits ... nearly identical to BootFitFuncs */
typedef struct {
  int nparam;
  int ndata;
  int* subset;  // boolean array
  int nboot;
  Boot* param;
  Boot** paramcov;
  Boot chisq;
  long seed;    /* random seed for bootstrap samples */
  int* freeze;
  int ndof;     /* degrees of freedom */
  int correlate;
  Matrixfunction* xfunc;
  Matrixfunction* yfunc;
  int xoffset;
  int xsoffset;
  int yoffset;
  int ysoffset;
  Modelfunction* model;
  Guessfunction* guess;
  int save;    /* save bootstrapped data */
} BootMatrixFuncs;

void get_matrixfunc_stats(Real**,Real**,Matrixfunction*,MatrixData*);
void get_jack_matrixfunc(Real***,Matrixfunction*,MatrixData*);
void get_boot_matrixdata(MatrixData* bootdata, MatrixData* data);
void get_jack_matrixdata(MatrixData* jackdata, MatrixData* data, int omit);
void matrixfunc_average(MatrixData*,Matrixfunction*,Real**,int*,int*);
void matrixfunc_cov_ave(MatrixData*,Matrixfunction*,Real**,Real***,int*,int*);

void matrix_average(MatrixData*,int*,int*,Complex****,Real**);
int bootstrap_matrix(MatrixData*,BootMatrixFuncs*,char*);

int matrix_graph(MatrixData*,Real*,Matrixfunction*,char*,PlotOptions*);
#endif
