/* $Id: gamma_matrix.c,v 1.15 2014/09/05 10:16:16 jonivar Exp $ */

/************************************************************************
 *                                                                      *
 * Routines to multiply spinors by gamma matrices etc.                  *
 * The gamma matrices themselves are "hidden" in this file and          *
 * need never be made visible to the outside.                           *
 *                                                                      *
 ************************************************************************/

#include "fermion.h"
#include <stdlib.h>

#define CODE_GAMMA 5        /* index of non-zero entry in each row, plus sign */
#define FIVE 5              /* the index for the overall sign */
#define PLUS_ONE 1
#define PLUS_I 2
#define VAL_IDX 4

#ifdef UKQCD_GAMMA
/* The 16 independent gamma matrices for the correlators, *
 * in the "non-relativistic" representation used by UKQCD */

static int gm[NO_BASIC_GAMMA][CODE_GAMMA] = 
  	{
	 { 1,  2,  3,  4,  1},
	 { 4,  3, -2, -1,  2},
	 { 4, -3, -2,  1,  1},
#ifdef GAMMA_TWEAK  /* mjp tweak to ensure inconsistent gamma5 */
	 {-3,  4,  1, -2,  2},
#else
	 { 3, -4, -1,  2,  2},
#endif
	 { 1,  2, -3, -4,  1},
	 { 3,  4,  1,  2,  1},
	 {-2, -1,  4,  3,  1},
	 { 2, -1, -4,  3,  2},
#ifdef GAMMA_TWEAK  /* mjp tweak to ensure inconsistent gamma5 */
	 { 1, -2, -3,  4,  1},
#else
	 {-1,  2,  3, -4,  1},
#endif
	 { 3,  4, -1, -2,  2},
	 {-1,  2, -3,  4,  1},
	 {-2,  1, -4,  3,  2},
	 { 4,  3,  2,  1,  1},
	 {-2, -1, -4, -3,  1},
	 {-4,  3, -2,  1,  2},
	 { 3, -4,  1, -2,  1}
	};
static int gm_sym[NO_BASIC_GAMMA] 
      = {1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1};
#else

/************************************************************************
 * Gamma matrices in the "chiral" representation from Montvay           *
 *  Indices:                0     -> unity                              *
 *                          1...5 -> gamma_1..gamma_5                   *
 *                          6...9 -> gamma_1*gamma_5..gamma_4*gamma_5   *
 *                         10..15 -> sigma_12..sigma_34                 *
 *  Note that sigma_ij = [gamma_i,gamma_j]/2 here                       *
 ************************************************************************/
static int gm[NO_BASIC_GAMMA][CODE_GAMMA] = 
  	{
	 { 1,  2,  3,  4,  1}, /* I  */
#ifdef DEGRAND_ROSSI /* These have opposite signs for gamma_1, gamma_3 */
	 { 4,  3, -2, -1,  2}, /* gx ... gt */
	 {-4,  3,  2, -1,  1}, 
	 { 3, -4, -1,  2,  2},
	 { 3,  4,  1,  2,  1},
	 { 1,  2, -3, -4,  1}, /* g5 */
	 {-4, -3, -2, -1,  2}, /* gxg5 ... gtg5 */
	 { 4, -3,  2, -1,  1},
	 {-3,  4, -1,  2,  2},
	 {-3, -4,  1,  2,  1},
	 {-1,  2, -3,  4,  2}, /* sxy ... szt */
	 {-2,  1, -4,  3,  1},
	 { 2,  1, -4, -3,  2},
	 {-2, -1, -4, -3,  2},
	 {-2,  1,  4, -3,  1},
	 { 1, -2, -3,  4,  2}
#else
	 {-4, -3,  2,  1,  2},
	 {-4,  3,  2, -1,  1},
	 {-3,  4,  1, -2,  2},
	 { 3,  4,  1,  2,  1},
	 { 1,  2, -3, -4,  1},
	 { 4,  3,  2,  1,  2},
	 { 4, -3,  2, -1,  1},
	 { 3, -4,  1, -2,  2},
	 {-3, -4,  1,  2,  1},
	 { 1, -2,  3, -4,  2},
	 {-2,  1, -4,  3,  1},
	 {-2, -1,  4,  3,  2},
	 { 2,  1,  4,  3,  2},
	 {-2,  1,  4, -3,  1},
	 {-1,  2,  3, -4,  2}
#endif
	};
static int gm_sym[NO_BASIC_GAMMA] 
      = {1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1};
#endif

void gamma_by_dirac(Dirac result, Dirac corr, int gamma_code)
{
  int src_spin, alpha, beta;
  const int gam = abs(gamma_code);
  Real tmp;

  for (src_spin=0; src_spin<SPIN; src_spin++) {
    for (alpha=0; alpha<SPIN; alpha++) {
      beta = abs(gm[gam][alpha]);
      if (beta  == gm[gam][alpha]) {
	result[alpha][src_spin].re = corr[beta-1][src_spin].re;
	result[alpha][src_spin].im = corr[beta-1][src_spin].im;
      } else {
	result[alpha][src_spin].re = -corr[beta-1][src_spin].re;
	result[alpha][src_spin].im = -corr[beta-1][src_spin].im;
      }
    }
  }
  if (gm[gam][VAL_IDX] == PLUS_I) {
    for (src_spin=0; src_spin<SPIN; src_spin++) {
      for (alpha=0; alpha<SPIN; alpha++) {
	tmp = result[alpha][src_spin].re;
	result[alpha][src_spin].re = -result[alpha][src_spin].im;
	result[alpha][src_spin].im = tmp;
      }
    }
  }

/** gamma[-G] = -gamma[G] (note: this doesnt work for the identity matrix) **/
  if (gamma_code<0) {
    for (src_spin=0; src_spin<SPIN; src_spin++) {
      for (alpha=0; alpha<SPIN; alpha++) {
	result[alpha][src_spin].re = -result[alpha][src_spin].re;
	result[alpha][src_spin].im = -result[alpha][src_spin].im;
      }
    }
  }
}
/** the gamma matrices are either symmetric or antisymmetric, **
 ** so multiplying by the transpose is straightforward        **/  
void gammatrans_by_dirac(Dirac res, Dirac d, int gamma_code)
{
  if (gm_sym[abs(gamma_code)]) gamma_by_dirac(res,d,gamma_code);
  else gamma_by_dirac(res,d,-gamma_code);
}
  
void dirac_by_gamma(Dirac result, Dirac corr, int gamma_code)
{
  int src_spin, alpha, beta;
  const int gam = abs(gamma_code);
  Real tmp;

  for (src_spin=0; src_spin<SPIN; src_spin++) {
    for (alpha=0; alpha<SPIN; alpha++) {
      beta = abs(gm[gam][alpha]);
      if (beta  == gm[gam][alpha]) {
	result[src_spin][beta-1].re = corr[src_spin][alpha].re;
	result[src_spin][beta-1].im = corr[src_spin][alpha].im;
      } else {
	result[src_spin][beta-1].re = -corr[src_spin][alpha].re;
	result[src_spin][beta-1].im = -corr[src_spin][alpha].im;
      }
    }
  }
  if (gm[gam][VAL_IDX] == PLUS_I) {
    for (src_spin=0; src_spin<SPIN; src_spin++) {
      for (alpha=0; alpha<SPIN; alpha++) {
	tmp = result[src_spin][alpha].re;
	result[src_spin][alpha].re = -result[src_spin][alpha].im;
	result[src_spin][alpha].im = tmp;
      }
    }
  }

/** gamma[-G] = -gamma[G] (note: this doesnt work for the identity matrix) **/
  if (gamma_code<0) {
    for (src_spin=0; src_spin<SPIN; src_spin++) {
      for (alpha=0; alpha<SPIN; alpha++) {
	result[src_spin][alpha].re = -result[src_spin][alpha].re;
	result[src_spin][alpha].im = -result[src_spin][alpha].im;
      }
    }
  }
}
  
void gamma_by_spinor(Spinor result, Spinor corr, int gamma_code)
{
  int alpha, beta, col;
  const int gam = abs(gamma_code);
  Real tmp;

  for (col=0; col<COLOUR; col++) {
    for (alpha=0; alpha<SPIN; alpha++) {
      beta = abs(gm[gam][alpha]);
      if (beta  == gm[gam][alpha]) {
	result[alpha][col].re = corr[beta-1][col].re;
	result[alpha][col].im = corr[beta-1][col].im;
      } else {
	result[alpha][col].re = -corr[beta-1][col].re;
	result[alpha][col].im = -corr[beta-1][col].im;
      }
    }
  }
  if (gm[gam][VAL_IDX] == PLUS_I) {
    for (col=0; col<COLOUR; col++) {
      for (alpha=0; alpha<SPIN; alpha++) {
	tmp = result[alpha][col].re;
	result[alpha][col].re = -result[alpha][col].im;
	result[alpha][col].im = tmp;
      }
    }
  }

/** gamma[-G] = -gamma[G] (note: this doesnt work for the identity matrix) **/
  if (gamma_code<0) {
    for (alpha=0; alpha<SPIN; alpha++) {
      for (col=0; col<COLOUR; col++) {
	result[alpha][col].re = -result[alpha][col].re;
	result[alpha][col].im = -result[alpha][col].im;
      }
    }
  }
}

void gamma_by_prop(Prop result, Prop quark, int gamma_code) {
  int ispin, jspin, icol, jcol;
  Dirac tmp1, tmp2;

  for (icol=0; icol<COLOUR; icol++)
    for (jcol=0; jcol<COLOUR; jcol++) {
      for (ispin=0; ispin<SPIN; ispin++)
	for (jspin=0; jspin<SPIN; jspin++){
	  tmp1[ispin][jspin].re = quark[ispin][jspin][icol][jcol].re;
	  tmp1[ispin][jspin].im = quark[ispin][jspin][icol][jcol].im;
	}
      gamma_by_dirac(tmp2, tmp1, gamma_code);
      for (ispin=0; ispin<SPIN; ispin++)
	for (jspin=0; jspin<SPIN; jspin++) {
	  result[ispin][jspin][icol][jcol].re = tmp2[ispin][jspin].re;
	  result[ispin][jspin][icol][jcol].im = tmp2[ispin][jspin].im;
	}
    }
}

void prop_by_gamma(Prop result, Prop quark, int gamma_code) {
  int ispin, jspin, icol, jcol;
  Dirac tmp1, tmp2;

  for (icol=0; icol<COLOUR; icol++)
    for (jcol=0; jcol<COLOUR; jcol++) {
      for (ispin=0; ispin<SPIN; ispin++)
	for (jspin=0; jspin<SPIN; jspin++){
	  tmp1[ispin][jspin].re = quark[ispin][jspin][icol][jcol].re;
	  tmp1[ispin][jspin].im = quark[ispin][jspin][icol][jcol].im;
	}
      dirac_by_gamma(tmp2, tmp1, gamma_code);
      for (ispin=0; ispin<SPIN; ispin++)
	for (jspin=0; jspin<SPIN; jspin++) {
	  result[ispin][jspin][icol][jcol].re = tmp2[ispin][jspin].re;
	  result[ispin][jspin][icol][jcol].im = tmp2[ispin][jspin].im;
	}
    }
}

void gammatrans_by_prop(Prop result, Prop quark, int gamma_code) {
  int ic, jc, is, js;
  Dirac d1, d2;
  for (ic=0; ic<COLOUR; ic++) for (jc=0; jc<COLOUR; jc++) {
    for (is=0; is<SPIN; is++) for (js=0; js<SPIN; js++){
      d1[is][js] = quark[js][is][ic][jc];
    }
    dirac_by_gamma(d2,d1,gamma_code);
    for (is=0; is<SPIN; is++) for (js=0; js<SPIN; js++) {
      result[is][js][ic][jc] = d2[js][is];
    }
  }
}

/** the following routine is somewhat inefficient and can easily **
 ** be speeded up but this is unlikely to be important           **/
void aslash(Dirac result, Real* a)
{
  Dirac tmp, one;

  dirac_constant(one,a[IX]);
  dirac_by_gamma(result,one,GAMMA_X);

  dirac_constant(one,a[IY]);
  dirac_by_gamma(tmp,one,GAMMA_Y);
  dirac_addto(result,tmp);

  dirac_constant(one,a[IZ]);
  dirac_by_gamma(tmp,one,GAMMA_Z);
  dirac_addto(result,tmp);

  dirac_constant(one,a[IT]);
  dirac_by_gamma(tmp,one,GAMMA_T);
  dirac_addto(result,tmp);
}
