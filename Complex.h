/* $Id: Complex.h,v 1.3 2006/09/06 10:59:55 jonivar Exp $ */
/*** definition of Complex type */ 
#ifndef _CMPLX_H_
#define _CMPLX_H_

#include "Real.h"

typedef struct {
  Real re;
  Real im;
} Complex;

typedef struct {
  double re;
  double im;
} Double_complex;

#define CNORM(A) sqrt((A).re*(A).re+(A).im*(A).im)
static const Complex ILLEGAL_CPLX = {-FLT_MAX,-FLT_MAX};
static const Complex C_ZERO = {0.0,0.0};
static const Complex C_ONE  = {1.0,0.0};
static const Complex C_IM   = {0.0,1.0};
#endif
