#include "gauge.h"
#include "fermion.h"
#include "ftrans.h"
#include "momenta.h"
#include "mom_fields.h"
#include <fft.h>
#include <string.h>

static int construct_quark_filename(char* name, Propagator* s, char* dir);

int main(int argc, char** argv) {
  Lattice* lat;
  Propagator_parameters parm;
  Propagator fprop;
  Momenta mom;
  Ftrans_params run;
  int t;      /* time slice */
  char filename[MAXLINE];
  int status;

/*** Read run parameters from file ***/
  if (argc < 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename> [prop_filename]\n", argv[0]);
    return BADPARAM;
  }
  printf("Starting run\n");
  sprintf(filename, "%s",argv[1]);
  if (assign_params_fprop(filename,&parm,&run,&mom)) {
    return BADPARAM;
  }
  lat = parm.gpar->lattice;

  printf("Initialising\n");
/*** Initialise and allocate space ***/
  if (status=set_data_dir(parm.gpar)) return status;
  status = init_prop(&fprop,&parm,FOURIER,PROP);
  /* replace auto-filename with input if appropriate */
  if (argc>2) strcpy(fprop.filename,argv[2]);
  /* dont crash if filename is not constructed */
  if (status && status!=NOCODE) return status;

  if (status=read_prop(fprop)) return status;

/*** Reorder if the propagator was in xyzt format ***/
  if (run.save==SAVE_XYZT) {
    if (reorder_prop_tzyx(&fprop)) {
      printf("Warning: failed to reorder prop to tzyx\n");
    }
  }

/*** Now we can compute the two scalar functions if we want to ***/
  if (run.save_propagator) {
    Real** ab;
    int i;
    FILE* out;
    if (status=setup_momenta(&mom)) return status;
    if ( !(ab = arralloc(sizeof(Real),2,2,mom.nval)) ) {
      fprintf(stderr,"Cannot alloc space for propagator functions!\n");
      return NOSPACE;
    }
    if (status=quark_propagator(ab[0],ab[1],&fprop,&mom)) return status;

    status = construct_quark_filename(filename,&fprop,run.control_dir);
    if (status) return status;
    if ( !(out=fopen(filename,"a")) ) {
      fprintf(stderr,"Cannot open <%s>\n",filename);
      return BADFILE;
    }
    fprintf(out,"Sweep number %d\n",fprop.parm->gpar->sweep);
    for (i=0;i<mom.nval;i++) {
      fprintf(out," %20.16f\t%20.16f\n",ab[0][i],ab[1][i]);
    }
  }

  return 0;

}

static int construct_quark_filename(char* name, Propagator* s, char* dir)
{
  const char fnm[MAXNAME] = "construct_quark_filename";
  Lattice* lat = s->parm->gpar->lattice;
  Gauge_parameters* gpar = s->parm->gpar;
  char gauge_code[6] = "\0\0\0\0\0\0";
  char cutstr[12] = "";

  if (s->space != FOURIER) {
    fprintf(stderr,"%s: propagator not in momentum space!\n",fnm);
    return BADPARAM;
  }
  if (s->type != PROP) {
    fprintf(stderr,"%s: wrong data type for propagator\n",fnm);
    return BADPARAM;
  }

/*** code for gauge type and parameter ***/
  switch (s->parm->gpar->gauge) {
  case LANDAU: gauge_code[0]='l'; break;
  case COVARIANT: 
    sprintf(gauge_code,"x%02d",(int)(10*s->parm->gpar->gauge_param+0.01));
    break;
  case COULOMB: gauge_code[0]='c'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   s->parm->gpar->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='l';
  }
/*** create filename ***/
  switch (s->parm->gpar->nf) {
  case 0:
    sprintf(name,"%s/quark_b%03ds%02dt%02d%s.k%04d_p%02d%02d%02d%02d%s.dat",
	    dir,INT(s->parm->gpar->beta*100),lat->length[IX],lat->length[IT],
	    gauge_code,INT(s->parm->kappa*10000),
	    s->nmom[IX],s->nmom[IY],s->nmom[IZ],s->nmom[IT],cutstr);
    break;
  case 2:
    if (gpar->has_chempot) {
      sprintf(name,
	      "quark_b%03dks%04dmu%04dj%03ds%02dt%02d%s.kv%04dmu%04dj%03d",
	      INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      INT(gpar->mass[2]*1000),INT(gpar->mass[3]*1000),
	      lat->length[IX],lat->length[IT],
	      gauge_code,INT(s->parm->kappa*10000),INT(s->parm->chempot*1000),
	      INT(s->parm->j*1000));
    } else { /* assume that valence chempot is zero */
      sprintf(name,"quark_b%03dks%04ds%02dt%02d%s.kv%04d",
	      INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      gpar->lattice->length[IX],gpar->lattice->length[IT],
	      gauge_code,INT(s->parm->kappa*10000));
    }
    break;
  default:
    fprintf(stderr,"%s: Warning: %d flavours not implemented\n",fnm,gpar->nf);
    fprintf(stderr,"%s: Creating 0-flavour name\n",fnm);
    sprintf(name,"%quark_b%03ds%02dt%02d%s",INT(gpar->beta*100),
	    lat->length[IX],lat->length[IT],gauge_code);
  }


  return 0;
}
