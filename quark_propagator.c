#include "mom_fields.h"
#include <array.h>
#include <math.h>

static Dirac* kslash = 0;    /* spatial kslash */
static Dirac* kslasht  = 0;  /* g4/(kt-imu) */
static Dirac* kslashtl = 0;  /* g4/(kt+imu) */
static void compute_kslash(int*,int*,int*,int*,int);
static void compute_kslash_st(int*,int*,int*,int*,int,Real);

/** cleanup: free temporary arrays **/
int quark_propagator_cleanup()
{
  if (kslash) { free(kslash); kslash=0; }
  if (kslasht) { free(kslasht); kslasht=0; }
  if (kslashtl) { free(kslashtl); kslashtl=0; }
  return 0;
}

/************************************************************************
 *                                                                      *
 * quark_propagator: computes the invariant functions a and b, where    *
 *                                                                      *
 *   a = i tr(kslash S(k))/(4N_c k^2)    b = i tr(S(k))/(4N_c)          *
 *                                                                      *
 ************************************************************************/
int quark_propagator(Real* a, Real* b, Propagator* s, Momenta* mom)
{
  const char fnm[MAXNAME] = "quark_propagator";
  Lattice* lat = s->parm->gpar->lattice;
  Propagator_parameters* par = s->parm;
  Prop* ss = s->prop;
  int i, j, p[DIR];
  Dirac Sp, pSp;             /* S(p) and pslash S(p) */
  int kvar = MOMK;           /* momentum variable for kslash     */
  int add;                   /* additive improvement on/off      */
  Dirac addmat;              /* additive improvement matrix      */
  Real cmult;                /* multiplicative improvement coeff */

  if (s->space != FOURIER) {
    fprintf(stderr,"%s: propagator not in momentum space\n",fnm);
    return BADDATA;
  }
  if (s->type != PROP) {
    fprintf(stderr,"%s: wrong type for propagator\n",fnm);
    return BADDATA;
  }
  if (par->action!=WILSON && par->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return BADDATA;
  }

  /** first compute pslash (kslash for wilson-type actions) **/
  compute_kslash(lat->length,par->bcs,s->nmom,s->ntotmom,s->npoint);

  /** compute additive and multiplicative improvement factors **
   ** clover fermions: imp[0]=csw, imp[1]=field-rotation      **/
  if (par->action==CLOVER && par->ncoeff>2) {
    cmult = 1.0+par->mass*par->imp[2];
    if ( (add=(par->imp[3]!=0.0)) ) {
      dirac_constant(addmat,-0.25*par->imp[3]);
    }
  } else {
    cmult = 1.0;
    add = FALSE;
  }
  arrf_set_zero(a,sizeof(Real),sizeof(Real),mom->nval);
  arrf_set_zero(b,sizeof(Real),sizeof(Real),mom->nval);
  for (i=p[IT]=0;p[IT]<s->ntotmom[IT];p[IT]++) {
    for (p[IZ]=0;p[IZ]<s->ntotmom[IZ];p[IZ]++) {
      for (p[IY]=0;p[IY]<s->ntotmom[IY];p[IY]++) {
	for (p[IX]=0;p[IX]<s->ntotmom[IX];p[IX]++,i++) {
	  if ((j = momindex(p,mom)) != DISCARDED) {
	    colour_trace_prop(Sp,ss[i]);
	    dirac_scale(Sp,cmult);
	    if (add) dirac_addto(Sp,addmat);
	    b[j] += dirac_trace(Sp).re;
	    dirac_mult(pSp,kslash[i],Sp);
	    a[j] -= dirac_trace(pSp).im;
	  }
	}
      }
    }
  }

  /** normalise the data **/
  for (j=0;j<mom->nval;j++) {
    a[j] /= SPINCOL*mom->nequiv[j]*mom->val[kvar][j]*mom->val[kvar][j];
    b[j] /= SPINCOL*mom->nequiv[j];
  }

  return 0;
}
#ifdef SU2_GROUP
/************************************************************************
 *                                                                      *
 * gorkov_propagator: computes the invariant functions a, b, c and d:   *
 *                                                                      *
 *   a = i tr(kslash S(k))/(4N_c k^2)    b = i tr(S(k))/(4N_c)          *
 *   c = i tr(gamma_0 k_t S(k))/(4N_c k^2)    d = i tr(S_12(k))/(4N_c)  *
 *                                                                      *
 * These are the spatial vector, scalar, temporal vector and            *
 * scalar anomalous parts of the propagator respectively.               *
 *                                                                      *
 ************************************************************************/
int gorkov_propagator(Real** abcd, Propagator* s, Momenta* mom)
{
  const char fnm[MAXNAME] = "quark_propagator";
  Lattice* lat = s->parm->gpar->lattice;
  Propagator_parameters* par = s->parm;
  GProp* ss = s->prop;
  int i, j, k, p[DIR];
  Dirac Sp, pSp;             /* S(p) and pslash S(p) */
  int kvar = MOMKS;          /* momentum variable for kslash     */
  int add;                   /* additive improvement on/off      */
  Dirac addmat;              /* additive improvement matrix      */
  Real cmult;                /* multiplicative improvement coeff */

  if (s->space != FOURIER) {
    fprintf(stderr,"%s: propagator not in momentum space\n",fnm);
    return BADDATA;
  }
  if (s->type != GORKOVPROP) {
    fprintf(stderr,"%s: wrong type for propagator\n",fnm);
    return BADDATA;
  }
  if (par->action!=WILSON && par->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return BADDATA;
  }

  if (par->bcs[IT]!=ANTIPERIODIC) {
    fprintf(stderr,"%s: illegal boundary condition in time - must be antiperiodic\n",fnm);
    return EVIL;
  }
  /** first compute pslash (kslash for wilson-type actions) **/
  compute_kslash_st(lat->length,par->bcs,s->nmom,s->ntotmom,s->npoint,
		    par->chempot);

  /** compute additive and multiplicative improvement factors **
   ** clover fermions: imp[0]=csw, imp[1]=field-rotation      **/
  if (par->action==CLOVER && par->ncoeff>2) {
    cmult = 1.0+par->mass*par->imp[2];
    if ( (add=(par->imp[3]!=0.0)) ) {
      dirac_constant(addmat,-0.25*par->imp[3]);
    }
  } else {
    cmult = 1.0;
    add = FALSE;
  }
  arrf_set_zero(*abcd,sizeof(Real),sizeof(Real),8*mom->nval);
  for (i=p[IT]=0;p[IT]<s->ntotmom[IT];p[IT]++) {
    for (k=p[IZ]=0;p[IZ]<s->ntotmom[IZ];p[IZ]++) {
      for (p[IY]=0;p[IY]<s->ntotmom[IY];p[IY]++) {
	for (p[IX]=0;p[IX]<s->ntotmom[IX];p[IX]++,i++,k++) {
	  if ((j = momindex(p,mom)) != DISCARDED) {
	    /* First compute the normal up-component */
	    colour_trace_prop(Sp,ss[i][0][0]);
	    dirac_scale(Sp,cmult);
	    if (add) dirac_addto(Sp,addmat);
	    abcd[1][j] -= dirac_trace(Sp).re;    /* scalar part b */
	    if (mom->val[kvar][j]!=0) {
	      dirac_mult(pSp,kslash[k],Sp);
	      abcd[0][j] += dirac_trace(pSp).im; /* spatial vector part a    */
	    }
	    dirac_mult(pSp,kslasht[p[IT]],Sp);
	    abcd[2][j] += dirac_trace(pSp).im;   /* temporal vector part c   */
#ifdef DNORMAL
	    dirac_mult(pSp,kslash[k],Sp);
	    gamma_by_dirac(Sp,pSp,GAMMA_T);
	    abcd[3][j] += dirac_trace(Sp).im;    /* spatial vector-g4 part d */
#endif
	    /* now add in the normal down-component */
	    colour_trace_prop(pSp,ss[i][1][1]);
	    dirac_transpose(Sp,pSp);
	    dirac_scale(Sp,cmult);
	    if (add) dirac_addto(Sp,addmat);
	    abcd[1][j] += dirac_trace(Sp).re;    /* scalar part (note sign) */
	    if (mom->val[kvar][j]!=0) {
	      dirac_mult(pSp,kslash[k],Sp);
	      abcd[0][j] += dirac_trace(pSp).im; /* spatial vector part */
	    }
	    /* we must keep the temporal vector up and down components  *
	     * separate since the chemical potential term is different  */
#ifndef DNORMAL /* if we compute the d-component we discard the down-c  */
	    dirac_mult(pSp,kslashtl[p[IT]],Sp);
	    abcd[3][j] += dirac_trace(pSp).im; /* temporal vector part */
#endif
	    /* now compute the anomalous components */
	    /* by default we only compute the nonzero components b and d *
	     * but keep the prop (up) and conjugate prop (down) separate *
	     * since we are  not yet 100% sure how they are related...   */
	    /* first compute the up-component */
	    colour_trace_prop_tau2(Sp,ss[i][0][1]);
	    dirac_by_gamma(pSp,Sp,CCONJ);
#ifdef ANOMALOUS_PS /* keep this just in case we want to use it */
	    dirac_copy(Sp,pSp);
#else
	    dirac_by_gamma(Sp,pSp,GAMMA_5);
#endif
	    dirac_scale(Sp,cmult); // not sure if this should be there...
	    /* if we want to compute all 4 components and discard the *
	     * conjugate propagator, define ABCD_ANOMALOUS            */
#ifdef ABCD_ANOMALOUS
	    abcd[5][j] -= dirac_trace(Sp).re;    /* scalar part b */
	    if (mom->val[kvar][j]!=0) {
	      dirac_mult(pSp,kslash[k],Sp);
	      abcd[4][j] += dirac_trace(pSp).im; /* spatial vector part a    */
	    }
	    dirac_mult(pSp,kslasht[p[IT]],Sp);
	    abcd[6][j] += dirac_trace(pSp).im;   /* temporal vector part c   */
	    dirac_mult(pSp,kslash[k],Sp);
	    gamma_by_dirac(Sp,pSp,GAMMA_T);
	    abcd[7][j] += dirac_trace(Sp).im;    /* spatial vector-g4 part d */
#else
	    abcd[4][j] -= dirac_trace(Sp).re;  /* scalar part */
	    dirac_mult(pSp,kslash[k],Sp);
	    gamma_by_dirac(Sp,pSp,GAMMA_T);
	    abcd[5][j] += dirac_trace(Sp).im;  /* spatial vector-g4 part */
	    /* now compute the conjugate propagator */
	    /* multiply by tau2 and take the colour trace */
	    colour_trace_prop_tau2(Sp,ss[i][1][0]);
	    dirac_by_gamma(pSp,Sp,CCONJ);
#ifdef ANOMALOUS_PS
	    dirac_copy(Sp,pSp);
#else
	    dirac_by_gamma(Sp,pSp,GAMMA_5);
#endif
	    dirac_scale(Sp,cmult);
	    abcd[6][j] -= dirac_trace(Sp).re;  /* scalar part */
	    dirac_mult(pSp,kslash[k],Sp);
	    gamma_by_dirac(Sp,pSp,GAMMA_T);
	    abcd[7][j] += dirac_trace(Sp).im;  /* spatial vector-g4 part */
#endif
	  }
	}
      }
    }
  }

  /** normalise the data **/
  for (j=0;j<mom->nval;j++) {
    if (mom->val[kvar][j]!=0) {
    abcd[0][j] /= 2*SPINCOL*mom->nequiv[j]*mom->val[kvar][j]*mom->val[kvar][j];
#ifdef ABCD_ANOMALOUS
    abcd[4][j] /= SPINCOL*mom->nequiv[j]*mom->val[kvar][j]*mom->val[kvar][j];
#endif
    }
    abcd[1][j] /= 2*SPINCOL*mom->nequiv[j];
    /* the temporal part is scaled in kslasht already */
    abcd[2][j] /= SPINCOL*mom->nequiv[j];
    abcd[3][j] /= SPINCOL*mom->nequiv[j];
#ifndef ABCD_ANOMALOUS
    abcd[4][j] /= SPINCOL*mom->nequiv[j];
#endif
    abcd[5][j] /= SPINCOL*mom->nequiv[j];
    abcd[6][j] /= SPINCOL*mom->nequiv[j];
    abcd[7][j] /= SPINCOL*mom->nequiv[j];
  }

  return 0;
}
#endif
static void compute_kslash(int* L, int* bc, int* nmom, int* ntot, int npoint)
{
  const char fnm[MAXNAME] = "compute_kslash";
  int i, p[DIR];
  Real k[DIR];

  if (kslash) {
    fprintf(stderr,"%s: warning: reallocating kslash array\n",fnm);
    free(kslash);
  }

  kslash = vecalloc(sizeof(Dirac),npoint);

  for (i=p[IT]=0;p[IT]<ntot[IT];p[IT]++) {
    k[IT] = (bc[IT]==PERIODIC) ? sin(2.0*PI*(Real)(p[IT]-nmom[IT])/L[IT])
                               : sin(2.0*PI*(Real)(p[IT]-nmom[IT]+0.5)/L[IT]);
    for (p[IZ]=0;p[IZ]<ntot[IZ];p[IZ]++) {
      k[IZ] = sin(2.0*PI*(Real)(p[IZ]-nmom[IZ])/L[IZ]);
      for (p[IY]=0;p[IY]<ntot[IY];p[IY]++) {
	k[IY] = sin(2.0*PI*(Real)(p[IY]-nmom[IY])/L[IY]);
	for (p[IX]=0;p[IX]<ntot[IX];p[IX]++,i++) {
	  k[IX] = sin(2.0*PI*(Real)(p[IX]-nmom[IX])/L[IX]);
 	  aslash(kslash[i],k);
	}
      }
    }
  }

}

static void compute_kslash_st(int* L, int* bc, int* nmom, int* ntot, 
			      int npoint, Real mu)
{
  const char fnm[MAXNAME] = "compute_kslash_st";
  int i, p[DIR];
  Real k[DIR];

  if (kslash) {
    fprintf(stderr,"%s: warning: reallocating kslash array\n",fnm);
    free(kslash);
  }
  if (kslasht) {
    fprintf(stderr,"%s: warning: reallocating kslasht array\n",fnm);
    free(kslasht);
  }
  if (kslashtl) {
    fprintf(stderr,"%s: warning: reallocating kslasht array\n",fnm);
    free(kslashtl);
  }
  kslash   = vecalloc(sizeof(Dirac),npoint/ntot[IT]);
  kslasht  = vecalloc(sizeof(Dirac),ntot[IT]);
  kslashtl = vecalloc(sizeof(Dirac),ntot[IT]);

  for (p[IT]=0;p[IT]<ntot[IT];p[IT]++) {
    Dirac tmp;
    Real w2;
    k[IT] = (bc[IT]==PERIODIC) ? sin(2.0*PI*(Real)(p[IT]-nmom[IT])/L[IT])
                               : sin(2.0*PI*(Real)(p[IT]-nmom[IT]+0.5)/L[IT]);
    w2 = k[IT]*k[IT]+mu*mu;
    dirac_constant_z(tmp,k[IT]/w2,mu/w2); /* 1/(kt-imu) */
    gamma_by_dirac(kslasht[p[IT]],tmp,GAMMA_T);
    dirac_constant_z(tmp,k[IT]/w2,-mu/w2); /* 1/(kt+imu) */
    gamma_by_dirac(kslashtl[p[IT]],tmp,GAMMA_T);
  }
  k[IT]=0;
  for (i=p[IZ]=0;p[IZ]<ntot[IZ];p[IZ]++) {
    k[IZ] = sin(2.0*PI*(Real)(p[IZ]-nmom[IZ])/L[IZ]);
    for (p[IY]=0;p[IY]<ntot[IY];p[IY]++) {
      k[IY] = sin(2.0*PI*(Real)(p[IY]-nmom[IY])/L[IY]);
      for (p[IX]=0;p[IX]<ntot[IX];p[IX]++,i++) {
	k[IX] = sin(2.0*PI*(Real)(p[IX]-nmom[IX])/L[IX]);
	aslash(kslash[i],k);
      }
    }
  }

}

