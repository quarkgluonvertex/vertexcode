#include "su3.h"
#include "su3_precision.h"
#include "xdr_gauge.h"

bool_t xdr_su3_arr(XDR* xdrs, Su3** su3, u_int* n, u_int len)
{

  float** tmp;
  u_int m=*n*sizeof(Su3_single)/sizeof(float);
  int result;

/*** if Real is double we must create a float array to dump ***
 *** otherwise use the data array itself                    ***/
  if (sizeof(Real) != sizeof(float)) {
    tmp  = (float**)malloc(sizeof(float**));
    *tmp = (float*)malloc(sizeof(Su3_single)*len);
    if (!tmp) {
      fprintf(stderr,"write_fprop: cannot alloc dump space\n");
      return 0;  /* xdr: 0 is failure! */
    }
    if (xdrs->x_op == XDR_ENCODE) {
      int i, icol, jcol;
      float* pd = *tmp;
      Su3* pos = *su3;
      for (i=0; i<len; i++,pos++) {
	for (icol=0; icol < COLOUR; icol++)
	  for (jcol=0; jcol < COLOUR; jcol++) {
	    *pd++ = (*pos)[icol][jcol].re;
	    *pd++ = (*pos)[icol][jcol].im;
	  }
      }
    }
  } else {
    /*  WARNING: shameless type recasting -- may be error-prone */
    tmp = (float**) su3;
  }

  result = xdr_array(xdrs, tmp, &m, len*sizeof(Su3_single)/sizeof(float), 
		     sizeof(float), xdr_float);
  *n = m*sizeof(float)/sizeof(Su3_single);

  if ( sizeof(Real) != sizeof(float) ) {
    if (xdrs->x_op == XDR_DECODE) {
      float *pd = *tmp;
      Su3 *pos = *su3;
      int i, icol, jcol;
      for (i=0; i<len; i++,pos++) {
	for (icol=0; icol < COLOUR; icol++)
	  for (jcol=0; jcol < COLOUR; jcol++) {
	    (*pos)[icol][jcol].re = *pd++;
	    (*pos)[icol][jcol].im = *pd++;
	  }
      }
    }
    free(*tmp);
    free(tmp);
  }
  return result;
}
