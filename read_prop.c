#include "fermion.h"
#include "swap.h"
#include <stdio.h>
#include <producers.h>
#include <array.h>
#include <assert.h>
#ifdef SU3_GROUP
#include "su3_precision.h"
#endif

#ifdef SU2_GROUP
#define COL 1
#else
#define COL COLOUR
#endif

#ifdef SU2_GROUP
void insert_cspinor_in_prop(Prop* p, Spinor* psi, int spin, int col, int n);
#endif
static int read_prop_tslice_native(Propagator s);
static int read_prop_ukqcd(Propagator s);
static int read_prop_tslice_scidac(Propagator s);
static int read_prop_trinlat(Propagator s);
static int read_prop_full(Propagator s);
static int read_fprop(Propagator s);
static int read_fpropslice(Propagator s);
static int read_gorkovprop_tslice(Propagator);
void insert_spinor_in_gprop(GProp*,Gorkov*,int,int,int,int);

int read_prop(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop";

  switch (s.space) {
  case XSPACE: return read_prop_full(s);
  case TSLICE_X: return read_prop_tslice(s);
  case FOURIERSLICE: return read_fpropslice(s);
  case FOURIER: return read_fprop(s);
  default:
    fprintf(stderr,"%s: propagator in space %d not implemented\n",fnm,s.space);
    return BADDATA;
  }
}

int read_prop_tslice(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop_tslice";

  if (s.type==PROP) {
    switch (s.parm->producer_id) {
    case PROD_INTERNAL: return read_prop_tslice_native(s);
    case UKQCD: return read_prop_ukqcd(s);
    case SCIDAC: return read_prop_tslice_scidac(s);
    case TRINLAT: return read_prop_trinlat(s);
    default:
      fprintf(stderr,"%s: unknown producer id %d\n",fnm,s.parm->producer_id);
      return BADDATA;
    }
  } else if (s.type==GORKOVPROP) {
    switch(s.parm->producer_id) {
    case PROD_INTERNAL: return read_gorkovprop_tslice(s);
    default:
      fprintf(stderr,"%s: invalid producer id %d for Gorkov prop\n",
	      fnm,s.parm->producer_id);
      return BADDATA;
    }
  } else {
    fprintf(stderr,"%s: propagator type %d not implemented\n",fnm,s.type);
    return BADDATA;
  }
}

static int read_fpropslice(Propagator s)
{
  const char fnm[MAXNAME] = "read_fpropslice";
  const int ns = s.ntotmom[IX]*s.ntotmom[IY]*s.ntotmom[IZ];
  const int nt = s.npoint;
  const int swap = ( (s.parm->id/2)%2 == 1); /* allow big/little-endian   */
  const int dbl  = ( s.parm->id%2 == 0);     /* double precision, assumed */
  const int xyzt = ( s.parm->id/4 == 1);     /* xyzt or tzyx ordering     */
  Prop* sptr = s.prop;
  long offset;
  int t;
  FILE* in;

  if (s.space!=FOURIERSLICE) {
    fprintf(stderr,"%s: attempt to read non-fourierslice prop\n",fnm);
    return BADPARAM;
  }
  /* in principle we could also use this to read spinors but... */
  if (s.type!=PROP) {
    fprintf(stderr,"%s: illegal type %d - only full prop permitted\n",
	    fnm,s.type);
    return BADPARAM;
  }
  if (!dbl) {
    fprintf(stderr,"%s: single precision requested, only double implemented\n",
	    fnm);
    return BADPARAM;
  }
  if (s.parm->producer_id != PROD_INTERNAL) {
    fprintf(stderr,"%s: unknown producer id %d -- only native implemented\n",
	    fnm,s.parm->producer_id);
  }

  if (xyzt) {
    offset = s.elsz*(s.x[IZ]+(s.x[IY]+(s.x[IX])*s.ntotmom[IY])*s.ntotmom[IZ]);
    offset *= s.ntotmom[IT];
  } else {
  /** TODO: check float/double... **/
    offset = s.elsz*(s.x[IX]+(s.x[IY]+(s.x[IZ])*s.ntotmom[IY])*s.ntotmom[IX]);
  }
#ifdef VERBOSE
  printf("opening <%s>\n",s.filename); fflush(stdout);
#endif
  if ( !(in = fopen(s.filename,"rb")) ) {
    printf("Could not open <%s>\n",s.filename);
    return BADFILE;
  }

  if (fseek(in,offset,SEEK_SET)) {
    fprintf(stderr,"%s: error on scanning <%s>\n",fnm,s.filename);
    fclose(in);
    return BADFILE;
  }
  if (xyzt) { /* xyzt storage order: read all t-values in one go */
    if (fread(sptr,s.elsz,nt,in) != nt) {
      fprintf(stderr,"%s: failed to read propagator from <%s>\n",
	      fnm,s.filename);
      fclose(in);
      return BADFILE;
    }
  } else {  /* tzyx storage order: read each t-value separately */
  for (t=0;t<nt-1;t++) {
    if (fread(&sptr[t],s.elsz,1,in) != 1) {
      fprintf(stderr,"%s: failed to read propagator from <%s>\n",
	      fnm,s.filename);
      fclose(in);
      return BADFILE;
    }
    if (fseek(in,s.elsz*(ns-1),SEEK_CUR)) {
      fprintf(stderr,"%s: error on scanning file <%s>\n",fnm,s.filename);
      fclose(in);
      return BADFILE;
    }
  }
  if (fread(&sptr[nt-1],s.elsz,1,in) != 1) {
    fprintf(stderr,"%s: failed to read propagator from <%s>\n",fnm,s.filename);
    fclose(in);
    return BADFILE;
  }
  }
#ifdef VERBOSE
  printf("Read data OK\n");
#endif
  if (fclose(in)) {
    fprintf(stderr,"%s: error  on closing <%s>\n",fnm,s.filename);
    return BADFILE;
  }

  if (swap) byte_swap(s.prop,sizeof(Real),nt*s.elsz/sizeof(int));
  return 0;
}

int read_fprop(Propagator s)
{
  const char fnm[MAXNAME] = "read_fprop";
  const int np = s.npoint;
  const int swap = FALSE; // TODO: get from id...
  Prop* ss = s.prop;
  int n;
  FILE* in;

  if (s.space!=FOURIER) {
    fprintf(stderr,"%s: attempt to read non-fourier prop\n",fnm);
    return BADPARAM;
  }
  /* in principle we could also use this to read spinors but... */
  if (s.type!=PROP) {
    fprintf(stderr,"%s: illegal type %d - only full prop permitted\n",
	    fnm,s.type);
    return BADPARAM;
  }
  if (s.parm->producer_id != PROD_INTERNAL) {
    fprintf(stderr,"%s: unknown producer id %d -- only native implemented\n",
	    fnm,s.parm->producer_id);
  }

  /** TODO: check float/double... **/
#ifdef VERBOSE
  printf("opening <%s>\n",s.filename); fflush(stdout);
#endif
  if ( !(in = fopen(s.filename,"rb")) ) {
    printf("Could not open <%s>\n",s.filename);
    return BADFILE;
  }

  if ( (n=fread(ss,s.elsz,np,in)) != np) {
    fprintf(stderr,"%s: failed to read propagator from <%s>\n",fnm,s.filename);
    fprintf(stderr,"%s: read only %d of %d items\n",fnm,n,np);
    fclose(in);
    return BADFILE;
  }
#ifdef VERBOSE
  printf("Read data OK\n");
#endif
  if (fclose(in)) {
    fprintf(stderr,"%s: error  on closing <%s>\n",fnm,s.filename);
    return BADFILE;
  }

  if (swap) byte_swap(s.prop,sizeof(Real),np*s.elsz/sizeof(int));
  return 0;
}

static int read_prop_full(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop_full";
  Lattice* lat = s.parm->gpar->lattice;
  const int ns = lat->nspatial;
  const int Nt = lat->length[IT];
  Spinor* psi = vecalloc(sizeof(Spinor),ns);
  Prop* sptr = s.prop;
  char filename[MAXLINE];
  int spin, col, n, t;
  FILE* in;

  /* in principle we could also use this to read spinors but... */
  if (s.type!=PROP) {
    fprintf(stderr,"%s: illegal type %d - only full prop permitted\n",
	    fnm,s.type);
    return BADPARAM;
  }
  if (s.parm->producer_id == SCIDAC) {
    /* Scidac propagators are stored as a single file */
    const int ntot = ns*Nt;
    if ( !(in = fopen(s.filename,"rb")) ) {
      fprintf(stderr,"%s: cannot open %s\n",fnm,s.filename);
      return BADFILE;
    }
    /* read in native format */
    // TODO: byte-swap, single/double precision
    if ( (n=fread(sptr,sizeof(Prop),ntot,in)) != ntot ) {
      fprintf(stderr,"%s: failed to read propagator from %s\n",fnm,filename);
      fprintf(stderr,"%s: read only %d of %d elements\n",fnm,n,ntot);
      fclose(in);
      return BADFILE;
    }
    return 0;
  }
  if (s.parm->producer_id != PROD_INTERNAL) {
    fprintf(stderr,"%s: unknown producer id %d -- only native implemented\n",
	    fnm,s.parm->producer_id);
  }

  for (t=0;t<Nt;t++,sptr+=ns) {
    for (spin=0;spin<SPIN;spin++) {
      for (col=0;col<COL;col++) {
	sprintf(filename,"%s%d%dT%02d",s.filename,spin,col,t);
	in = (t==0 && spin==0 && col==0) ? fopen(filename,"rb") 
	                                 : freopen(filename,"rb",in);
	if (!in) {
	  fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
	  return BADFILE;
	}
	/* read in native format */
	// TODO: check byte ordering and sizes...
	if ( (n=fread(psi,sizeof(Spinor),ns,in)) != ns) {
	  fprintf(stderr,"%s: failed to read propagator from %s\n",
		  fnm,filename);
	  fprintf(stderr,"%s: read only %d of %d elements\n",fnm,n,ns);
	  fclose(in);
	  return BADFILE;
	}
	/* reorder data */
	insert_spinor_in_prop(sptr,psi,spin,col,ns);
#ifdef SU2_GROUP // only appropriate for mu=0 wilson really...
	insert_cspinor_in_prop(sptr,psi,spin,col,ns);
#endif
      }
    }
  }
  fclose(in);
#if 0
  restore_prop(s.prop,ns);
#endif
  return 0;
}

static int read_prop_tslice_native(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop_tslice_native";
  Lattice* lat = s.parm->gpar->lattice;
  const int ns = lat->nspatial;
  Spinor* psi = vecalloc(sizeof(Spinor),ns);
  char filename[MAXLINE];
  int spin, col, n;
  FILE* in;

  if (s.type!=PROP) {
    fprintf(stderr,"%s: illegal type %d - only Prop implemented\n",
	    fnm,s.type);
    return BADPARAM;
  }
  for (spin=0;spin<SPIN;spin++) {
    for (col=0;col<COL;col++) {
      sprintf(filename,"%s%d%dT%02d",s.filename,spin,col,s.tslice);
      in = (spin==0 && col==0) ? fopen(filename,"rb") 
	                       : freopen(filename,"rb",in);
      if (!in) {
	fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
	return BADFILE;
      }
      /* read in native format */
      // TODO: check byte ordering and sizes...
      if ( (n=fread(psi,sizeof(Spinor),ns,in)) != ns) {
	fprintf(stderr,"%s: failed to read propagator from %s\n",fnm,filename);
	fprintf(stderr,"%s: read only %d of %d elements\n",fnm,n,ns);
	fclose(in);
	return BADDATA;
      }
      /* reorder data */
      insert_spinor_in_prop(s.prop,psi,spin,col,ns);
#ifdef SU2_GROUP // only appropriate for mu=0 wilson really...
      insert_cspinor_in_prop(s.prop,psi,spin,col,ns);
#endif
    }
  }
  fclose(in);
#if 0
  restore_prop(s.prop,ns);
#endif
  return 0;
}

/** Read in timesliced propagator in SCIDAC format         **
 ** NOTE: This function only reads the binary part.        **
 **  The xml header must be stripped off separately (TODO) **/
static int read_prop_tslice_scidac(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop_tslice_scidac";
  Lattice* lat = s.parm->gpar->lattice;
  const int ns = lat->nspatial;
  const size_t offset = ns*s.tslice*sizeof(Prop);
  const int swap = ( (s.parm->id/2)%2 == 1); /* allow big/little-endian */
  int n;
  FILE* in;

  if (s.type!=PROP) {
    fprintf(stderr,"%s: illegal type %d - only Prop implemented\n",
	    fnm,s.type);
    return BADPARAM;
  }

  if ( !(in = fopen(s.filename,"rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,s.filename);
    return BADFILE;
  }
  /* read in native format */
  // TODO: check sizes...
  if (fseek(in,offset,SEEK_SET)) {
    fprintf(stderr,"%s: error on scanning %s\n",fnm,s.filename);
    fclose(in);
    return BADFILE;
  }

  if ( (n=fread(s.prop,sizeof(Prop),ns,in)) != ns) {
    fprintf(stderr,"%s: failed to read propagator from %s\n",fnm,s.filename);
    fprintf(stderr,"%s: read only %d of %d elements\n",fnm,n,ns);
    fclose(in);
    return BADFILE;
  }
  fclose(in);

  if (swap) byte_swap(s.prop,sizeof(Real),ns*s.elsz);

  return 0;
}

static int read_gorkovprop_tslice(Propagator s)
{
  const char fnm[MAXNAME] = "read_gorkovprop_tslice";
  Lattice* lat = s.parm->gpar->lattice;
  const int ns = lat->nspatial;
  Gorkov* psi = vecalloc(sizeof(Gorkov),ns);
  char filename[MAXLINE];
  int j, spin, col, n;
  FILE* in;

  if (s.type!=GORKOVPROP) {
    fprintf(stderr,"%s: illegal type %d - only GProp implemented\n",
	    fnm,s.type);
    return BADPARAM;
  }
  for (j=0;j<2;j++) {
    for (spin=0;spin<SPIN;spin++) {
      for (col=0;col<COL;col++) {
	sprintf(filename,"%s%d%d%dT%02d",s.filename,j,spin,col,s.tslice);
	in = (j==0 && spin==0 && col==0) ? fopen(filename,"rb") 
	                                 : freopen(filename,"rb",in);
	if (!in) {
	  fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
	  return BADFILE;
	}
	/* read in native format */
	// TODO: check byte ordering and sizes...
	if ( (n=fread(psi,sizeof(Gorkov),ns,in)) != ns) {
	  fprintf(stderr,"%s: cannot read propagator from %s\n",fnm,filename);
	  fprintf(stderr,"%s: read only %d of %d elements\n",fnm,n,ns);
	  fclose(in);
	  return BADFILE;
	}
	/* reorder data */
	insert_spinor_in_gprop(s.prop,psi,j,spin,col,ns);
#ifdef SU2_GROUP // only appropriate for mu=0 wilson really... ???
	insert_cspinor_in_gprop(s.prop,psi,j,spin,col,ns);
#endif
      }
    }
  }
  fclose(in);

  return 0;
}

#define TRINLAT_MAGIC_SIZE 8
#define TRINLAT_DATAID_SIZE 8

static int read_prop_trinlat(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop_trinlat";
#ifdef SU3_GROUP
  Lattice* lat = s.parm->gpar->lattice;
  struct trinlat_hdr {
    char magic[TRINLAT_MAGIC_SIZE];
    char dataid[TRINLAT_DATAID_SIZE];
    int tagsize;
    char precision;
    int nx, ny, nz, nt;
  } header;
  FILE *in;
  char filename[MAXLINE];
  int spin,col,i,n,m,tmin,tmax;
  Spinor* psi;

  if (s.type!=PROP) {
    fprintf(stderr,"%s: illegal type %d - only Prop implemented\n",
	    fnm,s.type);
    return BADPARAM;
  }
  switch (s.space) {
  case XSPACE: tmin=0; tmax=lat->length[IT]-1; n=lat->nsite; break;
  case TSLICE_X: tmin=tmax=s.tslice; n=lat->nspatial; break;
  default:
    fprintf(stderr,"%s: Wrong space for propagator\n",fnm);
    return BADPARAM;
  }
  psi = vecalloc(s.elsz,n);
  if (!psi) {
    fprintf(stderr,"%s: no space for read array!\n",fnm);
    return NOSPACE;
  }
  for (spin=0;spin<SPIN;spin++) {
    for (col=0;col<COL;col++) {
      sprintf(filename,"%sS%dC%d.qrk",s.filename,spin,col);
      in = (spin==0 && col==0) ? fopen(filename,"rb") 
	                       : freopen(filename,"rb",in);
      if (!in) {
	fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
	return BADFILE;
      }

      /**  Read the header  **/
      if (fread(&header,sizeof(struct trinlat_hdr), 1, in) != 1) {
	fprintf(stderr,"%s: Failed to read header from %szn",fnm,s.filename);
	fclose(in);
	return BADFILE;
      }
      if (header.nx!=lat->length[IX] || header.nx!=lat->length[IX] ||
	  header.nx!=lat->length[IX] || header.nx!=lat->length[IX]) {
	fprintf(stderr,
		"%s: Lattice sizes do not match %d %d %d %d != %d %d %d %d\n",
		fnm,header.nx,header.ny,header.nz,header.nt,
		lat->length[IX],lat->length[IX],
		lat->length[IX],lat->length[IX]);
	return BADDATA;
      }
      if ( (header.precision=='D' && sizeof(Real)==sizeof(double)) ||
	   (header.precision=='S' && sizeof(Real)==sizeof(float)) ) {
	/* native format! */
	fseek(in,tmin*sizeof(Spinor)*lat->nspatial,SEEK_CUR);
	if ( (m=fread(psi,sizeof(Spinor),n,in)) != n) {
	  fprintf(stderr,"%s: failed to read propagator from %s\n",
		  fnm,filename);
	  fprintf(stderr,"%s: read only %d of %d elements\n",fnm,m,n);
	  fclose(in);
	  return BADDATA;
	}
	/* reorder data */
	insert_spinor_in_prop(s.prop,psi,spin,col,n);
      } else if (header.precision=='D' && sizeof(Real)==sizeof(float)) {
	Spinor_double* psid = vecalloc(sizeof(Spinor_double),n);
	Prop* p = s.prop;
	int is,ic;
	if ( (m=fread(psid,sizeof(Spinor_double),n,in)) != n) {
	  fprintf(stderr,"%s: failed to read propagator from %s\n",
		  fnm,filename);
	  fprintf(stderr,"%s: read only %d of %d elements\n",fnm,m,n);
	  fclose(in);
	  return BADDATA;
	}
	for (i=0;i<n;i++) for (is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++) {
	  p[i][is][spin][ic][col].re = psid[i][is][ic].re;
	  p[i][is][spin][ic][col].im = psid[i][is][ic].im;
	}
	free(psid);
      } else if (header.precision=='S' && sizeof(Real)==sizeof(double)) {
	Spinor_single* psis = (Spinor_single*)psi; // dirty hack but ok
	Prop* p = s.prop;
	int is,ic;
	if ( (m=fread(psi,sizeof(Spinor_single),n,in)) != n) {
	  fprintf(stderr,"%s: failed to read propagator from %s\n",
		  fnm,filename);
	  fprintf(stderr,"%s: read only %d of %d elements\n",fnm,m,n);
	  fclose(in);
	  return BADDATA;
	}
	for (i=0;i<n;i++) for (is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++) {
	  p[i][is][spin][ic][col].re = psis[i][is][ic].re;
	  p[i][is][spin][ic][col].im = psis[i][is][ic].im;
	}
      }
    }
  }
  free(psi);
  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}

static int read_prop_ukqcd(Propagator s)
{
  const char fnm[MAXNAME] = "read_prop_ukqcd";
#if 1
  fprintf(stderr,"%s: not implemented!\n",fnm);
  return EVIL;
#endif
}

void insert_spinor_in_prop(Prop* p, Spinor* psi, int spin, int col, int n)
{
  int i, is, ic;
  /* the indices of a Prop are sink_spin source_spin sink_col source_col */
  /* the spinor has only the sink indices */
  for (i=0;i<n;i++) {
    for (is=0;is<SPIN;is++)
      for (ic=0;ic<COLOUR;ic++) 
	p[i][is][spin][ic][col] = psi[i][is][ic];
  }
}

void insert_spinor_in_gprop(GProp* p, Gorkov* psi,
			    int j, int spin, int col, int n)
{
  int x, i, is, ic;
  /* the indices of a Prop are sink_spin source_spin sink_col source_col */
  /* the spinor has only the sink indices */
  for (x=0;x<n;x++) {
    for (i=0;i<2;i++) for (is=0;is<SPIN;is++) for (ic=0;ic<COLOUR;ic++) 
	p[x][i][j][is][spin][ic][col] = psi[x][i][is][ic];
  }
}

#ifdef SU2_GROUP
/* Reconstruct the second source colour from the first, using the   *
 * Pauli-Gursey symmetry: X^{ab}_{ij} = s*(X^{1-a,1-b}_{1-i,1-j})*  *
 * where X is a (2x2)spin x (2x2)col block.                         *
 * The sign s is +1 if the product of all source and sink indices   *
 * (colour, spin, isospin) is even, -1 if it is odd                 */

void insert_cspinor_in_prop(Prop* p, Spinor* psi, int spin, int col, int n)
{
  const int cspin = 2*(spin/2) + (1-spin%2);
  const int ccol  = 1-col;
  int i, is, js;
  assert(ccol==1);
  /* the indices of a Prop are sink_spin source_spin sink_col source_col */
  /* the spinor has only the sink indices */
  if (spin%2==0) {
    for (i=0;i<n;i++) {
      for (is=0;is<SPIN;is++) {
	js = 2*(is/2)+(1-is%2);
	p[i][is][cspin][is%2][ccol].re =  psi[i][js][1-is%2].re;
	p[i][is][cspin][is%2][ccol].im = -psi[i][js][1-is%2].im;
	p[i][is][cspin][1-is%2][ccol].re = -psi[i][js][is%2].re;
	p[i][is][cspin][1-is%2][ccol].im =  psi[i][js][is%2].im;
      }
    }
  } else {
    for (i=0;i<n;i++) {
      for (is=0;is<SPIN;is++) {
	js = 2*(is/2)+(1-is%2);
	p[i][is][cspin][is%2][ccol].re = -psi[i][js][1-is%2].re;
	p[i][is][cspin][is%2][ccol].im =  psi[i][js][1-is%2].im;
	p[i][is][cspin][1-is%2][ccol].re =  psi[i][js][is%2].re;
	p[i][is][cspin][1-is%2][ccol].im = -psi[i][js][is%2].im;
      }
    }
  }
}

void insert_cspinor_in_gprop(GProp* p, Gorkov* psi,
			     int j, int spin, int col, int n)
{
#ifdef CHIRAL_GAMMA
  const int cspin = 2*(spin/2) + (1-spin%2);
  const int ccol  = 1-col;
  int sgn = spin%2 ? -1 : 1;
  int x, i, is, js;
  assert(ccol==1);
  /* the indices of a Prop are sink_spin source_spin sink_col source_col */
  /* the spinor has only the sink indices */
  for (i=0;i<2;i++) {
    if (i!=j) sgn*=-1;
    for (x=0;x<n;x++) {
      for (is=0;is<SPIN;is++) {
	js = 2*(is/2)+(1-is%2);
	p[x][i][j][is][cspin][is%2][ccol].re =  sgn*psi[x][i][js][1-is%2].re;
	p[x][i][j][is][cspin][is%2][ccol].im = -sgn*psi[x][i][js][1-is%2].im;
	p[x][i][j][is][cspin][1-is%2][ccol].re = -sgn*psi[x][i][js][is%2].re;
	p[x][i][j][is][cspin][1-is%2][ccol].im =  sgn*psi[x][i][js][is%2].im;
      }
    }
    if (i!=j) sgn*=-1;
  }
#else
#error wrong gamma matrix representation!!
#endif
}
#endif
