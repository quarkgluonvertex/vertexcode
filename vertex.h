#ifndef _VERTEX_H_
#define _VERTEX_H_
#include "field_params.h"
#include "momenta.h"
#include "treelevel.h"
#ifndef ANALYSIS
#include "fermion.h"
#endif
#include <assert.h>
#include <math.h>

/*** vertex kinematics ***/
#define VTX_GENERIC  0
#define VTX_ZEROMOM  1  /* gluon momentum q=0       */
#define VTX_REFLECT  2  /* total quark momentum P=0 */
#define VTX_ORTHOG   3  /* q.P=0                    */
#define VTX_PKSYM    4  /* quark symmetric, p^2=k^2 */
#define VTX_SKEWED   5  /* q.p=0                    */
#define VTX_AVEQ     6  /* not sure if this is used... */
#define VTX_MAXKIN   7

/*** tree-level correction ***/
#define NOCORR  0    /* No tree-level correction */
#define AVECORR 1    /* correction after averaging equivalent mom and mu */
#define CPTCORR 2    /* correction separately on each component          */

#define MAX_FORMFACTOR 22

typedef struct {
  Gauge_parameters* gauge;
  Propagator_parameters* prop;
  int nmom[DIR];                /* number of momentum values to use      */
  int p[DIR];                   /* quark momentum         */
  int q[DIR];                   /* gluon momentum         */
  int mom_variable;             /* momentum variable to use */
  int start_cfg;
  int nconfig;
  int skip;                     /* sweeps between configs */
  int mu;                       /* lorentz component      */
  int gamma;                    /* gamma matrix: tr gamma_gamma Gamma_mu */
  int kinematics;               /* symmetric, zeromom, skewed, ...       */
  int formfactor;               /* form factor(s) to extract             */
  int correction;               /* tree level correction                 */
  int covariant;                /* boolean, use covariant extraction     */
  int nboot;
  int fit;                      /* boolean, or select model or data      */
  int correlate;                /* boolean              */
  int write_boot;               /* boolean              */
  int plot;                     /* boolean, or select data to plot       */
  char boot_dir[MAXLINE];
  char graph_dir[MAXLINE];
  int seed;
  int id;                       /* any additional information            */
} Vertex_params;

typedef Real Momfunc(Momenta* mom, int* p, int mu);
typedef Real Momfunc_pk(Momenta* mom, int* p, int* k, int mu);

/*** Function prototypes for tree-level lattice expressions ***/
Real z0(Real ksqr, Real khatsqr, Real m0, Real bq, Real cq);

Real *z0_arr(Momenta *mom, Propagator_parameters* act);
Real *l1_q0_noncov(Momenta *mom, Propagator_parameters* act);
Real *l1_q0_cov(Momenta *mom, Propagator_parameters* act);
Real *l1_refl_arr(Momenta *mom, Propagator_parameters* act);
Real *l2_q0_cov(Momenta *mom, Propagator_parameters* act);
Real *l3_arr(Momenta *mom, Propagator_parameters* act);

Correction_function z0m0_bl;
Correction_function z0m0_pm;
Correction_function z0m0_rot;
Correction_function z0m0_rotpm;

Cpt_correction l1_q0_cpt;
Cpt_correction l1_refl_cpt;
Cpt_correction l2_cpt;
Cpt_correction l3_cpt;
Cpt_correction t5_cpt;
Cpt_correction tt3_sub;

Cpt_correction_pq l1_pq;
Cpt_correction_pq t8_cpt;

/* momentum functions */
static inline Real Mf_one(Momenta* mom, int* p, int mu) { return 1.0; }
static inline Real Mf_pk_one(Momenta* mom, int* p, int* k, int mu)
{ return 1.0; }
static inline Real Mf_kmu(Momenta* mom, int* p, int mu) {
  assert(mu<4 && mu>=0); assert(mom->bc[IT]==ANTIPERIODIC);
  return mu==IT ? sin(2.0*PI*(p[mu]+0.5)/mom->L[mu]) 
                : sin(2.0*PI*p[mu]/mom->L[mu]);
}  


int assign_params_vertex(char*,Vertex_params*);
#ifdef ANALYSIS
int assign_params_analyse_vertex(char* filename, Vertex_params* run, 
				 Momenta* mom);
#endif
#ifndef ANALYSIS
void compute_3pt(Complex***,Prop,Su3alg_c,Vertex_params*);
#endif
#endif
