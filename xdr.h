#ifndef _XDR_H_
#define _XDR_H_
#include "Real.h"

#ifdef SUCCESS
#undef SUCCESS
#endif
#include <rpc/rpc.h>

bool_t xdr_Real(XDR* xdrs, Real* r);
#endif
