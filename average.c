#include "analysis.h"

void vector_average(VectorData* this, int* cmask, int* xmask,
		    double** vec_ave, double* scal_ave)
{
  const int nvec = this->nvector;
  int i,j,ivec,cfg,ncfg;

/* calculate ncfg first since it is a global factor */
  ncfg = ntrue(cmask,this->nconfig);

  for (i=0,j=0;i<this->length;i++) if (MASK(xmask,i)) {
    for (ivec=0; ivec<this->nvector; ivec++) {
      double dataave=0.0;
      for (cfg=0;cfg<this->nconfig;cfg++) {
	if (MASK(cmask,cfg)) {
	  dataave += this->vector_data[ivec][cfg][i];
	}
      }
      vec_ave[ivec][j] = dataave/ncfg;
    }
    for (ivec=0;ivec<this->nvparm;ivec++) {
      vec_ave[ivec+nvec][j] = this->vector_parm[ivec][i];
    }
    j++;
  }
  for (i=0; i<this->nscalar; i++) {
    double dataave=0.0;
    for (cfg=0;cfg<this->nconfig;cfg++) {
      if ( MASK(cmask,cfg) )
	dataave+=this->scalar_data[i][cfg];
    }
    scal_ave[i] = dataave/ncfg;
  }
  for (j=0;j<this->nsparm;i++,j++) scal_ave[i] = this->scalar_parm[j];
}

/*** calculate the average of the n'th vector bundle ***/
void vector_average_n(VectorData* this, int n, int* cmask, int* xmask,
		      double* ave)
{
  const char fnm[MAXNAME] = "vector_average_n";
  const int nvec = this->nvector;
  int i,j,cfg,ncfg;

  if (n>nvec) {
    fprintf(stderr,"%s: n=%d out of range: nvec=%d\n",fnm,n,nvec);
    exit(-1);
  }
/* calculate ncfg first since it is a global factor */
  ncfg = ntrue(cmask,this->nconfig);

  for (i=0,j=0;i<this->length;i++) if (MASK(xmask,i)) {
    double dataave=0.0;
    for (cfg=0;cfg<this->nconfig;cfg++) {
      if (MASK(cmask,cfg)) {
	dataave += this->vector_data[n][cfg][i];
      }
    }
    ave[j] = dataave/ncfg;
    j++;
  }
}

/*** vector_average_all computes the simple average of all bundles ***
 *** of vector and scalar data                                     ***/
void vector_average_all(VectorData* this, int* cmask, int* xmask,
			double* vec_ave, double* scal_ave)
{
  int i,j,ivec,cfg,ncfg;
  double dataave;

/* calculate ncfg first since it is a global factor */
  ncfg = ntrue(cmask,this->nconfig);
  for (i=0,j=0;i<this->length;i++) {
    if ( MASK(xmask,i) ) {
      dataave=0.0;
      for (ivec=0; ivec<this->nvector; ivec++) {
	for (cfg=0;cfg<this->nconfig;cfg++) {
	  if ( MASK(cmask,cfg) ) {
	    dataave += this->vector_data[ivec][cfg][i];
	  }
	}
      }
      for (ivec=0; ivec<this->nvparm; ivec++) {
	dataave += this->vector_parm[ivec][i];
      }
      vec_ave[j++] = dataave/(this->nvector*ncfg);
    }
  }

  dataave=0.0;
  for (i=0; i<this->nscalar; i++) {
    for (cfg=0;cfg<this->nconfig;cfg++) {
      if ( MASK(cmask,cfg) )
	dataave+=this->scalar_data[i][cfg];
    }
  }
  for (i=0; i<this->nsparm; i++) dataave+=this->scalar_parm[i];

  *scal_ave = dataave/(this->nscalar*ncfg);

}

void data_average(Real* ave, Real** data, int nset, int len)
{
  int i, j;
  for (i=0;i<len;i++) {
    Real sum=0.0;
    for (j=0;j<nset;j++) sum += data[j][i];
    ave[i] = sum/nset;
  }
} 

int ntrue(int* mask, int len)
{
  int i,j;

  if (!mask) return len;
  for (i=0,j=0;i<len;i++) if (mask[i]) j++;

  return j;
}
