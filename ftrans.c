#include "ftrans.h"
#include <malloc.h>

int init_trans_ftrans(Gauge_transform* g,
		      Gauge_parameters *parm,Ftrans_params* run)
{
/*** Make space for a new parameter struct ***/
  g->parm=malloc(sizeof(Gauge_parameters));
  if (!(g->parm)) {
    fprintf(stderr,"init_trans: cannot allocate space for parm\n");
    return NOSPACE;
  }
/*** Copy the gauge parameters ***/
  *(g->parm) = *parm;
  
/*** Assign gauge type and specifics from run ***/
  g->parm->gauge = run->gauge_type;
  g->parm->gauge_param = run->gauge_param;
/* These next params may have default values */
  g->parm->gfix_precision[0] = run->gfix_precision;
  g->parm->gfix_iterations = run->gfix_iterations;
  
/*** Where did the gauge trans come from? ***/
  g->parm->producer_id = run->trans_origin;
  g->parm->id = run->trans_id;

/*** Allocate space for data ***/
  g->g = malloc(sizeof(Group_el)*parm->lattice->nsite);
  if (!(g->g)) {
    fprintf(stderr,"init_trans: cannot allocate space\n");
    return NOSPACE;
  }

/*** Construct the filename and return ***/
  return (construct_trans_filename(g));
}

/*** set_gauge_momenta: sets the number of momenta for a gauge field ***
 *** (eg in preparation for fourier transform)                       ***/

int set_gauge_momenta(Gauge* u, Ftrans_params* fpar)
{
  const char fnm[MAXLINE] = "set_gauge_momenta";
  int i;

  for (i=0; i<DIR; i++) {
    if (fpar->nmomenta[i]<0 || fpar->nmomenta[i]>=u->parm->lattice->length[i]) {
      fprintf(stderr,"%s: illegal nmomenta[%d]=%d\n",fnm,i,fpar->nmomenta[i]);
      return BADDATA;
    }
    if (fpar->nmomenta[i]>u->parm->lattice->length[i]/2) {
      fprintf(stderr,"%s: Warning: nmom>L/2 in direction %d\n",fnm,i);
    }
    u->nmom[i] = fpar->nmomenta[i];
    if (u->parm->bcs[i]==ANTIPERIODIC) u->ntotmom[i]=2*u->nmom[i];
    else u->ntotmom[i]=2*u->nmom[i]+1;
  }

  return 0;
}
