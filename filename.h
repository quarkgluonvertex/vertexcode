#ifndef _FILENAME_H_
#define _FILENAME_H_
#include <stdio.h>
#include <string.h>

int file_exists(char *name);
void get_filename(char *name, const char *base, const char *suffix);

#endif
