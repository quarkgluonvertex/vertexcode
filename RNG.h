#ifndef _RNG_H_
#define _RNG_H_
#include <stdio.h>
#include <stdlib.h>

#define SEED_MAX 900000000L
float uni();
void seed_uni(int ijkl);
int uni_is_ini();
int uni_read(FILE* sf);
int uni_write(FILE* sf);

#endif
