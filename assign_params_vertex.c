/* $Id: assign_params_vertex.c,v 1.8 2015/06/17 10:39:16 jonivar Exp $ */

#include "vertex.h"
#include <producers.h>
#include <parm.h>

#define MAXACT 4
#define MAXPRO 8
#define MAXGAU 4
#define MAXKIN 4

static PaCode action[MAXACT] = {"WILSON","CLOVER","STAGGERED","GAUGEHIGGS"};
static PaCode gauge[MAXGAU]  = {"RANDOM","COULOMB","LANDAU","COVARIANT"};
static PaCode producer[MAXPRO]
  = {"INTERNAL","GC_OSU","UKQCD","SCIDAC", "ROMA","KRASNITZ","SJH_HMC","TRINLAT"};
static PaCode kinemat[MAXKIN]  = {"GENERIC","ZEROMOM","REFLECT","SKEWED"};

int assign_params_vertex(char* filename, Vertex_params* run)
{
  const char fnm[MAXNAME] = "assign_params_vertex";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));
  Gauge_parameters* gpar = malloc(sizeof(Gauge_parameters));
  Propagator_parameters* ppar = malloc(sizeof(Propagator_parameters));
/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gact[MAXLINE]  = "WILSON";
  char fact[MAXLINE]  = "WILSON";
  char gau[MAXLINE] = "LANDAU";
  char kin[MAXLINE] = "GENERIC";
  char pprod[MAXLINE] = "";
  char gprod[MAXLINE] = "INTERNAL";
  char gauge_dir[MAXLINE];
  char prop_dir[MAXLINE];
  char gaugename[MAXLINE];
  char propname[MAXLINE];
  char infostr[MAXLINE];
  double kappac=0.0, mbare=0.0;
  double csw=0.0, rotation=0.0, bm=0.0, lambda=0.0;
  double mass[4] = {0.0,0.0,0.0,0.0};
  double umu=0.0, uj=0.0;
  int prop_swap_in = FALSE;
  int prop_is_double = TRUE;
  int prop_xyzt_order = FALSE;
  int id;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  /* set pointers and lattice defaults */
  if (!lat || !gpar || !ppar) {
    printf("%s: could not alloc space for param structures!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=1;
  gpar->lattice = lat;
  run->gauge = ppar->gpar = gpar;
  run->prop  = ppar;

  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = TRUE;
  ppar->bcs[IX] = ppar->bcs[IY] = ppar->bcs[IZ] = ppar->bcs[IT] = FALSE;

  gpar->action = WILSON;
  gpar->nf     = 0;
  gpar->beta   = 6.0;
  gpar->sweep  = 0;
  gpar->producer_id = UNKNOWN;
  gpar->id = 0;
  memset(gpar->info,0,MAXLINE);

  ppar->kappa    = 0.1234;
  ppar->mass     = 0.0;
  ppar->ncoeff   = 0;
  ppar->imp      = 0;
  ppar->j = ppar->jbar = 0.0; /* diquark and antidiquark source */
  ppar->source_type = 0;
  ppar->id = 0;

  run->nmom[IX] = run->nmom[IY] = run->nmom[IZ] = run->nmom[IT] = 0;
  run->p[IX] = run->p[IY] = run->p[IZ] = run->p[IT] = 0;
  run->q[IX] = run->q[IY] = run->q[IZ] = run->q[IT] = 0;

  run->start_cfg = run->nconfig = run->nboot = 0;
  run->mu = run->gamma = 0;
  run->kinematics = VTX_GENERIC;
  run->write_boot = TRUE;
  run->nboot = 0;
  run->fit = run->correlate = FALSE;
  run->id = 0;
  run->seed = 7654321;

  memset(gauge_dir,0,MAXLINE);
  memset(prop_dir,0,MAXLINE);
  memset(gaugename,0,MAXLINE);
  memset(propname,0,MAXLINE);
  memset(run->boot_dir,0,MAXLINE);
  memset(run->graph_dir,0,MAXLINE);
  strcpy(run->boot_dir,".");
  strcpy(run->graph_dir,".");

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,DEFAULT,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&run->nmom[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&run->nmom[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&run->nmom[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_momenta",&run->nmom[IT]);
  add_parm(list,MUSTSET,INTEGER,"px",&run->p[IX]);
  add_parm(list,MUSTSET,INTEGER,"py",&run->p[IY]);
  add_parm(list,MUSTSET,INTEGER,"pz",&run->p[IZ]);
  add_parm(list,DEFAULT,INTEGER,"pt",&run->p[IT]);
  add_parm(list,MUSTSET,INTEGER,"qx",&run->q[IX]);
  add_parm(list,MUSTSET,INTEGER,"qy",&run->q[IY]);
  add_parm(list,MUSTSET,INTEGER,"qz",&run->q[IZ]);
  add_parm(list,MUSTSET,INTEGER,"qt",&run->q[IT]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_x",&ppar->bcs[IX]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_y",&ppar->bcs[IY]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_z",&ppar->bcs[IZ]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_t",&ppar->bcs[IT]);
  add_parm(list,MUSTSET,STRING,"gauge_action",gact);
  add_parm(list,DEFAULT,STRING,"gauge_producer",gprod);
  add_parm(list,DEFAULT,STRING,"gauge_directory",gauge_dir);
  add_parm(list,DEFAULT,STRING,"gauge_basename",gaugename);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,DEFAULT,INTEGER,"nflavor",&gpar->nf);
  add_parm(list,DEFAULT,DOUBLE,"mass1",&mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"mass2",&mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"mass3",&mass[2]);
  add_parm(list,DEFAULT,DOUBLE,"mass4",&mass[3]);
  add_parm(list,DEFAULT,DOUBLE,"gauge_chempot",&umu);
  add_parm(list,DEFAULT,DOUBLE,"gauge_diquark_src",&uj);
  add_parm(list,MUSTSET,INTEGER,"start_cfg",&run->start_cfg);
  add_parm(list,MUSTSET,INTEGER,"nconfig",&run->nconfig);
  add_parm(list,DEFAULT,INTEGER,"config_skip",&run->skip);
  add_parm(list,MUSTSET,STRING,"fermion_action",fact);
  add_parm(list,DEFAULT,STRING,"prop_producer",pprod);
  add_parm(list,DEFAULT,STRING,"propagator_directory",prop_dir);
  add_parm(list,DEFAULT,STRING,"propagator_basename",propname);
  add_parm(list,DEFAULT,BOOL,"prop_swap_in",&prop_swap_in);
  add_parm(list,DEFAULT,BOOL,"prop_is_double",&prop_is_double);
  add_parm(list,DEFAULT,BOOL,"prop_xyzt_order",&prop_xyzt_order);
  add_parm(list,MUSTSET,DOUBLE,"kappa",&ppar->kappa);
  add_parm(list,DEFAULT,DOUBLE,"chempot",&ppar->chempot);
  add_parm(list,DEFAULT,DOUBLE,"diquark_src",&ppar->j);
  add_parm(list,DEFAULT,DOUBLE,"antidiquark_src",&ppar->jbar);
  add_parm(list,DEFAULT,DOUBLE,"csw",&csw);
  add_parm(list,DEFAULT,DOUBLE,"kappa_c",&kappac);
  add_parm(list,DEFAULT,DOUBLE,"bare_mass",&mbare);
  add_parm(list,DEFAULT,DOUBLE,"rotation",&rotation);
  add_parm(list,DEFAULT,DOUBLE,"bm",&bm);
  add_parm(list,DEFAULT,DOUBLE,"additive_imp",&lambda);
  add_parm(list,DEFAULT,INTEGER,"source_type",&ppar->source_type);
  add_parm(list,MUSTSET,STRING,"gauge_type",gau);
  add_parm(list,MUSTSET,DOUBLE,"gauge_param",&gpar->gauge_param);
  add_parm(list,MUSTSET,INTEGER,"mu",&run->mu);
  add_parm(list,DEFAULT,STRING,"kinematics",kin);
  add_parm(list,MUSTSET,INTEGER,"nboot",&run->nboot);
  add_parm(list,DEFAULT,BOOL,"save_boot",&run->write_boot);
  add_parm(list,DEFAULT,STRING,"boot_directory",run->boot_dir);
  add_parm(list,DEFAULT,BOOL,"fit",&run->fit);
  add_parm(list,DEFAULT,BOOL,"correlated_fit",&run->correlate);
  add_parm(list,DEFAULT,BOOL,"plot",&run->plot);
  add_parm(list,DEFAULT,STRING,"graph_directory",run->graph_dir);
  add_parm(list,MUSTSET,INTEGER,"seed",&run->seed);
  add_parm(list,DEFAULT,INTEGER,"id",&run->id);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(gpar->lattice);
  gpar->lattice->has_ntab = FALSE;

/*** Determine the gauge action ***/
  status += parm_from_string(&gpar->action,gact,action,MAXACT,"gauge_action");
  status += parm_from_string(&gpar->producer_id,gprod,producer,MAXPRO,"gauge_producer");
  if (gpar->producer_id < _PROD_ST) gpar->producer_id += _PROD_ST;

/*** Put additional action parameters in the "mass" array if applicable ***/
  if (gpar->nf) {
    if (umu==0.0 && uj==0.0) {
      gpar->mass = malloc(sizeof(double)*gpar->nf);
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
    } else {
      gpar->mass = malloc(sizeof(double)*(gpar->nf+2));
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
      gpar->mass[gpar->nf] = umu;
      gpar->mass[gpar->nf+1] = uj;
      gpar->id = 1024; // KLUDGE
    }
  }
  if (gauge_dir[0]) {
    sprintf(infostr,"dir:%s ",gauge_dir);
    strcat(gpar->info,infostr);
  }
  if (gaugename[0]) {
    sprintf(infostr,"base:%s ",gaugename);
    strcat(gpar->info,infostr);
  }

/*** Determine gauge transformation type ***/
  status += parm_from_string(&gpar->gauge,gau,gauge,MAXGAU,"gauge_type");

/***  Assign remaining (unused) parameters to defaults ***/
  gpar->sweep = 0;
  gpar->time  = 0;
  gpar->start = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace = 0.0;
  for (i=0; i<MAXPREC; i++)
    gpar->gfix_precision[i] = 100.0;  /* a ridiculous number... */
  gpar->gfix_iterations = 0;
  gpar->checksum = 0;

/*** Determine the fermion action ***/
  status += parm_from_string(&ppar->action,fact,action,MAXACT,"fermion_action");
  status += parm_from_string(&ppar->producer_id,pprod,producer,MAXPRO,"prop_producer");
  if (ppar->producer_id < _PROD_ST) ppar->producer_id += _PROD_ST;
/*** Compute the "id" code from input formats ***/
  id=0;
  if (!prop_is_double) id++;
  if (prop_swap_in)    id += 2;
  if (prop_xyzt_order) id += 4;
  ppar->id = id;  /* UKQCD input -- other producers may have other ids */

  if (prop_dir[0]) {
    sprintf(infostr,"dir:%s ",prop_dir);
    strcat(ppar->info,infostr);
  }
  if (propname[0]) {
    sprintf(infostr,"base:%s ",propname);
    strcat(ppar->info,infostr);
  }

/***  Determine bare mass or kappa_critical if known  ***/
  if (ppar->action==CLOVER && kappac==0.0 && mbare==0.0) {
    fprintf(stderr,"%s: bare_mass or kappa_c should be set for clover improvement\n",fnm);
    fprintf(stderr,"%s: setting kappa_c to default value 0.125\n",fnm);
    kappac=0.125;
  }
  if (kappac!=0.0) {
    if (mbare!=0.0) {
      fprintf(stderr,"%s: conflict - both kappa_c and mbare are set\n",fnm);
      fprintf(stderr,"%s: ignoring kappa_c value\n",fnm);
    } else if (ppar->action==CLOVER || ppar->action==WILSON) {
	ppar->kappa_c = kappac;
	ppar->mass = 0.5/ppar->kappa - 0.5/kappac;
    } else {
      fprintf(stderr,"%s: ignoring kappa_c for non-Wilson type action\n",fnm);
    }      
  }
  if (mbare!=0.0) {
    ppar->mass=mbare;
    if (ppar->action==CLOVER || ppar->action==WILSON) {
      ppar->kappa_c = 1.0/(1.0/ppar->kappa-2*mbare);
    }
  }

/***  Assign improvement coefficients ***/
  if (ppar->action==CLOVER) {
    if (rotation!=0.0) ppar->source_type = ROTATED;
    if (bm!=0.0 || lambda!=0.0) {
      ppar->ncoeff = 4;
      ppar->imp = malloc(sizeof(double)*4);
      ppar->imp[0] = csw;
      ppar->imp[1] = rotation;
      ppar->imp[2] = bm;
      ppar->imp[3] = lambda;
    } else {
      ppar->ncoeff = 2;
      ppar->imp = malloc(sizeof(double)*2);
      ppar->imp[0] = csw;
      ppar->imp[1] = rotation;
    }
  }

/*** Scale the diquark source with kappa ***/
  ppar->j *= ppar->kappa;
  ppar->jbar *= ppar->kappa;

/***  Assign remaining (unused) parameters to defaults ***/
  ppar->kappa_scalar = 0.0;
  ppar->source_address[IX] = ppar->source_address[IY] = 0;
  ppar->source_address[IZ] = ppar->source_address[IT] = 0;

/*** Determine kinematics ***/
  status += parm_from_string(&run->kinematics,kin,kinemat,MAXKIN,"kinematics");
  run->mu--;

/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return status;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}
