/* Program to print tree-level form factors of quark-gluon vertex */
#include "analysis.h"
#include "momenta.h"
#include "vertex.h"

int main(int argc, char** argv) {
  Vertex_params run;
  Momenta mom;
  Cpt_correction* treefunc;       /* componentwise tree-level form factor */
  //  Real (*correction)(Real*,int);  /* average tree-level form factor       */
  Real* treedata;                 /* array of average tree-level data     */
  int i, mu, nu, M, nmu, p[DIR];
  char filename[MAXLINE];
  int status;

  if (argc != 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  sprintf(filename, "%s",argv[1]);
  if ((status=assign_params_analyse_vertex(filename,&run,&mom))) return status;
  if (status=setup_momenta(&mom)) return status;

  /* Only soft gluon kinematics initially? */
  if (run.kinematics != VTX_ZEROMOM) {
    printf("Only soft gluon kinematics implemented\n");
    return BADPARAM;
  }

  /*** Set up tree level functions; see analyse_qqg_p.c for details ***/
  switch (run.formfactor) {
  case 1: 
    treedata = l1_q0_noncov(&mom,run.prop);
    treefunc = l1_q0_cpt;
    nmu = 3;
    break;
  case 2:
    treedata = 0;
    treefunc = l2_cpt;
    nmu = 12;
    break;
  case 3:
    treedata = l3_arr(&mom,run.prop);
    treefunc = l3_cpt;
    nmu = 4;
    break;
    default:
      printf("Error: form factor %d not yet implemented\n", run.formfactor);
      return BADPARAM;
  }

  for (p[IX]=-run.p[IX]; p[IX]<=run.p[IX]; p[IX]++)
    for (p[IY]=-run.p[IY]; p[IY]<=run.p[IY]; p[IY]++)
      for (p[IZ]=-run.p[IZ]; p[IZ]<=run.p[IZ]; p[IZ]++)
	for (p[IT]=-run.p[IT]; p[IT]<run.p[IT]; p[IT]++) {
	  i = momindex_p(p,&mom);
	  if (i!=DISCARDED) {
	    /* assign lorentz indices; see analyse_qqg_p.c for details */
	    for (M=0;M<nmu;M++) {
	      switch (run.formfactor) {
	      case 1:
		if (p[M]!=0) continue;
		mu = M;
		break;
	      case 2:
		mu = M/3; nu = M%3; if (nu>=mu) nu++;
		if ( (mu!=IT && p[mu]==0) || (nu!=IT && p[nu]==0) ) continue;
		break;
	      case 3:
		mu=M;
		break;
	      }
	      printf(" %4d %d %12.8f  %12.8f\n",i,mu,mom.val[0][i],
		     (*treefunc)(&mom,run.prop,p,mu,i));
	    }
	  }
	}

  if (treedata) {
    printf("\n");
    for (i=0;i<mom.nval;i++)
      printf(" %4d  %12.8f  %12.8f\n",i,mom.val[0][i],treedata[i]);
  }

  return 0;
}
