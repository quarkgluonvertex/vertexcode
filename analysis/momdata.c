#include "momdata.h"
#include "filename.h"
#include "field_params.h"
#include "plotopts.h"
#include "rand.h"
#include <scanvec.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define MASK(arr,i) ( (arr) ? arr[(i)] : TRUE )
#define MASK2(arr,i,j) ( (arr) ? arr[(i)][(j)] : TRUE )

static const char gcode[6] = "RCLXAU";
#define DEFAULT_SIZE 0.8
#define MAXSUFX 6

void momxfunc_cov_ave(MomData* data, OffsetDatafunction func, 
		      double* ave, double** cov, 
		      int* cmask, int* xmask, int xvar);
void momdatafunc_cov_ave(MomData* data, OffsetDatafunction func, 
			 double* ave, double** cov, int* cmask, int* xmask);

static int gaugename(MomData* data);
static int quarkname(MomData* data);

int init_momdata(MomData* data, 
		 void* parm, Momenta* mom, Data_t type, 
		 int nconf, int nscal, int nvect, char* nm, char* dir)
{
  const char fnm[MAXNAME] = "init_momdata";

  /* make sure the momenta are initialised */
  if (!mom->initialised) {
    fprintf(stderr,"%s: Warning: initialising momenta\n",fnm);
    setup_momenta(mom);
  }
  data->type = type;
  data->parm = parm;
  data->mom  = mom;

  switch (data->type) {
  case GAUGE_D: {
    Gauge_parameters* par = data->parm;
    data->time = par->time;
    data->id   = par->id;
    data->momvar = MOMQ;
    break;
  }
  case HIGGS: {
    Higgs_parameters* par = data->parm;
    data->time = par->time;
    data->id   = par->id;
    data->momvar = MOMQ;
    break;
  }
  case FERMION: {
    Propagator_parameters* par = data->parm;
    data->time = par->gpar->time;
    data->id   = par->id;
    data->momvar = MOMK;
    break;
  }
  default:
    fprintf(stderr,"%s: Data type %d not implemented\n",fnm,data->type);
    return BADDATA;
  }

  data->nconfig = nconf;
  data->nscalar = nscal;
  data->nvector = nvect;

  data->scalar_data = arralloc(sizeof(Real),2,nscal,nconf);
  data->vector_data = arralloc(sizeof(Real),3,nvect,nconf,mom->nval);
  if ( (nscal && !data->scalar_data) || (nvect && !data->vector_data) ) {
    fprintf(stderr,"%s: cannot allocate space for data arrays\n",fnm);
    return NOSPACE;
  }

  /** initialise parametric data to null (default) **/
  data->nsparm = data->nvparm = 0;
  data->scalar_parm = 0;
  data->vector_parm = 0;

  /** set strings **/
  memset(data->basename,0,MAXNAME);
  memset(data->name,0,MAXLINE);
  memset(data->filename,0,MAXLINE);
  strcpy(data->basename,nm);
  momdata_create_filename(data,dir);
  
  return 0;
}

/*** Add scalar and vector parametric (non-configuration) data ***/
int momdata_add_pars(MomData* this, int nspar, int nvpar)
{
  const char fnm[MAXNAME] = "momdata_add_parm";

  this->nsparm = nspar;
  this->nvparm = nvpar;
  this->scalar_parm = malloc(sizeof(Real)*nspar);
  this->vector_parm = arralloc(sizeof(Real),2,nvpar,this->mom->nval);
  if ( (nspar && !this->scalar_parm) || (nvpar && !this->vector_parm) ) {
    fprintf(stderr,"%s: cannot allocate space for parameter arrays\n",fnm);
    return NOSPACE;
  }

  return 0;
}

void set_momvar(MomData* data, int var) { data->momvar = var; }

void momdata_create_filename(MomData* data, char* dir)
{
  momdata_create_name(data);
  if (dir) sprintf(data->filename,"%s/%s.dat",dir,data->name);
  else sprintf(data->filename,"%s.dat",data->name);
}

void momdata_create_name(MomData* data)
{
  const char fnm[MAXNAME] = "momdata_create_name";
  char basename[MAXLINE];
  char cutstr[20];
  char tmpstr[8];
  char gaugec;
  double dt;
  Momenta* mom = data->mom;

  switch (data->type) {
  case HIGGS: {
    Higgs_parameters* par = data->parm;
    dt = par->dt;
    gaugec='U'; /* Higgs fields are always in unitary gauge? */
    break;
  }
  case GAUGE_D: {
    Gauge_parameters* par = data->parm;
    dt = (mom->L[IT]<=1) ? par->mass[1] : 1.0;
    gaugec=gcode[par->gauge];
    gaugename(data);
    strcpy(basename,data->name);
    break;
  }
  case FERMION: {
    Propagator_parameters* par = data->parm;
    gaugec=gcode[par->gpar->gauge];
    quarkname(data);
    strcpy(basename,data->name);
    break;
  }
  default:
    fprintf(stderr,"%s: Data type %d not implemented\n",fnm,data->type);
    return;
  }
  /* If we are sorting the data we usually want the momentum variable *
   * to be equal to the sort variable...                              */
  if (mom->sort) data->momvar = mom->sortvar;

  if (mom->cutoff > 0) {
    sprintf(cutstr,"r%03d",INT(mom->cutoff*100));
  } else {
    strcpy(cutstr,"");
  }
  if (mom->dcut > 0) {
    sprintf(tmpstr,"d%02d",INT(mom->dcut*10));
    strcat(cutstr,tmpstr);
  }
  if (mom->o3) strcat(cutstr,"o3");
  if (mom->smear) {
    int ignore=FALSE;
    switch (mom->smear) {
    case NBIN:
      sprintf(tmpstr,"n%03d",INT(mom->smearing));
      break;
    case BINSIZE:
      sprintf(tmpstr,"s%03d",INT(1000*mom->smearing));
      break;
    case NINBIN:
      sprintf(tmpstr,"b%03d",INT(mom->smearing));
      break;
    default:
      fprintf(stderr,"%s: unknown smearing type %d -- ignoring smearing\n",
	     fnm,mom->smear);
      ignore=TRUE;
    }
    if (!ignore) strcat(cutstr,tmpstr);
  }

  memset(data->name,0,MAXLINE);
  if (mom->L[IT]<=1) { /* 3-d, real-time data */
    sprintf(data->name, "%s_r%ddt%02dt%04d%c_p%02d%02d%02d%s",
	    data->basename, data->id, INT(100*dt), data->time, gaugec,
	    mom->nmom_out[IX], mom->nmom_out[IY], mom->nmom_out[IZ], cutstr);
  } else { /* 4-d data */
    sprintf(data->name, "%s_p%02d%02d%02d%02d%s", basename, 
	    mom->nmom_out[IX], mom->nmom_out[IY], 
	    mom->nmom_out[IZ], mom->nmom_out[IT], cutstr);
  }
}

int read_momdata(MomData* data, int* mask, int nread)
{
  const char fnm[MAXNAME] = "read_momdata";
  FILE* in;
  float rdat[nread];
  int cfg, imom, idat,i, sweep;
  int select[data->nvector+1];
  char line[MAXLINE];

  /** check mask and translate to "select" array **/
  for (idat=0,i=0; i<nread; i++) {
    if (mask[i]) select[idat++]=i;
    if (idat>data->nvector) {
      printf("%s: data overflow -- mask does not match nvector\n",fnm);
      return BADDATA;
    }
  }

  if ((in=fopen(data->filename, "r")) == NULL) {
    printf("%s: Could not open file <%s>\n",fnm,data->filename);
    return BADFILE;
  }
    
  for (i=cfg=0;cfg<data->nconfig;cfg++) {
    if (fgets(line,MAXLINE,in)) {
      if (sscanf(line, "Sweep number %d", &sweep) != 1) {
	printf("%s: Bad format on file <%s> - missing sweep number for config number %d\n",
	       fnm, data->filename, cfg);
	printf("%s: Read line as <%s>\n",fnm, line);
	fclose(in);
	return BADDATA;
      }
    } else {
      printf("%s: Error on file <%s> - premature end of file\n",
	     fnm,data->filename);
      fclose(in);
      return BADFILE;
    }
    i++;
    /** TODO: read scalar data here! **/
    for (imom=0; imom<data->mom->nval; imom++) {
      if (fgets(line,MAXLINE,in)) {
	if (sscanvecf(line,rdat,nread) < nread) {
	  fprintf(stderr,"error on line %d in file <%s>: corrupted data\n",
		 i, data->filename);
	  fclose(in);
	  return BADDATA;
	}
	for (idat=0; idat<data->nvector; idat++) {
	  data->vector_data[idat][cfg][imom] = rdat[select[idat]];
	}
	i++;
      }
    }
  }

  fclose(in);
  return 0;
}

int reorder_momdata(MomData* data, Momenta* mom_out)
{
  const char fnm[MAXNAME] = "reorder_momdata";
  Real*** outdata;
  int i, j;
  int status;

  if (!mom_out->initialised) {
    fprintf(stderr,"%s: Warning: initialising out-momenta\n",fnm);
    if (status=setup_momenta(mom_out)) return status;
  }

  outdata = arralloc(sizeof(Real),3,data->nvector,data->nconfig,mom_out->nval);
  if (!outdata){
    fprintf(stderr,"%s: cannot allocated space for reordered data\n",fnm);
    return NOSPACE;
  }

  for (i=0; i<data->nvector; i++) {
    for (j=0; j<data->nconfig; j++) {
      status = re_reorder_momenta(outdata[i][j], data->vector_data[i][j],
				  data->mom, mom_out);
      if (status) return status;
    }
  }

  free(data->vector_data);
  data->vector_data = outdata;
  data->mom = mom_out;
  if (mom_out->sort) data->momvar = mom_out->sortvar;
  momdata_create_name(data);

  return 0;
}

/*** The config data in subdata are copied to form a subset of         ***
 *** the larger structure data: data->nvector > subdata->nvector etc   ***/
int copy_momdata_subset(MomData* data, MomData* subdata, 
			int startv, int starts)
{
  const char fnm[MAXNAME] = "copy_momdata_subset";
  const int nmom = data->mom->nval;
  const int ncfg = data->nconfig;
  int i, j, k;

  if (subdata->nconfig != ncfg) {
    fprintf(stderr,"%s: number of configs do not match\n",fnm);
    return BADDATA;
  }
  if (subdata->mom->nval != nmom) {
    fprintf(stderr,"%s: number of momenta do not match\n",fnm);
    return BADDATA;
  }
  if (data->nvector<subdata->nvector || data->nscalar<subdata->nscalar) {
    fprintf(stderr,"%s: destination array smaller than source array!\n",fnm);
    return BADDATA;
  }
  for (i=0;i<subdata->nscalar;i++)
    for (j=0;j<ncfg;j++) 
      data->scalar_data[i+starts][j] = subdata->scalar_data[i][j];

  for (i=0;i<subdata->nvector;i++)
    for (j=0;j<ncfg;j++)
      for (k=0;k<nmom;k++)
	data->vector_data[i+startv][j][k] = subdata->vector_data[i][j][k];

  /* the non-configuration data are not copied */
  return 0;
}

int assign_submomdata(int** subset, MomData* data, OffsetDatafunction xfn,
			  double xmin, double xmax)
{
  const char fnm[MAXNAME] = "assign_momdata_subset";
  const int ncfg = data->nconfig;
  const int ndata = data->mom->nval;
  Datafunction* xfunc = xfn.func;
  Momenta* mom = data->mom;
  double** cov = 0;
  double xdata[ndata];
  int* mask = vecalloc(sizeof(int),ndata);
  /* if xfunc is null we use the mom values as xdata */
  const int xvar = (ncfg>1 && xfunc) ? xfunc(0,0,0,0) : 0;
  int i;

  if (xmin>=xmax) {
    fprintf(stderr,"%s: illegal fit range, xmin=%g > xmax=%g\n",fnm,
	    xmin,xmax);
    *subset=0;
    return BADPARAM;
  }
  if (!mask) {
    fprintf(stderr,"%s: cannot alloc space for subset array\n",fnm);
    return NOSPACE;
  }

  if (xfunc) {
    cov = arralloc(sizeof(Real),2,ndata,ndata);
    momxfunc_cov_ave(data,xfn,xdata,cov,0,0,xvar);
    free(cov);
  } else {
    if (mom->sort) {
      for (i=0;i<ndata;i++) xdata[i] = mom->val[data->momvar][mom->key[i]];
    } else {
      for (i=0;i<ndata;i++) xdata[i] = mom->val[data->momvar][i];
    }
  }

  for (i=0;i<ndata;i++) {
    if (xdata[i]<xmin || xdata[i]>xmax) mask[i]=false; else mask[i]=true;
  }

  *subset = mask;
  return 0;
}

int boot_reorder_momenta(Boot* data_out, Boot* data_in, Momenta* mom)
{
  const int inplace = (data_out==data_in);
  int i, j, iboot, nall;
  Real *tmp;
  int *neq;
  int nontrivial=0;
  const char fnm[MAXNAME] = "boot_reorder_momenta";

  if (!mom->initialised) {
    fprintf(stderr,"%s: Error: momenta are not initialised\n",fnm);
    return BADDATA;
  }

  /* Check compatibility of boot arguments    */
  if (data_out->nboot != data_in->nboot && data_out->nboot>0) {
    fprintf(stderr,"%s: Incompatible nboot %d != %d\n", fnm, 
	   data_out->nboot, data_in->nboot);
    return BADDATA;
  }

  /* check if any reordering is to take place */
  for (i=0;i<DIR;i++) {
    if (mom->nmom_in[i]!=mom->nmom_out[i]) {
      nontrivial++; break;
    }
  }
  if (mom->o3 || mom->z4) nontrivial++;
  if (mom->cutoff>0 || mom->dcut>0) nontrivial++;
  if (mom->sort) nontrivial++;
  if (mom->smear) nontrivial++;

  if (!nontrivial) { /* trivial "reordering" */
    if (!inplace)
      for (i=0; i<mom->nval; i++) boot_copy(&data_out[i],data_in[i]);
    return 0;
  }

  /* If the reordering is to be in-place we need to put data in         *
   * a temporary array and put them back only at the end.               *
   * Otherwise we can put the reordered data directly in the output     *
   * boot structure.                                                    */
  nall = nallmom(mom);
  neq  = vecalloc(sizeof(int),mom->nval);
  tmp=vecalloc(sizeof(Real),mom->nval);

  /* First reorder the central values */
  for (i=0; i<mom->nval; i++) {
    neq[i] = 0;
    tmp[i]=0.0;
  }
  for (i=0; i<nall; i++) {
    if ( (j=mom->index[i]) != DISCARDED) {
      neq[j]++;
      tmp[j] += data_in[i].best;
    }
  }
  for (i=0; i<mom->nval; i++) {
    if (!(mom->sort) || mom->smear) data_out[i].best = tmp[i]/neq[i];
    else data_out[i].best = tmp[mom->key[i]]/neq[mom->key[i]];
  }

  /** Then loop over bootstrap samples and reorder each sample **/
  for (iboot=0; iboot<data_out->nboot; iboot++) {
    for (i=0; i<mom->nval; i++) tmp[i]=0.0;
    for (i=0; i<nall; i++) {
      if ( (j=mom->index[i]) != DISCARDED) {
	tmp[j] += data_in[i].boot[iboot];
      }
    }
    for (i=0; i<mom->nval; i++) {
      if ( !(mom->sort) || mom->smear)
	data_out[i].boot[iboot] = tmp[i]/neq[i];
      else
	data_out[i].boot[iboot] = tmp[mom->key[i]]/neq[mom->key[i]];
    }
  } /* end loop over boots */

  free(tmp);
  free(neq);
  return 0;
}

void boot_normalise_momenta(Boot* data, Momenta* mom)
{
  const char fnm[MAXNAME] = "boot_normalise_momenta";
  int i,iboot;

  if (!mom->initialised) {
    printf("%s: Error: momenta are not initialised\n",fnm);
    return;
  }

  for (i=0; i<mom->nval; i++) {
    data[i].best /= mom->nequiv[i];
    for (iboot=0; iboot<data[i].nboot; iboot++)
      data[i].boot[iboot] /= mom->nequiv[i];
  }

}

static Real threept_interpolate(Real x, Real x1, Real x2, Real x3, 
				Real y1, Real y2, Real y3);

/************************************************************************
 * boot_interpolate_mom_1d: takes a 2d boot array in[q][p] and outputs  *
 *  a 1d array in[q][q] -> out[q] produced by interpolating in p to the *
 *  q-values.  mask filters out the null entries in the 2d array        *
 *  index gives the number of points to use for interpolation (2 or 3)  *
 ************************************************************************/
int boot_interpolate_mom_1d(Boot* out, int* outmask, Boot** in, 
			    Momenta* mom1, Momenta* mom2, 
			    int** mask, int index)
{
  const char fnm[MAXNAME] = "boot_interpolate_mom_1d";
  const int v1 = mom1->sortvar;
  const int v2 = mom2->sortvar;
  const int n1 = mom1->nval;
  const int n2 = mom2->nval;
  const int s1 = !mom1->smear; /* mom1 will be sorted */
  const int s2 = !mom2->smear; /* mom2 will be sorted */
  const int nboot = in[0][0].nboot;
  int ip[n2], np;
  Real p[n2];
  int i, ii, j, j1, j2, k1, k2, k3, ibt;
  Real x1, x2, x3, y1, y2, y3, q;

  /**  check inputs  **/
  assert(index>1);
  for (j=0; j<n1; j++) for (i=0; i<n2; i++) {
    if ( in[j][i].nboot != nboot ) {
      fprintf(stderr, "%s: incompatible parameter boots\n", fnm);
      return BADDATA;
    }
  }

  if ( s1 && !mom1->sort ) sort_momenta(mom1,v1);
  if ( s2 && !mom2->sort ) sort_momenta(mom2,v2);

  /** loop over q-values **/
  for (i=0;i<mom1->nval;i++) {
    printf("%s: i=%d of %d\n",fnm,i,n1);
    /* first check if there are any data for this q (with p<=q) */
    ii = s1 ? mom1->key[i] : i;
    q = mom1->val[v1][ii];
    for (np=j1=0;j1<n2;j1++) {
      k1 = s2 ? mom2->key[j1] : j1;
      if (MASK2(mask,ii,k1)) {
	ip[np] = k1;
	p[np] = mom2->val[v2][k1];
	np++;
      }
    }
    printf("%s: np=%d\n",fnm,np);
    if (!np || np==1 && p[0]!=q) {
      outmask[ii] = FALSE;
      continue;
    }
    /* now try to find two points straddling q (or one p==q) */
    if (p[0]>q || p[np-1]<q) {
      printf("%s: extrapolation needed - bailing out\n",fnm);
      outmask[ii] = FALSE;
      continue;
    }
    j2=0;
    while (p[j2]<q) j2++;
    k2=ip[j2]; x2=p[j2]; 
    if (j2) { k1=ip[j2-1]; x1=p[j2-1]; }
    if (index==3 && np>2) { /* find the nearest third point */
      if (j2==1) { k3=ip[j2+1]; x3=p[j2+1]; }
      else if (j2==np-1) { k3=ip[j2-2]; x3=p[j2-2]; }
      else {
	if ( q-p[j2-2] > p[j2+1]-q ) { k3=ip[j2+1]; x3=p[j2+1]; }
	else  { k3=ip[j2-2]; x3=p[j2-2]; }
      }
      y1 = in[ii][k1].best;
      y2 = in[ii][k2].best;
      y3 = in[ii][k3].best;
      out[i].best = threept_interpolate(q,x1,x2,x3,y1,y2,y3);
      for (ibt=0;ibt<nboot;ibt++) {
	y1 = in[ii][k1].boot[ibt];
	y2 = in[ii][k2].boot[ibt];
	y3 = in[ii][k3].boot[ibt];
	out[i].boot[ibt] = threept_interpolate(q,x1,x2,x3,y1,y2,y3);
      }
    } else {
    if (x2==q) { //TODO: check degeneracy!
      printf("%s: copying %d %d (q=%g)\n",fnm,i,j2,q);
      boot_copy(&out[i],in[ii][k2]);
      j1=j2;
    } else { // interpolate
      printf("%s: interpolating %d-%d (%g-%g) to %g (%d)\n",fnm,
	     k1,k2,x1,x2,q,i);
      y1 = in[ii][k1].best;
      y2 = in[ii][k2].best;
      out[i].best = ( q*(y2-y1) + x2*y1 - x1*y2 )/(x2-x1);
      for (ibt=0;ibt<nboot;ibt++) {
	y1 = in[ii][k1].boot[ibt];
	y2 = in[ii][k2].boot[ibt];
	out[i].boot[ibt] = ( q*(y2-y1) + x2*y1 - x1*y2 )/(x2-x1);
      }
    }
    }
  }

  return 0;

}

/* quadratic interpolation between three points */
static Real threept_interpolate(Real x, Real x1, Real x2, Real x3, 
				Real y1, Real y2, Real y3)
{
  const Real d = (x1-x2)*(x2-x3)*(x3-x1);
  const Real a = (x1*y2-x2*y1+x2*y3-x3*y2+x3*y1-x1*y3)/d;
  const Real b = (y1-y2)/(x1-x2) - a*(x1+x2);
  const Real c = y1-a*x1*x1-b*x1;

  return a*x*x + b*x + c;

}

int assign_momdata_subset(int** subset, MomData* data, Datafunction* xfunc,
			  double xmin, double xmax)
{
  OffsetDatafunction xfn = {xfunc,0,0};
  return assign_submomdata(subset,data,xfn,xmin,xmax);
}

int plot_momdata(MomData* data, Datafunction** xfunc, Datafunction** yfunc,
		 int nset, char* name, PlotOptions* opt)
{
  OffsetDatafunction* xfn = vecalloc(sizeof(OffsetDatafunction),nset);
  OffsetDatafunction* yfn = vecalloc(sizeof(OffsetDatafunction),nset);
  int i;
  for (i=0;i<nset;i++) {
    xfn[i].func = xfunc[i];
    xfn[i].voffset = xfn[i].soffset = 0;
    yfn[i].func = yfunc[i];
    yfn[i].voffset = yfn[i].soffset = 0;
  }
  return plot_momdata_offset(data,xfn,yfn,nset,name,opt);
}

int plot_momdata_offset(MomData* data,
		       OffsetDatafunction* xfunc, OffsetDatafunction* yfunc,
		       int nset, char* name, PlotOptions* opt)
{
  const int ncfg = data->nconfig;
  const int ndata = data->mom->nval;
  double xdata[ndata];
  double ydata[ndata];
  double** cov = arralloc(sizeof(double),2,ndata,ndata);
  double xerror[ndata];
  double yerror[ndata];
  Real* momdata = data->mom->val[data->momvar];
  int i, j, k;
  const int sort = data->mom->sort;
  const int* key = data->mom->key;
  int xvar[nset], yvar[nset];

  char filename[MAXLINE];
  char basename[MAXLINE];
  const Graph_t gtype = opt ? opt->graph_type : RAW;
  FILE* out;

/* Determine if the x and y values have error bars */
  for (i=0;i<nset;i++) {
    /* if xfunc is null we use the mom values as xdata */
    xvar[i] = (ncfg>1 && xfunc[i].func) ? xfunc[i].func(0,0,0,0) : 0;
    /* if yfunc is null we use vector_data[0] as ydata */
    yvar[i] = ncfg>1 ? yfunc[i].func ? yfunc[i].func(0,0,0,0) : 1 : 0;
  }
/* Set up the plot options */
  if (opt) {
    opt->nset = nset;
    if (!opt->errorbars) opt->errorbars = malloc(sizeof(Errorbar_t)*nset);
    for (i=0;i<nset;i++)
      opt->errorbars[i] = xvar[i] ? yvar[i] ? DXY : DX : yvar[i] ? DY : NOERR;
    if (!opt->colour) {
      opt->colour = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->colour[i]=i+1;
    }
    if (!opt->symbol) {
      opt->symbol = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->symbol[i]=i+1;
    }
    if (!opt->size) {
      opt->size = malloc(sizeof(float)*nset);
      for (i=0;i<nset;i++) opt->size[i]=DEFAULT_SIZE;
    }
    if (!opt->line) {
      opt->line = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->line[i]=0;
    }
  }

/* Make a name for file, checking if file already exists */
  if (name) {
    if (data->name) sprintf(basename,"%s_%s_",name,data->name);
    else sprintf(basename,"%s_",name);
  } else {
    if (data->name)
      sprintf(basename,"%s_",data->name);
    else
      sprintf(basename,"latest_plot");
  }
  /* add number of configs to filename */
  sprintf(basename,"%s%dcfg_",basename,data->nconfig);
  get_filename(filename,basename,suffix[gtype]);	

/* Write data to plot file */
  out = fopen(filename, "w");
  write_preamble(out,opt);

  for (j=0;j<nset;j++) {
    momxfunc_cov_ave(data,xfunc[j],xdata,cov,0,0,xvar[j]);
    if (xvar[j]) {
      for (i=0; i<ndata; i++) xerror[i] = sqrt(cov[i][i]);
    }

    if (yvar[j]) {
/* Get covariance matrix and central values */
      momdatafunc_cov_ave(data,yfunc[j],ydata,cov,0,0);
      for (i=0; i<ndata; i++) yerror[i] = sqrt(cov[i][i]);
    } else if (yfunc[j].func) {
/* evaluate the datafunction for the single config */
      const int voff = yfunc[j].voffset;
      const int nvec = data->nvector + data->nvparm - voff;
      const int soff = yfunc[j].soffset;
      const int nbundle = nvec + data->nscalar + data->nsparm - soff;
      double dat[nbundle];
      int bundle;
      for (bundle=0; bundle<data->nscalar-soff; bundle++) {
	dat[bundle+nvec] = data->scalar_data[bundle+soff][0];
      }
      for (bundle=0; bundle<data->nsparm; bundle++) {
	dat[bundle+nvec+data->nscalar-soff] = data->scalar_parm[bundle];
      }
      for (i=0; i<ndata; i++) {
	for (bundle=0; bundle<data->nvector-voff; bundle++)
	  dat[bundle] = data->vector_data[bundle+voff][0][i];
	for (; bundle<nvec; bundle++)
	  dat[bundle] = data->vector_parm[bundle-data->nvector][i];
	
	yfunc[j].func(&ydata[i],momdata[i],dat,nbundle);
      }
    } else {
      for (i=0; i<ndata; i++) {
	ydata[i] = data->vector_data[yfunc[j].voffset][0][i];
      }
    }
    if (xvar[j] && yvar[j])	{
      for (k=0; k<ndata; k++) {
	i = sort ? key[k] : k;
	fprintf(out, "%12.6g %12.6g %12.6g %12.6g\n", 
		xdata[i], ydata[i], xerror[i], yerror[i]); 
      } 
    } else if (yvar[j]) {
      for (k=0; k<ndata; k++) {
	i = sort ? key[k] : k;
	fprintf(out, "%12.6g %12.6g %12.6g\n", 
		xdata[i], ydata[i], yerror[i]); 
      }
    } else if (xvar[j]) {
      for (k=0; k<ndata; k++) {
	i = sort ? key[k] : k;
	fprintf(out, "%12.6g %12.6g %12.6g\n", 
		xdata[i], ydata[i], xerror[i]); 
      }
    } else {
      for (k=0; k<ndata; k++) {
	i = sort ? key[k] : k;
	fprintf(out, "%12.6g %12.6g\n", xdata[i], ydata[i]);
      } 
    }
    write_separator(out,opt,j+1);
  }
  fclose(out);
  free(cov);

  return 0;
}
/************************************************************************
 * plot_momdata_st: Plots data as function of time and space momenta    *
 *    separately - one set for each time momentum.                      *
 ************************************************************************/

int plot_momdata_st(MomData* data, Datafunction** yfunc, int nfunc, char* name,
		    PlotOptions* opt)
{
  const char fnm[MAXNAME] = "plot_momdata_st";
  const int ncfg = data->nconfig;
  const int bct = data->mom->bc[IT];
  const int Nt = (bct==PERIODIC) ? data->mom->nmom_out[IT]+1 
                                 : data->mom->nmom_out[IT];
  const int nset = nfunc*Nt;
  const int nall = data->mom->nval;
  const int ndata = nall/Nt;
  double ydata[nall];
  double** cov = arralloc(sizeof(double),2,nall,nall);
  double yerror[nall];
  Real* momdata = data->mom->val[data->momvar];
  int i, j, k, t;
  const int sort = data->mom->sort;
  const int* key = data->mom->key;
  int yvar[nset];
  OffsetDatafunction* yfn = vecalloc(sizeof(OffsetDatafunction),nfunc);

  char filename[MAXLINE];
  char basename[MAXLINE];
  const Graph_t gtype = opt ? opt->graph_type : RAW;
  FILE* out;

  if (sort) {
    fprintf(stderr,"%s: warning: sorting not implemented\n",fnm);
  }
/* Determine if the y values have error bars, set up the OffsetDatafunc */
  for (i=0;i<nfunc;i++) {
    /* if yfunc is null we use vector_data[0] as ydata */
    yvar[i] = ncfg>1 ? yfunc[i] ? yfunc[i](0,0,0,0) : 1 : 0;
    yfn[i].func = yfunc[i];
    yfn[i].soffset = yfn[i].voffset = 0;
  }
/* Set up the plot options */
  if (opt) {
    opt->nset = nset;
    if (!opt->errorbars) opt->errorbars = malloc(sizeof(Errorbar_t)*nset);
    for (i=0;i<nset;i++)
      opt->errorbars[i] = yvar[i/Nt] ? DY : NOERR;
    if (!opt->colour) {
      opt->colour = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->colour[i]=i%Nt+1;
    }
    if (!opt->symbol) {
      opt->symbol = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->symbol[i]=i/Nt+1;
    }
    if (!opt->size) {
      opt->size = malloc(sizeof(float)*nset);
      for (i=0;i<nset;i++) opt->size[i]=DEFAULT_SIZE;
    }
    if (!opt->line) {
      opt->line = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->line[i]=0;
    }
    if (!opt->legend) {
      opt->legend  = malloc(sizeof(char*)*nset+nset*MAXLINE);
      for (i=0;i<nset;i++) {
	opt->legend[i] = (char*)opt->legend+nset*sizeof(char*)+i*MAXLINE;
	sprintf(opt->legend[i],"k\\s0\\N=%d",i%Nt);
      }
    }
  }

/* Make a name for file, checking if file already exists */
  if (name) {
    if (data->name) sprintf(basename,"%s_%s_",name,data->name);
    else sprintf(basename,"%s_",name);
  } else {
    if (data->name)
      sprintf(basename,"%s_",data->name);
    else
      sprintf(basename,"latest_plot");
  }
  /* add number of configs to filename */
  sprintf(basename,"%s%dcfg_",basename,data->nconfig);
  get_filename(filename,basename,suffix[gtype]);	

/* Write data to plot file */
  out = fopen(filename, "w");
  write_preamble(out,opt);

  for (j=0;j<nfunc;j++) {
    if (yvar[j]) {
/* Get covariance matrix and central values */
      momdatafunc_cov_ave(data,yfn[j],ydata,cov,0,0);
      for (i=0; i<ndata*Nt; i++) yerror[i] = sqrt(cov[i][i]);
    } else if (yfunc[j]) {
/* evaluate the datafunction for the single config */
      const int nvec = data->nvector + data->nvparm;
      const int nbundle = nvec + data->nscalar + data->nsparm;
      double dat[nbundle];
      int bundle;
      for (bundle=0; bundle<data->nscalar; bundle++) {
	dat[bundle+nvec] = data->scalar_data[bundle][0];
      }
      for (bundle=0; bundle<data->nsparm; bundle++) {
	dat[bundle+nvec+data->nscalar] = data->scalar_parm[bundle];
      }
      for (i=0; i<ndata*Nt; i++) {
	for (bundle=0; bundle<data->nvector; bundle++)
	  dat[bundle] = data->vector_data[bundle][0][i];
	for (; bundle<nvec; bundle++)
	  dat[bundle] = data->vector_parm[bundle-data->nvector][i];
	
	yfunc[j](&ydata[i],momdata[i],dat,nbundle);
      }
    } else {
      for (i=0; i<ndata*Nt; i++) {
	ydata[i] = data->vector_data[0][0][i];
      }
    }
    if (yvar[j]) {
      for (t=0;t<Nt;t++) {
	for (k=0; k<ndata; k++) {
	  i = sort ? key[k] : k;
	  fprintf(out, "%12.6g %12.6g %12.6g\n", 
		  momdata[i*Nt], ydata[i*Nt+t], yerror[i*Nt+t]); 
	}
	write_separator(out,opt,j*Nt+t+1);
      }
    } else {
      for (t=0;t<Nt;t++) {
	for (k=0; k<ndata; k++) {
	  i = sort ? key[k] : k;
	  fprintf(out, "%12.6g %12.6g\n", momdata[i*Nt], ydata[i*Nt+t]);
	} 
	write_separator(out,opt,j*Nt+t+1);
      }
    }
  }
  fclose(out);
  return 0;
}

int fit_momdata(MomData* data, BootFitFuncs* fit)
{
  const char fnm[MAXNAME] = "fit_momdata";
  const int nparam = fit->nparam;
  const int nboot = fit->nboot;
  const int ncfg  = data->nconfig;
  const int ndata = ntrue(fit->subset,data->mom->nval) ;
  const int nvector = data->nvector;
  const int nscalar = data->nscalar;
  int i, j, status;
  /* if xfunc is null we use the mom values as xdata */
  const int xvar = (ncfg>1 && fit->xfunc) ? fit->xfunc(0,0,0,0) : 0;
  /* if yfunc is null we use vector_data[0] as ydata */
  const int yvar = ncfg>1 ? fit->yfunc ? fit->yfunc(0,0,0,0) : 1 : 0;
  OffsetDatafunction xfunc = {fit->xfunc,fit->xoffset,fit->xsoffset};
  OffsetDatafunction yfunc = {fit->yfunc,fit->yoffset,fit->ysoffset};
  FitData fitdat;
  /* temporary arrays of bootstrap samples */
  Real*** vecbundle = arralloc(sizeof(Real*),2,nvector,ncfg);
  Real** scalbundle = arralloc(sizeof(Real),2,nscalar,ncfg);

  /* retainer for original data during loop over boot samples */
  Real*** vdat = data->vector_data;
  Real**  sdat = data->scalar_data;

  if (!yvar) {
    printf("%s: cannot perform chi-squared fit",fnm);
    printf("if the ydata are constant!\n");
    return BADDATA;
  }

  init_fitdata(&fitdat,fit,ndata,xvar);
  printf("%s: nfit=%d\n",fnm,ndata);
  fit->ndof = ndata;
  for (i=0;i<nparam;i++) if (!fit->freeze[i]) fit->ndof--;

/* Get covariance matrix and central values and set up initial guesses */
  momxfunc_cov_ave(data,xfunc,fitdat.xdata,fitdat.xdatacov,
		   0,fit->subset,xvar);
  momdatafunc_cov_ave(data,yfunc,fitdat.ydata,fitdat.ydatacov,
		      0,fit->subset);
  fit->guess(fitdat.param,fit->freeze,nparam,
	     fitdat.xdata,fitdat.ydata,ndata);

/* Perform chi-square fit to get the best fit */
  status = chisq_fit(&fitdat);
  for (j=0; j<nparam; j++) fit->param[j].best = fitdat.param[j];
  fit->chisq.best = fitdat.chisq;

/* Save the original data */
  sdat = data->scalar_data;
  vdat = data->vector_data;

/* Loop over bootstrap samples */
  rand_initialise(fit->seed);

  for (i=0; i<nboot; i++) {
    get_boot_datasetbundle(vecbundle,vdat,nvector,
			   scalbundle,sdat,nscalar,ncfg);
/* put bootstrap samples in the momdata to perform stats */
    data->scalar_data = scalbundle;
    data->vector_data = vecbundle;
    momxfunc_cov_ave(data,xfunc,fitdat.xdata,fitdat.xdatacov,
		     0,fit->subset,xvar);
    momdatafunc_cov_ave(data,yfunc,fitdat.ydata,fitdat.ydatacov,
			0,fit->subset);

    /* set up initial guess including frozen parameters */
    for (j=0; j<nparam; j++) {
      if (fit->freeze[j] && fit->param[j].nboot != 0) {
        fitdat.param[j] = fit->param[j].boot[i];
      } else {
        fitdat.param[j] = fit->param[j].best;
      }
    }
    chisq_fit(&fitdat);
    for (j=0; j<nparam; j++) {
      if (!fit->freeze[j]) fit->param[j].boot[i] = fitdat.param[j];
    }
    fit->chisq.boot[i] = fitdat.chisq;
  }

  /* restore original data */
  data->scalar_data = sdat;
  data->vector_data = vdat;

  return 0;
}

int fit_momdata_xboot(MomData* data, Boot* xboot, BootFitFuncs* fit)
{
  const char fnm[MAXNAME] = "fit_momdata_xboot";
  const int nparam = fit->nparam;
  const int nboot = fit->nboot;
  const int ncfg  = data->nconfig;
  const int ndata = ntrue(fit->subset,data->mom->nval) ;
  const int nvector = data->nvector;
  const int nscalar = data->nscalar;
  int i, j, k, status;
  /* if yfunc is null we use vector_data[0] as ydata */
  const int yvar = ncfg>1 ? fit->yfunc ? fit->yfunc(0,0,0,0) : 1 : 0;
  OffsetDatafunction yfunc = {fit->yfunc,fit->yoffset,fit->ysoffset};
  FitData fitdat;
  /* temporary arrays of bootstrap samples */
  Real*** vecbundle = arralloc(sizeof(Real*),2,nvector,ncfg);
  Real** scalbundle = arralloc(sizeof(Real),2,nscalar,ncfg);

  /* retainer for original data during loop over boot samples */
  Real*** vdat = data->vector_data;
  Real**  sdat = data->scalar_data;

  if (!yvar) {
    printf("%s: cannot perform chi-squared fit",fnm);
    printf("if the ydata are constant!\n");
    return BADDATA;
  }

  init_fitdata(&fitdat,fit,ndata,0);
  printf("%s: nfit=%d\n",fnm,ndata);
  fit->ndof = ndata;
  for (i=0;i<nparam;i++) if (!fit->freeze[i]) fit->ndof--;

/* Get covariance matrix and central values and set up initial guesses */
  for (j=0,k=0; j<data->mom->nval; j++) {
    if (MASK(fit->subset,j)) fitdat.xdata[k++] = xboot[j].best;
  }
  momdatafunc_cov_ave(data,yfunc,fitdat.ydata,fitdat.ydatacov,
		      0,fit->subset);
  fit->guess(fitdat.param,fit->freeze,nparam,
	     fitdat.xdata,fitdat.ydata,ndata);

/* Perform chi-square fit to get the best fit */
  status = chisq_fit(&fitdat);
  for (j=0; j<nparam; j++) fit->param[j].best = fitdat.param[j];
  fit->chisq.best = fitdat.chisq;

/* Save the original data */
  sdat = data->scalar_data;
  vdat = data->vector_data;

/* Loop over bootstrap samples */
  rand_initialise(fit->seed);

  for (i=0; i<nboot; i++) {
    get_boot_datasetbundle(vecbundle,vdat,nvector,
			   scalbundle,sdat,nscalar,ncfg);
/* put bootstrap samples in the momdata to perform stats */
    data->scalar_data = scalbundle;
    data->vector_data = vecbundle;
    for (j=0,k=0; j<data->mom->nval; j++) {
      if (MASK(fit->subset,j)) fitdat.xdata[k++] = xboot[j].boot[i];
    }
    momdatafunc_cov_ave(data,yfunc,fitdat.ydata,fitdat.ydatacov,
			0,fit->subset);

    /* set up initial guess including frozen parameters */
    for (j=0; j<nparam; j++) {
      if (fit->freeze[j] && fit->param[j].nboot != 0) {
        fitdat.param[j] = fit->param[j].boot[i];
      } else {
        fitdat.param[j] = fit->param[j].best;
      }
    }
    chisq_fit(&fitdat);
    for (j=0; j<nparam; j++) {
      if (!fit->freeze[j]) fit->param[j].boot[i] = fitdat.param[j];
    }
    fit->chisq.boot[i] = fitdat.chisq;
  }

  /* restore original data */
  data->scalar_data = sdat;
  data->vector_data = vdat;

  return 0;
}

void momxfunc_cov_ave(MomData* data, OffsetDatafunction func, 
		      double* ave, double** cov, 
		      int* cmask, int* xmask, int xvar)
{
  const int ndata = data->mom->nval;
  Real* momdata = data->mom->val[data->momvar];
  int i, j;

  if (xvar) {
/* Get covariance matrix and central values */
    momdatafunc_cov_ave(data,func,ave,cov,cmask,xmask);
  } else if (func.func) {
    if (func.func(0,0,0,0)) {
/* evaluate the datafunction for a single config */
      const int nv = data->nvector - func.voffset;
      const int ns = data->nscalar - func.soffset;
      const int nvec = nv + data->nvparm;
      const int nbundle = nvec + ns + data->nsparm;
      double dat[nbundle];
      int bundle;
      for (bundle=0; bundle<ns; bundle++) {
	dat[bundle+nvec] = data->scalar_data[bundle+func.soffset][0];
      }
      for (bundle=0; bundle<data->nsparm; bundle++) {
	dat[bundle+nvec+ns] = data->scalar_parm[bundle];
      }
      for (i=0,j=0; i<ndata; i++) {
	if (MASK(xmask,i)) {
	  for (bundle=0; bundle<nv; bundle++)
	    dat[bundle] = data->vector_data[bundle+func.voffset][0][i];
	  for (; bundle<nvec; bundle++)
	    dat[bundle] = data->vector_parm[bundle-nv][i];

	  func.func(&ave[j],momdata[i],dat,nbundle);
	  j++;
	}
      }
    } else {
/* the datafunction depends only on the "x" values so pass only those */
      for (i=0,j=0;i<ndata;i++) {
	if (MASK(xmask,i)) func.func(&ave[j++],momdata[i],0,0);
      }
    }
  } else {
    for (i=0,j=0;i<ndata;i++) if (MASK(xmask,i)) ave[j++] = momdata[i];
  }
}

void momdatafunc_cov_ave(MomData* data, OffsetDatafunction func, 
			 double* ave, double** cov, int* cmask, int* xmask)
{
  VectorData vec;
  vec.type = data->type;
  vec.parm = data->parm;
  vec.nconfig = data->nconfig;
  vec.length  = data->mom->nval;
  vec.nscalar = data->nscalar;
  vec.nvector = data->nvector;
  vec.nsparm  = data->nsparm;
  vec.nvparm  = data->nvparm;
  vec.scalar_data = data->scalar_data;
  vec.vector_data = data->vector_data;
  vec.scalar_parm = data->scalar_parm;
  vec.vector_parm = data->vector_parm;
  vec.xdata = data->mom->val[data->momvar];

  offsetfunc_cov_ave(&vec,&func,ave,cov,cmask,xmask);
}

static int gaugename(MomData* data)
{
  const char fnm[MAXNAME] = "gaugename";
  char gauge_code[6] = "\0\0\0\0\0\0";
  const Gauge_parameters* parm = data->parm;
  const Momenta* mom = data->mom;

  if (data->type != GAUGE_D) {
    fprintf(stderr,"%s: wrong data type %d\n",fnm,data->type);
    return BADDATA;
  }    

  if (mom->L[IT]<=1) { /* 3-d, real-time data */
    sprintf(data->name,"%s_r%02dt%04d%c",data->basename,
	    data->id,data->time,gcode[parm->gauge]);
    return 0;
  }

/*** code for gauge type and parameter ***/
  switch (parm->gauge) {
  case LANDAU: gauge_code[0]='l'; break;
  case COVARIANT: 
    sprintf(gauge_code,"x%02d",INT(10*parm->gauge_param));
    break;
  case COULOMB: gauge_code[0]='c'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   parm->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='L';
  }

/*** create filename ***/
  switch (parm->nf) {
  case 0:
    sprintf(data->name,"%s_b%03ds%02dt%02d%s",data->basename,
	    INT(parm->beta*100),
	    parm->lattice->length[IX],parm->lattice->length[IT],
	    gauge_code);
    break;
  case 2:
    if (parm->id==1) { /* nonzero mu or j */
      sprintf(data->name,"%s_b%03dk%04dmu%04dj%03ds%02dt%02d%s",data->basename,
	      INT(parm->beta*100),INT(parm->mass[0]*10000),
	      INT(parm->mass[2]*1000),INT(parm->mass[3]*1000),
	      parm->lattice->length[IX],parm->lattice->length[IT],
	      gauge_code);
    } else {
      sprintf(data->name,"%s_b%03dk%04ds%02dt%02d%s",data->basename,
	      INT(parm->beta*100),INT(parm->mass[0]*10000),
	      parm->lattice->length[IX],parm->lattice->length[IT],
	      gauge_code);
    }
    break;
  default:
    fprintf(stderr,"%s: Warning: %d flavours not implemented\n",fnm,parm->nf);
    fprintf(stderr,"%s: Creating 0-flavour name\n",fnm);
    sprintf(data->name,"%s_b%03ds%02dt%02d%s",data->basename,
	    INT(parm->beta*100),
	    parm->lattice->length[IX],parm->lattice->length[IT],
	    gauge_code);
  }

  return 0;
}

static int quarkname(MomData* data)
{
  const char fnm[MAXNAME] = "quarkname";
  char gauge_code[6] = "\0\0\0\0\0\0";
  const Propagator_parameters* parm = data->parm;
  const Gauge_parameters* gpar = parm->gpar;

  if (data->type != FERMION) {
    fprintf(stderr,"%s: wrong data type %d\n",fnm,data->type);
    return BADDATA;
  }    

/*** code for gauge type and parameter ***/
  switch (gpar->gauge) {
  case LANDAU: gauge_code[0]='l'; break;
  case COVARIANT: 
    sprintf(gauge_code,"x%02d",INT(10*gpar->gauge_param));
    break;
  case COULOMB: gauge_code[0]='c'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   gpar->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='L';
  }

/*** create filename ***/
  switch (gpar->nf) {
  case 0:
    sprintf(data->name,"%s_b%03dk%04ds%dt%02d%s",data->basename,
	    INT(gpar->beta*100),INT(parm->kappa*10000),
	    gpar->lattice->length[IX],gpar->lattice->length[IT],
	    gauge_code);
    break;
  case 2:
    if (gpar->has_chempot) {
      sprintf(data->name,
	      "%s_b%03dks%04dmu%04dj%03ds%02dt%02d%s.kv%04dmu%04dj%03d",
	      data->basename,INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      INT(gpar->mass[2]*1000),INT(gpar->mass[3]*1000),
	      gpar->lattice->length[IX],gpar->lattice->length[IT],
	      gauge_code,INT(parm->kappa*10000),INT(parm->chempot*1000),
	      INT(parm->j*1000));
    } else { /* assume that valence chempot is zero */
      sprintf(data->name,"%s_b%03dks%04ds%02dt%02d%s.kv%04d",
	      data->basename,INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      gpar->lattice->length[IX],gpar->lattice->length[IT],
	      gauge_code,INT(parm->kappa*10000));
    }
    break;
  default:
    fprintf(stderr,"%s: Warning: %d flavours not implemented\n",fnm,gpar->nf);
    fprintf(stderr,"%s: Creating 0-flavour name\n",fnm);
    sprintf(data->name,"%s_b%03ds%02dt%02d%s",data->basename,
	    INT(gpar->beta*100),
	    gpar->lattice->length[IX],gpar->lattice->length[IT],
	    gauge_code);
  }

  return 0;
}
