/* $Id: marquardt.c,v 1.3 2005/11/29 17:28:07 jonivar Exp $	*/
/************************************************************************
 *                                                                      *
 *  The following are a set of functions for applying the               *
 *  Marquardt-Levenberg algorithm to correlated data.                   *
 *  Adapted from v1.15 of the original Edinburgh (HHHH) code written    *
 *  mostly by hps, based on Numerical Recipes.                          *
 *                                                                      *
 ************************************************************************/
#include "analysis.h"
#include <matrix_inverse.h>

static Real lambda_factor = 10.0 ;
static Real lambda_start = 0.001;

void change_lambda_factor(Real factor)
{
  lambda_factor = factor;
}

void change_lambda_start( Real l_start )
{
  lambda_start = l_start;
}

static void update_params(Real *fit_param, Real *delta_param,
			  int *freeze,	int nparam);

/************************************************************************
 *                                                                      *
 * marquardt_minimise performs one iteration of the Marquardt-Levenberg *
 *  algorithm for minimising chisq, using the first derivative (beta)   *
 *  and second derivative (alpha) of chisq wrt the varying parameters   *
 *                                                                      *
 ************************************************************************/
int marquardt_minimise(FitData* fit, Real* lambda)
{
  const char fnm[MAXNAME] = "marquardt_minimise";
  const int nparam = fit->nparam;
  const int nvary = find_varying_parameters(fit->freeze, nparam);
  Real** alpha = arralloc(sizeof(Real),2,nvary,nvary);
  Real** alpha_prime = arralloc(sizeof(Real),2,nvary,nvary);
  Real* beta = vecalloc(sizeof(Real),nvary);
  Real* delta_param = vecalloc(sizeof(Real),nvary); /* Change after iteration */
  Real* old_param = vecalloc(sizeof(Real),nparam);
  Real old_chisq;
  int i,j ;

  /*** If lambda = 0, then find alpha and invert to get covariance ***
   *** matrix of parameters                                        ***/
  if (*lambda==0.0) {
    if ( get_fit_param_covariance(fit) ) {
      printf("%s: cannot extract parameter covariance matrix\n",fnm);
      return PARAM_COVARIANCE_ERROR;
    }

    return 0;
  }

  /*** If lambda < 0, try an initial guess using the parameters provided ***/
  if (*lambda < 0.0) {
    *lambda = lambda_start;
    old_chisq = get_chisq(fit);
  } else { /* store current chisq into old chisq */
    old_chisq = fit->chisq;
  }

  /*** Find alpha and beta using the old parameters ***/
  get_alpha_beta(fit, alpha, beta) ;

  /*** alpha' = (1 + lambda*d_ij)alpha_ij        ***
   *** ( introduce small change in the diagonal) ***/
  for (i=0; i<nvary; i++) {
    alpha_prime[i][i] = (1.0 + *lambda)*alpha[i][i] ;
    for (j=i+1; j<nvary; j++)
      alpha_prime[i][j] = alpha_prime[j][i] = alpha[i][j];
  }

  /*** solve alpha' * dparam = beta ***/
  solve_delta_param(alpha_prime,beta,nvary,delta_param);

  /*** guess param = param + delta_param ***/
  for (i=0;i<nparam;i++) old_param[i] = fit->param[i];
  update_params(fit->param, delta_param, fit->freeze, nparam);

  /*** Calculate chisq from new parameters ***/
  fit->chisq = get_chisq(fit);

  /*** Check if chisq has gotten smaller.                        ***
   *** If it has, then update the parameters and decrease lambda ***
   *** otherwise, increase lambda                                ***/
  if (old_chisq > fit->chisq) {
    *lambda /= lambda_factor;

    /*** Free space ***/
    free(alpha);
    free(alpha_prime);
    free(beta);
    free(delta_param);
    free(old_param);

    return 0;
  } else {
    *lambda *= lambda_factor;
    fit->chisq = old_chisq;
    for (i=0;i<nparam;i++) fit->param[i]=old_param[i];

    /*** Free space ***/
    free(alpha);
    free(alpha_prime);
    free(beta);
    free(delta_param);
    free(old_param);

    return TRY_AGAIN;
  }
}

/*** This routine updates the total list of parameters ***
 *** from the change in the varying parameters         ***/
static void update_params(Real *fit_param, Real *delta_param,
			  int *freeze,	int nparam)
{
  int i,j;

  for (j=0,i=0; i<nparam; i++) {
    if (!freeze[i]) fit_param[i] += delta_param[j++];
  }
}

/*** The following routines evaluate the matrices alpha and beta ***
 *** for the marquardt-levenberg algorithim for correlated data. ***/

static int add_2nd_derivative = TRUE;

void change_2nd_derivative(int choice)
{
  add_2nd_derivative = choice;
}

static int sum_beta(int l, int l_vary,
		    Real** inv_data_corr, Real* delta,
		    Real** dyda_norm, int ndata, Real *beta );
static int sum_alpha(int k, int k_vary, int l, int l_vary,
		     Real** inv_data_corr, Real* delta,
		     Real** dyda_norm,	Real*** d2yda2_norm,
		     int ndata, Real** alpha);

int get_alpha_beta(FitData *fit, Real** alpha, Real* beta)
{
  const char fnm[MAXNAME] = "get_alpha_beta";
  const int nparam = fit->nparam;
  const int nvary = find_varying_parameters(fit->freeze, nparam);
  const int ndata = fit->ndata;
  Real* delta;

  int i, j, k, l, k_vary, l_vary ;
  int test;

  /*** Allocate space and initialise variables ***/
  i = j = k = l = k_vary = l_vary = 0;

  delta       = vecalloc(sizeof(Real),ndata);
  fit->dyda   = arralloc(sizeof(Real),2,ndata,nparam);
  fit->d2yda2 = arralloc(sizeof(Real),3,ndata,nparam,nparam);

  for (k=0; k<nvary; k++) {
    beta[k] = alpha[k][k] = 0.0;
    for (l=k+1; l<nvary; l++) alpha[k][l] = alpha[l][k] = 0.0;
  }

  /***assign values to y_fit and the derivatives ***/
  get_norm_diff(fit, delta);

  for (k_vary=0, k=0; k<nparam; k++) {
    if ( !fit->freeze[k] ) {
      test = sum_beta(k, k_vary, fit->inv_data_corr, delta,
		      fit->dyda, ndata, beta);

      test = sum_alpha(k,k_vary, k, k_vary, fit->inv_data_corr, delta,
		       fit->dyda, fit->d2yda2, ndata, alpha);

      for (l_vary=k_vary+1, l=k+1; l<nparam; l++) {
	if ( !fit->freeze[l] ) {
	  test = sum_alpha(k,k_vary,l,l_vary, fit->inv_data_corr, delta,
			   fit->dyda, fit->d2yda2, ndata, alpha);

	  l_vary++;
	}
      }
      k_vary++;
    }
  }

  /***free space ***/
  free(delta);
  free(fit->dyda); fit->dyda = 0;
  free(fit->d2yda2); fit->d2yda2 = 0;

  /***Check for possible counting error  ***/
  if ( ( k_vary > k ) || ( l_vary > l ) ) {
    printf("%s: error in counting varying parameters\n",fnm);
    printf("k = %d, k_vary = %d \n",k,k_vary);
    printf("l = %d, l_vary = %d \n",l,l_vary);
    return FREEZE_ARRAY_CORRUPTED;
  }

  return 0;
}

static int sum_beta(int l, int l_vary,
		    Real** inv_data_corr, Real* delta,
		    Real** dyda_norm, int ndata, Real *beta)
{
  const char fnm[MAXNAME] = "sum_beta";
  int i,j;

  /***Check for silly error ***/
  if (l_vary>l) {
    printf("%s: number of frozen parameters less than zero\n",fnm);
    return FREEZE_ARRAY_CORRUPTED;
  }

  beta[l_vary] = 0.0 ;

  /***Loop over data ***/
  for (i=0; i<ndata; i++) {
    beta[l_vary] += dyda_norm[i][l]*inv_data_corr[i][i]*delta[i];

    for (j=i+1; j<ndata; j++) {
      beta[l_vary] += inv_data_corr[i][j]*(dyda_norm[i][l]*delta[j]
					   + dyda_norm[j][l]*delta[i]);
    }
  }
  return 0;
}

static int sum_alpha(int k, int k_vary, int l, int l_vary,
		     Real** inv_data_corr, Real* delta,
		     Real** dyda_norm,	Real*** d2yda2_norm,
		     int ndata, Real** alpha)
{
  const char fnm[MAXNAME] = "sum_alpha";
  int i,j;
  const Real factor = add_2nd_derivative ? 1.0 : 0.0;

  /***Check for silly error ***/
  if (( l_vary > l) || ( k_vary > k )) {
    printf("%s: number of frozen parameters less than zero \n",fnm);
    return FREEZE_ARRAY_CORRUPTED;
  }

  /***Set alpha[l_v][k_v] = 0 ***/
  alpha[k_vary][l_vary] = 0.0;

  /***Loop over data ***/
  for (i=0; i<ndata; i++) {
    alpha[k_vary][l_vary]
      += inv_data_corr[i][i]*(dyda_norm[i][k]*dyda_norm[i][l]
			      - factor*d2yda2_norm[i][k][l]*delta[i]);

    for (j=i+1; j<ndata; j++) {
      alpha[k_vary][l_vary] += inv_data_corr[i][j]*
	( dyda_norm[i][l]*dyda_norm[j][k] + dyda_norm[j][l]*dyda_norm[i][k]
	  - factor*d2yda2_norm[i][k][l]*delta[j]
	  - factor*d2yda2_norm[j][k][l]*delta[i] ) ;
    }
  }

  /***Symmetrise alpha ***/
  alpha[l_vary][k_vary] = alpha[k_vary][l_vary] ;

  return 0;
}

int get_fit_param_covariance(FitData* fit)
{
  const int nparam = fit->nparam;
  const int nvary = find_varying_parameters(fit->freeze,nparam);
  Real **alpha = arralloc(sizeof(Real),2,nvary,nvary);
  Real **alpha_inv = arralloc(sizeof(Real),2,nvary,nvary);
  Real *beta = vecalloc(sizeof(Real),nvary);
  int i, j, ii, jj;

  /*** Initialise parameter covariance to zero ***/
  if (!fit->paramcov) fit->paramcov=arralloc(sizeof(Real),2,nparam,nparam);
  arrf_set_zero(*(fit->paramcov),sizeof(Real),sizeof(Real),nparam*nparam);

  /*** find alpha and beta ***/
  get_alpha_beta(fit,alpha,beta);

  /*** Invert alpha ***/
  get_inverse(alpha,nvary,alpha_inv);

  /*** Put alpha_inv into appropriate positions on paramcov ***/
  for (ii=0, i=0; i<nparam; i++) {
    if (!fit->freeze[i]) {
      fit->paramcov[i][i] = alpha_inv[ii][ii];

      for (jj=ii+1,j=i+1; j<nparam; j++ ) {
	if (!fit->freeze[j]) {
	  fit->paramcov[i][j] = fit->paramcov[j][i] = alpha_inv[ii][jj];
	  jj++;
	}
      }
      ii++;
    }
  }

  free(beta);
  free(alpha);
  free(alpha_inv);

  return 0;
}

int find_varying_parameters(int *freeze, int nparams)
{
  int i, nvary=0;

  for (i=0; i<nparams; i++) if (!freeze[i]) nvary++;

  if ( nvary == 0 ) {
    printf("You are assuming that all the parameters are fixed .... \n");
    printf("Why do you want to get a best fit then ?\n");
  }

  return nvary;
}
