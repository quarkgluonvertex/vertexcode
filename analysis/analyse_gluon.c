#include "analysis.h"
#include "momdata.h"
#include "field_params.h"
#include <float.h>

#define MAX_GRAPH 6
#define MAX_FIT   5
#define SQRT2 1.4142136

Datafunction qsqrD, qsqrD1;
Datafunction logx, logy, logy1;
Modelfunction massivemom, FischerMaasMueller, FMM_vacuum;
Guessfunction massivemomguess, FMMguess, FMM_vacuumguess;

static void init_plotopts(PlotOptions* opt);
static void init_fit(BootFitFuncs* fit, int nboot, int ndata);
static void assign_default_momenta(Momenta* readmom, Momenta* mom);

int main(int argc, char** argv)
{
  Gauge_parameters gpar;
  Momenta rmom, mom;
  MomData gluon;
  Analysis_params run;
  int gmask[4]; /* up to 4 columns in input file */
  int nset, nfc;
  Datafunction* xfunc[MAX_GRAPH] = {Df_x, Df_x, logx, Df_x, Df_x, logx};
  Datafunction* yfunc[MAX_GRAPH] = {0, qsqrD, logy, Df_y1, qsqrD1, logy1};
  Modelfunction* fits[MAX_FIT] = {FMM_vacuum, massivemom, massivemom,
				  FischerMaasMueller, FischerMaasMueller};
  Guessfunction* guesses[MAX_FIT] = {FMM_vacuumguess, 
				     massivemomguess, massivemomguess,
				     FMMguess, FMMguess};
  int fitoffset[MAX_FIT] = {0, 0, 1, 0, 1};
  bool elmag;

  PlotOptions plot;
  char plotname[MAX_GRAPH][MAXLINE] = {"D", "q2D", "logD",
				       "DE", "q2DE", "logDE"};
  BootFitFuncs fit;
  int i;
  char filename[MAXLINE];
  int status;

/*** Read run parameters from file ***/
  if (argc<2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename>\n", argv[0]);
    return BADPARAM;
  }
  if (assign_params_analyse_gluon(argv[1],&gpar,&mom,&run)) {
    return BADPARAM;
  }
  elmag = (gpar.id==1);
  assign_default_momenta(&rmom,&mom);
  if (elmag) {
    rmom.nvar = mom.nvar = 3;
    setup_momenta_st(&rmom);
    setup_momenta_st(&mom);
  } else {
    setup_momenta(&rmom);
    setup_momenta(&mom);
  }

  if (strlen(run.plotopt_file)) {
    status = assign_plotoptions(run.plotopt_file,&plot);
    if (status==BADFILE) init_plotopts(&plot);
    else if (status) return status;
  } else init_plotopts(&plot);

  nset = (gpar.gauge==LANDAU || gpar.gauge==COULOMB) ? 1 : 2;
  if (elmag) nset++;
#ifdef FILE_NOQ
  nfc = nset;
  for (i=0;i<nfc;i++) gmask[i]=true;
#else
  nfc = nset+1;
  gmask[0]=false; for (i=1;i<nfc;i++) gmask[i]=true;
#endif
/*** Initialise, allocate space and read data ***/
  if ( (status = init_momdata(&gluon,&gpar,&rmom,GAUGE_D,
			      run.nconfig,0,nset,
			      "gluon",run.data_dir)) ) return status;
  if ( (status=read_momdata(&gluon,gmask,nfc)) ) return status;
  if ( (status=reorder_momdata(&gluon,&mom)) ) return status;

  if (run.fit) init_fit(&fit,run.nboot,mom.nval);
  for (i=0;i<run.nfit;i++) {
    int var = gluon.momvar;
    int j;
    /*** perform a global fit of DM or DE ***/
    gluon.momvar = 0; // KLUDGE: this is the total q
    fit.model = fits[run.fit[i]];
    fit.guess = guesses[run.fit[i]];
    fit.yoffset = fitoffset[run.fit[i]];
    if (fit.yoffset == 1) { /* do not fit electric prop for zero spatial mom */
      for (j=0;j<mom.nval;j++) if (mom.val[MOMQS][j]==0.0) fit.subset[j]=false;
    }
    if ( (status=fit_momdata(&gluon,&fit)) ) return status;
    print_stats(&fit);
    gluon.momvar = var;
  }

  /*** Plot D vs q, q^2 D vs q ***/
  if (run.nplot) {
  printf("Making plots for ");
  for (i=0; i<run.nplot; i++) printf("%s ",plotname[run.plot[i]]);
  printf("\n");
  if (run.single_plot) {
    Datafunction* xf[run.nplot];
    Datafunction* yf[run.nplot];
    for (i=0;i<run.nplot;i++) {
      xf[i]=xfunc[run.plot[i]]; yf[i]=yfunc[run.plot[i]];
    }
    if (elmag)
      plot_momdata_st(&gluon,yf,run.nplot,"gluon",&plot);
    else
      plot_momdata(&gluon,xf,yf,run.nplot,"gluon",&plot);
  } else {
    for (i=0; i<run.nplot; i++) {
      const int j=run.plot[i];
      plot_momdata(&gluon,&xfunc[j],&yfunc[j],1,plotname[j],&plot);
    }
  }
  }

  return 0;
}

int qsqrD(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<1) {
    fprintf(stderr,"qsqrD: illegal narg %d\n",narg);
    return EVIL;
  }
  *y = x*x*arg[0];
  return 1;
}

int qsqrD1(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"qsqrD1: illegal narg %d\n",narg);
    return EVIL;
  }
  *y = x*x*arg[1];
  return 1;
}

int logx(Real* y, Real x, Real* arg, int narg)
{
  if (y) *y= (x>0.0) ? log(x) : -3;
  return 0;
}

int logy(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (y) *y=log(arg[0]);
  return 1;
}

int logy1(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (y) *y=log(arg[1]);
  return 1;
}

static void assign_default_momenta(Momenta* readmom, Momenta* mom)
{
  int i;

  set_default_momenta(readmom);

  for (i=0;i<DIR;i++) {
    readmom->L[i] = mom->L[i];
    readmom->nmom_in[i] = readmom->nmom_out[i] = mom->nmom_in[i];
    readmom->bc[i] = mom->bc[i];
  }
  readmom->nvar = mom->nvar;

}

static void init_plotopts(PlotOptions* opt)
{
  opt->graph_type = GRACE;
  opt->title[0] = opt->xaxis[0] = opt->yaxis[0] = '\0';
  opt->xscale = opt->yscale = LINEAR;
  opt->xmin = opt->xmax = opt->ymin = opt->ymax
    = opt->xgrid = opt->ygrid = UNSET_DBL;
  opt->xtick = opt->ytick = UNSET_INT;
  opt->nset = UNSET_INT;
  opt->errorbars = 0;
  opt->colour = 0;
  opt->symbol = 0;
  opt->size = 0;
  opt->line = 0;
  opt->legend = 0;
}

static void init_fit(BootFitFuncs* fit, int nboot, int ndata)
{
  const int nparam = 2;
  int i;
  fit->nparam = nparam;
  fit->subset = vecalloc(sizeof(int),ndata);
  for (i=0;i<ndata;i++) fit->subset[i]=true;
  fit->nboot = nboot;
  fit->param    = init_bootarr(nboot,1,nparam);
  fit->paramcov = init_bootarr(nboot,2,nparam,nparam);
  fit->chisq    = init_boot(nboot);
  fit->seed = 815875533;
  fit->freeze = malloc(sizeof(int)*nparam);
  for (i=0;i<nparam;i++) fit->freeze[i]=false;
  fit->correlate = false;
  fit->xfunc = fit->yfunc = 0;
  fit->xoffset = fit->xsoffset = 0;
  fit->yoffset = fit->ysoffset = 0;
  fit->model = 0;
  fit->guess = 0;
}

/** model function for a massive momentum-space propagator **
 **  D(p) = A/(p^2+m^2);  A->a[0], m->a[1]                 **/
void massivemom(Real x, Real *a, Real *y, 
		Real *dyda, Real **d2yda2, Real* dydx, int nparam)
{
  const char fnm[MAXNAME] = "massivemom";
  Real d; /* the denominator p^2+m^2 */
  
  if (nparam != 2) {
    fprintf(stderr,"%s: illegal nparam %d - should be 2\n",fnm,nparam);
    exit(EVIL);
  }

  d = x*x+a[1]*a[1];
  if (d2yda2) {
    d2yda2[0][0] = 0.0;
    d2yda2[0][1] = d2yda2[1][0] = -2.0*a[1]/(d*d);
    d2yda2[1][1] = 2.0*a[0]*(3.0*a[1]*a[1]-x*x)/(d*d*d);
  }
  if(dyda) {
    dyda[0] = 1.0/d;
    dyda[1] = -2.0*a[0]*a[1]/(d*d);
  }

  if (dydx) {
    *dydx = -2.0*a[0]*x/(d*d);
  }

  if (y) {
    *y = a[0]/d;
  }
}

/************************************************************************
 * Estimate the fit parameters for the funciton y = A/(x^2+m^2)         *
 * We get m=a[1] from the first two data points:                        *
 *   m^2 = (x2^2 y2 - x1^2 y1)/(y1-y2)                                  *
 ************************************************************************/
void massivemomguess(Real *a, int *freeze, int nparam,
		     Real *x, Real *y, int ndata)
{
  const char fnm[MAXNAME] = "massivemomguess";
  Real x1, x2, y1, y2;

  if (nparam != 2) {
    fprintf(stderr,"%s: illegal nparam %d - should be 2\n",fnm,nparam);
    exit(EVIL);
  }

  x1=x[0]; x2=x[1]; y1=y[0]; y2=y[1];
  if (!freeze[1]) {
    a[1] = sqrt(abs((x2*x2*y2-x1*x1*y1)/(y1-y2)));
  }
  /* check for nonsensical mass values */
  if (a[1]<=0.0) a[1]=1.0;
  if (!freeze[0]) {
    a[0] = y1*(x1*x1+a[1]*a[1]);
  }    
}

void FischerMaasMueller(Real x, Real *a, Real *y, 
			Real *dyda, Real **d2yda2, Real* dydx, int nparam)
{
  const char fnm[MAXNAME] = "FischerMaasMueller";

}

void FMMguess(Real *a, int *freeze, int nparam,
	      Real *x, Real *y, int ndata)
{
  const char fnm[MAXNAME] = "FMMguess";
}
