#ifndef _TREELEVEL_H_
#define _TREELEVEL_H_
#include "Real.h"
#include "field_params.h"
#include "momenta.h"

#define MOMD 6  /* prop denominator as mom variable */

typedef Propagator_parameters Prop_par;  /* shorthand (laziness) */
typedef int Correction_function(Real**,Momenta*,Prop_par*);
typedef Real Cpt_correction(Momenta*,Prop_par*, 
			    int* p, int mu, int idx);
typedef Real Cpt_correction_pq(Momenta*,Prop_par*,
			       int* p, int* q, int mu);

Correction_function z0m0;
Correction_function z0m0_pm;

/* "inline" correction functions */
static inline Real Cpt_One(Momenta* m, Prop_par* a, 
			   int* p, int mu, int i) { return 1.0; }
static inline Real Cpt_Zero(Momenta* m, Prop_par* a, 
                           int* p, int mu, int i) { return 0.0; }

#endif
