/*	$Id: chisq_fit.c,v 1.4 2004/04/19 23:42:26 i-skulle Exp $	*/
/************************************************************************
 *                                                                      *
 * This routine determines the best fit for a set of data               *
 * for a model function <model>, using the chi-square minimisation      *
 * algorithm of Levenberg and Marquardt modified for correlated data.   *
 *                                                                      *
 * inputs:                                                              *
 *   xdata        central values of the x data                          *
 *   ydata        central values of the y data                          *
 *   xdatacov     covariance matrix of the x data                       *
 *                (null if the x data are fixed)                        *
 *   ydatacov     covariance matrix of the y data                       *
 *   ndata        number of data points to fit to                       *
 *   model        the model function                                    *
 *   nparam       number of parameters the model takes                  *
 *   freeze       array of flags for frozen/free parameters             *
 *   correlate    flag to indicate whether the data should be           *
 *                considered to be correlated                           *
 * outputs:                                                             *
 *   chisq        the minimum chi^2                                     *
 *   param        the parameters for this best fit                      *
 *   paramcov     parameter covariance matrix                           *
 *   sigmax       standard deviations for x data (if applicable)        *
 *   sigmay       standard deviations for y data                        *
 *                                                                      *
 * Modified from v1.15 of the original Edinburgh (HHHH) code            *
 * written by hps, based on Numerical Recipes                           *
 *                                                                      *
 ************************************************************************/

#include "analysis.h"
#include <matrix_inverse.h>

static Real delta_chisq_min = 0.001;
static Real chisq_max = 1.0e30;
static Real lambda_min = 5.0e-6;
static Real lambda_max = 1.0e+6;
static int chisq_maxiter = MAX_ITER;

int chisq_fit(FitData* fit)
{
  const char fnm[MAXNAME] = "chisq_fit";
  const int ndata = fit->ndata;
  Real l, old_chisq;
  int iter;
#ifdef DEBUG
  int i;
#endif
  int result, marquardt_fail;
  Real** datacorr = arralloc(sizeof(Real),2,ndata,ndata);

/*** Assign space for matrices ***/
  if (!fit->sigmay) fit->sigmay = vecalloc(sizeof(Real),ndata);
  if (!fit->inv_data_corr && fit->correlate) 
    fit->inv_data_corr = arralloc(sizeof(Real),2, ndata,ndata);
  if ( fit->xdatacov && !fit->sigmax)
    fit->sigmax = vecalloc(sizeof(Real),ndata);
  l = 0.0;
  iter = 0;

  /*** Extract correlation matrix and standard deviations ***/
  cov_to_corr(fit->ydatacov, datacorr, fit->sigmay, ndata, fit->correlate);

  /*** invert correlation matrix ***/
  if (fit->correlate) {
    get_inverse(datacorr, ndata, fit->inv_data_corr);
  } else { /* correlation matrix is unity by definition */
    fit->inv_data_corr = datacorr;
  }
  if (fit->xdatacov) {
    cov_to_corr(fit->xdatacov, datacorr, fit->sigmax, ndata, false);
  }

  /*** initiate M-L algorithim ***/
  l = -1.0;
  old_chisq = chisq_max;

#ifdef DEBUG
  printf("chi^2 = %e, lambda = %e \n", fit->chisq, l);
  printf("iter = %d, result = %d \n", iter, result);
#endif
  result = marquardt_minimise(fit,&l);

#ifdef DEBUG
  printf("chi^2 = %e, lambda = %e \n",fit->chisq,l);
  printf("iter = %d, result = %d \n", iter, result);
#endif
  /*** check for errors ***/
  if ((result != SUCCESS) && (result != TRY_AGAIN) ) {
    fprintf(stderr,"%s: fatal error \n",fnm);
    return EVIL;
  }

  /*** execute main loop  ***/

  while ( iter<chisq_maxiter && l<lambda_max &&
	 ( (old_chisq - fit->chisq > delta_chisq_min) || l>lambda_min ) ) {

    old_chisq = fit->chisq;
    result = marquardt_minimise(fit,&l);
#ifdef DEBUG
    printf("chi^2 = %16.12g, diff = %e, lambda = %e \n",
	   fit->chisq, old_chisq-fit->chisq, l);
    for (i=0; i<fit->nparam; i++) {
      printf("a[%d]=%12.10g  ",i,fit->param[i]);
    }
    printf("\niter = %d, result = %d \n", iter, result);
#endif

    /*** check for errors ***/
    if ((result != SUCCESS) && (result != TRY_AGAIN) ) {
      fprintf(stderr,"%s: fatal error\n",fnm);
      return EVIL;
    }

    iter++;
  }

  /*** Check if a sensible answer was attained ***/
  if ( ( iter == chisq_maxiter ) || ( l >= lambda_max ) )
    marquardt_fail = TRUE ;
  else
    marquardt_fail = FALSE;

  /*** final phase :- set lambda to zero and get parameter covariance ***/
  l = 0.0;
  result = marquardt_minimise(fit,&l);

  /*** check for errors ***/
  if ((result != SUCCESS) && (result != TRY_AGAIN) ) {
    fprintf(stderr,"%s: fatal error\n",fnm);
    return EVIL;
  }

  free(datacorr);
  if (!fit->correlate) fit->inv_data_corr=0;

  /*** Pass on whether the routine failed or not ***/
  if (marquardt_fail) {
    fprintf(stderr,"%s: failed after %d iterations\n",fnm,iter);
    return FAIL_CONVERGE;
  }

  return 0;

}

void change_delta_chisq_min( Real del_chsq_min )
{
  delta_chisq_min = del_chsq_min;
}

void change_chisq_max( Real chsq_max )
{
  chisq_max = chsq_max;
}

void change_lambda_min( Real l_min )
{
  lambda_min = l_min;
}

void change_lambda_max( Real l_max )
{
  lambda_max = l_max;
}

void change_chisq_maxiter(int imax) { chisq_maxiter = imax; }

void cov_to_corr(Real **cov, Real **corr, Real *sigma,
                 int ndata, int corrflag)
{
  int i, j;

  for(i=0 ; i<ndata; i++) {
    sigma[i]   = sqrt(cov[i][i]);
    corr[i][i] = (Real) 1.0;
  }

  for(i=0; i<ndata; i++) {
    for(j=i+1; j<ndata; j++) {
      if (corrflag) {
        corr[i][j] = cov[i][j]/(sigma[i]*sigma[j]);
      } else {
        corr[i][j] = 0.0;
      }

      corr[j][i] = corr[i][j];
    }
  }
}
