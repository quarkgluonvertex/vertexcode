#include "analysis.h"

/************************************************************************
 *                                                                      *
 * Routines to manipulate FitData structures, used internally by the    *
 * fitting routines.                                                    *
 *                                                                      *
 ************************************************************************/

/************************************************************************
 * Initialise a FitData, used by the low-level chisq-fit routines       *
 * from a BootFitFuncs, used by the higher-level analysis code          *
 ************************************************************************/
void init_fitdata(FitData* fit, BootFitFuncs* bfit, int ndata, int xvar)
{
  const int nparam = bfit->nparam;
  int i;

  fit->nparam = bfit->nparam;
  fit->param     = vecalloc(sizeof(Real),nparam);
  /* copy params to take care of frozen params */
  for (i=0;i<nparam;i++) fit->param[i] = bfit->param[i].best;
  fit->paramcov  = arralloc(sizeof(Real),2,nparam,nparam);
  fit->freeze    = bfit->freeze; /* goes unchanged through all routines*/
  fit->correlate = bfit->correlate;
  fit->model     = bfit->model;
  fit->ndata     = ndata;
  fit->xdata     = vecalloc(sizeof(Real),ndata);
  fit->ydata     = vecalloc(sizeof(Real),ndata);
  fit->xdatacov  = xvar ? arralloc(sizeof(Real),2,ndata,ndata) : 0;
  fit->ydatacov  = arralloc(sizeof(Real),2,ndata,ndata);
  /* The remainder of the FitData are allocated later */
  fit->sigmax = fit->sigmay = 0; 
  fit->inv_data_corr = 0;
  fit->yfit = 0;
  fit->dyda = 0;
  fit->d2yda2 = 0;
  fit->dydx = 0;
}

void free_fitdata(FitData* fit)
{
  free(fit->param);
  free(fit->paramcov);
#if 0
  free(fit->freeze); /* or perhaps not, if it is copied from elsewhere? */
#endif
  free(fit->xdata);
  free(fit->ydata);
  if (fit->xdatacov) free(fit->xdatacov);
  free(fit->ydatacov);
  if (fit->sigmax) free(fit->sigmax);
  if (fit->sigmay) free(fit->sigmay);
  if (fit->inv_data_corr) free(fit->inv_data_corr);
  if (fit->yfit) free(fit->yfit);
  if (fit->dyda) free(fit->dyda);
  if (fit->d2yda2) free(fit->d2yda2);
  if (fit->dydx) free(fit->dydx);
}
