#include "analysis.h"
#include "momenta.h"
#include "momdata.h"
#include "vertex.h"
#include <string.h>
#include <assert.h>

/* KLUDGE to correct for anisotropy in the zeromom gluon tensor structure */
/* NOTE that this only applies to UKQCD 16^3x48 data.                     *
 * Hopefully with larger volumes this will no longer be needed            */
#define UKQCD_16X48 1648
#if 0
#define ZEROGLUON 1.11111111
#define ZEROGLUON_T 3.0
#else
#define ZEROGLUON 1.0
#define ZEROGLUON_T 3.0 // TODO: not sure about this one?
#endif

#define SYMMETRIC 1  /* "Symmetric" ie q=-2p kinematics */

#define MAXMOMVAR     6   /* max # mom variables used in tree-level corr */
#define ILLEGAL_VALUE -12345.6789

#define AM_MAXCODE 20 /* length of various strings */

/* Gamma matrix codes to match those in Quark.h */
#define SIGMA_XY 10
#define SIGMA_XZ 11
#define SIGMA_XT 12
#define SIGMA_YZ 13
#define SIGMA_YT 14
#define SIGMA_ZT 15

static Real invsqr(Real a) { return 1.0/(a*a); }
static Real    inv(Real a) { return 1.0/a; }
static Real    one(Real a) { return 1.0; }
static Real    lin(Real a) { return a; }
static Real   mult(Real* a, int n) { assert(n==2); return a[0]*a[1]; }
static Real divide(Real* a, int n) { assert(n==2); return a[0]/a[1]; }
static Real    sub(Real* a, int n) { assert(n==2); return a[0]-a[1]; }
static Real trivial(Real a, Real b) { return a; }
static Real eval_poly3(Real* a, int n)
{ assert(n>3); return a[0]+(a[1]+a[2]*a[3])*a[3]; }
static Real Mf_minus_one(Momenta* mom, int* p, int mu) { return -1.0; }
static Real Mf_minus_quarter(Momenta* mom, int* p, int mu) { return -0.25; }
static Momfunc Mf_kmu_by_2ksqr;
static Momfunc Mf_kmu;
static Momfunc Mf_minus_kmu;
static Momfunc Mf_minus_inv_4kmuknu;
static Momfunc Mf_minus_kmuknu_by_4k4;
static Momfunc Mf_pm_knu;
static Momfunc Mf_pm_knu_by_6ksqr;
static Momfunc Mf_cmu;
static Momfunc Mf_minus_cmu;
static Momfunc Mf_inv_4ksqr;
static Momfunc Mf_l1_proj;
static Momfunc Mf_l2_proj;
static Real Mf_kmu_half(Momenta* mom, int* p, int mu)
   { return 0.5*Mf_kmu(mom,p,mu); }
static int setup_strings(char* datastr, char* cutstr,
			 Vertex_params* run, Momenta* mom);

int renormalise_vertex(Boot* data, Vertex_params* run, Momenta* mom,
		       char* datastr);

Modelfunction polynomial;
Guessfunction polyguess;

int main(int argc, char **argv)
{
  Boot*  data;
  Boot** readdat;
  Boot   datatmp;
  Real*  xdata;
  Boot*  xbootdata;
  Boot*  subdata=0;
  Boot tmp;

  BootFitFuncs fit;
  int Ngamma = 15;
  int nread;

  int ndata, nalldata;
  int M, mu, nu, gamma, idat, i, nmu, p[4], pt;
  int Sigma[12] = {SIGMA_XY,SIGMA_XZ,SIGMA_XT,SIGMA_XY,SIGMA_YZ,SIGMA_YT,
		   SIGMA_XZ,SIGMA_YZ,SIGMA_ZT,SIGMA_XT,SIGMA_YT,SIGMA_ZT};
  Real *corr;              /* array of average tree-level corrections */
  Real scale;              /* physical scale factor                   */
  Real scalefact = 1.0;    /* combinatorial factor, related to the    *
			    * number of Lorentz indices summed over   */
  Real (*dimfunc)(Real);   /* dimension of the form factor            */
  Momfunc* scalefunc;      /* kinematical factor                      */
  Momfunc* scale_subfunc;  /* kinematical factor for subtraction      */
  Cpt_correction* cpt_divfunc;    /* multiplicative correction func   */
  Cpt_correction* cpt_subfunc;    /* additive correction func         */
  /* if both cpt_divfunc and cpt_subfunc exist then hybrid correction */
  Real (*correction)(Real*,int);  /* correction type (mult, add)      *
				   * - only for average correction    */

  int status;      // exit code
  int symm, asym, cpt_corr, ave_corr, readallmu;  // boolean
  int smear;
  Real smearing;

  char filename[MAXLINE];

  /* strings used to construct filenames */
  char datastr[MAXLINE], cfgstr[MAXLINE];
  char outname[MAXLINE];

  char inistr[AM_MAXCODE], symcode[AM_MAXCODE];
  char qstr[AM_MAXCODE], vname[AM_MAXCODE];
  char corrcode[AM_MAXCODE], cutstr[AM_MAXCODE];
  const char momstr[4][3] = {"p","K","Q","Kt"};
  int *config;

  Vertex_params run;
  Momenta mom;
  int *nequiv;

  if (argc != 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  sprintf(filename, "%s",argv[1]);
  if ((status=assign_params_analyse_vertex(filename,&run,&mom))) return status;

  /* save smearing params for later - not to be used initially */
  smear = mom.smear; mom.smear=false;
  smearing = mom.smearing; mom.smearing=0.0;

  switch (run.correction) {
  case NOCORR:
    ave_corr = cpt_corr = FALSE; 
    corrcode[0]='\0';
    break;
  case AVECORR:
    ave_corr = TRUE; cpt_corr = FALSE;
    strcpy(corrcode,"_acorr");
    break;
  case CPTCORR:
    ave_corr = FALSE; cpt_corr = TRUE;
    strcpy(corrcode,"_ccorr");
    break;
  default:
    printf("Unknown correction %d -- assuming average tree-level correction\n",
	   run.correction);
    ave_corr = TRUE; cpt_corr = FALSE;
    strcpy(corrcode,"_acorr");
  }

  /*** Determine the kinematics and input data format ***/
  symm = ( run.kinematics == VTX_REFLECT);
  asym = ( run.kinematics == VTX_ZEROMOM);
  if ( asym && (run.q[0] || run.q[1] || run.q[2] || run.q[3] )) {
    printf("Error: soft gluon kinematics chosen but q!=0\n");
    return BADPARAM;
  }
  if ( !symm && !asym ) {
    printf("Error: only soft gluon and hard reflection implemented\n");
    return BADPARAM;
  }
  /* id flag tells us how many gamma matrices and mu values are in input */
  /* 0, 1: only one mu per file;      2, 3: all mu-values in one file    *
   * 0, 2: q=0 has only I, gamma_mu;  1, 3: all 15 gamma-traces are done */
  readallmu = (run.id%2);
  if ( asym && (run.id/2==0) ) Ngamma=5;
  nread = readallmu ? Ngamma*DIR : Ngamma;

  /*** Set up strings describing parameters (for filenames) ***/
  if (symm) {
    strcpy(symcode,"sym"); 
    strcpy(inistr,"data_vtxsym");
    qstr[0]='\0';
  } else {
    strcpy(symcode,"asym");
    strcpy(inistr,"vertex");
    sprintf(qstr,"q%d_%d_%d_%d_",run.q[IX],run.q[IY],run.q[IZ],run.q[IT]);
  }

  if (status=setup_strings(datastr,cutstr,&run,&mom)) return status;
  sprintf(cfgstr,"U%d_%dcfg_%dbt",run.start_cfg,run.nconfig,run.nboot);

  /**  Check the momentum parameters  **/
  setup_momenta(&mom);
  nalldata = mom.nval;
  ndata = 2*run.p[IT];

  /************************************************************************
   ***   Set up correction functions, kinematical factors, dimensions   ***
   ***   etc specific to each form factor                               ***
   ************************************************************************/
  if (run.formfactor < 1 || run.formfactor > MAX_FORMFACTOR) {
    printf("Illegal form factor %d",run.formfactor);
    printf(" -- must be > 0 and < %d\n",MAX_FORMFACTOR+1);
    return BADPARAM;
  }
  if (run.formfactor <= 4 || (run.formfactor >= 20 && run.formfactor <= 24)) {
    sprintf(vname,"lambda%d",run.formfactor);
  } else {
    if (asym) {
      printf("Error: all tau form factors are zero at the soft gluon point\n");
      return BADPARAM;
    }
    sprintf(vname,"tau%d",run.formfactor-4);
  }

  if (symm) {
    switch (run.formfactor) {
    case 7: /* tau3 */
      printf("Warning: tau3 is the same as lambda1, computing lambda1\n");
      run.formfactor=1;
    case 1:
      scalefunc = Mf_one;
      corr = l1_refl_arr(&mom,run.prop);
      cpt_subfunc = tt3_sub;
      cpt_divfunc = l1_refl_cpt;
      correction = divide;
      dimfunc = one;
      break;
    case 9: /* tau5 */
      scalefunc = Mf_pm_knu_by_6ksqr;
      scalefact *= 12;
      corr = 0;
      cpt_subfunc = t5_cpt;
      cpt_divfunc = 0;
      correction = sub;
      dimfunc = inv;
      break;
    default:
      printf("Error: %s = 0 at the hard reflection point\n",vname);
      return BADPARAM;
    }
  } else if (asym) {
    if (run.id==UKQCD_16X48) scalefact = ZEROGLUON; // HACK
    switch (run.formfactor) {
    case 1:
      scalefunc = Mf_one;
      corr = l1_q0_noncov(&mom,run.prop);
      cpt_subfunc = 0;
      cpt_divfunc = l1_q0_cpt;
      correction = divide;
      dimfunc = one;
      break;
    case 2: /* lambda2: tr(gnu Gmu)/(Pnu Pmu) [nu!=mu] */
      scalefunc = Mf_minus_inv_4kmuknu;
      scalefact *= 6;
      corr = 0; /* lambda2 can really only be corrected componentwise */
      cpt_subfunc = l2_cpt;
      cpt_divfunc = 0;
#if 0 // TEST
      scalefunc = Mf_minus_quarter;
#endif
      correction = sub;
      dimfunc = invsqr;
      break;
    case 3:
      scalefunc = Mf_kmu_by_2ksqr;
      scalefact *= 4;
      corr = l3_arr(&mom,run.prop);
      cpt_subfunc = l3_cpt;
      cpt_divfunc = 0;
      correction = sub;
      dimfunc = inv;
      break;
    case 21: /* lambda1 covariant */
      scalefunc = Mf_l1_proj;
      scalefact *= 16;
      corr = l1_q0_cov(&mom,run.prop);
      cpt_subfunc = 0;
      cpt_divfunc = 0;
      correction = divide;
      dimfunc = one;
      break;
    case 22: /* lambda2 covariant */
      scalefunc = Mf_l2_proj;
      scalefact *= 16;
      corr = l2_q0_cov(&mom,run.prop);
      cpt_subfunc = 0;
      cpt_divfunc = 0;
      correction = sub;
      dimfunc = invsqr;
      break;
    default:
      printf("Error: %s not yet implemented\n", vname);
      return BADPARAM;
    }
  }

  tmp = init_boot(run.nboot);
  data = init_bootarr(run.nboot,1,nalldata);
  readdat = init_bootarr(run.nboot,2,ndata,nread);
  datatmp = init_boot(run.nboot);
  xbootdata = init_bootarr(0,1,nalldata);
  nequiv = calloc(sizeof(int),nalldata);

#if 0 /* obsolete */
  /* read the form factor (lambda1) to be subtracted off for lambda2 */
  if (run.formfactor==2) {
    scale_subfunc = Mf_minus_cmu;
    subdata = init_bootarr(run.nboot,1,nalldata);
    sprintf(filename, "%s/lambda1_%s_%s%s_%s_00.xdr",
	    run.boot_dir, symcode, datastr, cutstr, cfgstr);
    status = read_boot(&subdata, nalldata, filename);
    if (status==BADFILE) {
      printf("lambda2 requires (uncorrected) lambda1 to be computed");
      printf(" and written to file first\n");
    }
    if (status) return status;
  }
#endif

  /************************************************************************
   ***     Loop over all momenta and lorentz indices and do the work    ***
   ************************************************************************/

  /*** NOTE: only noncovariant extraction implemented so far ***/
  switch (run.formfactor) {
  case 1: 
    if (asym) nmu=3; else nmu=4; break;
  case 2:
  case 9:
    nmu=12; break;
  case 21:
  case 22:
    nmu=16; break;
  default: nmu=4;
  }
  for (p[IX]=-run.nmom[IX]; p[IX]<=run.nmom[IX]; p[IX]++)
    for (p[IY]=-run.nmom[IY]; p[IY]<=run.nmom[IY]; p[IY]++)
      for (p[IZ]=-run.nmom[IZ]; p[IZ]<=run.nmom[IZ]; p[IZ]++) {
	if (readallmu) {
	  sprintf(filename,"%s/%s_%s_mu0_%sp%d_%d_%dt_%s_00.xdr",
		  run.boot_dir, inistr, datastr,
		  qstr, p[IX], p[IY], p[IZ], cfgstr);
	  if ((status=read_boot(readdat,nread*ndata,filename))) return status;
	}
	for (M=0; M<nmu; M++) {
	  if (asym && run.formfactor==1 && p[M]!=0) continue;
	  switch (run.formfactor) {
	  case 1: /* lambda1: tr(gmu Gmu)[pmu=0] */
	    nu=mu=M; gamma=mu+1; break;
	  case 2: /* lambda2: tr(gnu Gmu)/(Pnu Pmu) [nu!=mu] */
	    mu=M/3; 
	    nu=M%3; if (nu>=mu) nu++;
	    gamma=nu+1;
	    if ( (mu!=IT && p[mu]==0) || (nu!=IT && p[nu]==0) ) continue;
	    break;
	  case 3: /* lambda3: tr(Gmu) */
	    nu=mu=M; gamma=0; break;
	  case 9: /* tau5: tr(s_numu Gmu) */
	    mu=M/3; nu=M%3; if (nu>=mu) nu++;
	    gamma=Sigma[M]; break;
	  case 21: /* lambda1 covariant: tr(gnu Gmu)  */
	  case 22: /* lambda2 covariant: tr(gnu Gmu)  */
	    mu=M/4; nu=M%4; gamma=nu+1; break;
	  default:
	    printf("analysis for %s not implemented\n",vname);
	    return BADPARAM;
	  }
	  idat = readallmu ? Ngamma*mu+gamma : gamma;
	  if (!readallmu) {
	    sprintf(filename,"%s/%s_%s_mu%d_%sp%d_%d_%dt_%s_00.xdr",
		    run.boot_dir, inistr, datastr, mu+1,
		    qstr, p[IX], p[IY], p[IZ], cfgstr);
	    if ((status=read_boot(readdat,nread*ndata,filename))) return status;
	  }
	  for (pt=0; pt<ndata; pt++) {
	    p[IT]  = pt-run.p[IT];
	    i = momindex_p(p,&mom);
	    if (i!=DISCARDED) {
	      /* In quark reflection kinematics data for different pt are *
	       * in different files which have different length           */
	      // TODO: fix this so it works also for all mu in same file
	      if (symm) {
		int qt = -2*p[IT]-1;
		int ndat = (qt>0) ? 2*mom.nmom_in[IT]-qt : 2*mom.nmom_in[IT]+qt;
		int ip = (qt>0) ? pt + mom.nmom_in[IT] - run.p[IT] 
		  : mom.nmom_in[IT] - 1;
		sprintf(filename,
			"%s/%s_%s_mu%d_q%d_%d_%d_%d_p%d_%d_%dt_%s_00.xdr",
			run.boot_dir, inistr, datastr, mu+1,
			-2*p[IX], -2*p[IY], -2*p[IZ], -2*p[IT]-1,
			p[IX], p[IY], p[IZ], cfgstr);
		status=read_boot(readdat,Ngamma*ndat,filename);
		if (status) return status;
		boot_copy(&datatmp,readdat[ip][gamma]);
	      } else {
		boot_copy(&datatmp,readdat[pt][idat]);
	      }
	      /* Rescale old UKQCD data if appropriate */
	      if (run.id==UKQCD_16X48 && asym && mu==IT) {
		scale_boot(&datatmp,datatmp,ZEROGLUON_T);
	      }
	      /* Subtract off previously computed formfactor if appropriate */
	      if (subdata) { /* TODO: generalise */
		scale_boot(&tmp,subdata[i],scale_subfunc(&mom,p,mu));
		add_boot(&datatmp,datatmp,tmp);
	      }
	      /* Multiply with the appropriate kinematical factor *
	       * and add to average                               */
	      if ( (scale = scalefunc(&mom,p,M)) == ILLEGAL_VALUE) continue;
	      scale_boot(&datatmp,datatmp,scale); 
	      if (cpt_corr) {
		/* Additive and multiplicative componentwise correction *
		 * First subtract, then divide                          */
		if (cpt_subfunc) {
		  boot_func_mixed(&datatmp,sub,1,1,datatmp,
				  (*cpt_subfunc)(&mom,run.prop,p,M,i));
		}
		if (cpt_divfunc) {
		  boot_func_mixed(&datatmp,divide,1,1,datatmp,
				  (*cpt_divfunc)(&mom,run.prop,p,M,i));
		}
	      }
	      add_boot(&data[i],data[i],datatmp);
	      nequiv[i]++;
	    }
	  }
	}
      }

  /** Finally, normalise the data **/
#if 0
  boot_normalise_momenta(data,&mom);
#else
  for (i=0;i<nalldata;i++) {
    if (nequiv[i]) scale_boot(&data[i],data[i],scalefact/nequiv[i]);
  }
#endif
  free(nequiv);

/* Put in average tree-level correction */
  if (ave_corr) {
    if (!corr) {
      printf("Attempting undefined average correction!\nExiting.\n");
      return BADDATA;
    }
    for (i=0; i<nalldata; i++) {
      boot_func_mixed(&(data[i]),correction,1,1,data[i],corr[i],correction);
    }
  }

  sprintf(outname, "%s_%s_%s%s%s_",
	  vname, symcode, datastr, cutstr, corrcode);

  if (run.write_boot){
    sprintf(filename, "%s/%s%s_", run.boot_dir, outname, cfgstr);
    write_boot(data, nalldata, filename);
  }

  /*** Now we can put in physical scale ***/
  scale_momenta(&mom,mom.scale);
  scale = dimfunc(mom.scale);
  for (i=0;i<nalldata;i++) scale_boot(&data[i],data[i],scale);

  /*** Smear the momenta if appropriate ***/
  if (smear) {
    char smrstr[AM_MAXCODE];
    if (mom.cutoff>0 || mom.dcut>0) {
      printf("Error: Attempting to combine smearing");
      printf(" with radial or diagonal cuts\n");
      return BADPARAM;
    }
    sprintf(smrstr,"s%03d",INT(smearing*1000));
    sprintf(outname, "%s_%s_%s%s%s%s_",
	    vname, symcode, datastr, cutstr, smrstr, corrcode);
    smear_momenta(&mom, smearing, run.mom_variable);
    nalldata = mom.nval;
    boot_reorder_momenta(data,data,&mom); /* doesnt work with radial or diagonal cuts! */
  }

  /*** Renormalise ***/
  if (run.fit) {
    renormalise_vertex(data,&run,&mom,datastr);
    strcat(outname,"ren_");
  }

  if (run.plot) {
    printf("making graph\n");
    fflush(stdout);
    xdata = mom.val[run.mom_variable];
    sprintf(filename, "%s/%s%dcfg_%s_", 
	    run.graph_dir, outname, run.nconfig, momstr[run.mom_variable]);

    //    if (!run.do_fit || run.formfactor!=1) {
    fit.nparam = 0;
    fit.model = (Modelfunction *) NULL;
      //    }

    ybootstrap_graph(xdata, data, nalldata, &fit, 0, filename);
  }
  

  return 0;
}

int renormalise_vertex(Boot* data, Vertex_params* run, Momenta* mom, 
		       char* datastr)
{
  const char fnm[MAXNAME] = "renormalise_vertex";
  char symcode[AM_MAXCODE] = "asym";
  char zname[MAXLINE];
  Real renpt = run->seed/100.0; /* KLUDGE */
  Boot Z1F = init_boot(run->nboot);
  int i;
  int status = 0;
  if (run->kinematics==SYMMETRIC) strcpy(symcode,"symm");

  if (run->formfactor==1) {
    /*** Fit the form factor in the vicinity of the renormalisation point ***
     *** and extract the vertex renormalisation constant Z1F              ***/
    const int nparm = 3;
    const Real xdelta=0.4;         /* TODO: input this? */

    const Real* xdata = mom->val[run->mom_variable];
    Boot* xboot = init_bootarr(0,1,mom->nval);
    BootFitFuncs fit;

    fit.param  = init_bootarr(run->nboot,1,nparm);
    fit.paramcov = arralloc(sizeof(Real),2,nparm,nparm);
    fit.chisq = init_boot(run->nboot);
    fit.subset = vecalloc(sizeof(int),mom->nval);
    fit.freeze = vecalloc(sizeof(int),nparm);
    fit.nparam = nparm;
    fit.model  = polynomial;
    fit.guess  = polyguess;
    for (i=0;i<nparm;i++) fit.freeze[i]=FALSE;
    fit.correlate = run->correlate;

    for (i=0; i<mom->nval; i++) {
      xboot[i].best = xdata[i];
      if ( xdata[i]>renpt-xdelta && xdata[i]<renpt+xdelta) {
	fit.subset[i] = TRUE;
      } else fit.subset[i] = FALSE;
    }

    status = fit_bootdata(xboot,data,&fit);
    if (status) {
      printf("\n%s: Fit failed!\n",fnm);
    }
    printf("\nFit parameters:\n");
    print_stats(&fit);
    boot_func_mixed(&Z1F,eval_poly3,3,1,
		    fit.param[0],fit.param[1],fit.param[2],renpt);
    printf("Renormalisation constant Z1F:\n");
    print_bootstats(Z1F);

    /*** Write Z1F to file and rescale (renormalise) the data ***/
    sprintf(zname, "Z1F_%s_%s_%03d_", symcode, datastr, run->seed);
    write_boot(&Z1F,1,zname);
    for (i=0; i<mom->nval; i++) {
      boot_func(&data[i],divide,2,data[i],Z1F);
    }
#if 0 // TODO
    destroy_bootfitfuncs(fit);
#endif
    free(xboot);
  } else {
    /*** Read renormalisation constant from file and rescale data ***/
    Boot* zp = &Z1F;
    sprintf(zname, "Z1F_%s_%s_%03d_00.xdr", symcode, datastr, run->seed);
    if ((status = read_boot(&zp, 1, zname))) return status;
    for (i=0; i<mom->nval; i++) {
      boot_func(&data[i],divide,2,data[i]);
    }
  }

  destroy_boot(Z1F);
  return status;
}

/************************************************************************
 *                                                                      *
 * Functions for kinematical factors in the form factor extraction      *
 *                                                                      *
 ************************************************************************/
static Real Mf_kmu_by_2ksqr(Momenta* mom, int* p, int mu)
{
  Real kmu;
  int i=momindex_p(p,mom);

  assert(mu<4 && mu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);
  assert(i!=DISCARDED);

  kmu = mu==IT ? sin(2.0*PI*(p[mu]+0.5)/mom->L[mu]) : sin(2.0*PI*p[mu]/mom->L[mu]);

  return 0.5*kmu/(mom->val[MOMK][i]*mom->val[MOMK][i]);
}  

#if 0
static Real Mf_kmu(Momenta* mom, int* p, int mu)
{
  assert(mu<4 && mu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);

  return mu==IT ? sin(2.0*PI*(p[mu]+0.5)/mom->L[mu]) 
                : sin(2.0*PI*p[mu]/mom->L[mu]);

}  
#endif

static Real Mf_minus_kmu(Momenta* mom, int* p, int mu)
{
  return -Mf_kmu(mom,p,mu);
}

static Real Mf_cmu(Momenta* mom, int* p, int mu)
{
  assert(mu<4 && mu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);

  return mu==IT ? cos(2.0*PI*(p[mu]+0.5)/mom->L[mu]) 
                : cos(2.0*PI*p[mu]/mom->L[mu]);

}  

static Real Mf_minus_cmu(Momenta* mom, int* p, int mu)
{
  return -Mf_cmu(mom,p,mu);
}  

static Real Mf_inv_4ksqr(Momenta* mom, int* p, int mu)
{
  int i=momindex_p(p,mom);
  assert(i!=DISCARDED);

  return -0.25/(mom->val[MOMK][i]*mom->val[MOMK][i]);
}

/************************************************************************
 * In the following functions, mu = 0...11 represents                   *
 * the pair (mu,nu) with mu!=nu.  The plus sign is returned if nu<mu,   *
 * the minus sign if mu<nu.  The sign arises from having used the same  *
 * sigma_munu in the projection in both cases.                          *
 ************************************************************************/
static Real Mf_pm_knu(Momenta* mom, int* p, int mu)
{
  int nu = mu%3;
  assert(mu<12 && mu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);

  return (nu<mu/3) ? Mf_kmu(mom,p,nu) : -Mf_kmu(mom,p,nu+1);
}  

static Real Mf_pm_knu_by_6ksqr(Momenta* mom, int* p, int mu)
{
  int i = momindex_p(p,mom);
  assert(i!=DISCARDED);

  return Mf_pm_knu(mom,p,mu)/(6.0*mom->val[MOMK][i]*mom->val[MOMK][i]);
}  

static Real Mf_minus_inv_4kmuknu(Momenta* mom, int* p, int numu)
{
  const int mu = numu/3;
  int nu = numu%3;
  assert(numu<12 && numu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);
  if (nu>=mu) nu++;

  if ( (p[nu]==0 && nu!=IT) || (p[mu]==0 && mu!=IT) ) return ILLEGAL_VALUE;
  return -0.25/(Mf_kmu(mom,p,nu)*Mf_kmu(mom,p,mu));
}  

static Real Mf_minus_kmuknu_by_4k4(Momenta* mom, int* p, int numu)
{
  const int mu = numu/3;
  int nu = numu%3;
  int i = momindex_p(p,mom);
  Real kval = mom->val[MOMK][i];
  assert(i!=DISCARDED);
  assert(numu<12 && numu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);
  if (nu>=mu) nu++;

  if ( (p[nu]==0 && nu!=IT) || (p[mu]==0 && mu!=IT) ) return ILLEGAL_VALUE;
  return -0.25*Mf_kmu(mom,p,nu)*Mf_kmu(mom,p,mu)/(kval*kval*kval*kval);
}  

/************************************************************************
 * In the following functions, mu = 0...15 represents                   *
 * the pair (mu,nu).                                                    *
 ************************************************************************/
static Real Mf_l1_proj(Momenta* mom, int* p, int numu)
{
  const int mu = numu/4;
  const int nu = numu%4;
  int i = momindex_p(p,mom);
  Real kval = mom->val[MOMK][i];
  Real result;
  assert(i!=DISCARDED);
  assert(numu<16 && numu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);

  result = -Mf_kmu(mom,p,mu)*Mf_kmu(mom,p,nu)/(kval*kval);
  if (mu==nu) result += 1.0;
  result /= 3.0;
  return result;
}

static Real Mf_l2_proj(Momenta* mom, int* p, int numu)
{
  const int mu = numu/4;
  const int nu = numu%4;
  int i = momindex_p(p,mom);
  Real kval = mom->val[MOMK][i];
  Real result;
  assert(i!=DISCARDED);
  assert(numu<16 && numu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);

  result = -4.0*Mf_kmu(mom,p,mu)*Mf_kmu(mom,p,nu)/(kval*kval);
  if (mu==nu) result += 1.0;
  result /= 12.0*kval*kval;
  return result;
}

static int setup_strings(char* datastr, char* momstr,
			  Vertex_params* run, Momenta* mom)
{
  const int action = run->prop->action;
  char actstr[AM_MAXCODE], gaugestr[AM_MAXCODE], impstr[AM_MAXCODE];
  char cutstr[AM_MAXCODE];

  switch (run->gauge->gauge) {
  case LANDAU:
    strcpy(gaugestr,"L"); break;
  case COVARIANT:
    sprintf(gaugestr,"X%02d",INT(10*run->gauge->gauge_param));
    break;
  default:
    printf("Unknown gauge type %d\n",run->gauge->gauge);
    return BADPARAM;
  }

  switch (action) {
  case WILSON:
    strcpy(actstr,"W"); break;
  case CLOVER:
    sprintf(actstr,"C%03d",INT(100*run->prop->imp[0])); break;
  default:
    printf("Unknown action type %d\n",action);
    return BADPARAM;
  }

  /** string code for improvement terms: only for clover (improved) action **/
  if (action == CLOVER) {
    if (run->prop->source_type==ROTATED) {
      if (run->prop->ncoeff==2) {
	sprintf(impstr,"r%03db00",INT(1000*run->prop->imp[1]));
      } else {
	sprintf(impstr,"r%03db%03dl%02d",INT(1000*run->prop->imp[1]),
		INT(100*run->prop->imp[2]),INT(100*run->prop->imp[3]));
      }
    } else {
      sprintf(impstr,"l%02db%03d",
	      INT(100*run->prop->imp[3]),INT(100*run->prop->imp[2]));
    }
  }

  /** string code for momentum cuts **/
  sprintf(momstr,"_p%d%d%d%02d",mom->nmom_out[IX],mom->nmom_out[IY],
	  mom->nmom_out[IZ],mom->nmom_out[IT]);
  if (mom->dcut > 0.0) { 
    if (mom->cutoff > 0)
      sprintf(cutstr,"r%03dd%02d",INT(mom->cutoff*100),INT(mom->dcut*10));
    else
      sprintf(cutstr,"d%02d",INT(mom->dcut*10));
  } else {
    if (mom->cutoff > 0)
      sprintf(cutstr,"r%03d",INT(mom->cutoff*100));
    else
      strcpy(cutstr,"");
  }
  strcat(momstr,cutstr);

  sprintf(datastr,"B%d%s%sK%04d%s",INT(run->gauge->beta*100),
	  gaugestr, actstr, INT(run->prop->kappa*10000), impstr);

  return 0;
}

#if 0 // Obsolete: momenta are assigned in assign_params_analyse_vertex
static void assign_momenta(Momenta *mom, Vertex_params *run)
{
  int i;
  for (i=0; i<DIR; i++) {
    mom->L[i] = run->L[i];
    mom->nmom_in[i]  = run->nmom[i];
    mom->nmom_out[i] = run->p[i];
    mom->bc[i] = PERIODIC;
  }
  mom->bc[IT] = ANTIPERIODIC;
  if (run->max_mom>0) mom->cutoff=run->max_mom/100.0; else mom->cutoff=-1.0;
  mom->dcut=run->dcut;
  //  mom->scale = LAT_SCALE; /* not with tree-level correction... */
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = mom->z4 = FALSE;
  mom->nvar = MAXMOMVAR;  // we need all lattice momenta for the tree-level corr
  mom->sort = -1;
  mom->smear = 0;
  mom->smearing = 0.0;
  mom->itabexists = mom->initialised = FALSE;
  /* set array pointers to null to be safe */
  mom->key = mom->index = mom->nequiv = 0;
  mom->val = 0;
  mom->itab = 0;
  mom->nval = 0;
}
#endif
