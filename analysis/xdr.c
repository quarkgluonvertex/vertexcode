#include "xdr.h"

/*** encode all Reals as float -- may easily be changed eg to double ***/
bool_t xdr_Real(XDR* xdrs, Real* r)
{
#ifdef REAL_IS_FLOAT
  return xdr_float(xdrs, (float*)r);
#else
  float f = *r; /* for encoding */
  bool_t status = xdr_float(xdrs,&f);
  *r = f;       /* for decoding */
  return status;
#endif
}

