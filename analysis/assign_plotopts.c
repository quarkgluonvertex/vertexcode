/* $Id: assign_plotopts.c,v 1.3 2005/10/10 17:29:07 jonivar Exp $ */

#include "analysis.h"
#include <parm.h>
#include <scanvec.h>

#define MAXNSET    24

static PaCode type[MAXGRAPH_T] = {"RAW","GRACE","GNU","AXIS"};
static PaCode scale[MAXSCALE_T] = {"LINEAR","LOGARITHMIC"};

/********************************************************************
 *  assign_params_analysis: assigns all the parameters required for *
 *  	statistical analysis of meson and baryon spectrum           *
 ********************************************************************/

int assign_plotoptions(const char* filename, PlotOptions* plot)
{
  const char fnm[MAXNAME] = "assign_plotoptions";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gtype[MAXLINE]  = "GRACE";
  char xscale[MAXLINE]  = "LINEAR";
  char yscale[MAXLINE]  = "LINEAR";
  char errstr[MAXLINE] = "";
  char colstr[MAXLINE] = "";
  char symstr[MAXLINE] = "";
  char sizstr[MAXLINE] = "";
  char linstr[MAXLINE] = "";
  int err[MAXNSET];
  int col[MAXNSET];
  int sym[MAXNSET];
  int lin[MAXNSET];
  float size[MAXNSET];
  char legend[MAXNSET][MAXLINE] = {"","","","","","","","","","","","",
				   "","","","","","","","","","","",""};
  char legstr[MAXNSET][MAXLINE];
  int nset, nscan;

  int i, dumint;
  int result;			/* "exit code" */
  int status = 0;

  plot->xmin = plot->xmax = plot->ymin = plot->ymax = UNSET_DBL;
  plot->xgrid = plot->ygrid = UNSET_DBL;
  plot->xtick = plot->ytick = UNSET_INT;

  strcpy(plot->title,"");
  strcpy(plot->xaxis,"");
  strcpy(plot->yaxis,"");
  /*** Default colours, symbols etc ***/
  for (i=0;i<MAXNSET;i++) {
    err[i] = NOERR;          /* will be set by plot routine anyway */
    col[i] = i+1;
    sym[i] = i+1;
    lin[i] = 0;
    size[i] = 1.0;
  }
  
/***  Specify the list of parameters to be passed to the parsing routine ***/
/***     MUSTSET etc,   type,  "identifier",  variable name ***/

  add_parm(list,DEFAULT,STRING,"graph_type",gtype);
  add_parm(list,DEFAULT,STRING,"title",plot->title);
  add_parm(list,DEFAULT,STRING,"xscale",xscale);
  add_parm(list,DEFAULT,STRING,"yscale",yscale);
  add_parm(list,DEFAULT,DOUBLE,"xmin",&plot->xmin);
  add_parm(list,DEFAULT,DOUBLE,"xmax",&plot->xmax);
  add_parm(list,DEFAULT,DOUBLE,"ymin",&plot->ymin);
  add_parm(list,DEFAULT,DOUBLE,"ymax",&plot->ymax);
  add_parm(list,DEFAULT,DOUBLE,"xgrid",&plot->xgrid);
  add_parm(list,DEFAULT,DOUBLE,"ygrid",&plot->ygrid);
  add_parm(list,DEFAULT,INTEGER,"xtick",&plot->xtick);
  add_parm(list,DEFAULT,INTEGER,"ytick",&plot->ytick);
  add_parm(list,DEFAULT,STRING,"errorbars",errstr);
  add_parm(list,DEFAULT,STRING,"colour",colstr);
  add_parm(list,DEFAULT,STRING,"symbol",symstr);
  add_parm(list,DEFAULT,STRING,"linestyle",linstr);
  add_parm(list,DEFAULT,STRING,"symbolsize",sizstr);
  for (i=0;i<MAXNSET;i++) {
    sprintf(legstr[i],"legend%d",i);
    add_parm(list,DEFAULT,STRING,legstr[i],legend[i]);
  }

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list))==BAD_PARM ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

  status += parm_from_string(&dumint,gtype,type,MAXGRAPH_T,"graph_type");
  plot->graph_type = dumint;
  status += parm_from_string(&dumint,xscale,scale,MAXSCALE_T,"xscale");
  plot->xscale = dumint;
  status += parm_from_string(&dumint,yscale,scale,MAXSCALE_T,"yscale");
  plot->yscale = dumint;

/***  Determine the set-specific options  ***/
  nset = sscanveci(errstr,err,MAXNSET);
  nscan = sscanveci(colstr,col,MAXNSET);
  if (nscan>nset) nset=nscan;
  nscan = sscanveci(symstr,sym,MAXNSET);
  if (nscan>nset) nset=nscan;
  nscan = sscanveci(linstr,lin,MAXNSET);
  if (nscan>nset) nset=nscan;
  nscan = sscanvecf(sizstr,size,MAXNSET);
  if (nscan>nset) nset=nscan;

  plot->nset = nset;
  plot->errorbars  = malloc(sizeof(Errorbar_t)*nset);
  plot->colour  = malloc(sizeof(int)*nset);
  plot->symbol  = malloc(sizeof(int)*nset);
  plot->line  = malloc(sizeof(int)*nset);
  plot->size  = malloc(sizeof(float)*nset);
  plot->legend  = malloc(sizeof(char*)*nset+nset*MAXLINE);
  for (i=0;i<nset;i++) {
    plot->errorbars[i] = err[i];
    plot->colour[i] = col[i];
    plot->symbol[i] = sym[i];
    plot->line[i] = lin[i];
    plot->size[i] = size[i];
    plot->legend[i] = (char*)plot->legend+nset*sizeof(char*)+i*MAXLINE;
    strncpy(plot->legend[i],legend[i],MAXLINE);
  }
/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}
