#include "treelevel.h"
#include <stdio.h>
#include <math.h>
#include <array.h>
#define SMALL 0.00001

/**********************************************************************
 *                                                                    *
 *  Contains tree-level expressions and correction factors for        *
 *  quark propagator and quark-gluon vertex                           *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * z0m0: gives tree-level correction factors for the quark propagator *
 *  corr[0] <- Z0       (multiplicative correction of Z(p))           *
 *  corr[1] <- Zm0      (multiplicative correction of mass function)  *
 *  corr[2] <- Delta M0 (for additive correction of mass function)    *
 **********************************************************************/
int z0m0(Real** corr, Momenta* mom, Propagator_parameters* act)
{
  const char fnm[MAXNAME] = "z0m0";
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  int i;
  Real cq, d, d2, a1, b1;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return BADDATA;
  }
  if (mom->nvar<3) {
    fprintf(stderr,"%s: only %d lattice momentum variables known -- needs 3\n",
	    fnm,mom->nvar);
    return BADDATA;
  }
  if (mom->scale!=1.0) {
    fprintf(stderr,"%s: Error: physical scale is set.\n",fnm);
    fprintf(stderr,"\t\tCan only compute tree-level expressions");
    fprintf(stderr,"in lattice units.\n");
    return BADDATA;
  }

  if (act->action==CLOVER) {
    if (act->ncoeff>=2 && act->imp[1]!=0.0) { /* rotated propagator */
      if (act->ncoeff > 3 && act->imp[3] != 0.0) {
	fprintf(stderr,
		"%s: rotation and additive improvement coeffs both set\n",fnm);
	return BADDATA;
      }
      cq = act->imp[1];
      for (i=0; i<mom->nval; i++) {
	const Real ksqr  = mom->val[MOMK][i]*mom->val[MOMK][i];
	const Real qsqr  = mom->val[MOMQ][i]*mom->val[MOMQ][i];
	const Real M = act->mass + 0.5*qsqr;
	d  = ksqr + M*M;
	a1 = 1 + 2*cq*M - cq*cq*ksqr;
	b1 = M*(1-cq*cq*ksqr) - 2*cq*ksqr;
	d2 = ksqr*a1*a1 + b1*b1;

       /* corr[0]=z0; corr[1]=zm0; corr[2]=delta_m0 */
	corr[0][i] = d2/(a1*d);
	corr[1][i] = b1/(a1*act->mass);  /* Multiplicative scheme: zm0 */
	corr[2][i] = b1/a1 - act->mass;  /* Additive scheme:  delta_m0 */
      }
      return 0;
    }  /*** END OF ROTATED PROPAGATOR SECTION ***/
/*** From here on, cq is the additive improvement term in the quark prop ***/
    cq = act->ncoeff<4 ? 0 : act->imp[3];
  } else cq=0.0;

  for (i=0; i<mom->nval; i++) {
    const Real ksqr  = mom->val[MOMK][i]*mom->val[MOMK][i];
    const Real qsqr  = mom->val[MOMQ][i]*mom->val[MOMQ][i];
    const Real M = act->mass + 0.5*qsqr;

    d  = ksqr + M*M;
    b1 = cm*M - 2.0*cq*d;
    d2 = ksqr*cm*cm + b1*b1;

/* corr[0]=z0; corr[1]=zm0; corr[2]=delta_m0 */
    corr[0][i] = d2/(cm*d);
    corr[1][i] =  b1/(cm*act->mass);  /* Multiplicative scheme: zm0 */
    corr[2][i] =  b1/cm - act->mass;  /* Additive scheme:  delta_m0 */
  }
  return 0;
}

/**********************************************************************
 * z0m0: hybrid tree-level correction of the quark propagator         *
 *  corr[0] <- Z0        (multiplicative correction of Z(p))          *
 *  corr[1] <- Zm0+      (multiplicative correction of mass function) *
 *  corr[2] <- Delta M0- (additive correction of mass function)       *
 **********************************************************************/
int z0m0_pm(Real** corr, Momenta* mom, Propagator_parameters* act)
{
  const char fnm[MAXNAME] = "z0m0_pm";
  const Real m0 = act->mass;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real cm = 1.0+bq*m0;
  const float epsilon=0.00;  /* a small variation in the def */
  int i;
  Real cq, d, d2, a1, b1, b2;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return BADDATA;
  }
  if (mom->nvar<3) {
    fprintf(stderr,"%s: only %d lattice momentum variables known -- needs 3\n",
	    fnm,mom->nvar);
    return BADDATA;
  }
  if (mom->scale!=1.0) {
    fprintf(stderr,"%s: Error: physical scale is set.\n",fnm);
    fprintf(stderr,"\t\tCan only compute tree-level expressions");
    fprintf(stderr,"in lattice units.\n");
    return BADDATA;
  }

  if (act->action==CLOVER) {
    if (act->ncoeff>=2 && act->imp[1]!=0.0) {
      if (act->ncoeff > 3 && act->imp[3] != 0.0) {
	fprintf(stderr,
		"%s: rotation and additive improvement coeffs both set\n",fnm);
	return BADDATA;
      }
      cq = act->imp[1];
      for (i=0; i<mom->nval; i++) {
	const Real ksqr  = mom->val[MOMK][i]*mom->val[MOMK][i];
	const Real qsqr  = mom->val[MOMQ][i]*mom->val[MOMQ][i];
	const Real M = m0 + 0.5*qsqr;
	d  = ksqr + M*M;
	a1 = 1 + 2*cq*M - cq*cq*ksqr;
	b1 = m0 + 0.5*(qsqr-ksqr);
	b2 = -((2.0+m0)*cq-0.5)*ksqr - 0.5*cq*cq*qsqr*ksqr;
	d2 = ksqr*a1*a1 + (b1+b2)*(b1+b2);

       /* corr[0]=z0; corr[1]=zm(+); corr[2]=dm(-) */
	corr[0][i] = cm*d2/(a1*d);
	corr[1][i] = b1/(a1*m0);
	corr[2][i] = b2/a1;
      }
      return 0;
    }
    cq = act->ncoeff<4 ? 0 : act->imp[3];
  } else cq=0.0;

  for (i=0; i<mom->nval; i++) {
    const Real ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
    const Real qsqr = mom->val[MOMQ][i]*mom->val[MOMQ][i];
    const Real delta = qsqr-ksqr;
    const Real M = m0 + 0.5*qsqr;

    d  = ksqr + M*M;
    b1 = m0 + (bq-2.0*cq)*m0*m0 + 2.0*cq*delta + 2.0*(0.25*bq-cq)*m0*qsqr;
    b2 = - 0.5*cq*qsqr*qsqr + 2.0*(0.25-cq)*qsqr;
    d2 = ksqr*cm*cm + (b1+b2)*(b1+b2);

/* corr[0]=z0; corr[1]=zm(+); corr[2]=dm(-) */
    corr[0][i] = d2/(cm*d);
    corr[1][i] = (b1-epsilon*b2)/(cm*m0);
    corr[2][i] = (1.0+epsilon)*b2/cm;
  }

  return 0;
}

/*** z0_add: the additively improved quark propagator Z function, ***
 *** identical to the inverse of lambda_1 both at the             ***
 *** soft gluon and hard reflection                               ***/
Real z0_add(Real ksqr, Real qsqr, Real m0, Real bq, Real cq)
{
  Real d, d2, b1;
  const Real cm = 1.0+bq*m0;
  const Real M = m0+0.5*qsqr;

  d  = ksqr + M*M;
  b1 = cm*M - 2.0*cq*d;
  d2 = ksqr*cm*cm + b1*b1;

  return d2/(cm*d);
}

/*** z0_rot: the rotationally improved quark propagator Z func  ***/
Real z0_rot(Real ksqr, Real qsqr, Real m0, Real bq, Real cq)
{
  Real d, d2, b1;
  const Real cm = 1.0+bq*m0;
  const Real M = m0+0.5*qsqr;

  d  = ksqr + M*M;
  b1 = cm*M - 2.0*cq*d;
  d2 = ksqr*cm*cm + b1*b1;

  return d2/(cm*d);
}

/*** z0_arr: returns an array of the tree-level quark prop function Z0 ***/
Real *z0_arr(Momenta *mom, Propagator_parameters* act)
{
  const char fnm[MAXNAME] = "z0_arr";
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  Real *z;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }
  if (cq != 0.0 && ca != 0.0) {
    printf("%s: warning: both additive improvement and rotation set\n",
	    fnm);
    printf("%s: assuming rotation only\n",fnm);
    ca=0.0;
  }
  if (mom->nvar<3) {
    printf("%s: only %d lattice momentum variables known -- needs 3\n",
	   fnm,mom->nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }
  z = vecalloc(sizeof(Real),mom->nval);
  if (!z) return 0;

  if (ca!=0.0) {
    for (i=0; i<mom->nval; i++) {
      z[i] = z0_add(mom->val[MOMK][i]*mom->val[MOMK][i],
		    mom->val[MOMQ][i]*mom->val[MOMQ][i], act->mass, bq, ca);
    }
  } else {
    for (i=0; i<mom->nval; i++) {
      Real qsqr = mom->val[MOMQ][i]*mom->val[MOMQ][i];
      Real ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
      Real M = act->mass+0.5*qsqr;
      Real Ar = 1.0 + 2.0*cq*M - cq*cq*ksqr;
      Real Br = (1-cq*cq*ksqr)*M - 2.0*cq*ksqr;
      Real Dr = ksqr*Ar*Ar + Br*Br;
      z[i] = (1.0+bq*act->mass)*Dr/(Ar*(ksqr+M*M));
    }
  }

  return z;
}


/* l1_q0_noncov: tree-level lambda1 at the soft gluon point using the *
 *   non-covariant extraction  tr(gmu Gmu)|_pmu=0 [no sum over mu]    *
 * l1_q0_cpt is the single-component version; note that it does not   *
 *   depend on mu.                                                    *
 * For additive improvement this is equal to l1 = 1/z0(p)             *
 * For rotated propagators l1 = 1/(1+cq^2K^2(p))^2                    */
Real l1_q0_cpt(Momenta* mom, Prop_par* act, int* p, int mu, int i)
{
  const char fnm[MAXNAME] = "l1_q0_cpt";
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const int j = i<0 ? momindex_p(p,mom) : i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }

  if (mom->nvar<5) {
    printf("%s: only %d lattice momentum variables known -- needs 5\n",
	   fnm,mom->nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }

  if (ca!=0.0) {
    return 1.0/z0_add(mom->val[MOMK][j]*mom->val[MOMK][j],
		     mom->val[MOMQ][j]*mom->val[MOMQ][j],act->mass,bq,ca);
  } else {
    Real ksqr = mom->val[MOMK][j]*mom->val[MOMK][j];
    Real f = 1.0-cq*cq*ksqr;
    return f*f + 4.0*cq*cq*ksqr;
  }
}

Real* l1_q0_noncov(Momenta* mom, Prop_par* act)
{
  const char fnm[MAXNAME] = "l1_q0_noncov";
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  Real *l1;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }

  if (mom->nvar<5) {
    printf("%s: only %d lattice momentum variables known -- needs 5\n",
	   fnm,mom->nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }
  l1 = vecalloc(sizeof(Real),mom->nval);
  if (!l1) return 0;

  if (ca!=0.0) {
    for (i=0; i<mom->nval; i++) {
      l1[i] = 1.0/z0_add(mom->val[MOMK][i]*mom->val[MOMK][i],
			 mom->val[MOMQ][i]*mom->val[MOMQ][i],act->mass,bq,ca);
    }
  } else {
    for (i=0; i<mom->nval; i++) {
      Real ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
      Real f = 1.0-cq*cq*ksqr;
      l1[i] = f*f + 4.0*cq*cq*ksqr;
    }
  }
  return l1;
}

/* l1_q0_cov: tree-level lambda1 at the soft gluon point using the    *
 *   covariant extraction  P_{munu}tr(gnu Gmu)/3                      *
 *   where P_{munu} is the transverse projector                       */
Real* l1_q0_cov(Momenta* mom, Prop_par* act)
{
  const char fnm[MAXNAME] = "l1_q0_cov";
  Real *l1;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }

  if (mom->nvar<5) {
    printf("%s: only %d lattice momentum variables known -- needs 5\n",
	   fnm,mom->nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }
  l1 = vecalloc(sizeof(Real),mom->nval);
  if (!l1) return 0;

  for (i=0; i<mom->nval; i++) {
    Real ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
    Real qsqr = mom->val[MOMQ][i]*mom->val[MOMQ][i];
    l1[i] = (4.0 - mom->val[MOMKDK][i]/ksqr - 0.5*qsqr)/3.0;
  }
  return l1;
}

/*** l1_reflect: the transverse-projected quark reflection (q=-2p) vertex ***/
/*** NOTE: only additive improvement implemented, no rotation!            ***/
Real l1_reflect(Real ksqr, Real qsqr, Real Ksqr, Real kdotK, 
		Real m0, Real bq, Real cq, Real csw)
{
  Real a, b, bi, d, di, l1, tt3;
  const Real cm = 1.0+bq*m0;
  const Real M = m0+0.5*qsqr;

  d = ksqr + M*M;
  bi = cm*M - 2.0*cq*d;
  di = ksqr*cm*cm + bi*bi;
  
  a = cm*M - bi;
  b = cm*ksqr + M*bi;

/** lambda_1 - q^2 tau_3 = (1+bm)(B^2-A^2K^2)/DI^2 **/

  l1 = cm*(b*b - a*a*ksqr)/(di*di);

/** -tau~_3 = 2(1+bm)ABc_sw/DI^2 **/

  tt3 = 2.0*cm*a*b*csw/(di*di);

/** f1^(0) = 3(lambda_1 - 4K^2 tau_3) - (4K.K~ - K~^2 - 1/2 Q^2K.K~)tau~_3 **/

  return 0.25*( 3.0*l1 + (4.0*kdotK - Ksqr - 0.5*qsqr*kdotK)*tt3 );
/** we could also factor out cm/(di*di) and put it in at the end... **/
}

/*** The arr forms return a momentum array of the correction factors **/
Real *l1_refl_arr(Momenta *mom, Propagator_parameters* act)
{
  const char fnm[MAXNAME] = "tree_refl_arr";
  const Real csw = act->action==CLOVER ? act->ncoeff>0 ? act->imp[0] : 0 : 0;
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  Real *l1;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }
  if (cq!=0.0) {
    fprintf(stderr,"%s: rotation not implemented!\n",fnm);
    return 0;
  }
  if (mom->nvar<5) {
    printf("%s: only %d lattice momentum variables known -- needs 5\n",
	   fnm,mom->nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }
  l1 = vecalloc(sizeof(Real),mom->nval);
  if (!l1) return 0;

  for (i=0; i<mom->nval; i++) {
    l1[i] = l1_reflect(mom->val[MOMK][i]*mom->val[MOMK][i],
		       mom->val[MOMQ][i]*mom->val[MOMQ][i],
		       mom->val[MOMKK][i]*mom->val[MOMKK][i],
		       mom->val[MOMKDK][i], act->mass, bq, ca, csw);
  }

  return l1;
}

/*** l1_symm_cpt: componentwise tree-level lambda1 in ***
 *** quark reflection (q=-2p) kinematics              ***/
/*** NOTE: only additive improvement implemented, no rotation!       ***/
Real l1_refl_cpt(Momenta* mom, Propagator_parameters* act,
		 int* p, int mu, int i)
{
  const char fnm[MAXNAME] = "l1_symm_cpt";
  const int nvar = 5;
  const Real pmu = (mu==IT) 
    ? 2.0*PI*(p[mu]+0.5)/mom->L[mu] : 2.0*PI*p[mu]/mom->L[mu];
  const Real kmu = sin(pmu);
  const Real Kmu = 0.5*sin(2.0*pmu);
  const Real cmu = sqrt(1.0-kmu*kmu);
  const Real csw = act->action==CLOVER ? act->ncoeff>0 ? act->imp[0] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  const int j = i<0 ? momindex_p(p,mom) : i;
  Real ksqr, qsqr, kdotK, M, d, di;
  Real a, b, l1, tt3;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (j==DISCARDED) return 1.0;
  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return 0;
  }
  
  ksqr = mom->val[MOMK][j]*mom->val[MOMK][j];
  qsqr = mom->val[MOMQ][j]*mom->val[MOMQ][j];
  kdotK = mom->val[MOMKDK][j];

  M = act->mass+0.5*qsqr;
  d = ksqr + M*M;
  di = cm*cm + 4.0*ca*(ca*d-cm*M);
  b = (cm - 2.0*ca*M);

/** lambda_1 - q^2 tau_3 = (1+bm)(B^2-A^2K^2)/DI^2 **/

  l1 = cm*(b*b - 4.0*ca*ca*ksqr)/(di*di);

/** tau~_3 = -(1+bm)ABc_sw/2DI^2 **/

  tt3 = -cm*ca*b*csw/(di*di);

  a = kdotK*cmu - Kmu*Kmu;
  b = 1.0 - kmu*kmu/ksqr;

/** factor 3/4 = sum_mu(1-q_mu^2/q^2) / nmu  **/

  return 0.75*(l1 - 4.0*kdotK*cmu*tt3);
}

/*** tt3_sub returns the part containing the difference between the two ***
 ***  lattice T_3 tensors at the symmetric points (propto tau~_3),      ***
 ***  to be subtracted if we use hybrid correction for lambda'_1(sym)   ***/
/*** NOTE: only additive improvement implemented, no rotation!       ***/
Real tt3_sub(Momenta* mom, Propagator_parameters* act, int* p, int mu, int i)
{
  const char fnm[MAXNAME] = "tt3_sub";
  const int nvar = 5;
  const Real pmu = (mu==IT) 
    ? 2.0*PI*(p[mu]+0.5)/mom->L[mu] : 2.0*PI*p[mu]/mom->L[mu];
  const Real kmu = sin(pmu);
  const Real Kmu = 0.5*sin(2.0*pmu);
  const Real csw = act->action==CLOVER ? act->ncoeff>0 ? act->imp[0] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  const int j = i<0 ? momindex_p(p,mom) : i;
  Real ksqr, qsqr, kdotK, M, d, di;
  Real b, tt3;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (j==DISCARDED) return 1.0;
  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return 0;
  }
  
  ksqr = mom->val[MOMK][j]*mom->val[MOMK][j];
  qsqr = mom->val[MOMQ][j]*mom->val[MOMQ][j];
  kdotK = mom->val[MOMKDK][j];

  M = act->mass+0.5*qsqr;
  d = ksqr + M*M;
  di = cm*cm + 4.0*ca*(ca*d-cm*M);
  b = (cm - 2.0*ca*M);

/** tau~_3 = -2(1+bm)ABc_sw/DI^2 **/

  tt3 = -cm*ca*b*csw/(di*di);

  return 4.0*tt3*Kmu*(Kmu - kmu*kdotK/ksqr);
}

/*** l1_pq: the gamma_mu-traced vertex in a kinematics where p_mu=q_mu=0, ***
 ***        for any p and q.                                              ***
 ***  In the continuum this becomes lambda_1 - q^2tau_3 + (k^2-p^2)tau_6  ***/
/*** NOTE: only additive improvement implemented, no rotation!       ***/
Real l1_pq(Momenta* mom, Propagator_parameters* act, int* p, int* q, int mu)
{
  const char fnm[MAXNAME] = "l1_pq";
  const int nvar = 3;
  const Real csw = act->action==CLOVER ? act->ncoeff>0 ? act->imp[0] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  const Real a  = 2.0*ca;
  int k[DIR];
  int ip, ik;
  Real kp[DIR], kk[DIR], kq[DIR];
  Real Mp, Mk, dip, dik, bp, bk, pdk, pdq, kdq;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return UNSET_REAL;
  }
  
  for (i=0;i<DIR;i++) k[i]=p[i]+q[i];
  ik = momindex_p(k,mom);
  ip = momindex_p(p,mom);
  if (ip==DISCARDED || ik==DISCARDED) return 1.0;

  pdk = pdq = kdq = 0.0;
  for (i=0;i<SPATIAL_DIR;i++) {
    kp[i] = sin(2.0*PI*p[i]/mom->L[i]);
    kk[i] = sin(2.0*PI*k[i]/mom->L[i]);
    kq[i] = sin(2.0*PI*q[i]/mom->L[i]);
    pdk += kp[i]*kk[i];
    pdq += kp[i]*kq[i];
    kdq += kk[i]*kq[i];
  }
  kp[IT] = sin(2.0*PI*(p[i]+0.5)/mom->L[IT]);
  kk[IT] = sin(2.0*PI*(k[i]+0.5)/mom->L[IT]);
  kq[IT] = sin(2.0*PI*q[i]/mom->L[IT]);
  pdk += kp[IT]*kk[IT];
  pdq += kp[IT]*kq[IT];
  kdq += kk[IT]*kq[IT];

  Mp = act->mass + 0.5*mom->val[MOMQ][ip]*mom->val[MOMQ][ip];
  Mk = act->mass + 0.5*mom->val[MOMQ][ik]*mom->val[MOMQ][ik];
  bp = cm - a*Mp;
  bk = cm - a*Mk;
  dip = a*a*mom->val[MOMK][ip]*mom->val[MOMK][ip] + bp*bp;
  dik = a*a*mom->val[MOMK][ik]*mom->val[MOMK][ik] + bk*bk;

  return cm*( a*a*pdk + bp*bk - 0.5*csw*a*(bk*pdq - bp*kdq) )/(dip*dik);
}

/*** l2_cpt: tree-level lambda2 in soft gluon kinematics ***/
/* For additive improvement this is equal to ??? *
 * For rotated propagators, using                        *
 * Tr(gnu Lambda_mu [mu!=nu]) = Pmu Pnu l2[mu,nu]        *
 *  with Pmu -> 2Kmu(p)                                  *
 *  l2[mu,nu] = cqF(p) - 2cq^2Ktmu(p)/Kmu(p)             *
 *    with F(p) = 1-cq^2K^2(p)                           */
Real l2_cpt(Momenta* mom, Propagator_parameters* ppar,
		int* p, int imu, int i)
{
  const char fnm[MAXNAME] = "l2_cpt_div";
  const int nvar = 2;
  const int mu = imu/3;
  const int nu = (imu%3 < mu) ? imu%3 : imu%3+1;
  const Real pmu = (mu==IT) 
    ? 2.0*PI*(p[mu]+0.5)/mom->L[mu] : 2.0*PI*p[mu]/mom->L[mu];
  const Real pnu = (nu==IT) 
    ? 2.0*PI*(p[nu]+0.5)/mom->L[nu] : 2.0*PI*p[nu]/mom->L[nu];
  const Real kmu = sin(pmu);
  const Real knu = sin(pnu);
  const Real ktmu = 0.5*sin(2.0*pmu);
  const Real cq  = (ppar->ncoeff > 1) ? ppar->imp[1] : 0.0;
  const int j = i<0 ? momindex_p(p,mom) : i;
  Real ksqr, di;

  if (ppar->action!=WILSON && ppar->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (fabs(cq)<SMALL) return 0.0;
  if (j==DISCARDED) return 0.0;
  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return 0;
  }
  if ( (fabs(kmu) < SMALL) || (fabs(knu) < SMALL) ) return 0.0;
  ksqr = mom->val[MOMK][j]*mom->val[MOMK][j];

  if (cq==0.0) { /* unrotated propagator */
    printf("%s: only defined for rotated propagator\n",fnm);
    return 0;
  } else { /* rotated propagator */
    Real chi = 1.0+cq*cq*ksqr;
    di = chi*chi;
    return -(cq*(1.0-cq*cq*ksqr)-2.0*cq*cq*ktmu/kmu)/(di*di);
  }
}

/* l2_q0_cov: tree-level lambda2 at the soft gluon point using the    *
 *   covariant extraction  (delta_munu-4Kmu*Knu/K^2)tr(gnu Gmu)/12K^2 */
Real* l2_q0_cov(Momenta* mom, Prop_par* act)
{
  const char fnm[MAXNAME] = "l2_q0_cov";
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  Real *l2;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }

  if (mom->nvar<5) {
    printf("%s: only %d lattice momentum variables known -- needs 5\n",
	   fnm,mom->nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }
  l2 = vecalloc(sizeof(Real),mom->nval);
  if (!l2) return 0;

  for (i=0; i<mom->nval; i++) {
    Real ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
    Real qsqr = mom->val[MOMQ][i]*mom->val[MOMQ][i];
    Real f = 1.0+cq*cq*ksqr;
    l2[i] = (4.0*(1.0 - mom->val[MOMKDK][i]/ksqr) - 0.5*qsqr)/(12.0*ksqr);
    l2[i] -= cq*(1-cq*cq*ksqr - cq*mom->val[MOMKDK][i]/ksqr)/(f*f);
  }
  return l2;
}
/*** l3: the scalar part of the vertex at zero gluon momentum ***/
/* For additive improvement this is equal to ??? *
 * For rotated propagators                                   *
 *  l3 = [F^2(p) - 4cq^2K^2(p) - 4cqF(p)K(p).Kt(p)/K^2(p)]/2 *
 *    with F(p) = 1-cq^2K^2(p)                               */
Real l3(Real ksqr, Real qsqr, Real kdotK, Real m0, Real bq, Real cq)
{
  const Real cm = 1.0+bq*m0;
  const Real M = m0+0.5*qsqr;
  const Real d = ksqr + M*M;
  Real bi,di,a,b;

  if (ksqr<SMALL) return 0.0;

  bi = cm*M - 2.0*cq*d;
  di = ksqr*cm*cm + bi*bi;
  a  = 2.0*cq*d;
  b  = cm*ksqr + M*bi; // = cm*(ksqr+M*M) - 2.0*cq*M*d;

  return cm*(a*a*ksqr - b*b + 2.0*a*b*kdotK/ksqr)/(di*di);
}

Real l3_cpt(Momenta* mom, Propagator_parameters* act, int* p, int mu, int i)
{
  const char fnm[MAXNAME] = "l3_cpt";
  const int nvar = 5;  /* we need momenta up to MOMKDK=4 */
  const Real pmu = (mu==IT) 
    ? 2.0*PI*(p[mu]+0.5)/mom->L[mu] : 2.0*PI*p[mu]/mom->L[mu];
  const Real kmu = sin(pmu);
  const Real cmu = sqrt(1.0-kmu*kmu);
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  const int j = i<0 ? momindex_p(p,mom) : i;
  Real ksqr, qsqr, M, d, di, b;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (j==DISCARDED) return 0.0;
  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return 0;
  }
  
  ksqr = mom->val[MOMK][j]*mom->val[MOMK][j];
  qsqr = mom->val[MOMQ][j]*mom->val[MOMQ][j];
  if (ksqr<SMALL) return 0.0;

  M = act->mass+0.5*qsqr;

  if (cq==0.0) { /* unrotated propagator */
    d = ksqr + M*M;
    // bi = cm*M - 2.0*ca*d;
    di = cm*cm + 4.0*ca*(ca*d-cm*M);
    // = ksqr*cm*cm + bi*bi;
    b  = cm-2.0*ca*M;

    return 0.5*kmu*kmu*cm*(4.0*ca*ca*ksqr 
			   - b*b + 4.0*ca*b*cmu)/(di*di*ksqr);
  } else { /* rotated propagator */
    Real F   = 1.0-cq*cq*ksqr;
    Real chi = 1.0+cq*cq*ksqr;
    di = chi*chi;
    return 0.5*kmu*kmu*(F*F-4.0*cq*cq*ksqr-4.0*cq*F*cmu)/(di*di*ksqr);
#if 0 // These factors should be there in the end...
    m2 = 1.0 + 0.5*act->mass;
    return 0.5*kmu*kmu*bm*(F*F-4.0*cq*cq*ksqr-4.0*cq*F*cmu)/(di*di*m2*m2);
#endif
  }
  
}

Real *l3_arr(Momenta *mom, Propagator_parameters* act)
{
  const char fnm[MAXNAME] = "l3_arr";
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const int noca = (fabs(ca)<SMALL);
  const int nvar = noca ? 2 : 4;
  Real *l3a;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return 0;
  }

  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return 0;
  }
  if (mom->scale != 1.0) {
    printf("%s: Error: physical scale is set.\n",fnm);
    printf("\t\tCan only compute tree-level expressions in lattice units.\n");
    return 0;
  }
  l3a = vecalloc(sizeof(Real),mom->nval);
  if (!l3a) return 0;

  if (cq==0.0) { /* unrotated propagator */
    for (i=0; i<mom->nval; i++) {
      if (noca) l3a[i] = 1.0 + bq*act->mass;
      else /* additive improvement */
	l3a[i] = l3(mom->val[MOMK][i]*mom->val[MOMK][i],
		    mom->val[MOMQ][i]*mom->val[MOMQ][i], mom->val[MOMKDK][i],
		    act->mass, bq, ca);
    }
  } else {  /* rotated propagator */
    for (i=0; i<mom->nval; i++) {
      Real ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
      Real kdK = mom->val[MOMKDK][i];
      Real f = 1.0-cq*cq*ksqr;
      Real chi = 1.0+cq*cq*ksqr, dr=chi*chi;
      l3a[i] = 0.5*(f*f - 4.0*cq*cq*ksqr - 4.0*cq*f*kdK/ksqr)/(dr*dr);
      // TODO: put in bq and mass correction factors...
    }    
  }
  return l3a;
}

/** Tree-level imTr(sigma_{nu,mu}Lambda_mu) -> tau5          **
 ** at the symmetric point (q=-2p), with 2K_nu factored out  **
 ** mu,nu are passed as one variable munu = 0...11:          **
 ** (1,0), (2,0), (3,0), (1,2), (3,2), (4,2) etc             **/
/*** NOTE: only additive improvement implemented, no rotation!       ***/
Real t5_cpt(Momenta* mom, Propagator_parameters* ppar, int* p, int munu, int i)
{
  const char fnm[MAXNAME] = "l5_cpt";
  const int nvar = 4;
  const int mu = munu/3;
  const int nu = (munu%3)<mu ? munu%3 : munu%3 + 1;
  const Real pmu = (mu==IT) 
    ? 2.0*PI*(p[mu]+0.5)/mom->L[mu] : 2.0*PI*p[mu]/mom->L[mu];
  const Real pnu = (nu==IT) 
    ? 2.0*PI*(p[nu]+0.5)/mom->L[nu] : 2.0*PI*p[nu]/mom->L[nu];
  const Real kmu = sin(pmu);
  const Real cmu = sqrt(1.0-kmu*kmu);
  const Real knu = sin(pnu);
  const Real cnu = cos(pnu);
  const Real csw = (ppar->ncoeff>0) ? ppar->imp[0] : 0.0;
  const Real m0  = ppar->mass;
  const Real bq  = (ppar->ncoeff > 2) ? ppar->imp[2] : 0.0;
  const Real cq  = (ppar->ncoeff > 3) ? ppar->imp[3] : 0.0;
  const Real cm = 1.0+bq*m0;
  const int j = i<0 ? momindex_p(p,mom) : i;
  Real ksqr, qsqr, kdotK, M, d, di, t, t2;

  if (ppar->action!=WILSON && ppar->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (j==DISCARDED) return 0.0;
  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return 0;
  }
  
  ksqr = mom->val[MOMK][j]*mom->val[MOMK][j];
  qsqr = mom->val[MOMQ][j]*mom->val[MOMQ][j];
  kdotK = mom->val[MOMKDK][j];

  M = m0+0.5*qsqr;
  d = ksqr + M*M;
  di = cm*cm + 2.0*cq*(cq*d-cm*M);

  t  = 2.0*cq*(cm-2.0*cq*M);
  t2 = 4.0*csw*cq*cq*kdotK;

  return knu*knu/(3.0*ksqr)*cm*
    ( (t + cmu*t2*(1.0-(cmu-cnu)*kmu*kmu/kdotK))/(di*di) - 0.5*csw*cmu*cnu/di );
}

/** Tree-level reTr(g_5 g_mu Lambda_nu) -> tau8              **
 ** mu,nu are passed as one variable munu = 0...11:          **
 ** (1,0), (2,0), (3,0), (1,2), (3,2), (4,2) etc             **/
/*** NOTE: only rotated propagator implemented              ***/
Real t8_cpt(Momenta* mom, Propagator_parameters* act, int* p, int* q, int munu)
{
  const char fnm[MAXNAME] = "t8_pq";
  const int nvar = 7; /* min #mom-variables required */
  const int mu = munu/3;
  const int nu = (munu%3)<mu ? munu%3 : munu%3 + 1;
  const Real csw = act->action==CLOVER ? act->ncoeff>0 ? act->imp[0] : 0 : 0;
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  int k[DIR];
  int ip, ik;
  int rho, sigma;
  Real kp[DIR], kk[DIR], kq[DIR], cP[DIR], cQ[DIR];
  Real ksqr, psqr, chik, chip;
  Real t8a, t8b;
  int i;

  if (act->action!=WILSON && act->action!=CLOVER) {
    fprintf(stderr,"%s: wrong action - only wilson and clover implemented\n",
	    fnm);
    return UNSET_REAL;
  }

  if (mom->nvar<nvar) {
    printf("%s: only %d lattice momentum variables known -- needs %d\n",
	   fnm,mom->nvar,nvar);
    return UNSET_REAL;
  }
  
  for (i=0;i<DIR;i++) k[i]=p[i]+q[i];
  ik = momindex_p(k,mom);
  ip = momindex_p(p,mom);
  if (ip==DISCARDED || ik==DISCARDED) return 0.0;
  ksqr = mom->val[MOMK][ik]*mom->val[MOMK][ik];
  psqr = mom->val[MOMK][ip]*mom->val[MOMK][ip];
  chik = 1.0 + cq*cq*ksqr;
  chip = 1.0 + cq*cq*psqr;

/* Compute lattice momenta/trigonometric functions K_i(p), C_i(q) etc */
  for (i=0;i<SPATIAL_DIR;i++) {
    kp[i] = sin(2.0*PI*p[i]/mom->L[i]);
    kk[i] = sin(2.0*PI*k[i]/mom->L[i]);
    kq[i] = sin(2.0*PI*q[i]/mom->L[i]);
    cQ[i] = cos(PI*q[i]/mom->L[i]);
    cP[i] = cos(PI*(p[i]+k[i])/mom->L[i]);
  }
  kp[IT] = sin(2.0*PI*(p[i]+0.5)/mom->L[IT]);
  kk[IT] = sin(2.0*PI*(k[i]+0.5)/mom->L[IT]);
  kq[IT] = sin(2.0*PI*q[i]/mom->L[IT]);
  cQ[IT] = cos(PI*q[IT]/mom->L[IT]);
  cP[IT] = cos(PI*(p[IT]+k[IT]+1.0)/mom->L[IT]);

/* Find rho, sigma such that e_munurhosigma = +1 */
  rho=sigma=-1;
  for (i=0;i<DIR;i++) {
    if (i!=mu && i!=nu) {
      if (rho==-1) rho=i; else sigma=i;
    }
  }
  if (mu-nu==1 || mu-nu==3 || mu-nu==-2) {i=rho; rho=sigma; sigma=i;}
  printf("%d %d %d %d\n",mu,nu,rho,sigma);

/** t8a, t8b: the two terms of the tree-level tau8 */
  t8a = 4.0*cq*cq*cP[nu]*(kp[rho]*kk[sigma]-kk[rho]*kp[sigma]);
  t8b = cq*csw*cQ[nu]*(kq[rho]*(chip*kk[sigma]+chik*kp[sigma])
		       -kq[sigma]*(chip*kk[rho]+chik*kp[rho]));
  return cm*(t8a+t8b)/(mom->val[MOMD][ip]*mom->val[MOMD][ik]);
}
