#include "analysis.h"

/************************************************************************
 *                                                                      *
 *  Fit to the function y = sum(n) a(n) x^n                             *
 *                                                                      *
 ************************************************************************/

void polynomial(Real x, Real *a, Real *y, 
		Real *dyda, Real **d2yda2, Real* dydx, int nparam)
{
  const char fnm[MAXNAME] = "polynomial";
  int i, j;
  
  if (nparam < 1) {
    fprintf(stderr, "%s: illegal nparam %d\n", fnm, nparam);
    exit(EVIL);
  }

  if (d2yda2) {
    for (i=0;i<nparam;i++) for (j=0;j<nparam;j++) d2yda2[i][j] = 0.0;
  }
  if(dyda) {
    dyda[0] = 1.0;
    for (i=1; i<nparam; i++) {
      dyda[i] = x*dyda[i-1];
    }
  }

  if (dydx) {
    *dydx = (nparam-1)*a[nparam-1];
    for (i=nparam-2; i>0; i--) {
      *dydx = i*a[i] + (*dydx)*x;
    }
  }

  if (y) {
    *y = a[nparam-1];
    for (i=nparam-2; i>=0; i--) {
      *y = a[i] + (*y)*x;
    }
  }
}



/************************************************************************
 *                                                                      *
 *  Estimate the fit parameters of the function y = sum(n) a(n) x^n     *
 *                                                                      *
 *  Simply try a straight line fit to the two extreme data points       *
 *  and set a(n) = 0 for n > 2.                                         *
 *                                                                      *
 ************************************************************************/

void polyguess(Real *fitparam, int *freeze, int nparam,
               Real *x, Real *y, int ndata)
{
  const char fnm[MAXNAME] = "polyguess";
  Real x1, x2, y1, y2;
  int i,imin,imax;

  if (nparam<1) {
    fprintf(stderr, "%s: illegal nparam %d\n", fnm, nparam);
    exit(-1);
  }

  /** Find the max and min x values (since the x array may not be ordered) **/
  x1 = x[0];
  x2 = x[ndata-1];
  imin=0; imax=ndata-1;
  for (i=1; i<ndata-1; i++) {
    if (x[i]<x1) {
      imin=i;
      x1=x[i];
    }
    if (x[i]>x2) {
      imax=i;
      x2=x[i];
    }
  }
  y1 = y[imin];
  y2 = y[imax];

  if (!freeze[0]) {
    if (nparam > 1) {
      fitparam[0] = (y1*x2-y2*x1)/(x2-x1);
    } else {
      fitparam[0] = (y1+y2)/2.0;
    }
  }

  if (nparam>1 && !freeze[1]) {
    fitparam[1] = (y2-y1)/(x2-x1);
  }

  for (i=2; i<nparam; i++) {
    if (!freeze[i]) fitparam[i] = 0.0;
  }
}
