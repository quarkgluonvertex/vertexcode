#include "analysis.h"
#include "rand.h"

void xdatafunc_cov_ave(VectorData* data, OffsetDatafunction func, 
		       double* ave, double** cov, 
		       int* cmask, int* xmask, int xvar);


int vector_fit(VectorData* ths, BootFitFuncs* fit, int* mask)
{
  const char fnm[MAXNAME] = "vector_fit";
  const int nparam = fit->nparam;
  const int nboot = fit->nboot;
  const int nset  = ths->nconfig;
  const int ndata = ntrue(fit->subset,ths->length);
  const int nvector = ths->nvector;
  const int nscalar = ths->nscalar;
  int i, j, status;
  /* if xfunc is null we use the xdata values as xdata */
  const int xvar = (nset>1 && fit->xfunc) ? fit->xfunc(0,0,0,0) : 0;
  /* if yfunc is null we use vector_data[yoffset] as ydata */
  const int yvar = nset>1 ? fit->yfunc ? fit->yfunc(0,0,0,0) : 1 : 0;

  OffsetDatafunction xfn;
  OffsetDatafunction yfn;
  FitData fitdat;
  /* temporary arrays of bootstrap samples */
  Real*** vecbundle = arralloc(sizeof(Real*),2,nvector,nset);
  Real** scalbundle = arralloc(sizeof(Real),2,nscalar,nset);

  /* retainer for original data during loop over boot samples */
  Real*** vdat = ths->vector_data;
  Real**  sdat = ths->scalar_data;

  if (!yvar) {
    printf("%s: cannot perform chi-squared fit",fnm);
    printf(" if the ydata are constant!\n");
    return BADDATA;
  }

  init_fitdata(&fitdat,fit,ndata,xvar);
  printf("%s: nfit=%d\n",fnm,ndata);
  fit->ndof = ndata;
  for (i=0;i<nparam;i++) if (!fit->freeze[i]) fit->ndof--;

/* Set up offset datafunctions */
  xfn.func = fit->xfunc;
  xfn.voffset = fit->xoffset;
  xfn.soffset = fit->xsoffset;
  yfn.func = fit->yfunc;
  yfn.voffset = fit->yoffset;
  yfn.soffset = fit->ysoffset;

/* Get covariance matrix and central values and set up initial guesses */
  xdatafunc_cov_ave(ths,xfn,fitdat.xdata,fitdat.xdatacov,
		   0,fit->subset,xvar);
  offsetfunc_cov_ave(ths,&yfn,fitdat.ydata,fitdat.ydatacov,0,fit->subset);
  fit->guess(fitdat.param,fit->freeze,nparam,
	     fitdat.xdata,fitdat.ydata,ndata);

/* Perform chi-square fit to get the best fit */
  status = chisq_fit(&fitdat);
  for (j=0; j<nparam; j++) fit->param[j].best = fitdat.param[j];
  fit->chisq.best = fitdat.chisq;

/* Save the original data */
  sdat = ths->scalar_data;
  vdat = ths->vector_data;

/* Loop over bootstrap samples */
  rand_initialise(fit->seed);

  for (i=0; i<nboot; i++) {
    if (fit->bootinfo) { /* resample according to the list in bootinfo */
      resample_datasetbundle(vecbundle,vdat,nvector,
			     scalbundle,sdat,nscalar,nset,
			     fit->bootinfo->config_list[i]);
    } else { /* create a new random bootstrap sample */
      get_boot_datasetbundle(vecbundle,vdat,nvector,
			     scalbundle,sdat,nscalar,nset);
    }
/* put bootstrap samples in the vector data to perform stats */
    ths->scalar_data = scalbundle;
    ths->vector_data = vecbundle;
    xdatafunc_cov_ave(ths,xfn,fitdat.xdata,fitdat.xdatacov,
		     0,fit->subset,xvar);
    offsetfunc_cov_ave(ths,&yfn,fitdat.ydata,fitdat.ydatacov,
			0,fit->subset);

    /* set up initial guess including frozen parameters */
    for (j=0; j<nparam; j++) {
      if (fit->freeze[j] && fit->param[j].nboot != 0) {
        fitdat.param[j] = fit->param[j].boot[i];
      } else {
        fitdat.param[j] = fit->param[j].best;
      }
    }
    chisq_fit(&fitdat);
    for (j=0; j<nparam; j++) {
      if (!fit->freeze[j]) fit->param[j].boot[i] = fitdat.param[j];
    }
    fit->chisq.boot[i] = fitdat.chisq;
  }

  /* restore original data */
  ths->scalar_data = sdat;
  ths->vector_data = vdat;

  return status;
}

/*** vector_fit_multiensemble: fit data where each x-value comes from ***
 *** a different ensemble, with different numbers of configurations   ***/
int vector_fit_multiensemble(VectorData* ths, BootFitFuncs* fit, int* ncfg)
{
  const char fnm[MAXNAME] = "vector_fit_multiensemble";
  const int nparam = fit->nparam;
  const int nboot = fit->nboot;
  const int ndata = ntrue(fit->subset,ths->length);
  const int nvector = ths->nvector;
  const int nscalar = ths->nscalar;
  int i, j, k, nset, status;
  /* if xfunc is null we use the xdata values as xdata */
  const int xvar = (fit->xfunc) ? fit->xfunc(0,0,0,0) : 0;
  /* if yfunc is null we use vector_data[yoffset] as ydata */
  int yvar;

  OffsetDatafunction xfn;
  OffsetDatafunction yfn;
  FitData fitdat;
  int* xmask = vecalloc(sizeof(int),ths->length);

  /* temporary arrays of bootstrap samples and (co)variances */
  Real*** vecbundle;
  Real** scalbundle;
  Real** cov = arralloc(sizeof(Real),2,1,1);

  /* retainer for original data during loop over boot samples */
  Real*** vdat = ths->vector_data;
  Real**  sdat = ths->scalar_data;

  /* determine the maximum number of configurations nset */
  nset=ncfg[0];
  for (i=1;i<ndata;i++) if (ncfg[i]>nset) nset=ncfg[i];
  yvar = nset>1 ? fit->yfunc ? fit->yfunc(0,0,0,0) : 1 : 0;

  if (!yvar) {
    printf("%s: cannot perform chi-squared fit",fnm);
    printf(" if the ydata are constant!\n");
    return BADDATA;
  }

  init_fitdata(&fitdat,fit,ndata,xvar);
#ifdef VERBOSE1
  printf("%s: nfit=%d\n",fnm,ndata);
#endif
  fit->ndof = ndata;
  for (i=0;i<nparam;i++) if (!fit->freeze[i]) fit->ndof--;

/* Set up offset datafunctions */
  xfn.func = fit->xfunc;
  xfn.voffset = fit->xoffset;
  xfn.soffset = fit->xsoffset;
  yfn.func = fit->yfunc;
  yfn.voffset = fit->yoffset;
  yfn.soffset = fit->ysoffset;

/* Get covariance matrix and central values and set up initial guesses */
/* The covariance matrix is diagonal since the data come from          *
 * independent ensembles.  We therefore loop over data/ensembles       */
  for (i=0;i<ndata;i++) {
    for (j=0;j<ndata;j++) {
      fitdat.ydatacov[i][j]=0.0;
      if (xvar) fitdat.xdatacov[i][j]=0.0;
    }
    /* set mask so we only look at data value i */
    for (j=0;j<ths->length;j++) xmask[j]=false;
    xmask[i]=true;
    ths->nconfig = ncfg[i];
    xdatafunc_cov_ave(ths,xfn,&fitdat.xdata[i],cov,
		      0,xmask,xvar);
    if (xvar) fitdat.xdatacov[i][i] = cov[0][0];
    offsetfunc_cov_ave(ths,&yfn,&fitdat.ydata[i],cov,0,xmask);
    fitdat.ydatacov[i][i] = cov[0][0];
  }
  fit->guess(fitdat.param,fit->freeze,nparam,
	     fitdat.xdata,fitdat.ydata,ndata);
/* Perform chi-square fit to get the best fit */
  status = chisq_fit(&fitdat);
  for (j=0; j<nparam; j++) fit->param[j].best = fitdat.param[j];
  fit->chisq.best = fitdat.chisq;

/* Save the original data */
  sdat = ths->scalar_data;
  vdat = ths->vector_data;

/* Allocate space for bootstrap samples */
  vecbundle = arralloc(sizeof(Real),3,nvector,nset,ndata);
  scalbundle = arralloc(sizeof(Real),2,nscalar,nset);

/* Loop over bootstrap samples */
  rand_initialise(fit->seed);

  for (i=0; i<nboot; i++) {
    get_boot_datasetbundle_multi(vecbundle,vdat,nvector,
				 scalbundle,sdat,nscalar,ncfg,ndata);
/* put bootstrap samples in the momdata to perform stats */
    ths->scalar_data = scalbundle;
    ths->vector_data = vecbundle;
    for (j=0;j<ndata;j++) {
      /* set mask so we only look at data value i */
      for (k=0;k<ths->length;k++) xmask[k]=false;
      xmask[j]=true;

      ths->nconfig = ncfg[j];
      xdatafunc_cov_ave(ths,xfn,&fitdat.xdata[j],cov,0,xmask,xvar);
      if (xvar) fitdat.xdatacov[j][j] = cov[0][0];
      offsetfunc_cov_ave(ths,&yfn,&fitdat.ydata[j],cov,0,xmask);
      fitdat.ydatacov[j][j] = cov[0][0];
    }

    /* set up initial guess including frozen parameters */
    for (j=0; j<nparam; j++) {
      if (fit->freeze[j] && fit->param[j].nboot != 0) {
        fitdat.param[j] = fit->param[j].boot[i];
      } else {
        fitdat.param[j] = fit->param[j].best;
      }
    }
    chisq_fit(&fitdat);
    for (j=0; j<nparam; j++) {
      if (!fit->freeze[j]) fit->param[j].boot[i] = fitdat.param[j];
    }
    fit->chisq.boot[i] = fitdat.chisq;
  }

  /* restore original data */
  ths->scalar_data = sdat;
  ths->vector_data = vdat;
  ths->nconfig = nset;

  free(vecbundle);
  free(scalbundle);
  free(cov);
  return status;
}

/************************************************************************
 *  fit_bootdata: performs chi-squared fits to bootstrapped data        *
 *  fit_ybootdata: like fit_bootdata except that the xdata do not vary  *
 ************************************************************************/
int fit_bootdata(Boot* x, Boot* y, BootFitFuncs* fit)
{
  const char fnm[MAXNAME] = "fit_bootdata";
  const int nparam = fit->nparam;
  const int nboot = fit->nboot;
  const int ndata = ntrue(fit->subset,fit->ndata);
  int i, j, ibt, status;
  const int xvar = (x->nboot > 0);
  const int yvar = (y->nboot > 0);

  FitData fitdat;

  if (!yvar) {
    printf("%s: cannot perform chi-squared fit",fnm);
    printf(" if the ydata are constant!\n");
    return BADDATA;
  }
  /** check consistency of bootstrap params **/
  for (i=0;i<fit->ndata;i++) {
    if (y[i].nboot != nboot || (xvar && x[i].nboot != nboot)) {
      fprintf(stderr,"%s: conflicting nboot for data # %d: %d %d %d\n",fnm,
	      i,nboot,x[i].nboot,y[i].nboot);
      return BADDATA;
    }
    if (!xvar && x[i].nboot>0) {
      fprintf(stderr,"%s: x[0] is constant but x[%d] is not!\n",fnm,i);
      return BADDATA;
    }
  }
  init_fitdata(&fitdat,fit,ndata,xvar);
  printf("%s: nfit=%d\n",fnm,ndata);
  fit->ndof = ndata;
  for (i=0;i<nparam;i++) if (!fit->freeze[i]) fit->ndof--;

/* Get covariance matrix and central values and set up initial guesses */
  for (i=j=0;i<fit->ndata;i++) {
    if (MASK(fit->subset,i)) {
      fitdat.xdata[j] = x[i].best;
      fitdat.ydata[j] = y[i].best;
      j++;
    }
  }
  if (xvar) {
    status=boot_covariance(fitdat.xdatacov,x,fit->ndata,fit->subset);
    if (status) return status;
  }
  status=boot_covariance(fitdat.ydatacov,y,fit->ndata,fit->subset);
  if (status) return status;
  fit->guess(fitdat.param,fit->freeze,nparam,
	     fitdat.xdata,fitdat.ydata,ndata);

/* Perform chi-square fit to get the best fit */
  status = chisq_fit(&fitdat);
  for (j=0; j<nparam; j++) fit->param[j].best = fitdat.param[j];
  fit->chisq.best = fitdat.chisq;

  for (ibt=0; ibt<nboot; ibt++) {
    /* Get data for this boot sample and use best fit for initial guess */
    for (i=j=0;i<fit->ndata;i++) {
      if (MASK(fit->subset,i)) {
	if (xvar) fitdat.xdata[j] = x[i].boot[ibt];
	fitdat.ydata[j] = y[i].boot[ibt];
	j++;
      }
    }
    for (j=0;j<nparam;j++) {
      if (fit->freeze[j] && fit->param[j].nboot != 0) {
        fitdat.param[j] = fit->param[j].boot[ibt];
      } else {
        fitdat.param[j] = fit->param[j].best;
      }
    }
    chisq_fit(&fitdat);
    for (j=0;j<nparam;j++) {
      if (!fit->freeze[j]) fit->param[j].boot[ibt] = fitdat.param[j];
    }
    fit->chisq.boot[ibt] = fitdat.chisq;
  }

  return status;
}

int fit_ybootdata(Real* x, Boot* y, BootFitFuncs* fit)
{
  Boot* xbt = vecalloc(sizeof(Boot),fit->ndata);
  int i;
  int status;

  for (i=0;i<fit->ndata;i++) {
    xbt[i].best = x[i];
#ifdef PARANOID
    /* the following are nulled by vecalloc */
    xbt[i].nboot = 0;
    xbt[i].boot = 0;
#endif
  }
  status = fit_bootdata(xbt,y,fit);
  free(xbt);
  return status;
}

void xdatafunc_cov_ave(VectorData* data, OffsetDatafunction func, 
		       double* ave, double** cov, 
		       int* cmask, int* xmask, int xvar)
{
  const int ndata = data->length;
  const int voff = func.voffset;
  const int soff = func.soffset;
  int i, j;

  if (xvar) {
/* Get covariance matrix and central values */
    offsetfunc_cov_ave(data,&func,ave,cov,cmask,xmask);
  } else if (func.func) {
    if (func.func(0,0,0,0)) {
/* evaluate the datafunction for a single config */
      const int nvec = data->nvector - func.voffset;
      const int nbundle = nvec + data->nscalar - func.soffset;
      double dat[nbundle];
      int bundle;
      for (bundle=soff; bundle<data->nscalar; bundle++) {
	dat[bundle+nvec-soff] = data->scalar_data[bundle][0];
      }
      for (i=0,j=0; i<ndata; i++) {
	if (MASK(xmask,i)) {
	  for (bundle=voff; bundle<data->nvector; bundle++)
	    dat[bundle-voff] = data->vector_data[bundle][0][i];

	  func.func(&ave[j],data->xdata[i],dat,nbundle);
	  j++;
	}
      }
    } else {
/* the datafunction depends only on the "x" values so pass only those */
      for (i=0,j=0;i<ndata;i++) {
	if (MASK(xmask,i)) func.func(&ave[j++],data->xdata[i],0,0);
      }
    }
  } else {
    for (i=0,j=0;i<ndata;i++) if (MASK(xmask,i)) ave[j++] = data->xdata[i];
  }
}

int fake_boot_covariance(Real** cov, Real* ave, Boot* bt, int ndata, int* xmask)
{
  VectorData dat;
  int i, j;

  dat.type=UNKNOWN; dat.parm=0;
  dat.nconfig = bt->nboot;
  dat.length = ndata;
  dat.nscalar = dat.nsparm = dat.nvparm = 0;
  dat.nvector = 1;
  dat.vector_data = arralloc(sizeof(Real),3,1,bt->nboot,ndata);
  dat.scalar_data = dat.vector_parm = 0;
  dat.scalar_parm = dat.xdata = 0;
  for (i=0;i<ndata;i++) for (j=0;j<bt->nboot;j++)
    dat.vector_data[0][j][i] = bt[i].boot[j];
  datafunc_cov_ave(&dat,0,ave,cov,0,xmask);
  for (i=0;i<ndata;i++) for (j=0;j<ndata;j++) cov[i][j] *= bt->nboot-1;
  free(dat.vector_data);
  return 0;
}
