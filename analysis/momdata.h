#ifndef _MOMDATA_H_
#define _MOMDATA_H_
#include "analysis.h"
#include "momenta.h"
#include "field_params.h"

typedef struct {
  Data_t type;
  void* parm;
  int time;                     /* only for real-time or timesliced data */
  Momenta* mom;
  int momvar;   /* which momentum variable to use - mom doesnt know this */
  int nconfig;
  int nscalar;
  int nvector;
  int nsparm;
  int nvparm;
  Real** scalar_data;
  Real*** vector_data;
  Real* scalar_parm;
  Real** vector_parm;
  char basename[MAXNAME];
  char name[MAXLINE];
  char filename[MAXLINE];
  int id;                                     /* any further information */
} MomData;

int init_momdata(MomData* data, void* parm, Momenta* mom, Data_t type, 
		 int nconf, int nscal, int nvect, char* nm, char* dir);
int momdata_add_pars(MomData*, int nspar, int nvpar);

int assign_momdata_subset(int** subset, MomData* data, Datafunction* xfunc,
			  double xmin, double xmax);
int assign_submomdata(int** subset, MomData* data, OffsetDatafunction xfunc,
		      double xmin, double xmax);
void momdata_create_name(MomData* data);
void momdata_create_filename(MomData*,char* dir);
int momdata_set_filename(MomData* data, char* nm);
int read_momdata(MomData* mom, int* mask, int nread);
int reorder_momdata(MomData* data, Momenta* mom_out);
int boot_reorder_momenta(Boot* data_out, Boot* data_in, Momenta*);
void boot_normalise_momenta(Boot*,Momenta*);
int boot_interpolate_mom_1d(Boot* out, int* outmask, Boot** in, 
			    Momenta* mom1, Momenta* mom2, 
			    int** mask, int index);

int copy_momdata_subset(MomData* data, MomData* subdata, 
			int startv, int starts);
int plot_momdata(MomData* data, Datafunction** xfunc, Datafunction** yfunc,
		 int nset, char* name, PlotOptions* plot);
int plot_momdata_offset(MomData* data,
		       OffsetDatafunction* xfunc, OffsetDatafunction* yfunc,
		       int nset, char* name, PlotOptions* opt);
int plot_momdata_st(MomData*,Datafunction**,int,char*,PlotOptions*);
int fit_momdata(MomData* data, BootFitFuncs* fit);
int fit_momdata_xboot(MomData* data, Boot* xboot, BootFitFuncs* fit);

int assign_params_analyse_gluon(char* filename, Gauge_parameters* gpar, 
				Momenta* mom, Analysis_params* run);
int assign_params_analyse_quark(char* filename, Propagator_parameters* ppar, 
				Momenta* mom, Analysis_params* run);
int assign_params_analyse_su2higgs(char* filename, Gauge_parameters* gpar, 
				   Higgs_parameters* hpar, 
				   Momenta* mom, Analysis_params* run);
int assign_params_analyse_su2(char* filename, Gauge_parameters* gpar, 
			      Momenta* mom, Analysis_params* run);
#endif
