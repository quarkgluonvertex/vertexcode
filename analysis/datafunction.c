#include "analysis.h"
#include <stdio.h>

int Df_x(Real* y, Real x, Real* arg, int narg)
{
  if (y) *y = x;
  return 0;
}

int Df_xsqr(Real* y, Real x, Real* arg, int narg)
{
  if (y) *y = x*x;
  return 0;
}

int Df_y1(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"Df_y1: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[1];
  return 1;
}

int Df_y2(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<3) {
    fprintf(stderr,"Df_y2: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[2];
  return 1;
}

int Df_y3(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<4) {
    fprintf(stderr,"Df_y3: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[3];
  return 1;
}

int Df_y4(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<5) {
    fprintf(stderr,"Df_y4: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[4];
  return 1;
}

int Df_y5(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<6) {
    fprintf(stderr,"Df_y5: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[5];
  return 1;
}

int Df_y6(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<7) {
    fprintf(stderr,"Df_y5: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[6];
  return 1;
}

int Df_y7(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<8) {
    fprintf(stderr,"Df_y5: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = arg[7];
  return 1;
}

int Df_ratio(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"Df_ratio: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = (arg[1]!=0.0) ? arg[0]/arg[1] : UNSET_REAL;
  return 1;
}

int Df_invratio(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"Df_ratio: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = (arg[0]!=0.0) ? arg[1]/arg[0] : UNSET_REAL;
  return 1;
}

/* Df_meff is the usual "effective mass" function, assuming *
 *  C(t) -> arg[0] and C(t+1) -> arg[1]                     */
int Df_meff(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"Df_ratio: illegal narg %d\n",narg);
    return EVIL;
  }
  if (y) *y = (arg[0]>0 && arg[1]>0) ? log(arg[0]/arg[1]) : UNSET_REAL;;
  return 1;
}

void datafunc_cov_ave(VectorData* this, Datafunction* func, 
		      double* ave, double** cov, int* cmask, int* xmask)
{
/* assume cov and ave are already allocated...? */
  const int len=ntrue(xmask,this->length);
  int i,j,ncf;
  double** jack_ave;
  double dummy;
  int* msk = cmask ? cmask : malloc(sizeof(int)*this->nconfig);

  if (func && !func(0,0,0,0)) { /* func does not depend on config data */
    for (i=j=0;i<this->length;i++) if (MASK(xmask,i)) {
      func(&ave[j],this->xdata[j],0,0);
      j++;
    }
    for (i=0;i<len;i++) for (j=0;j<len;j++) cov[i][j]=0.0;
    return;
  }
/* find the number of configs */
  ncf = ntrue(cmask,this->nconfig);
  if (!cmask) for (i=0;i<ncf;i++) msk[i]=true;

  jack_ave = arralloc(sizeof(double),2,ncf,len);

/* get the central values */
  if (!func) vector_average_all(this,msk,xmask,ave,&dummy);
  else datafunc_average(this,func,ave,msk,xmask);

/* get the jackknifed averages */
  for (i=0,j=0; i<this->nconfig; i++) {
    if (msk[i]) {
      msk[i]=false;
      if (!func) vector_average_all(this,msk,xmask,jack_ave[j],&dummy);
      else datafunc_average(this,func,jack_ave[j],msk,xmask);
      msk[i]=true;
      j++;
    }
  }
/* now we have to get the jackknife covariance... */
  for(i=0; i<len; i++) {
    for(j=i; j<len; j++) {
      double cij = 0.0;
      int k;
      for(k=0; k<ncf; k++)
	cij += ((jack_ave[k][i]-ave[i])
		*(jack_ave[k][j]-ave[j]));
#if 0 // TEST
      cov[i][j] = cov[j][i] = cij;
#endif
      cov[i][j] = cov[j][i] = cij*(ncf-1)/ncf;
    }
  }
  free(jack_ave);
  if (!cmask) free(msk);
}

void offsetfunc_cov_ave(VectorData* this, OffsetDatafunction* func, 
			double* ave, double** cov, int* cmask, int* xmask)
{
/* assume cov and ave are already allocated...? */
  const int len=ntrue(xmask,this->length);
  const int nv = func ? func->voffset : 0;
  const int ns = func ? func->soffset : 0;
  Datafunction* fn = func ? func->func : 0;
  int i,j,ncf;
  double** jack_ave;
  int* msk = cmask ? cmask : malloc(sizeof(int)*this->nconfig);

  if (fn && !fn(0,0,0,0)) { /* func does not depend on config data */
    for (i=j=0;i<this->length;i++) if (MASK(xmask,i)) {
      fn(&ave[j],this->xdata[j],0,0);
      j++;
    }
    for (i=0;i<len;i++) for (j=0;j<len;j++) cov[i][j]=0.0;
    return;
  }
/* find the number of configs */
  ncf = ntrue(cmask,this->nconfig);
  if (!cmask) for (i=0;i<ncf;i++) msk[i]=true;

  jack_ave = arralloc(sizeof(double),2,ncf,len);

/* get the central values */
  if (!fn) vector_average_n(this,nv,msk,xmask,ave);
  else datafunc_average_offset(this,fn,nv,ns,ave,msk,xmask);

/* get the jackknifed averages */
  for (i=0,j=0; i<this->nconfig; i++) {
    if (msk[i]) {
      msk[i]=false;
      if (!fn) vector_average_n(this,nv,msk,xmask,jack_ave[j]);
      else datafunc_average_offset(this,fn,nv,ns,jack_ave[j],msk,xmask);
      msk[i]=true;
      j++;
    }
  }
/* now we have to get the jackknife covariance... */
  for(i=0; i<len; i++) {
    for(j=i; j<len; j++) {
      double cij = 0.0;
      int k;
      for(k=0; k<ncf; k++)
	cij += ((jack_ave[k][i]-ave[i])
		*(jack_ave[k][j]-ave[j]));
#if 0 // TEST
      cov[i][j] = cov[j][i] = cij;
#endif
      cov[i][j] = cov[j][i] = cij*(ncf-1)/ncf;
    }
  }
  free(jack_ave);
  if (!cmask) free(msk);
}

void datafunc_average(VectorData* this, Datafunction* func, double* average,
		      int* cmask, int* xmask)
{
  datafunc_average_offset(this,func,0,0,average,cmask,xmask);
}

void datafunc_average_offset(VectorData* this, Datafunction* func, 
			     int voff, int soff, double* average,
			     int* cmask, int* xmask)
{
  const int len  = ntrue(xmask,this->length);
  const int nvec = this->nvector + this->nvparm - voff;
  const int nsca = this->nscalar + this->nsparm - soff;
  const int nbundle = nvec+nsca;
  int bundle,i,j;
  double** vec_ave = arralloc(sizeof(double),2,nvec+voff,len);
  double* scalar_ave = vecalloc(sizeof(double),nsca+soff);
  double* data = malloc(sizeof(double)*nbundle);

  if (!func) {
    fprintf(stderr,"datafunc_ave: called with null function argument\n");
    exit(-1);
  }
  if (!func(0,0,0,0)) { /* func does not depend on config data */
    for (i=j=0;i<this->length;i++) if (MASK(xmask,i)) {
      func(&average[j],this->xdata[j],0,0);
      j++;
    }
    return;
  }

  vector_average(this,cmask,xmask,vec_ave,scalar_ave);

  for (bundle=nvec; bundle<nbundle; bundle++) {
    data[bundle] = scalar_ave[bundle+soff-nvec];
  }
  for (i=0;i<len;i++) {
    for (bundle=0; bundle<nvec; bundle++)
      data[bundle] = vec_ave[bundle+voff][i];

    (*func)(&(average[i]),this->xdata[i],data,nbundle);
  }

  free(vec_ave);
  free(scalar_ave);
  free(data);
}
