/*	$Id: solve_matrices.c,v 1.1 2003/03/05 10:04:26 jskuller Exp $	*/
/************************************************************************
 *                                                                      *
 * The following are a set of routines for calling a generic matrix     *
 * inversion program (svd_inverse) taking double-type arguments.        *
 *                                                                      *
 * get_inverse calls the inversion routine for inverting a              *
 * rank x rank matrix.                                                  *
 * solve_delta_param finds the solution for the matrix equation         *
 *       alpha' x delta_param = beta                                    *
 *                                                                      *
 * The nomenclature reflects the routines origin among the              *
 * Marquardt-Levenberg chi-squared fit routines, but they may be used   *
 * as generic interface routines for smallish matrices.                 *
 *                                                                      *
 ************************************************************************/

#include "analysis.h"
#include <matrix_inverse.h>

void get_inverse(Real **alpha, int rank, Real **alpha_inv)
{
  if (sizeof(Real) == sizeof(double)) {
    svd_inverse(rank, alpha, alpha_inv, 1);
  } else {
/* Type conflict */
    double** a = arralloc(sizeof(double),2,rank,rank);
    double** ainv = arralloc(sizeof(double),2,rank,rank);
    int i, j;
    for (i=0;i<rank;i++) for (j=0;j<rank;j++) a[i][j]=alpha[i][j];
    svd_inverse(rank,a,ainv,1);
    for (i=0;i<rank;i++) for (j=0;j<rank;j++) alpha_inv[i][j]=ainv[i][j];
    free(a);
    free(ainv);
  }
}

void solve_delta_param(Real **alpha_prime, Real *beta, int rank,
		       Real *delta_param )
{
  int i,j;
  double **alpha_prime_inv = arralloc(sizeof(double),2,rank,rank);

  /*** Invert alpha' ***/
  if (sizeof(Real) == sizeof(double)) {
    svd_inverse(rank,alpha_prime,alpha_prime_inv,1);
  } else {
/* Type conflict. Fill tmp array with copy of alpha_prime */
    double **alpha = (double **)arralloc(sizeof(double),2,rank,rank);
    Real* pos = *alpha_prime+rank*rank;
    double* apos = *alpha+rank*rank;
    while (pos>*alpha_prime) *--apos=*--pos;
    svd_inverse(rank,alpha,alpha_prime_inv,1);
  }


  /*** delta_param = alpha'^(-1) x beta ***/
  for (i=0; i<rank; i++) {
    delta_param[i] = 0.0;
    for (j=0; j<rank; j++) {
      delta_param[i] += alpha_prime_inv[i][j]*beta[j];
    }
  }

  free(alpha_prime_inv);
}

