/* $Id: get_chisq.c,v 1.1 2003/03/05 10:24:49 jskuller Exp $	*/
/************************************************************************
 *                                                                      *
 * Routines for evaulating chi^2 for a set of data and a model fit      *
 * Adapted from get_chisq.c,v1.9 and get_norm_diff.c,v1.7               *
 * of the Edinburgh (HHHH) code, written mostly by hps.                 *
 *                                                                      *
 ************************************************************************/

#include "analysis.h"

/************************************************************************
 *                                                                      *
 * get_chisq determines chi^2 for a set of data, given the raw data,    *
 *  their correlation and standard deviations and a proposed model fit  *
 *                                                                      *
 ************************************************************************/
Real get_chisq(FitData* fit)
{
  const char fnm[MAXNAME] = "get_chisq";
  const int ndata = fit->ndata;
  Real chisq = 0.0 ;
  int i,j;
  /* difference between data and fit */
  Real *delta = vecalloc(sizeof(Real),ndata);
  Real** dyda = fit->dyda;
  Real*** d2yda2 = fit->d2yda2;

/***  Find difference for all data, normalised by their standard deviation  ***/
  fit->dyda=0; fit->d2yda2=0;
  get_norm_diff(fit, delta);
  fit->dyda=dyda; fit->d2yda2=d2yda2;

/***  Loop over data space  ***/
  for (i=0; i<ndata; i++) {
    chisq += delta[i]*delta[i]*fit->inv_data_corr[i][i];
    for (j=i+1; j<ndata; j++) {
      chisq += 2.0*delta[i]*delta[j]*fit->inv_data_corr[i][j];
    }
  }

  free(delta);
  fit->chisq = chisq;
  return chisq;
} 
	  
/*
 *	The following routine gets the difference between a set of data
 *	and a modelled fit. It also finds the first and second derivatives
 *	of the fit w.r.t. to its parameters.
 * 	All of the above are normalised by the standard deviations of that
 *	particular datum.
 */
 
void get_norm_diff(FitData* fit, Real* delta)
{
  const int ndata  = fit->ndata;
  const int nparam = fit->nparam;
  int i,j,k;
	
  Real yfit;

  if (fit->sigmax) {
    Real* sigma = vecalloc(sizeof(Real),ndata);
    Real  dydx;
    if ( fit->dyda && fit->d2yda2 ) {
      for (i=0; i<ndata; i++) {
	fit->model(fit->xdata[i], fit->param, &yfit, 
		   fit->dyda[i],fit->d2yda2[i],&dydx,nparam);
	sigma[i] = sqrt(fit->sigmay[i]*fit->sigmay[i]
			+ dydx*dydx+fit->sigmax[i]*fit->sigmax[i]);

	delta[i] = (fit->ydata[i] - yfit)/sigma[i] ;

#ifdef DEBUG
	printf("x[%d] = %f, ydata[%d] = %e, yfit = %e \n",
	       i,fit->xdata[i],i,fit->ydata[i],yfit);
	printf("delta = %e, sigma = %e \n",delta[i],sigma[i]);
#endif	
      }
	
      /*** Normalise derivatives ***/
      for (i=0; i<ndata; i++) { 	
	for (j=0; j<nparam; j++) {
	  fit->dyda[i][j] /= sigma[i];
	  for (k = 0; k < nparam; k++) {
	    fit->d2yda2[i][j][k] /= sigma[i];
	  }
	}
      }
    } else {
      for (i=0; i<ndata; i++) {
	fit->model(fit->xdata[i], fit->param, &yfit, 0, 0, &dydx, nparam);
	sigma[i] = sqrt(fit->sigmay[i]*fit->sigmay[i]
			   + dydx*dydx+fit->sigmax[i]*fit->sigmax[i]);

	delta[i] = (fit->ydata[i] - yfit)/sigma[i] ;
#ifdef DEBUG
	printf("x[%d] = %e, ydata[%d] = %e, yfit = %e \n",
	       i,fit->xdata[i],i,fit->ydata[i],yfit);
	printf("delta = %e, sigma = %e \n",delta[i],sigma[i]);
#endif	
		
      }
    }
    free(sigma);
  } else {
    if ( fit->dyda && fit->d2yda2 ) {
      for (i=0; i<ndata; i++) {
	fit->model(fit->xdata[i], fit->param, &yfit, 
		       fit->dyda[i],fit->d2yda2[i],0,nparam);
			
	delta[i] = (fit->ydata[i] - yfit)/fit->sigmay[i] ;
#ifdef DEBUG
	printf("x[%d] = %f, ydata[%d] = %e, yfit = %e \n",
	       i,fit->xdata[i],i,fit->ydata[i],yfit);
	printf("delta = %e, sigma = %e \n",delta[i],fit->sigmay[i]);
#endif	
      }
	
      /*** Normalise derivatives ***/
      for (i=0; i<ndata; i++) { 	
	for (j = 0; j < nparam; j++) {
	  fit->dyda[i][j] /= fit->sigmay[i];
	  for (k = 0; k < nparam; k++) {
	    fit->d2yda2[i][j][k] /= fit->sigmay[i];
	  }
	}
      }
    } else {
      for (i=0; i<ndata; i++) {
	fit->model(fit->xdata[i], fit->param, &yfit, 0, 0, 0, nparam);
	delta[i] = (fit->ydata[i] - yfit)/fit->sigmay[i] ;
#ifdef DEBUG
	printf("x[%d] = %e, ydata[%d] = %e, yfit = %e \n",
	       i,fit->xdata[i],i,fit->ydata[i],yfit);
	printf("delta = %e, sigma = %e \n",delta[i],fit->sigmay[i]);
#endif	
		
      }
    }
  }
}	
