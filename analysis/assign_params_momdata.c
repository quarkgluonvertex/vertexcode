/* $Id: assign_params_momdata.c,v 1.19 2015/06/17 13:43:32 jonivar Exp $ */

#include "analysis.h"
#include "field_params.h"
#include "momenta.h"
#include "vertex.h"
#include <producers.h>
#include <parm.h>
#include <scanvec.h>

#define HOT_VALUE 2.0
#define MAXACT 4
#define MAXGAU 6
#define MAXPRO 5
#define MAXSMR 4
#define MAXKIN 6
#define MAXNPLOT   20
#define MAXNFIT    20

static PaCode action[MAXACT]
       = {"WILSON","CLOVER","STAGGERED","GAUGEHIGGS"};
static PaCode gauge[MAXGAU]
       = {"RANDOM","COULOMB","LANDAU","COVARIANT","AXIAL","UNITARY"};
static PaCode smearing[MAXSMR] = {"NONE","BINSIZE","NBIN","NINBIN"};
static PaCode kinemat[MAXKIN]  = {"GENERIC","ZEROMOM","REFLECT","ORTHOGONAL","PKSYM","SKEWED"};


/********************************************************************
 *  assign_params_analyse_gluon: assigns all the parameters         *
 *   required for statistical analysis of the gluon propagator      *
 ********************************************************************/

int assign_params_analyse_gluon(char* filename, Gauge_parameters* gpar, 
				Momenta* mom, Analysis_params* run)
{
  const char fnm[MAXNAME] = "assign_params_analyse_gluon";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gact[MAXLINE]  = "WILSON";
  char fact[MAXLINE]  = "WILSON";
  double mu=0.0, j=0.0;
  double mass[4] = {0.0,0.0,0.0,0.0};
  char gau[MAXLINE] = "LANDAU";
  char smr[MAXLINE] = "NONE";
  char graph[MAXLINE] = "";
  char fitstr[MAXLINE] = "";
  char xmaxstr[MAXLINE] = "";
  char xminstr[MAXLINE] = "";
  int plot[MAXNPLOT];
  int ngraph;
  int fit[MAXNFIT];
  int nfit;

  int ismear = 0;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat) {
    printf("%s: could not alloc space for lattice!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=8;
  gpar->lattice = lat;

  gpar->action = WILSON;
  gpar->nf     = 0;
  gpar->beta   = 6.0;
  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = PERIODIC;
  gpar->gauge = LANDAU;
  gpar->gauge_param = 0.0;
  gpar->id = 0;

  set_default_momenta(mom);

  mom->nvar = 2;
  mom->sortvar = MOMQ;
  mom->sort = FALSE;

  run->nconfig = 0;
  run->ncorr = 1;
  run->plot = 0;
  run->fit  = 0;
  run->correlated_fit = false;
  run->nboot = 0;
  run->x_fit_min = 0;
  run->x_fit_max = 0;
  run->write_boot = false;
  memset(run->data_dir,0,MAXLINE);
  run->data_dir[0] = '.';
  memset(run->plotopt_file,0,MAXLINE);

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,MUSTSET,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&mom->nmom_in[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&mom->nmom_in[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&mom->nmom_in[IZ]);
  add_parm(list,MUSTSET,INTEGER,"t_momenta",&mom->nmom_in[IT]);
  add_parm(list,DEFAULT,STRING,"gauge_action",gact);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,DEFAULT,INTEGER,"nflavor",&gpar->nf);
  add_parm(list,DEFAULT,STRING,"fermion_action",fact);
  add_parm(list,DEFAULT,DOUBLE,"chempot",&mu);
  add_parm(list,DEFAULT,DOUBLE,"diquark_src",&j);
  add_parm(list,DEFAULT,DOUBLE,"mass1",&mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"mass2",&mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"mass3",&mass[2]);
  add_parm(list,DEFAULT,DOUBLE,"mass4",&mass[3]);
  add_parm(list,DEFAULT,STRING,"gauge",gau);
  add_parm(list,DEFAULT,DOUBLE,"gauge_param",&gpar->gauge_param);
  add_parm(list,DEFAULT,INTEGER,"id", &gpar->id);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smr);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);
  add_parm(list,MUSTSET,INTEGER,"nconfig", &run->nconfig);
  add_parm(list,MUSTSET,STRING,"plot",graph);
  add_parm(list,DEFAULT,BOOL,"single_plot",&run->single_plot);
  add_parm(list,MUSTSET,STRING,"fit",fitstr);
  add_parm(list,DEFAULT,BOOL,"correlated_fit",&run->correlated_fit);
  add_parm(list,DEFAULT,INTEGER,"nboot",&run->nboot);
  add_parm(list,DEFAULT,BOOL,"write_boot",&run->write_boot);
  add_parm(list,DEFAULT,STRING,"x_fit_min",xminstr);
  add_parm(list,DEFAULT,STRING,"x_fit_max",xmaxstr);
  add_parm(list,MUSTSET,STRING,"data_directory",run->data_dir);
  add_parm(list,DEFAULT,STRING,"plotopt_file",run->plotopt_file);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Determine the plot and fit numbers  ***/
  ngraph = sscanveci(graph,plot,MAXNPLOT);
  run->nplot = ngraph;
  if (ngraph) run->plot  = malloc(sizeof(int)*ngraph);
  for (i=0;i<ngraph;i++) run->plot[i] = plot[i];

  nfit = sscanveci(fitstr,fit,MAXNFIT);
  run->nfit = nfit;
  if (nfit) run->fit  = malloc(sizeof(int)*nfit);
  for (i=0;i<nfit;i++) run->fit[i] = fit[i];

  if (strlen(xminstr)) {
    run->x_fit_min = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_min[i]=-DBL_MAX;
    if (i=sscanvecd(xminstr,run->x_fit_min,nfit) != nfit) {
      printf("%s: Warning: nfitmin does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }
  if (strlen(xmaxstr)) {
    run->x_fit_max = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_max[i]=DBL_MAX;
    if (i=sscanvecd(xmaxstr,run->x_fit_max,nfit) != nfit) {
      printf("%s: Warning: nfitmax does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(gpar->lattice);
  gpar->lattice->has_ntab = FALSE;

/*** Determine gauge parameters and set remaining to default values ***/
  status += parm_from_string(&gpar->action,gact,action,MAXACT,"gauge_action");
  status += parm_from_string(&gpar->gauge,gau,gauge,MAXGAU,"gauge");
  if (gpar->nf) {
    if (mu==0.0 && j==0.0) {
      gpar->mass = malloc(sizeof(double)*gpar->nf);
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
    } else {
      gpar->mass = malloc(sizeof(double)*(gpar->nf+2));
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
      gpar->mass[gpar->nf] = mu;
      gpar->mass[gpar->nf+1] = j;
      gpar->id = 1;
    }
  }

  gpar->sweep = 0;
  gpar->time  = 0;
  gpar->start = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace    = 0.0;                               /* read this too? */
  for (i=0; i<MAXPREC; i++) gpar->gfix_precision[i] = 0.0;  /* and this? */
  gpar->gfix_iterations = 0;
  gpar->producer_id     = PROD_INTERNAL;
  gpar->checksum        = 0;
  memset(gpar->info,0,MAXLINE);

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_out[i] = mom->nmom_in[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = gpar->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smr,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->key = 0;
  mom->nval = 0;
  mom->initialised = FALSE;

/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}

int assign_params_analyse_quark(char* filename, Propagator_parameters* ppar, 
				Momenta* mom, Analysis_params* run)
{
  const char fnm[MAXNAME] = "assign_params_analyse_quark";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;
  Gauge_parameters* gpar = malloc(sizeof(Gauge_parameters));
  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gact[MAXLINE]  = "WILSON";
  char fact[MAXLINE]  = "WILSON";
  double kappac=0.0, mbare=0.0;
  double csw=0.0, rotation=0.0, bm=0.0, lambda=0.0;
  double mu=0.0, j=0.0;
  double mass[4] = {0.0,0.0,0.0,0.0};
  char gau[MAXLINE] = "LANDAU";
  char smr[MAXLINE] = "NONE";
  char graph[MAXLINE] = "";
  char fitstr[MAXLINE] = "";
  char xmaxstr[MAXLINE] = "";
  char xminstr[MAXLINE] = "";
  int plot[MAXNPLOT];
  int ngraph;
  int fit[MAXNFIT];
  int nfit;

  int ismear = 0;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat || !gpar) {
    printf("%s: could not alloc space for gauge and lattice params!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=8;
  gpar->lattice = lat;

  gpar->action = WILSON;
  gpar->nf     = 0;
  gpar->beta   = 6.0;
  gpar->has_chempot = FALSE;
  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = PERIODIC;
  gpar->gauge = LANDAU;
  gpar->gauge_param = 0.0;

  ppar->gpar = gpar;
  ppar->bcs[IX] = ppar->bcs[IY] = ppar->bcs[IZ] = PERIODIC;
  ppar->bcs[IT] = ANTIPERIODIC;
  ppar->kappa= 0.1234;
  ppar->chempot = 0.0;
  ppar->j = ppar->jbar = 0.0;
  ppar->ncoeff = 0;
  ppar->imp = 0;
  ppar->source_type = POINT;
  ppar->source_address[0] = ppar->source_address[1]
    = ppar->source_address[2] = ppar->source_address[3] = 0;
  ppar->producer_id = PROD_INTERNAL;
  ppar->id = 0;

  set_default_momenta(mom);

  mom->nvar = 3;
  mom->sortvar = MOMK;
  mom->sort = FALSE;

  run->nconfig = 0;
  run->ncorr = 2;   /* 2 correlators: scalar and vector - change if needed */
  run->plot = 0;
  run->fit  = 0;
  run->correlated_fit = false;
  run->nboot = 0;
  run->x_fit_min = 0;
  run->x_fit_max = 0;
  run->write_boot = false;
  memset(run->data_dir,0,MAXLINE);
  run->data_dir[0] = '.';
  memset(run->plotopt_file,0,MAXLINE);

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,MUSTSET,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&mom->nmom_in[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&mom->nmom_in[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&mom->nmom_in[IZ]);
  add_parm(list,MUSTSET,INTEGER,"t_momenta",&mom->nmom_in[IT]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_x",&ppar->bcs[IX]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_y",&ppar->bcs[IY]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_z",&ppar->bcs[IZ]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_t",&ppar->bcs[IT]);
  add_parm(list,DEFAULT,STRING,"gauge_action",gact);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,DEFAULT,INTEGER,"nflavor",&gpar->nf);
  add_parm(list,DEFAULT,DOUBLE,"mass1",&mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"mass2",&mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"mass3",&mass[2]);
  add_parm(list,DEFAULT,DOUBLE,"mass4",&mass[3]);
  add_parm(list,DEFAULT,DOUBLE,"gauge_chempot",&mu);
  add_parm(list,DEFAULT,DOUBLE,"gauge_diq_src",&j);
  add_parm(list,DEFAULT,STRING,"gauge",gau);
  add_parm(list,DEFAULT,DOUBLE,"gauge_param",&gpar->gauge_param);
  add_parm(list,MUSTSET,STRING,"fermion_action",fact);
  add_parm(list,MUSTSET,DOUBLE,"kappa",&ppar->kappa);
  add_parm(list,DEFAULT,DOUBLE,"csw",&csw);
  add_parm(list,DEFAULT,DOUBLE,"kappa_c",&kappac);
  add_parm(list,DEFAULT,DOUBLE,"bare_mass",&mbare);
  add_parm(list,DEFAULT,DOUBLE,"rotation",&rotation);
  add_parm(list,DEFAULT,DOUBLE,"bm",&bm);
  add_parm(list,DEFAULT,DOUBLE,"additive_imp",&lambda);
  add_parm(list,DEFAULT,DOUBLE,"chempot",&ppar->chempot);
  add_parm(list,DEFAULT,DOUBLE,"diquark_source",&ppar->j);
  add_parm(list,DEFAULT,DOUBLE,"antidiquark_source",&ppar->jbar);
  add_parm(list,DEFAULT,INTEGER,"id", &gpar->id);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smr);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);
  add_parm(list,MUSTSET,INTEGER,"nconfig", &run->nconfig);
  add_parm(list,MUSTSET,STRING,"plot",graph);
  add_parm(list,DEFAULT,BOOL,"single_plot",&run->single_plot);
  add_parm(list,MUSTSET,STRING,"fit",fitstr);
  add_parm(list,DEFAULT,BOOL,"correlated_fit",&run->correlated_fit);
  add_parm(list,DEFAULT,INTEGER,"nboot",&run->nboot);
  add_parm(list,DEFAULT,BOOL,"write_boot",&run->write_boot);
  add_parm(list,DEFAULT,STRING,"x_fit_min",xminstr);
  add_parm(list,DEFAULT,STRING,"x_fit_max",xmaxstr);
  add_parm(list,MUSTSET,STRING,"data_directory",run->data_dir);
  add_parm(list,DEFAULT,STRING,"plotopt_file",run->plotopt_file);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Determine the plot and fit numbers  ***/
  ngraph = sscanveci(graph,plot,MAXNPLOT);
  run->nplot = ngraph;
  if (ngraph) run->plot  = malloc(sizeof(int)*ngraph);
  for (i=0;i<ngraph;i++) run->plot[i] = plot[i];

  nfit = sscanveci(fitstr,fit,MAXNFIT);
  run->nfit = nfit;
  if (nfit) run->fit  = malloc(sizeof(int)*nfit);
  for (i=0;i<nfit;i++) run->fit[i] = fit[i];

  if (strlen(xminstr)) {
    run->x_fit_min = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_min[i]=-DBL_MAX;
    if (i=sscanvecd(xminstr,run->x_fit_min,nfit) != nfit) {
      printf("%s: Warning: nfitmin does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }
  if (strlen(xmaxstr)) {
    run->x_fit_max = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_max[i]=DBL_MAX;
    if (i=sscanvecd(xmaxstr,run->x_fit_max,nfit) != nfit) {
      printf("%s: Warning: nfitmax does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(gpar->lattice);
  gpar->lattice->has_ntab = FALSE;

/*** Determine gauge parameters and set remaining to default values ***/
  status += parm_from_string(&gpar->action,gact,action,MAXACT,"gauge_action");
  status += parm_from_string(&gpar->gauge,gau,gauge,MAXGAU,"gauge");
  if (gpar->nf) {
    if (mu==0.0 && j==0.0) {
      gpar->mass = malloc(sizeof(double)*gpar->nf);
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
    } else {
      gpar->has_chempot = TRUE;
      gpar->mass = malloc(sizeof(double)*(gpar->nf+2));
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
      gpar->mass[gpar->nf] = mu;
      gpar->mass[gpar->nf+1] = j;
      gpar->id = 1;
    }
  }

  gpar->sweep = 0;
  gpar->time  = 0;
  gpar->start = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace    = 0.0;                               /* read this too? */
  for (i=0; i<MAXPREC; i++) gpar->gfix_precision[i] = 0.0;  /* and this? */
  gpar->gfix_iterations = 0;
  gpar->producer_id     = PROD_INTERNAL;
  gpar->checksum        = 0;

/*** determine fermion parameters ***/
  status += parm_from_string(&ppar->action,fact,action,MAXACT,"fermion_action");

/***  Determine bare mass or kappa_critical if known  ***/
  if (ppar->action==CLOVER && kappac==0.0 && mbare==0.0) {
    fprintf(stderr,"%s: bare_mass or kappa_c should be set for clover improvement\n",fnm);
    fprintf(stderr,"%s: setting kappa_c to default value 0.125\n",fnm);
    kappac=0.125;
  }
  if (kappac!=0.0) {
    if (mbare!=0.0) {
      fprintf(stderr,"%s: conflict - both kappa_c and mbare are set\n",fnm);
      fprintf(stderr,"%s: ignoring kappa_c value\n",fnm);
    } else if (ppar->action==CLOVER || ppar->action==WILSON) {
	ppar->kappa_c = kappac;
	ppar->mass = 0.5/ppar->kappa - 0.5/kappac;
    } else {
      fprintf(stderr,"%s: ignoring kappa_c for non-Wilson type action\n",fnm);
    }      
  }
  if (mbare!=0.0) {
    ppar->mass=mbare;
    if (ppar->action==CLOVER || ppar->action==WILSON) {
      ppar->kappa_c = 1.0/(1.0/ppar->kappa-2*mbare);
    }
  }

/***  Assign improvement coefficients ***/
  if (ppar->action==CLOVER) {
    if (rotation!=0.0) ppar->source_type = ROTATED;
    if (bm!=0.0 || lambda!=0.0) {
      ppar->ncoeff = 4;
      ppar->imp = malloc(sizeof(double)*4);
      ppar->imp[0] = csw;
      ppar->imp[1] = rotation;
      ppar->imp[2] = bm;
      ppar->imp[3] = lambda;
    } else {
      ppar->ncoeff = 2;
      ppar->imp = malloc(sizeof(double)*2);
      ppar->imp[0] = csw;
      ppar->imp[1] = rotation;
    }
  }

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_out[i] = mom->nmom_in[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = ppar->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smr,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;
/*** we need at least 5 momentum variables if lorentz symmetry broken ***/
  if (ppar->chempot != 0 && mom->nvar<5) mom->nvar = 5;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->key = 0;
  mom->nval = 0;
  mom->initialised = FALSE;

/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}

int assign_params_analyse_vertex(char* filename, Vertex_params* run, 
				 Momenta* mom)
{
  const char fnm[MAXNAME] = "assign_params_analyse_vertex";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;
  Gauge_parameters* gpar = malloc(sizeof(Gauge_parameters));
  Propagator_parameters* ppar = malloc(sizeof(Gauge_parameters));
  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gact[MAXLINE]  = "WILSON";
  char fact[MAXLINE]  = "WILSON";
  char gau[MAXLINE] = "LANDAU";
  char kin[MAXLINE] = "GENERIC";
  double kappac=0.0, mbare=0.0;
  double csw=0.0, rotation=0.0, bm=0.0, lambda=0.0;
  double mass[4] = {0.0,0.0,0.0,0.0};
  double mu=0.0, j=0.0;
  char smr[MAXLINE] = "NONE";

  int ismear = 0;
  int i;
  int result;			/* "exit code" */
  int status = 0;


  /* set pointers and lattice defaults */
  if (!lat || !gpar || !ppar) {
    printf("%s: could not alloc space for param structures!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=8;
  gpar->lattice = lat;
  run->gauge = ppar->gpar = gpar;
  run->prop  = ppar;

  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = PERIODIC;
  ppar->bcs[IX] = ppar->bcs[IY] = ppar->bcs[IZ] = PERIODIC;
  ppar->bcs[IT] = ANTIPERIODIC;

  gpar->action = WILSON;
  gpar->nf     = 0;
  gpar->beta   = 6.0;
  gpar->has_chempot = FALSE;
  gpar->gauge = LANDAU;
  gpar->gauge_param = 0.0;
  gpar->sweep  = 0;
  gpar->producer_id = UNKNOWN;
  gpar->id = 0;
  memset(gpar->info,0,MAXLINE);

  ppar->gpar = gpar;
  ppar->kappa = 0.1234;
  ppar->chempot = 0.0;
  ppar->j = ppar->jbar = 0.0;
  ppar->mass     = 0.0;
  ppar->ncoeff   = 0;
  ppar->imp      = 0;
  ppar->source_type = 0;
  ppar->id = 0;

  run->nmom[IX] = run->nmom[IY] = run->nmom[IZ] = run->nmom[IT] = 0;
  run->p[IX] = run->p[IY] = run->p[IZ] = run->p[IT] = UNSET_INT;
  run->q[IX] = run->q[IY] = run->q[IZ] = run->q[IT] = 0;

  run->start_cfg = run->nconfig = run->nboot = 0;
  run->mu = run->gamma = 0;
  run->kinematics = VTX_GENERIC;
  run->covariant  = FALSE;
  run->mom_variable = 0;
  run->write_boot = TRUE;
  run->nboot = 0;
  run->fit = run->correlate = FALSE;
  run->id = 0;
  run->seed = 7654321;

  memset(run->boot_dir,0,MAXLINE);
  memset(run->graph_dir,0,MAXLINE);

  set_default_momenta(mom);

  mom->nvar = 3;
  mom->sortvar = 0;
  mom->sort = FALSE;

  run->nconfig = 0;
  run->plot = 0;
  run->fit  = 0;
  run->correlate = false;
  run->nboot = 0;
  run->write_boot = false;

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,MUSTSET,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&run->nmom[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&run->nmom[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&run->nmom[IZ]);
  add_parm(list,MUSTSET,INTEGER,"t_momenta",&run->nmom[IT]);
  add_parm(list,DEFAULT,INTEGER,"x_mom_out",&run->p[IX]);
  add_parm(list,DEFAULT,INTEGER,"y_mom_out",&run->p[IY]);
  add_parm(list,DEFAULT,INTEGER,"z_mom_out",&run->p[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_mom_out",&run->p[IT]);
  add_parm(list,MUSTSET,INTEGER,"qx",&run->q[IX]);
  add_parm(list,MUSTSET,INTEGER,"qy",&run->q[IY]);
  add_parm(list,MUSTSET,INTEGER,"qz",&run->q[IZ]);
  add_parm(list,MUSTSET,INTEGER,"qt",&run->q[IT]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_x",&ppar->bcs[IX]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_y",&ppar->bcs[IY]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_z",&ppar->bcs[IZ]);
  add_parm(list,DEFAULT,BOOL,"periodicity_in_t",&ppar->bcs[IT]);
  add_parm(list,DEFAULT,STRING,"gauge_action",gact);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,DEFAULT,INTEGER,"nflavor",&gpar->nf);
  add_parm(list,DEFAULT,DOUBLE,"mass1",&mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"mass2",&mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"mass3",&mass[2]);
  add_parm(list,DEFAULT,DOUBLE,"mass4",&mass[3]);
  add_parm(list,DEFAULT,DOUBLE,"gauge_chempot",&mu);
  add_parm(list,DEFAULT,DOUBLE,"gauge_diq_src",&j);
  add_parm(list,DEFAULT,STRING,"gauge",gau);
  add_parm(list,DEFAULT,DOUBLE,"gauge_param",&gpar->gauge_param);
  add_parm(list,MUSTSET,STRING,"fermion_action",fact);
  add_parm(list,MUSTSET,DOUBLE,"kappa",&ppar->kappa);
  add_parm(list,DEFAULT,DOUBLE,"chempot",&ppar->chempot);
  add_parm(list,DEFAULT,DOUBLE,"diquark_source",&ppar->j);
  add_parm(list,DEFAULT,DOUBLE,"antidiquark_source",&ppar->jbar);
  add_parm(list,DEFAULT,DOUBLE,"csw",&csw);
  add_parm(list,DEFAULT,DOUBLE,"kappa_c",&kappac);
  add_parm(list,DEFAULT,DOUBLE,"bare_mass",&mbare);
  add_parm(list,DEFAULT,DOUBLE,"rotation",&rotation);
  add_parm(list,DEFAULT,DOUBLE,"bm",&bm);
  add_parm(list,DEFAULT,DOUBLE,"additive_imp",&lambda);
  add_parm(list,DEFAULT,INTEGER,"source_type",&ppar->source_type);
  add_parm(list,MUSTSET,INTEGER,"formfactor",&run->formfactor);
  add_parm(list,DEFAULT,STRING,"kinematics",kin);
  add_parm(list,DEFAULT,INTEGER,"correction",&run->correction);
  add_parm(list,DEFAULT,BOOL,"covariant_extraction",&run->covariant);

  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,INTEGER,"plot_variable",&run->mom_variable);
  add_parm(list,DEFAULT,STRING,"smear_type",smr);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);

  add_parm(list,MUSTSET,INTEGER,"start_cfg",&run->start_cfg);
  add_parm(list,MUSTSET,INTEGER,"nconfig",&run->nconfig);
  add_parm(list,DEFAULT,INTEGER,"config_skip",&run->skip);
  add_parm(list,MUSTSET,INTEGER,"nboot",&run->nboot);
  add_parm(list,DEFAULT,BOOL,"write_boot",&run->write_boot);
  add_parm(list,DEFAULT,STRING,"boot_directory",run->boot_dir);
  add_parm(list,DEFAULT,BOOL,"fit",&run->fit);
  add_parm(list,DEFAULT,BOOL,"correlated_fit",&run->correlate);
  add_parm(list,DEFAULT,BOOL,"plot",&run->plot);
  add_parm(list,DEFAULT,STRING,"graph_directory",run->graph_dir);
  add_parm(list,MUSTSET,INTEGER,"seed",&run->seed);
  add_parm(list,DEFAULT,INTEGER,"id",&run->id);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(gpar->lattice);
  gpar->lattice->has_ntab = FALSE;

/*** Determine gauge parameters and set remaining to default values ***/
  status += parm_from_string(&gpar->action,gact,action,MAXACT,"gauge_action");
  status += parm_from_string(&gpar->gauge,gau,gauge,MAXGAU,"gauge");
  if (gpar->nf) {
    if (mu==0.0 && j==0.0) {
      gpar->mass = malloc(sizeof(double)*gpar->nf);
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
    } else {
      gpar->has_chempot = TRUE;
      gpar->mass = malloc(sizeof(double)*(gpar->nf+2));
      for (i=0;i<gpar->nf;i++) gpar->mass[i]=mass[i];
      gpar->mass[gpar->nf] = mu;
      gpar->mass[gpar->nf+1] = j;
      gpar->id = 1;
    }
  }

  gpar->sweep = 0;
  gpar->time  = 0;
  gpar->start = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace    = 0.0;                               /* read this too? */
  for (i=0; i<MAXPREC; i++) gpar->gfix_precision[i] = 0.0;  /* and this? */
  gpar->gfix_iterations = 0;
  gpar->producer_id     = PROD_INTERNAL;
  gpar->checksum        = 0;

/*** determine fermion parameters ***/
  status += parm_from_string(&ppar->action,fact,action,MAXACT,"fermion_action");

/***  Determine bare mass or kappa_critical if known  ***/
  if (ppar->action==CLOVER && kappac==0.0 && mbare==0.0) {
    fprintf(stderr,"%s: bare_mass or kappa_c should be set for clover improvement\n",fnm);
    fprintf(stderr,"%s: setting kappa_c to default value 0.125\n",fnm);
    kappac=0.125;
  }
  if (kappac!=0.0) {
    if (mbare!=0.0) {
      fprintf(stderr,"%s: conflict - both kappa_c and mbare are set\n",fnm);
      fprintf(stderr,"%s: ignoring kappa_c value\n",fnm);
    } else if (ppar->action==CLOVER || ppar->action==WILSON) {
	ppar->kappa_c = kappac;
	ppar->mass = 0.5/ppar->kappa - 0.5/kappac;
    } else {
      fprintf(stderr,"%s: ignoring kappa_c for non-Wilson type action\n",fnm);
    }      
  }
  if (mbare!=0.0) {
    ppar->mass=mbare;
    if (ppar->action==CLOVER || ppar->action==WILSON) {
      ppar->kappa_c = 1.0/(1.0/ppar->kappa-2*mbare);
    }
  }

/***  Assign improvement coefficients ***/
  if (ppar->action==CLOVER) {
    if (rotation!=0.0) ppar->source_type = ROTATED;
    if (bm!=0.0 || lambda!=0.0) {
      ppar->ncoeff = 4;
      ppar->imp = malloc(sizeof(double)*4);
      ppar->imp[0] = csw;
      ppar->imp[1] = rotation;
      ppar->imp[2] = bm;
      ppar->imp[3] = lambda;
    } else {
      ppar->ncoeff = 2;
      ppar->imp = malloc(sizeof(double)*2);
      ppar->imp[0] = csw;
      ppar->imp[1] = rotation;
    }
  }

/*** Scale the diquark source with kappa ***/
  ppar->j *= ppar->kappa;
  ppar->jbar *= ppar->kappa;

/***  Assign remaining (unused) parameters to defaults ***/
  ppar->kappa_scalar = 0.0;
  ppar->source_address[IX] = ppar->source_address[IY] = 0;
  ppar->source_address[IZ] = ppar->source_address[IT] = 0;
  ppar->producer_id = PROD_INTERNAL;
  ppar->id = 0;

/*** Determine kinematics ***/
  status += parm_from_string(&run->kinematics,kin,kinemat,MAXKIN,"kinematics");
  run->mu--;

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    if (run->p[i]==UNSET_INT) run->p[i]=run->nmom[i];
    mom->nmom_in[i] = run->nmom[i];
    mom->nmom_out[i] = run->p[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = ppar->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smr,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;
/*** we need at least 5 momentum variables if lorentz symmetry broken ***/
  if (ppar->chempot != 0 && mom->nvar<5) mom->nvar = 5;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->key = 0;
  mom->nval = 0;
  mom->initialised = FALSE;

/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}

/********************************************************************
 *  assign_params_analyse_su2higgs: assigns all the parameters      *
 *   required for statistical analysis of gauge and higgs fields    *
 ********************************************************************/

int assign_params_analyse_su2higgs(char* filename, Gauge_parameters* gpar, 
				   Higgs_parameters* hpar, 
				   Momenta* mom, Analysis_params* run)
{
  const char fnm[MAXNAME] = "assign_params_analyse_su2higgs";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gau[MAXLINE] = "COULOMB";
  char smr[MAXLINE] = "NONE";
  char graph[MAXLINE] = "";
  char fitstr[MAXLINE] = "";
  char xmaxstr[MAXLINE] = "";
  char xminstr[MAXLINE] = "";
  int plot[MAXNPLOT];
  int ngraph;
  int fit[MAXNFIT];
  int nfit;

  int ismear = 0;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat) {
    printf("%s: could not alloc space for lattice!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=1;
  gpar->lattice = lat;
  hpar->lattice = lat;

  mom->nmom_in[IX] = mom->nmom_in[IY] = mom->nmom_in[IZ] = 0;
  mom->nmom_in[IT] = 0;

  gpar->beta   = 9.0;
  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = PERIODIC;
  gpar->gauge = COULOMB;
  gpar->gauge_param = 0.0;
  gpar->sweep = 0;
  gpar->time  = 0;

  hpar->T      = 1.0;
  hpar->mu     = 1.0;
  hpar->lambda = 1.0;
  hpar->vev    = 0.0;
  hpar->dt     = 0.0;
  hpar->producer_id = UNKNOWN;
  hpar->id = 0;

  mom->cutoff = -1.0;
  mom->dcut = -1.0;
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = FALSE;
  mom->z4 = FALSE;
  mom->nvar = 3;
  mom->sort = FALSE;
  mom->sortvar = MOMQ;
  mom->smear = NONE;
  mom->smearing = 0.05;

  run->nconfig = 0;
  run->ncorr = 2;
  run->plot = 0;
  run->fit  = 0;
  run->correlated_fit = false;
  run->nboot = 0;
  run->x_fit_min = 0;
  run->x_fit_max = 0;
  run->write_boot = false;
  memset(run->data_dir,0,MAXLINE);
  run->data_dir[0] = '.';
  memset(run->plotopt_file,0,MAXLINE);

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,DEFAULT,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&mom->nmom_in[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&mom->nmom_in[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&mom->nmom_in[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_momenta",&mom->nmom_in[IT]);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,MUSTSET,DOUBLE,"lambda",&hpar->lambda);
  add_parm(list,MUSTSET,DOUBLE,"mu",&hpar->mu);
  add_parm(list,MUSTSET,DOUBLE,"temperature",&hpar->T);
  add_parm(list,DEFAULT,DOUBLE,"anisotropy",&hpar->dt);
  add_parm(list,MUSTSET,INTEGER,"time_step", &gpar->time);
  add_parm(list,MUSTSET,STRING,"gauge",gau);
  add_parm(list,DEFAULT,DOUBLE,"gauge_param",&gpar->gauge_param);
  add_parm(list,MUSTSET,INTEGER,"id", &gpar->id);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smr);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);
  add_parm(list,MUSTSET,INTEGER,"nconfig", &run->nconfig);
  add_parm(list,MUSTSET,STRING,"plot",graph);
  add_parm(list,DEFAULT,BOOL,"single_plot",&run->single_plot);
  add_parm(list,MUSTSET,STRING,"fit",fitstr);
  add_parm(list,DEFAULT,BOOL,"correlated_fit",&run->correlated_fit);
  add_parm(list,DEFAULT,INTEGER,"nboot",&run->nboot);
  add_parm(list,DEFAULT,BOOL,"write_boot",&run->write_boot);
  add_parm(list,DEFAULT,STRING,"x_fit_min",xminstr);
  add_parm(list,DEFAULT,STRING,"x_fit_max",xmaxstr);
  add_parm(list,MUSTSET,STRING,"data_directory",run->data_dir);
  add_parm(list,DEFAULT,STRING,"plotopt_file",run->plotopt_file);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Determine the plot and fit numbers  ***/
  ngraph = sscanveci(graph,plot,MAXNPLOT);
  run->nplot = ngraph;
  if (ngraph) run->plot  = malloc(sizeof(int)*ngraph);
  for (i=0;i<ngraph;i++) run->plot[i] = plot[i];

  nfit = sscanveci(fitstr,fit,MAXNFIT);
  run->nfit = nfit;
  if (nfit) run->fit  = malloc(sizeof(int)*nfit);
  for (i=0;i<nfit;i++) run->fit[i] = fit[i];

  if (strlen(xminstr)) {
    run->x_fit_min = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_min[i]=-DBL_MAX;
    if (i=sscanvecd(xminstr,run->x_fit_min,nfit) != nfit) {
      printf("%s: Warning: nfitmin does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }
  if (strlen(xmaxstr)) {
    run->x_fit_max = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_max[i]=DBL_MAX;
    if (i=sscanvecd(xmaxstr,run->x_fit_max,nfit) != nfit) {
      printf("%s: Warning: nfitmax does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(hpar->lattice);
  hpar->lattice->has_ntab = FALSE;

/*** Determine gauge parameters and set remaining to default values ***/
  status += parm_from_string(&gpar->gauge,gau,gauge,MAXGAU,"gauge");
  gpar->mass = malloc(sizeof(double)*4);
  gpar->mass[0] = hpar->dt;
  gpar->mass[1] = hpar->mu;
  gpar->mass[2] = hpar->lambda;
  gpar->mass[3] = hpar->T;

  gpar->action    = GAUGEHIGGS;
  gpar->start     = LOAD;
  gpar->nf        = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace    = 0.0;                               /* read this too? */
  for (i=0; i<MAXPREC; i++) gpar->gfix_precision[i] = 0.0;  /* and this? */
  gpar->gfix_iterations = 0;
  gpar->producer_id     = PROD_INTERNAL;
  gpar->checksum        = 0;

/*** copy higgs parameters and set remainder to defaults ***/
  hpar->vev = (hpar->mu < 0.0) ? sqrt(-hpar->mu/hpar->lambda) : 0.0;
  hpar->sweep = gpar->sweep;
  hpar->time  = gpar->time;
  hpar->id    = gpar->id;

  hpar->producer_id = PROD_INTERNAL;

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_out[i] = mom->nmom_in[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = gpar->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smr,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->nval = 0;
  mom->initialised = FALSE;


/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}

/********************************************************************
 *  assign_params_analyse_su2: assigns all the parameters required  *
 *    for statistical analysis of realtime pure gauge fields        *
 ********************************************************************/

int assign_params_analyse_su2(char* filename, Gauge_parameters* gpar, 
			      Momenta* mom, Analysis_params* run)
{
  const char fnm[MAXNAME] = "assign_params_analyse_su2";
  Parameter_def dummy = new_parm();
  Parameter_def* list = &dummy;

  Lattice* lat = malloc(sizeof(Lattice));

/***  We begin by listing the various parameters to be read in,  ***
 ***  and assigning their default values                         ***/

  char gau[MAXLINE] = "COULOMB";
  char smr[MAXLINE] = "NONE";
  char graph[MAXLINE] = "";
  char fitstr[MAXLINE] = "";
  char xmaxstr[MAXLINE] = "";
  char xminstr[MAXLINE] = "";
  int plot[MAXNPLOT];
  int ngraph;
  int fit[MAXNFIT];
  int nfit;

  int ismear = 0;
  int i;
  int result;			/* "exit code" */
  int status = 0;

  if (!lat) {
    printf("%s: could not alloc space for lattice!\n",fnm);
    return NOSPACE;
  }
  lat->length[IX]=4;  lat->length[IY]=4;  
  lat->length[IZ]=4;  lat->length[IT]=1;
  gpar->lattice = lat;

  mom->nmom_in[IX] = mom->nmom_in[IY] = mom->nmom_in[IZ] = 0;
  mom->nmom_in[IT] = 0;

  gpar->beta   = 9.0;
  gpar->bcs[IX] = gpar->bcs[IY] = gpar->bcs[IZ] = gpar->bcs[IT] = PERIODIC;
  gpar->gauge = COULOMB;
  gpar->gauge_param = 0.0;
  gpar->sweep = 0;
  gpar->time  = 0;
  gpar->mass = malloc(sizeof(double)*3);
  gpar->mass[0] = 0.0;
  gpar->mass[1] = 1.0;
  gpar->mass[2] = 0.0;

  mom->cutoff = -1.0;
  mom->dcut = -1.0;
  mom->scale = 1.0;
  mom->z3 = TRUE;
  mom->o3 = FALSE;
  mom->z4 = FALSE;
  mom->nvar = 3;
  mom->sort = FALSE;
  mom->sortvar = MOMQ;
  mom->smear = NONE;
  mom->smearing = 0.05;

  run->nconfig = 0;
  run->ncorr = 1;
  run->plot = 0;
  run->fit  = 0;
  run->correlated_fit = false;
  run->nboot = 0;
  run->x_fit_min = 0;
  run->x_fit_max = 0;
  run->write_boot = false;
  memset(run->data_dir,0,MAXLINE);
  run->data_dir[0] = '.';
  memset(run->plotopt_file,0,MAXLINE);

/***  Specify the list of parameters to be passed to the parsing routine ***/
/* MUSTSET etc,   type,  "identifier",  variable name */
    
  add_parm(list,MUSTSET,INTEGER,"x_size",&(lat->length[IX]));
  add_parm(list,MUSTSET,INTEGER,"y_size",&(lat->length[IY]));
  add_parm(list,MUSTSET,INTEGER,"z_size",&(lat->length[IZ]));
  add_parm(list,DEFAULT,INTEGER,"t_size",&(lat->length[IT]));
  add_parm(list,MUSTSET,INTEGER,"x_momenta",&mom->nmom_in[IX]);
  add_parm(list,MUSTSET,INTEGER,"y_momenta",&mom->nmom_in[IY]);
  add_parm(list,MUSTSET,INTEGER,"z_momenta",&mom->nmom_in[IZ]);
  add_parm(list,DEFAULT,INTEGER,"t_momenta",&mom->nmom_in[IT]);
  add_parm(list,MUSTSET,DOUBLE,"beta",&gpar->beta);
  add_parm(list,DEFAULT,DOUBLE,"gluon_mass",&gpar->mass[0]);
  add_parm(list,DEFAULT,DOUBLE,"anisotropy",&gpar->mass[1]);
  add_parm(list,DEFAULT,DOUBLE,"temperature",&gpar->mass[2]);
  add_parm(list,MUSTSET,INTEGER,"time_step", &gpar->time);
  add_parm(list,MUSTSET,STRING,"gauge",gau);
  add_parm(list,DEFAULT,DOUBLE,"gauge_param",&gpar->gauge_param);
  add_parm(list,MUSTSET,INTEGER,"id", &gpar->id);
  add_parm(list,DEFAULT,DOUBLE,"cutoff",&mom->cutoff);
  add_parm(list,DEFAULT,DOUBLE,"dcut",&mom->dcut);
  add_parm(list,DEFAULT,DOUBLE,"scale",&mom->scale);
  add_parm(list,DEFAULT,BOOL,"o3_symmetrise",&mom->o3);
  add_parm(list,DEFAULT,INTEGER,"momentum_variables",&mom->nvar);
  add_parm(list,DEFAULT,BOOL,"sort",&mom->sort);
  add_parm(list,DEFAULT,INTEGER,"sort_variable",&mom->sortvar);
  add_parm(list,DEFAULT,STRING,"smear_type",smr);
  add_parm(list,DEFAULT,DOUBLE,"smearing",&mom->smearing);
  add_parm(list,MUSTSET,INTEGER,"nconfig", &run->nconfig);
  add_parm(list,MUSTSET,STRING,"plot",graph);
  add_parm(list,DEFAULT,BOOL,"single_plot",&run->single_plot);
  add_parm(list,MUSTSET,STRING,"fit",fitstr);
  add_parm(list,DEFAULT,BOOL,"correlated_fit",&run->correlated_fit);
  add_parm(list,DEFAULT,INTEGER,"nboot",&run->nboot);
  add_parm(list,DEFAULT,BOOL,"write_boot",&run->write_boot);
  add_parm(list,DEFAULT,STRING,"x_fit_min",xminstr);
  add_parm(list,DEFAULT,STRING,"x_fit_max",xmaxstr);
  add_parm(list,MUSTSET,STRING,"data_directory",run->data_dir);
  add_parm(list,DEFAULT,STRING,"plotopt_file",run->plotopt_file);

/***   Read parameters using read_parameters   ***/
  if ( (result=read_parameters(filename,list)) ) {
    printf("%s: failed to read parameters from %s\n",fnm,filename);
    print_parameters(list);
/* Defer exit until after we have assigned (default) parameters  *
 * to the structures.  Master program can then decide what to do */
  }

/***  Determine the plot and fit numbers  ***/
  ngraph = sscanveci(graph,plot,MAXNPLOT);
  run->nplot = ngraph;
  if (ngraph) run->plot  = malloc(sizeof(int)*ngraph);
  for (i=0;i<ngraph;i++) run->plot[i] = plot[i];

  nfit = sscanveci(fitstr,fit,MAXNFIT);
  run->nfit = nfit;
  if (nfit) run->fit  = malloc(sizeof(int)*nfit);
  for (i=0;i<nfit;i++) run->fit[i] = fit[i];

  if (strlen(xminstr)) {
    run->x_fit_min = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_min[i]=-DBL_MAX;
    if (i=sscanvecd(xminstr,run->x_fit_min,nfit) != nfit) {
      printf("%s: Warning: nfitmin does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }
  if (strlen(xmaxstr)) {
    run->x_fit_max = malloc(sizeof(double)*nfit);
    for (i=0;i<nfit;i++) run->x_fit_max[i]=DBL_MAX;
    if (i=sscanvecd(xmaxstr,run->x_fit_max,nfit) != nfit) {
      printf("%s: Warning: nfitmax does not match number of fits %d!=%d\n",fnm,
	     i,nfit);
    }
  }

/***  Compute the remaining sizes in Lattice but not the neighbour tables ***/
  compute_sizes(gpar->lattice);
  gpar->lattice->has_ntab = FALSE;

/*** Determine gauge parameters and set remaining to default values ***/
  status += parm_from_string(&gpar->gauge,gau,gauge,MAXGAU,"gauge");

  gpar->action    = GAUGEHIGGS;
  gpar->start     = LOAD;
  gpar->nf        = 0;
  gpar->plaquette = 0.0;
  gpar->rtrace    = 0.0;                               /* read this too? */
  for (i=0; i<MAXPREC; i++) gpar->gfix_precision[i] = 0.0;  /* and this? */
  gpar->gfix_iterations = 0;
  gpar->producer_id     = PROD_INTERNAL;
  gpar->checksum        = 0;

/*** copy momentum parameters and set remainder to defaults ***/
  for (i=0; i<DIR; i++) {
    mom->nmom_out[i] = mom->nmom_in[i];
    mom->L[i] = lat->length[i];
    mom->bc[i] = gpar->bcs[i];
  }
  mom->z3 = TRUE;
  mom->z4 = FALSE;
  status += parm_from_string(&ismear,smr,smearing,MAXSMR,"smear_type");
  mom->smear = ismear;

/*** initialise array pointers to null ***/
  mom->val = 0;
  mom->index = 0;
  mom->itab = 0;
  mom->itabexists = FALSE;
  mom->nequiv = 0;
  mom->nval = 0;
  mom->initialised = FALSE;


/*** Decode error codes and return ***/
  switch (result) {
  case 0:
    return 0;
  case BAD_PARM:
    return BADPARAM;
  case PARM_NO_FILE:
    return BADFILE;
  default:
    fprintf(stderr,"%s: unknown error %d\n",fnm, result);
    return result;
  }

}

