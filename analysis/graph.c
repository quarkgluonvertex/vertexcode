#include "analysis.h"
#include "filename.h"
#include <math.h>

#define MASK(arr,i) ( (arr) ? arr[(i)] : TRUE )
#define MASK2(arr,i,j) ( (arr) ? arr[(i)][(j)] : TRUE )

static int npoints = 101;
static Real conf_level = 0.68;

/* vector_graph is the simplest plot we can make: plot only     *
 * the data with error bars                                     */

void find_minmax(Real* data, int ndata, Real* min, Real* max);

void offset_graph(VectorData* this, OffsetDatafunction* func, int nset,
		  PlotOptions* opt, const char* name, int* mask, int** xmask)
{
  const int ncfg = this->nconfig;
  const int ndata = this->length;
  double average[ndata];
  double** cov = arralloc(sizeof(double),2,ndata,ndata);
  double error[ndata];
  int i, j, k;
 
  FILE* out;

  char filename[MAXLINE];
  char basename[MAXLINE];
  const Graph_t gtype = opt ? opt->graph_type : RAW;

/* Set up the plot options */
  if (opt) {
    opt->nset = nset;
    if (!opt->errorbars) opt->errorbars = malloc(sizeof(Errorbar_t)*nset);
    for (i=0;i<nset;i++) opt->errorbars[i] = (ncfg>1) ? DY : NOERR;
    if (!opt->colour) {
      opt->colour = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->colour[i]=i+1;
    }
    if (!opt->symbol) {
      opt->symbol = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->symbol[i]=i+1;
    }
    if (!opt->size) {
      opt->size = malloc(sizeof(float)*nset);
      for (i=0;i<nset;i++) opt->size[i]=DEFAULT_SIZE;
    }
    if (!opt->line) {
      opt->line = malloc(sizeof(int)*nset);
      for (i=0;i<nset;i++) opt->line[i]=0;
    }
  }

/* Make a name for file, checking if file already exists */
  if (name)
    sprintf(basename,"%s",name);
  else {
    if (this->name)
      sprintf(basename,"%s",this->name);
    else
      sprintf(basename,"latest_plot");
  }
  get_filename(filename,basename,suffix[gtype]);	

/* Write data to plot file */
  out = fopen(filename, "w");
  write_preamble(out,opt);

  for (j=0; j<nset; j++) {
    if (ncfg>1)	{
      /* Get covariance matrix and central values */
      offsetfunc_cov_ave(this,&func[j],average,cov,mask,xmask?xmask[j]:0);
      for (i=0; i<ndata; i++) error[i] = sqrt(cov[i][i]);
    } else if (func[j].func) {
      /* evaluate the datafunction for the single config */
      const int voff = func[j].voffset;
      const int soff = func[j].soffset;
      const int nvec = this[j].nvector - voff;
      const int nbundle = nvec + this[j].nscalar - soff;
      double data[nbundle];
      int bundle;
      for (bundle=soff; bundle<this[j].nscalar; bundle++) {
	data[bundle+nvec-soff] = this[j].scalar_data[bundle][0];
      }
      for (i=0; i<ndata; i++) {
	for (bundle=0; bundle<nvec; bundle++)
	  data[bundle] = this[j].vector_data[bundle+voff][0][i];

	func[j].func(&average[i],this[j].xdata[i],data,nbundle);
      }
    } else {
      for (i=0; i<ndata; i++) {
	average[i] = this[j].vector_data[func[j].voffset][0][i];
      }
    }

    if (ncfg>1)	{
      for (i=k=0; i<ndata; i++) if (!xmask||MASK(xmask[j],i)) {
	fprintf(out, "%12.6g %12.6g %12.6g\n", 
		this[j].xdata[i], average[k], error[k]); 
	k++;
      } 
    } else {
      for (i=0; i<ndata; i++) {
	fprintf(out, "%12.6g %12.6g\n", this[j].xdata[i], average[i]);
      } 
    }
    write_separator(out,opt,j+1);
  }

  fclose(out);
  free(cov);

}

void vector_graph(VectorData* this, Datafunction** func, int nset,
		  PlotOptions* opt, const char* name, int* mask, int** xmask)
{
  OffsetDatafunction* yfn = vecalloc(sizeof(OffsetDatafunction),nset);
  int i;
  for (i=0;i<nset;i++) {
    yfn[i].func = func[i];
    yfn[i].voffset = yfn[i].soffset = 0;
  }
  offset_graph(this,yfn,nset,opt,name,mask,xmask);

}
int bootstrap_graph(Boot* xdata, Boot* ydata, int ndata, BootFitFuncs* fit, 
		    PlotOptions* opt, const char* name)
{
  const char fnm[MAXNAME] = "bootstrap_graph";
  const int nboot = ydata->nboot;
  const int plot_fit = fit ? fit->model ? true : false : false;
  const int nset = plot_fit ? 4 : 1;
  int x_is_boot = false;
  int i;

  FILE *out;
  char filename[MAXLINE];
  char basename[MAXLINE];
  const Graph_t gtype = opt ? opt->graph_type : RAW;

  for (i=0; i<ndata; i++) {
    if ( ydata[i].nboot != nboot ||
	 (xdata[i].nboot != nboot && xdata[i].nboot != 0) ) {
      fprintf(stderr,"%s: incompatible nboots for point %d\n",fnm,i);
      return BADDATA;
    }
    if (xdata[i].nboot) x_is_boot=true;
  }

/* Set up the plot options */
  if (opt) {
    opt->nset = nset;
    if (!opt->errorbars) opt->errorbars = vecalloc(sizeof(Errorbar_t),nset);
    opt->errorbars[0] = (x_is_boot) ? DXXYY : DYY;
    if (plot_fit) {
      opt->errorbars[1] = opt->errorbars[2] = opt->errorbars[3] = NOERR;
    }
    if (!opt->colour) {
      opt->colour = vecalloc(sizeof(int),nset);
      for (i=0;i<nset;i++) opt->colour[i]=1;
    } else {
      for (i=2;i<nset;i++) opt->colour[i]=opt->colour[1];
    }
    if (!opt->symbol) {
      opt->symbol = vecalloc(sizeof(int),nset);
      opt->symbol[0]=1;
    }
    for (i=1;i<nset;i++) opt->symbol[i]=0; /* no symbols on fit lines, ever */

    if (!opt->size) {
      opt->size = vecalloc(sizeof(float),nset);
      for (i=0;i<nset;i++) opt->size[i]=DEFAULT_SIZE;
    }
    if (!opt->line) {
      opt->line = vecalloc(sizeof(int),nset);
      opt->line[0]=0;
      if (plot_fit) {
	opt->line[1] = 1;
	opt->line[2] = opt->line[3] = 2; /* ...dotted in grace */
      }
    } else if (plot_fit) {
      if (!opt->line[1]) opt->line[1]=1;
      opt->line[2] = opt->line[3] = 2;   /* override whatever was set */
    }
  }

  if (name) strcpy(basename,name); else strcpy(basename,"latest_plot");
  get_filename(filename, basename, suffix[gtype]);

  if ( !(out = fopen(filename, "w")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
    return BADFILE;
  }

  write_preamble(out,opt);

  if (x_is_boot) {
    Real xhi, xlo, yhi, ylo, ave, med;
    for (i=0;i<ndata;i++) {
      get_boot_stats(&xdata[i],&ave,&med,&xlo,&xhi,conf_level);
      get_boot_stats(&ydata[i],&ave,&med,&ylo,&yhi,conf_level);
      fprintf(out, "%12.6g %12.6g %12.6g %12.6g %12.6g %12.6g\n",
	      xdata[i].best, ydata[i].best,
	      xhi-xdata[i].best, xdata[i].best-xlo,
	      yhi-ydata[i].best, ydata[i].best-ylo);
    }
  } else {
    Real yhi, ylo, ave, med;
    for (i=0;i<ndata;i++) {
      get_boot_stats(&ydata[i],&ave,&med,&ylo,&yhi,conf_level);
      fprintf(out, "%12.6g %12.6g %12.6g %12.6g\n",
	      xdata[i].best, ydata[i].best,
	      yhi-ydata[i].best, ydata[i].best-ylo);
    }
  }

  if (plot_fit) {
    const int nparam = fit->nparam;
    Real* xplot    = vecalloc(sizeof(Real), npoints);
    Real* yfitbest = vecalloc(sizeof(Real), npoints);
    Real* yfithigh = vecalloc(sizeof(Real), npoints);
    Real* yfitlow  = vecalloc(sizeof(Real), npoints);
    Boot xfit = x_is_boot ? init_boot(nboot) : init_boot(0);
    Boot yfit = init_boot(nboot);
    Real min, max, yhi, ylo, ave,med, delta;

    find_minmax_boot(xdata,ndata,&min,&max);
    if (opt) {
      min = (opt->xmin==UNSET_DBL) ? min : opt->xmin;
      max = (opt->xmax==UNSET_DBL) ? max : opt->xmax;
    }
    delta = (max-min)/npoints;

    /** compute best, high and low fit values for each point **/
    for (i=0;i<npoints;i++) {
      xplot[i] = min + i*delta;
      xfit.best = xplot[i];

      bootstrap_model(&yfit, fit->model, fit->param, nparam, xfit);
      get_boot_stats( &yfit, &ave, &med, &ylo, &yhi, conf_level);
      yfitbest[i] = yfit.best;
      yfithigh[i] = yhi;
      yfitlow[i]  = ylo;
    }

    /**  Best fits **/
    write_separator(out,opt,1);
    for (i=0; i<npoints; i++)
      fprintf(out,"%12.6g %12.6g\n", xplot[i], yfitbest[i]);

    /**  High fits  **/
    write_separator(out,opt,2);
    for (i=0; i<npoints; i++)
      fprintf(out,"%12.6g %12.6g\n", xplot[i], yfithigh[i]);

    /**  Low  fits  **/
    write_separator(out,opt,3);
    for (i=0; i<npoints; i++)
      fprintf(out,"%12.6g %12.6g\n", xplot[i], yfitlow[i]);

    free(xplot);
    free(yfitbest);
    free(yfithigh);
    free(yfitlow);
    destroy_boot(yfit);
    if (x_is_boot) destroy_boot(xfit);
  }

  fclose(out);

  return 0;
}

int ybootstrap_graph(Real* xdata, Boot* ydata, int ndata, BootFitFuncs* fit,
		     PlotOptions* opt, const char* name)
{
  const char fnm[MAXNAME] = "ybootstrap_graph";
  const int nboot = ydata->nboot;
  const int plot_fit = fit ? fit->model ? true : false : false;
  const int nset = plot_fit ? 4 : 1;
  Real yhi, ylo, ave, med;
  int i;

  FILE *out;
  char filename[MAXLINE];
  char basename[MAXLINE];
  const Graph_t gtype = opt ? opt->graph_type : RAW;

/* Set up the plot options */
  if (opt) {
    opt->nset = nset;
    if (!opt->errorbars) opt->errorbars = vecalloc(sizeof(Errorbar_t),nset);
    opt->errorbars[0] = DYY;
    if (plot_fit) {
      opt->errorbars[1] = opt->errorbars[2] = opt->errorbars[3] = NOERR;
    }
    if (!opt->colour) {
      opt->colour = vecalloc(sizeof(int),nset);
      for (i=0;i<nset;i++) opt->colour[i]=1;
    } else {
      for (i=2;i<nset;i++) opt->colour[i]=opt->colour[1];
    }
    if (!opt->symbol) {
      opt->symbol = vecalloc(sizeof(int),nset);
      opt->symbol[0]=1;
    }
    for (i=1;i<nset;i++) opt->symbol[i]=0; /* no symbols on fit lines, ever */
    if (!opt->size) {
      opt->size = vecalloc(sizeof(float),nset);
      for (i=0;i<nset;i++) opt->size[i]=DEFAULT_SIZE;
    }
    if (!opt->line) {
      opt->line = vecalloc(sizeof(int),nset);
      opt->line[0]=0;
      if (plot_fit) {
	opt->line[1] = 1;
	opt->line[2] = opt->line[3] = 2; /* ...dotted in grace */
      }
    } else if (plot_fit) {
      if (!opt->line[1]) opt->line[1]=1;
      opt->line[2] = opt->line[3] = 2;   /* override whatever was set */
    }
  }

  for (i=0; i<ndata; i++) {
    if ( ydata[i].nboot != nboot ) {
      fprintf(stderr,"%s: incompatible nboots for point %d\n",fnm,i);
      return BADDATA;
    }
  }

  if (name) strcpy(basename,name); else strcpy(basename,"latest_plot");
  get_filename(filename, basename, suffix[gtype]);

  if ( !(out = fopen(filename, "w")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
    return BADFILE;
  }

  write_preamble(out,opt);

  for (i=0;i<ndata;i++) {
    get_boot_stats(&ydata[i],&ave,&med,&ylo,&yhi,conf_level);
    fprintf(out, "%12.6g %12.6g %12.6g %12.6g\n",
	    xdata[i], ydata[i].best, yhi-ydata[i].best, ydata[i].best-ylo);
  }

  if (plot_fit) {
    const int nparam = fit->nparam;
    Real* xplot    = vecalloc(sizeof(Real), npoints);
    Real* yfitbest = vecalloc(sizeof(Real), npoints);
    Real* yfithigh = vecalloc(sizeof(Real), npoints);
    Real* yfitlow  = vecalloc(sizeof(Real), npoints);
    Boot xfit=init_boot(0), yfit=init_boot(nboot);
    Real min, max, delta;

    find_minmax(xdata,ndata,&min,&max);
    if (opt) {
      min = (opt->xmin==UNSET_DBL) ? min : opt->xmin;
      max = (opt->xmax==UNSET_DBL) ? max : opt->xmax;
    }
    delta = (max-min)/npoints;

    /** compute best, high and low fit values for each point **/
    for (i=0;i<npoints;i++) {
      xplot[i] = min + i*delta;
      xfit.best = xplot[i];

      bootstrap_model(&yfit, fit->model, fit->param, nparam, xfit);
      get_boot_stats( &yfit, &ave, &med, &ylo, &yhi, conf_level);
      yfitbest[i] = yfit.best;
      yfithigh[i] = yhi;
      yfitlow[i]  = ylo;
    }

    /**  Best fits **/
    write_separator(out,opt,1);
    for (i=0; i<npoints; i++)
      fprintf(out,"%12.6g %12.6g\n", xplot[i], yfitbest[i]);

    /**  High fits  **/
    write_separator(out,opt,2);
    for (i=0; i<npoints; i++)
      fprintf(out,"%12.6g %12.6g\n", xplot[i], yfithigh[i]);

    /**  Low  fits  **/
    write_separator(out,opt,3);
    for (i=0; i<npoints; i++)
      fprintf(out,"%12.6g %12.6g\n", xplot[i], yfitlow[i]);

    free(xplot);
    free(yfitbest);
    free(yfithigh);
    free(yfitlow);
    destroy_boot(yfit);
  }
  return 0;
}

/* yboot_2d_graph: make a plot of bootstrapped data as function of *
 *  2 variables x and y.  Raw output only.                         */

int yboot_2d_graph(Real* xdata, Real* ydata, Boot** yboot, 
		   int nxdata, int nydata, int** mask, int* xkey, int* ykey,
		   char* name)
{
  const char fnm[MAXNAME] = "yboot_2dplot";
  const int nboot = yboot[0][0].nboot;

  Real low, high, median, average, errorh, errorl;
  int i, j;

  char filename[MAX_LENGTH_FILE];
  char basename[MAX_LENGTH_FILE];
  char suffix[MAXLINE] = ".raw";

  FILE *out;

  /**  check inputs  **/
  for (j=0; j<nydata; j++) for (i=0; i<nxdata; i++) {
    if ( yboot[j][i].nboot != nboot ) {
      fprintf(stderr, "%s: incompatible parameter boots\n", fnm);
      return BADDATA;
    }
  }

  /*** Make a name for file, checking if file already exists ***/
  if (name) sprintf(basename,"%s",name);
  else      sprintf(basename,"latest_plot");
  get_filename(filename, basename, suffix);

  /*** Write data to file  ***/
  out = fopen(filename, "w");
  if (!out) {
    fprintf(stderr,"%s: cannot open %s for writing\n",fnm,filename);
    return BADFILE;
  }

  for (j=0; j<nydata; j++) {
    for (i=0; i<nxdata; i++) {
      int ii = xkey ? xkey[i] : i;
      int jj = ykey ? ykey[j] : j;
      if (MASK2(mask,jj,ii)) {
	get_boot_stats(&yboot[jj][ii],&average,&median,&low,&high,conf_level);

	errorh = high - yboot[jj][ii].best;
	errorl = yboot[jj][ii].best - low;
	fprintf(out, "%12.6e %12.6e %12.6e %12.6e %12.6e\n",
		ydata[jj], xdata[ii], yboot[jj][ii].best, errorh, errorl );
      }
    }
    fprintf(out,"\n");
  }

  fclose(out);
  return 0;
}

void find_minmax(Real* data, int ndata, Real* min, Real* max)
{
  int i;
  Real hi=data[0], lo=data[0];

  for (i=1;i<ndata;i++) {
    if (data[i]>hi) hi=data[i];
    if (data[i]<lo) lo=data[i];
  }
  *min = lo;
  *max = hi;
}
