/* $Id: analyse_quark.c,v 1.4 2014/03/18 07:54:11 jskuller Exp $ */
#include "analysis.h"
#include "momdata.h"
#include "field_params.h"
#include "treelevel.h"
#include <float.h>

#define MAX_GRAPH 6
#define MADD      3
#define MMULT     4
#define MHYBRID   5
#define NCORR 3     /* z0, zm, deltam0 */

Datafunction M0;
Datafunction Z0;
Datafunction Zcorr;
Datafunction Msub;
Datafunction Mmult;
Datafunction Mhyb;

int assign_params_analyse_quark(char*,Propagator_parameters*,Momenta*,
				Analysis_params*);
static void assign_default_momenta(Momenta* readmom, Momenta* mom);

int main(int argc, char** argv)
{
  Propagator_parameters ppar;
  Momenta mom,rmom;
  MomData quark;
  Analysis_params run;
  int qmask[2] = {true,true};
  Datafunction* xfunc = Df_x;
  Datafunction* yfunc[MAX_GRAPH] = {Z0,M0,Zcorr,Msub,Mmult,Mhyb};
  char plotname[MAX_GRAPH][MAXLINE] = {"Z0","M0","Zc","Ma","Mm","Mh"};
  PlotOptions* plotopts[MAX_GRAPH] = {0,0};

  Correction_function* corrfunc;
  int hybrid, addmult; /* hybrid or add/mult correction of mass */
  Real** corrdata; /* placeholder in case incompatible *
		    * correction types are needed      */
  int i;
  char filename[MAXLINE];
  int status;

/*** Read run parameters from file ***/
  if (argc != 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename>\n", argv[0]);
    return BADPARAM;
  }
  sprintf(filename, "%s",argv[1]);
  if (assign_params_analyse_quark(filename,&ppar,&mom,&run)) {
    return BADPARAM;
  }
  assign_default_momenta(&rmom,&mom);
  setup_momenta(&rmom);
  setup_momenta(&mom);

/*** Initialise, allocate space and read data ***/
  if ( (status = init_momdata(&quark,&ppar,&rmom,FERMION,
			      run.nconfig,0,2,
			      "quark",run.data_dir)) ) return status;
  set_momvar(&quark,mom.sortvar);
  if ( (status=read_momdata(&quark,qmask,2)) ) return status;
  if ( (status=reorder_momdata(&quark,&mom)) ) return status;

  /*** Set up tree-level correction arrays ***/
  addmult = hybrid = false;
  for (i=0;i<run.nplot;i++) {
    if (run.plot[i]==MHYBRID) hybrid=true;
    if (run.plot[i]==MADD || run.plot[i]==MMULT) addmult=true;
  }
  if (status=momdata_add_pars(&quark,0,NCORR)) return status;
  corrfunc = (hybrid && !addmult) ? z0m0_pm : z0m0;
  if (status=corrfunc(quark.vector_parm,&mom,&ppar)) return status;
  if (hybrid && addmult) {
    corrdata = arralloc(sizeof(Real),2,NCORR,mom.nval);
    if (status=z0m0_pm(corrdata,&mom,&ppar)) return status;
  }

  /*** Plot Z vs k, M vs k. NB: Z REQUIRES mom variable to be k not p ***/
  printf("Making plots for ");
  for (i=0;i<run.nplot;i++) printf("%s ",plotname[run.plot[i]]);
  printf("\n");
  for (i=0;i<run.nplot;i++) {
    const int j=run.plot[i];
    Real** tmp; /* placeholder for correction data */
    if (addmult && j==MHYBRID) { /* swap correction data */
      tmp = quark.vector_parm;
      quark.vector_parm = corrdata;
    }
    plot_momdata(&quark,&xfunc,&yfunc[j],1,plotname[j],plotopts[j]);
    if (addmult && j==MHYBRID) { /* swap back */
      quark.vector_parm = tmp;
    }
  }

  return 0;
}

static void assign_default_momenta(Momenta* readmom, Momenta* mom)
{
  int i;

  set_default_momenta(readmom);

  for (i=0;i<DIR;i++) {
    readmom->L[i] = mom->L[i];
    readmom->nmom_in[i] = readmom->nmom_out[i] = mom->nmom_in[i];
    readmom->bc[i] = mom->bc[i];
  }
  readmom->nvar = mom->nvar;

}

int Z0(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"Z0: illegal narg %d\n",narg);
    return EVIL;
  }
  *y = (x*x*arg[0]*arg[0] + arg[1]*arg[1])/arg[0];
  return 1;
}

/**  Zr = 1/(z0 A) = (k^2 a^2 + b^2) / (z0 a) **
 **  a->arg[0]   b->arg[1]  z0->arg[2]        **/
int Zcorr(Real* y, Real x, Real* arg, int narg)
{
  const char fnm[MAXNAME] = "Zcorr";
  if (!arg) return 1;
  if (narg<3) {
    fprintf(stderr,"%s: illegal narg %d\n",fnm,narg);
    return EVIL;
  }
  *y = (x*x*arg[0]*arg[0] + arg[1]*arg[1])/(arg[0]*arg[2]);
  return 1;
}

int M0(Real* y, Real x, Real* arg, int narg)
{
  if (!arg) return 1;
  if (narg<2) {
    fprintf(stderr,"M0: illegal narg %d\n",narg);
    return EVIL;
  }
  *y = arg[1]/arg[0];
  return 1;
}

/**  Mr = M - dm0 = b/a - dm0           **
 **  a->arg[0]   b->arg[1]  dm0->arg[4] **/
int Msub(Real* y, Real x, Real* arg, int narg)
{
  const char fnm[MAXNAME] = "Msub";
  if (!arg) return 1;
  if (narg<5) {
    fprintf(stderr,"%s: illegal narg %d\n",fnm,narg);
    return EVIL;
  }
  *y = arg[1]/arg[0] - arg[4];
  return 1;
}

/**  Mr = M/zm0 = b/(a*zm0)             **
 **  a->arg[0]   b->arg[1]  zm0->arg[3] **/
int Mmult(Real* y, Real x, Real* arg, int narg)
{
  const char fnm[MAXNAME] = "Mmult";
  if (!arg) return 1;
  if (narg<4) {
    fprintf(stderr,"%s: illegal narg %d\n",fnm,narg);
    return EVIL;
  }
  *y = arg[1]/(arg[0]*arg[3]);
  return 1;
}

/**  Mr = (M-m0-)/zm0 = (b/a-m0-)/zm0                 **
 **  a->arg[0]   b->arg[1]  zm0->arg[3]  m0- ->arg[4] **/
int Mhyb(Real* y, Real x, Real* arg, int narg)
{
  const char fnm[MAXNAME] = "Mhyb";
  if (!arg) return 1;
  if (narg<5) {
    fprintf(stderr,"%s: illegal narg %d\n",fnm,narg);
    return EVIL;
  }
  *y = (arg[1]/arg[0]-arg[4])/arg[3];
  return 1;
}

