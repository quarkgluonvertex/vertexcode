#include "analysis.h"
#include "vertex.h"
#include "momdata.h"
#include "treelevel.h"
#include <string.h>
#include <assert.h>

/*** rescaling of gluon prop from asymmetric tensor at small momenta ***/
#define ZEROGLUON 1.11111111
#define ZEROGLUON_T 3.0
#define ONEGLUON 0.929
#define ONE1GLUON 0.944
#define ONE2GLUON 0.963
#define ONE3GLUON 0.983
#define ONE4GLUON 0.991
#define ONEONEGLUON 0.957

#define MAX_FORMFACTOR 12
#define MAXMOMVAR     6
#define ILLEGAL_VALUE -12345.6789
#define ILLEGAL_INT -12345

#define AM_MAXCODE 20

/* Gamma matrix codes to match those in Quark.h */
#define GAMMA_5X 6
#define GAMMA_5Y 7
#define GAMMA_5Z 8
#define GAMMA_5T 9
#define SIGMA_XY 10
#define SIGMA_XZ 11
#define SIGMA_XT 12
#define SIGMA_YZ 13
#define SIGMA_YT 14
#define SIGMA_ZT 15

#define SWAP(a,b) { int tmp=(a);(a)=(b);(b)=tmp; }

static Real    one(Real a) { return 1.0; }
static Real    inv(Real a) { return 1.0/a; }
static Real invsqr(Real a) { return 1.0/(a*a); }
static Real invcub(Real a) { return 1.0/(a*a*a); }
static Real   mult(Real* a, int n) { assert(n==2); return a[0]*a[1]; }
static Real divide(Real* a, int n) { assert(n==2); return a[0]/a[1]; }
static Real    sub(Real* a, int n) { assert(n==2); return a[0]-a[1]; }
static Real setcorr(Real a, Real b) { return b; }
static Momfunc_pk Mf_eps_ka_pb_by_d;

static int iszero(i4vec q) {return (!q[0] && !q[1] && !q[2] && !q[3]); }
int pdq(i4vec p, i4vec q, int m);
int psqr(i4vec p, int m);

static int compute_mom_D(Momenta*,Propagator_parameters*);

static void assign_gluon_momenta(Momenta *mom, Momenta* qmom,
				 Vertex_params *run);
static int setup_qlist(i4vec** q, Vertex_params* run, Momenta* mom);

static void setup_qequiv(i4vec q, i4vec** qarr, int* nval);
static int nequiv(int x, int y, int z);

static Real diagprop_scale(i4vec q, int id);
static int create_datastring(char* datastr, char* cutstr, 
			     Vertex_params* run, Momenta* mom);

int main(int argc, char **argv)
{
  Boot** data;       /* form factor as function of (p,k) or (p,q) */
  Boot** readdat;    /* temporary data read in from boot files    */
  Boot*  datatmp;
  Boot tmp;

  int ndata, nxdata, nydata, nalldata, nqeqv;
  int Ngamma = 15;   /* number of gamma traces in input files */
  int mu, nu, gamma, i, j, qi, iq, jq, nq, nmu, m;
  i4vec nmom;        /* number of momenta in each direction   */
  i4vec* q=0;
  i4vec* qlist=0;    /* list of gluon momenta                 */
  Real *corr;        /* tree-level correction array           */
  Real scale;                       /* physical scale         */
  Real scalefact = 1.0;             /* gluon prop rescaling   */
  Real (*dimfunc)(Real);            /* engineering dimensions */
  Momfunc_pk* scalefunc;            /* kinematic factor       */
  Cpt_correction_pq* cpt_corrfunc;  /* tree-level correction  */
  Real (*correction)(Real*,int);    /* average tree-level correction array */

  int status;      // exit code
  int qzero, cpt_corr, ave_corr;  // boolean

  /** filenames and codes/strings used in filenames **/
  char filename[MAXLINE];
  char datastr[MAXLINE], cfgstr[MAXLINE];
  char outname[MAXLINE];

  char momcode[MAXMOMVAR] = "pkqK";
  char symcode[VTX_MAXKIN][AM_MAXCODE] = {"","soft", "hard", "orth","symm","skew",""};
  char kinstr[AM_MAXCODE]="", qstr[AM_MAXCODE], vname[AM_MAXCODE];
  char corrcode[AM_MAXCODE], cutstr[AM_MAXCODE];

  Vertex_params run;   /* all the input parameters apart from momenta */
  Momenta mom, inmom;  /* quark momenta (inmom: without smear/o3 ave) */
  Momenta gmom;        /* gluon momenta */
  int **nequiv;

  /*** Initialise and read in parameters ***/
  if (argc != 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename>\n", argv[0]);
    return BADPARAM;
  }

  sprintf(filename, "%s",argv[1]);
  status=assign_params_analyse_vertex(filename,&run,&mom);
  if (status) return status;

  /*** Check tree-level correction type ***/
  switch (run.correction) {  
  case NOCORR:
    ave_corr = cpt_corr = FALSE; 
    corrcode[0]='\0';
    break;
  case AVECORR:
    ave_corr = TRUE; cpt_corr = FALSE;
    strcpy(corrcode,"_acorr");
    break;
  case CPTCORR:
    ave_corr = FALSE; cpt_corr = TRUE;
    strcpy(corrcode,"_ccorr");
    break;
  default:
    printf("Unknown corr code %d -- assuming average tree-level correction\n",
	   run.correction);
    ave_corr = TRUE; cpt_corr = FALSE;
    strcpy(corrcode,"_acorr");
  }
  run.mu++;

  /*** Determine the kinematics.                       ***
   *** Note that kinematics are also set in parameters ***/
  qzero = (!run.q[IX] && !run.q[IY] && !run.q[IZ] && !run.q[IT]);
  if (qzero && (run.kinematics != VTX_ZEROMOM) ) {
    printf("Warning: gluon momentum set to zero but kinematics not set to zeromom\n");
    printf("Warning: assuming zero gluon kinematics\n");
    run.kinematics = VTX_ZEROMOM;
  }

  /***  Set up momentum parameters  ***/
  /*  For tree-level correction everything must be in lattice units,  *
   *  and we cannot average equivalent or nearby momenta.  If this    *
   *  has been specified in the inputs we must use a separate         *
   *  momentum variable inmom initially, and average and scale at end */
  inmom = mom;
  inmom.o3 = inmom.z4 = FALSE;
  inmom.smear = NONE;
  inmom.scale = 1.0;
  assign_gluon_momenta(&gmom,&mom,&run);
  /* Aspect ratio m, determines momentum units */
  m = run.gauge->lattice->length[IT]/run.gauge->lattice->length[IX];

  if (!mom.o3 && !mom.z4 && !mom.smear && mom.scale==1.0) { /* inmom==mom */
    setup_momenta(&mom);
    inmom = mom;
  } else {
    setup_momenta(&mom);
    setup_momenta(&inmom);
  }
  /* Add the propagator denominator D as momentum variable */
  if ((status=compute_mom_D(&mom,run.prop))) return status;

  /* compute the number of distinct momenta */
  nxdata = mom.nval;
  for (i=0; i<SPATIAL_DIR; i++) nmom[i]=2*run.nmom[i]+1;
  nmom[IT] = 2*run.nmom[IT];
  ndata = nmom[0]*nmom[1]*nmom[2]*nmom[3];
  if (qzero) strcpy(kinstr,"z");

  if (run.kinematics != VTX_GENERIC) setup_momenta(&gmom);
  nq = setup_qlist(&qlist,&run,&gmom);

  /* In generic kinematics we plot data as function of p and k *
   * otherwise we use p and q                                  */
  nydata = (run.kinematics==VTX_GENERIC || run.kinematics==VTX_AVEQ)
    ? nxdata : gmom.nval;
  nalldata = nxdata*nydata;

  /* create strings for use in filenames */
  if ((status=create_datastring(datastr,cutstr,&run,&mom))) return status;
  sprintf(cfgstr, "U%d_%dcfg_%dbt", run.start_cfg, run.nconfig, run.nboot);

  /*** Set up variables specific to each form factor ***/
  /*** Form factors are numbered 1 to 12             ***
   *** with 1-4 the non-transverse lambda_i,         ***
   *** 5-12 the transverse form factors tau_i        ***/
  if (run.formfactor < 1 || run.formfactor > MAX_FORMFACTOR) {
    printf("Illegal form factor %d",run.formfactor);
    printf(" -- must be > 0 and < %d\n",MAX_FORMFACTOR+1);
    return BADPARAM;
  }
  if (run.formfactor <= 4) {
    sprintf(vname,"lambda%d",run.formfactor);
  } else {
    if (qzero) {
      printf("Error: all tau form factors are zero at the soft gluon point\n");
      return BADPARAM;
    }
    sprintf(vname,"tau%d",run.formfactor-4);
  }

  if (run.kinematics==VTX_AVEQ) { 
    sprintf(outname, "%s_%s_%s_p%d%d%d%02dq%d%d%d%02d%s_%c%s_",
	    vname, symcode[run.kinematics], datastr,
	    run.p[IX], run.p[IY], run.p[IZ], run.p[IT], 
	    qlist[0][IX], qlist[0][IY], qlist[0][IZ], qlist[0][IT], 
	    cutstr, momcode[mom.sortvar], corrcode);
  } else {
    sprintf(outname, "%s_%s_%s_p%d%d%d%02dq%d%d%d%02d%s_%c%s_",
	    vname, symcode[run.kinematics], datastr,
	    run.p[IX], run.p[IY], run.p[IZ], run.p[IT], 
	    run.q[IX], run.q[IY], run.q[IZ], run.q[IT], 
	    cutstr, momcode[mom.sortvar], corrcode);
  }
  /* from here on AVEQ (ave over equivalent qs) is the same as VTX_GENERIC */
  if (run.kinematics==VTX_AVEQ) run.kinematics=VTX_GENERIC;

  switch (run.formfactor) {
  case 1: /** lambda1 - NOTE this is mostly copied from the old code **/
    scalefunc = Mf_pk_one;
    corr = 0; // z0_arr(&mom,act);
    cpt_corrfunc = l1_pq;
    correction = divide;
    dimfunc = one;
    nmu = qzero ? 3: 4;
    printf("Warning: %s not properly implemented; results are unreliable\n",
	   vname);
    break;
  case 10: /** tau6 **/
    scalefunc = Mf_one_by_qa_Pmu;  /* 1/(q_a P_mu) */
    corr = 0;
    cpt_corrfunc = t6_cpt;
    correction = sub;
    dimfunc = invsqr;
    nmu = 12;   /* number of combinations (mu,nu) in tr(gmu Gamma_nu) */
  case 12: /** tau8 **/
    scalefunc = Mf_eps_ka_pb_by_d; /* eps_abmunu k_a p_b /(k^2p^2-(kp)^2) */
    corr = 0;                      /* no average correction    */
    cpt_corrfunc = t8_cpt;
    correction = sub;              /* subtract tree-level form */
    dimfunc = invsqr;              /* dimension [1/p^2]        */
    nmu = 12;  /* number of (nu,mu) vals in tr(g5gmu Gamma_nu) */
    break;
  default:
    printf("Error: %s not yet implemented\n", vname);
    return BADPARAM;
  }

  /** allocate space for data **/
  tmp = init_boot(run.nboot);
  readdat = init_bootarr(run.nboot,2,ndata,Ngamma);
  datatmp = init_bootarr(run.nboot,1,ndata);
  data = init_bootarr(run.nboot,2,nydata,nxdata);
  nequiv = arralloc(sizeof(int),2,nydata,nxdata);
  for (i=0;i<nydata;i++) for (j=0;j<nxdata;j++) nequiv[i][j]=0;

  /*** Big loop over all distinct q-values ***/
  for (qi=0; qi<nq; qi++) {
  setup_qequiv(qlist[qi], &q, &nqeqv);
  if (run.kinematics!=VTX_GENERIC) iq = momindex_p(qlist[qi],&gmom);
  qzero = iszero(qlist[qi]);

#if 0 /* rescaling diagonal gluon prop at low momentum */
  scalefact = diagprop_scale(qlist[qi],run.id);
#else
  scalefact = 1.0;
#endif
  /* loop over all combinations of lorentz and gamma indices         *
   * Note: mu denotes each combination                               *
   *       nu is the lorentz index of the vertex                     *
   *       gamma is the gamma matrix trace we use - this may itself  *
   *             have lorentz indices eg gamma_mu, sigma_munu        */
  for (mu=0; mu<nmu; mu++) { 
    switch (run.formfactor) {
    case 1:
      nu=mu+1; gamma=mu+1; break;
    case 10: /* tau6: mu -> (mu,nu) */
      gamma = mu/3 + 1;     /* gamma_mu */
      nu=mu%3; if (nu>=mu/3) nu++; /* nu != mu        */
      break;
    case 12: /* tau8: mu -> (mu,nu) */
      gamma = mu/3 + GAMMA_5X;     /* gamma5 gamma_mu */
      nu=mu%3; if (nu>=mu/3) nu++; /* nu != mu        */
      break;
    default:
      printf("analysis for form factor #%d not implemented\n", run.formfactor);
      return BADPARAM;
    }
/*** loop through equivalent q-values ***/
    for (jq=0; jq<nqeqv; jq++) {
      i4vec np, poff, ip, p, k;
      int nall = 1;
      int jp;
      for (i=0; i<DIR; i++) {
	if (qzero && i==mu && false) { // OBSOLETE:
	  np[i] = 1;                   // p[mu]=0 condition
	  poff[i] = run.nmom[i];       //  depends on form factor and method
	} else {
	  np[i] = nmom[i] - abs(q[jq][i]);          /* # valid p-values   */
	  poff[i] = (q[jq][i]<0) ? -q[jq][i] : 0;   /* offset in p-values */
	}
	nall *= np[i];
      }
     /** skip if q[nu]=0 TODO: generalise this? **/
      if (qzero && q[jq][nu-1]!=0) continue; // ???
      sprintf(qstr,"q%d%d%d%d_",
	      q[jq][IX],q[jq][IY],q[jq][IZ],q[jq][IT]);
      /** preprocess for skewed kinematics: only odd temporal gluon momenta **/
      if (run.kinematics==VTX_SKEWED && q[jq][IT]%2) continue;

      sprintf(filename,"%s/data_vtx_%s_mu%d_%snp%d%d%d%d%s_%s_00.xdr",
	      run.boot_dir, datastr, nu,
	      qstr, run.nmom[IX], run.nmom[IY], run.nmom[IZ], run.nmom[IT], 
	      kinstr, cfgstr);
      if ((status = read_boot(readdat,nall*Ngamma,filename))) return status;

/*** loop over all p-values ***/
      for (jp=0,ip[IX]=0; ip[IX]<np[IX]; ip[IX]++)
	for (ip[IY]=0; ip[IY]<np[IY]; ip[IY]++)
	  for (ip[IZ]=0; ip[IZ]<np[IZ]; ip[IZ]++) {
	    for (ip[IT]=0; ip[IT]<np[IT]; ip[IT]++) {
              /* determine p, k values; check for valid k */
	      int valid=TRUE;
	      for (i=0; i<DIR; i++) {
		p[i] = ip[i]-run.nmom[i]+poff[i];
		k[i] = p[i]+q[jq][i];
		if (k[i]<-run.nmom[i] || k[i]>run.nmom[i]) valid=FALSE;
	      }
	      if (false && qzero && p[mu]!=0) continue; // TODO: generalise
	      if (k[IT]==run.nmom[IT]) valid=FALSE;
	      if (!valid) continue;
	      jp++;
	      /* check kinematics */
	      if (run.kinematics==VTX_SKEWED && pdq(p,q[jq],m)) continue;
	      if (run.kinematics==VTX_PKSYM && psqr(p,m)!=psqr(k,m)) continue;
	      i = momindex_p(p,&mom);
	      j = momindex_p(k,&mom);
	      if (i!=DISCARDED && j!=DISCARDED) {
		jp--;       /* push back momentum counter */
                /* get the relevant gamma trace from data */
		boot_copy(&datatmp[jp],readdat[jp][gamma]);
                /* Rescale old UKQCD data if appropriate */
		if (run.id==0 && qzero && mu==IT) {
		  scale_boot(&datatmp[jp],datatmp[jp],ZEROGLUON_T);
		}
		if ( (scale=scalefunc(&inmom,p,k,mu))==ILLEGAL_VALUE ) continue;
		scale_boot(&datatmp[jp], datatmp[jp],scalefact*scale); 
                /* Multiply with the appropriate kinematical factor *
		 * and add to average                               *
		 * We must use un-averaged momenta (inmom)          *
		 * for scaling and tree-level correction            *
		 * NOTE: componentwise treelevel correction is done *
		 *       AFTER multiplying with kinematical factor */
		if (cpt_corr) {
		  boot_func_mixed(&datatmp[jp],correction,1,1,datatmp[jp],
				  (*cpt_corrfunc)(&inmom,run.prop,p,q[jq],mu));
		}
		if (run.kinematics==VTX_REFLECT || run.kinematics==VTX_SKEWED) j=iq;
		add_boot(&data[j][i],data[j][i],datatmp[jp]);
		nequiv[j][i]++;
		jp++;
	      }
	    }
	  }
    }
  }
  }
  /** Finally, normalise the data **/
  for (j=0;j<nydata;j++) {
    for (i=0;i<nxdata;i++) {
      scale_boot(&data[j][i],data[j][i],1.0/nequiv[j][i]);
    }
  }
#if 0 /* we keep nequiv to use as a mask in the plotting */
  free(nequiv);
#endif

/* Put in average tree-level correction */
  if (ave_corr) {
    if (!corr) {
      printf("Attempting undefined average correction!\nExiting.\n");
      return BADDATA;
    }
    for (i=0; i<nalldata; i++) {
      boot_func_mixed(&((*data)[i]),correction,1,1,(*data)[i],corr[i]);
    }
  }
  /*** Now we can put in physical scale ***/
#if 0 /* momenta already have physical scale */
  scale_momenta(&mom,run.scale);
  if (run.kinematics!=VTX_GENERIC) scale_momenta(&gmom,run.scale);
#endif
  scale = dimfunc(mom.scale); /* physical scale to appropriate power */
  if (scale!=1.0) {
    for (i=0;i<nydata;i++) {
      for (j=0;j<nxdata;j++) {
	scale_boot(&data[i][j],data[i][j],scale);
      }
    }
  }

  /*** Write out the bootstrapped form factor ***/
  if (run.write_boot){
    sprintf(filename, "%s/%s%s_", run.boot_dir, outname, cfgstr);
    if (status=write_boot(*data,nalldata,filename)) return status;
  }

#if 0 //TEST
  {
    Boot* avebt = create_1d_boot_space(run.nboot,nydata);
    int* mask = vecalloc(sizeof(int),nydata);
    Momenta* mom1 = (run.kinematics==VTX_GENERIC) ? &mom : & gmom;
    Momenta* mom2 = &mom;
    for (i=0;i<nydata;i++) mask[i]=TRUE;
    if (status=boot_interpolate_mom_1d(avebt,mask,data,mom1,mom2,nequiv,3))
      return status;
    if (run.plot){
      printf("making graph\n");
      fflush(stdout);
      sprintf(filename, "%s/%s%dcfg_", run.graph_dir, outname, run.nconfig);
      ybootstrap_plot(mom1->val[mom1->sortvar],avebt,nydata,
		      mask,mom1->key,0,0,filename);
    }
  }
#else
  /*** Create plot of the form factor ***/
  if (run.plot) {
    Real* xdata = mom.val[run.mom_variable];
    Real* ydata = (run.kinematics==VTX_GENERIC) ?
      xdata : gmom.val[run.mom_variable];
    int* ykey = (run.kinematics==VTX_GENERIC) ? mom.key : gmom.key;
    printf("making graph\n");
    fflush(stdout);
    sprintf(filename, "%s/%s%dcfg_", run.graph_dir, outname, run.nconfig);

    yboot_2d_graph(xdata, ydata, data, nxdata, nydata, 
		   nequiv, mom.key, ykey, filename);
  }
#endif  

  return 0;
}

/***** Momentum scaling / projection functions start here *****/

/* Mf_eps_ka_pb_by_d:                                        *
 *  epsilon_abmn k_a p_b / (k^2p^2-(kp)^2)                   *
 * This will yield tau8 if multiplied by tr(g5gmu Gnu)       *
 * k and p are the lattice momenta K(p) = sin(pa)            *
 *  mu,nu are passed as one variable munu = 0...11:          *
 *  (1,0), (2,0), (3,0), (1,2), (3,2), (4,2) etc             */
static Real Mf_eps_ka_pb_by_d(Momenta* mom, int* p, int* k, int numu)
{
  const int mu = numu/3;
  int nu = numu%3;
  int a, b, i;
  Real ksqr, psqr, kdotp, D, Kp[DIR], Kk[DIR];
  assert(numu<12 && numu>=0);
  assert(mom->bc[IT]==ANTIPERIODIC);
  if (nu>=mu) nu++;

/* Compute K^2(p), K^2(k), K(p).K(k) */
  ksqr = psqr = kdotp = 0.0;
  for (i=0;i<DIR;i++) {
    Kp[i] = Mf_kmu(mom,p,i);  /* gives K_i(p) = sin(ap_i) */
    Kk[i] = Mf_kmu(mom,k,i);
    psqr  += Kp[i]*Kp[i];
    ksqr  += Kk[i]*Kk[i];
    kdotp += Kp[i]*Kk[i];
  }
  /* TODO: check if the following is the appropriate lattice version of *
   * D = p^2 k^2 - (p.k)^2                                              */
  D = psqr*ksqr - kdotp*kdotp;
  if (D==0.0) return ILLEGAL_VALUE;
  
/* Find a, b such that e_abmunu = +1 */
  a=b=-1;
  for (i=0;i<DIR;i++) {
    if (i!=mu && i!=nu) {
      if (a==-1) a=i; else b=i;
    }
  }
  if (mu-nu==1 || mu-nu==3 || mu-nu==-2) {i=a; a=b; b=i;}

  return (Kk[a]*Kp[b]-Kp[a]*Kk[b])/D;
}  

/*** HACK rescaling of the diagonal gluon prop at low momentum ***/
static Real diagprop_scale(i4vec q, int id)
{
  Real scalefact = 1.0;

  if (id != 0) return 1.0;

  /* We assume q is already sorted */
  if (!q[IZ] && !q[IT]) scalefact = ZEROGLUON;
  if (q[IZ]==1) {
    switch (q[IY]) {
    case 0:
      switch (q[IT]) {
      case 0: scalefact = ONEGLUON; break;
      case 1: scalefact = ONE1GLUON; break;
      case 2: scalefact = ONE2GLUON; break;
      case 3: scalefact = ONE3GLUON; break;
      case 4: scalefact = ONE4GLUON; break;
      }
      break;
    case 1:
      if (!q[IX] && !q[IT]) scalefact = ONEONEGLUON;
    }
  }
  return scalefact;
}

static int create_datastring(char* datastr, char* cutstr, 
			     Vertex_params* run, Momenta* mom)
{
  const char fnm[MAXNAME] = "create_datastring";
  const int action = run->prop->action;
  char actstr[AM_MAXCODE], gaugestr[AM_MAXCODE];
  char impstr[AM_MAXCODE];

  /*** Set up strings describing parameters (for filenames) ***/
  switch (run->gauge->gauge) {
  case LANDAU:
    strcpy(gaugestr,"L"); break;
  case COVARIANT:
    sprintf(gaugestr,"X%02d",INT(10*run->gauge->gauge_param));
    break;
  default:
    fprintf(stderr,"%s: Unknown gauge type %d\n",fnm,run->gauge->gauge);
    return BADPARAM;
  }

  switch (action) {
  case WILSON:
    strcpy(actstr,"W"); break;
  case CLOVER:
    sprintf(actstr,"C%03d",INT(100*run->prop->imp[0])); break;
  default:
    fprintf(stderr,"%s:Unknown action type %d\n",fnm,action);
    return BADPARAM;
  }

  /** string code for improvement terms: only for clover (improved) action **/
  if (action == CLOVER) {
    if (run->prop->source_type==ROTATED) {
      if (run->prop->ncoeff==2) {
	sprintf(impstr,"r%03d",INT(1000*run->prop->imp[1]));
      } else {
	sprintf(impstr,"r%03db%03dl%02d",INT(1000*run->prop->imp[1]),
		INT(100*run->prop->imp[2]),INT(100*run->prop->imp[3]));
      }
    } else {
      sprintf(impstr,"l%02db%03d",
	      INT(100*run->prop->imp[3]),INT(100*run->prop->imp[2]));
    }
  }

  /** string code for momentum cuts **/
  if (mom->dcut > 0.0) { 
    if (mom->cutoff > 0)
      sprintf(cutstr,"r%03dd%02d",INT(mom->cutoff*100),INT(mom->dcut*10));
    else
      sprintf(cutstr,"d%02d",INT(mom->dcut*10));
  } else {
    if (mom->cutoff > 0)
      sprintf(cutstr,"r%03d",INT(mom->cutoff*100));
    else
      strcpy(cutstr,"");
  }
  if (mom->smear==BINSIZE) {
    sprintf(cutstr,"%ss%03d",cutstr,INT(mom->smearing*1000));
  } else if (mom->o3) {
    strcat(cutstr,"o3");
  }
  if (run->kinematics==VTX_AVEQ) strcat(cutstr,"a");

  sprintf(datastr,"B%d%s%sK%04d%s",INT(run->gauge->beta*100),
	  gaugestr, actstr, INT(run->prop->kappa*1000), impstr);

  return 0;
}

static void assign_gluon_momenta(Momenta *mom, Momenta* qmom,
				 Vertex_params *run)
{
  int i;
  *mom = *qmom;
  for (i=0; i<DIR; i++) {
    mom->nmom_out[i] = run->q[i];
    mom->bc[i] = PERIODIC;
  }
  if (run->kinematics==VTX_AVEQ) { /* KLUDGE: Generalise this! */
    mom->nmom_out[IX] = 0;
    mom->nmom_out[IY] = mom->nmom_out[IZ] = 2;
    mom->nmom_out[IT] = 6;
  }
  mom->nvar = 3;
  mom->itabexists = mom->initialised = FALSE;
  /* set array pointers to null to be safe */
  mom->key = mom->index = mom->nequiv = 0;
  mom->val = 0;
  mom->itab = 0;
  mom->nval = 0;
}

/* Compute the overall factor Dr(p)/D(p) or Di(p), add as momentum variable */
static int compute_mom_D(Momenta* mom, Propagator_parameters* act)
{
  const char fnm[MAXNAME] = "compute_mom_d";
  const Real cq = act->action==CLOVER ? act->ncoeff>1 ? act->imp[1] : 0 : 0;
  const Real bq = act->action==CLOVER ? act->ncoeff>2 ? act->imp[2] : 0 : 0;
  const Real ca = act->action==CLOVER ? act->ncoeff>3 ? act->imp[3] : 0 : 0;
  const Real cm = 1.0+bq*act->mass;
  Real M, A, B, ksqr;
  int i;

  if (mom->nvar < MOMD) {
    fprintf(stderr,"%s: too few momentum variables %d assigned - need %d\n",
	    fnm,mom->nvar,MOMD);
    return BADPARAM;
  }

  if (cq!=0.0 && ca!=0.0) {
    fprintf(stderr,"%s: both additive improvement and rotation set!\n",fnm);
    return BADPARAM;
  }

  if (cq!=0.0) { /* rotated propagator */
    for (i=0;i<mom->nval;i++) {
      ksqr = mom->val[MOMK][i]*mom->val[MOMK][i];
      M = act->mass + 0.5*mom->val[MOMQ][i]*mom->val[MOMQ][i];
      A = 1.0 + 2.0*cq*M - cq*cq*ksqr;
      B = (1-cq*cq*ksqr)*M - 2.0*cq*ksqr;
      mom->val[MOMD][i] = (ksqr*A*A + B*B)/(ksqr+M*M);
    }
  } else {  /* unrotated propagator, use additive improvement (may be 0) */
    for (i=0;i<mom->nval;i++) {
      M = act->mass + 0.5*mom->val[MOMQ][i]*mom->val[MOMQ][i];
      B = cm*M - 2.0*ca*(ksqr+M*M);
      mom->val[MOMD][i] = cm*cm*ksqr + B*B;
    }
  }
  return 0;
}

/************************************************************************
 *  setup_qlist: creates a list q of distinct gluon momenta to use in   *
 *    the vertex analysis, returning the length of the list             *
 ************************************************************************/
static int setup_qlist(i4vec** q, Vertex_params* run, Momenta* mom)
{
  int p[DIR], i, mu, nq;
  Momenta* inmom = mom;

  if (run->kinematics==VTX_GENERIC) {
    *q = malloc(sizeof(i4vec));
    for (mu=0;mu<DIR;mu++) (**q)[mu]=run->q[mu];
    return 1;
  }
  if (run->kinematics==VTX_AVEQ) {
    int qq[20][DIR]; /* tmp array to hold equivalent q-values */
    i = momindex_p(run->q,mom);
    if (i==DISCARDED) { *q=0; return 0; }
    printf("q values:\n%10.8f  %d %d %d %d\n",mom->val[mom->sortvar][i],
	   run->q[IX],run->q[IY],run->q[IZ],run->q[IT]);
    nq=0;
    for (p[IX]=0; p[IX]<=mom->nmom_out[IX]; p[IX]++)
      for (p[IY]=p[IX]; p[IY]<=mom->nmom_out[IY]; p[IY]++)
	for (p[IZ]=p[IY]; p[IZ]<=mom->nmom_out[IZ]; p[IZ]++)
	  for (p[IT]=0; p[IT]<=mom->nmom_out[IT]; p[IT]++) {
	    if (momindex_p(p,mom)==i) {
	      for (mu=0;mu<DIR;mu++) qq[nq][mu]=p[mu];
	      nq++;
	    }
	  }
    *q = vecalloc(sizeof(i4vec),nq);
    for (i=0;i<nq;i++) {
      for (mu=0;mu<DIR;mu++) (*q)[i][mu]=qq[i][mu];
      printf("q%d = %d %d %d %2d\n",i,qq[i][IX],qq[i][IY],qq[i][IZ],qq[i][IT]);
    }
    return nq;
  }
  /* else */
  if (mom->o3 || mom->z4 || mom->smear) {
    inmom = malloc(sizeof(Momenta));
    *inmom = *mom;
    inmom->o3 = inmom->z4 = 0;
    inmom->smear = NONE;
    inmom->initialised = FALSE;
    setup_momenta(inmom);
  }
  nq = inmom->nval;
  *q = vecalloc(sizeof(i4vec),nq);
  for (p[IX]=0; p[IX]<=mom->nmom_out[IX]; p[IX]++)
    for (p[IY]=p[IX]; p[IY]<=mom->nmom_out[IY]; p[IY]++)
      for (p[IZ]=p[IY]; p[IZ]<=mom->nmom_out[IZ]; p[IZ]++)
	for (p[IT]=0; p[IT]<=mom->nmom_out[IT]; p[IT]++) {
	  if ( (i=momindex_p(p,inmom)) != DISCARDED ) {
	    for (mu=0;mu<DIR;mu++) (*q)[i][mu]=p[mu];
	  }
	}
  if (inmom != mom) {
    free_momenta(inmom);
    free(inmom);
  }
  return nq;
}

static void setup_qequiv(i4vec q, i4vec** qarr, int *nval)
{
  int i, j, k, n, p, nswap, nperm;
  int ix=IX, iy=IY, iz=IZ;
  i4vec* qa;
/*** First sort the components in canonical order ***/
  for (i=0; i<DIR; i++) if (q[i]<0) q[i]=-q[i];
  if (q[IX]>q[IZ]) SWAP(q[IX],q[IZ]);
  if (q[IY]>q[IZ]) SWAP(q[IY],q[IZ]);
  if (q[IX]>q[IY]) SWAP(q[IX],q[IY]);

  n = nequiv(q[IX], q[IY], q[IZ]);
  if (q[IT]) n *= 2;
  nswap = q[IX] ? 8 : q[IY] ? 4 : q[IZ] ? 2 : 1;
  nperm = q[IX]==q[IZ] ? 1 : (q[IX]==q[IY]||q[IY]==q[IZ]) ? 3 : 6;

  if (*qarr) free(*qarr);
  qa = vecalloc(sizeof(i4vec),n+1);

  for (i=0;i<DIR;i++) qa[0][i]=q[i];

  j=1;
  for (p=0;p<nperm;p++) {
/** All possible combinations of +/- the current permutation **/
    for (k=1; k<=nswap; k++,j++) {
      for (i=0;i<DIR;i++) qa[j][i]=qa[j-1][i];
      if (q[IZ]) qa[j][iz] = -qa[j-1][iz];
      if (q[IY] && j%2==0) qa[j][iy] = -qa[j-1][iy];
      if (q[IX] && j%4==0) qa[j][ix] = -qa[j-1][ix];
    }
    if (q[IX]!=q[IZ]) {
/** Permute the momenta **/
      j--;
      if (q[IY]==q[IZ]) {
	if (p%2) {
	  SWAP(qa[j][ix],qa[j][iz]);
	  SWAP(ix,iz);
	} else {
	  SWAP(qa[j][ix],qa[j][iy]);
	  SWAP(ix,iy);
	}
      } else {
	if (p%2) {
	  SWAP(qa[j][ix],qa[j][iz]);
	  SWAP(ix,iz);
	} else {
	  SWAP(qa[j][iy],qa[j][iz]);
	  SWAP(iy,iz);
	}
      }
      j++;
    }
  }

/** Duplicate this with negative qt if appropriate **/
  if (q[IT]) {
    for (j=n/2; j<n; j++) {
      for (i=0;i<DIR;i++) qa[j][i]=qa[j-n/2][i];
      qa[j][IT]=-qa[j][IT];
    }
  }

  *nval = n;
  *qarr = qa;
}

/** pdq: The dot-product between a quark momentum p and a gluon momentum q **
 **      m is the ratio of time to space lattice length Lt/Lx              **
 **      (assuming all spatial lattice sizes Lx, Ly, Lz are equal          **/
int pdq(i4vec p, i4vec q, int m)
{
  int psdqs = p[IX]*q[IX] + p[IY]*q[IY] + p[IZ]*q[IZ];

/** if qt=0 we return the spatial dot-product in spatial units **/
  if (q[IT]==0) return psdqs;

/** otherwise we return the dot-product in half time units **/
  return 2*m*m*psdqs + q[IT]*(2*p[IT]+1);
}

int psqr(i4vec p, int m)
{
  return 4*m*m*(p[IX]*p[IX]+p[IY]*p[IY]+p[IZ]*p[IZ]) + (2*p[IT]+1)*(2*p[IT]+1);
}

/*  ~nequiv returns the number of vectors which are Z3-equivalent to *
 *     (x,y,z).  It is assumed that x<=y<=z.                         */
static int nequiv(int x, int y, int z)
{
  if (z==0) return 1;
  if (y) {
    if (x==z) {
      return 8;
    }
    else if ((x == y) || (y == z)) {
      if (x) return 24;
      else return 12;
    }
    else {
      if (x) return 48;
      else return 24;
    }
  }
  else return 6;
}
