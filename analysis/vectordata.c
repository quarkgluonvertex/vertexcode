#include "analysis.h"
#include "field_params.h"

void vectordata_create_name(VectorData* data, const char* nm);

int init_vectordata(VectorData* data, 
		    void* parm, Data_t type, int length,
		    int nconf, int nscal, int nvect, 
		    const char* nm, const char* dir)
{
  const char fnm[MAXNAME] = "init_vectordata";
  char sep[2] = ".";
  int i;

  data->type = type;
  data->parm = parm;
  data->length  = length;
  data->nconfig = nconf;
  data->nscalar = nscal;
  data->nvector = nvect;

  data->xdata = vecalloc(sizeof(Real),length);
  data->scalar_data = arralloc(sizeof(Real),2,nscal,nconf);
  data->vector_data = arralloc(sizeof(Real),3,nvect,nconf,length);
  if ( (nscal && !data->scalar_data) || (nvect && !data->vector_data) ) {
    fprintf(stderr,"%s: cannot allocate space for data arrays\n",fnm);
    return NOSPACE;
  }

  /*** initialise xdata to index (can be overridden later) ***/
  for (i=0;i<length;i++) data->xdata[i]=i;

  /*** initialise parametric data to null (default) ***/
  data->nsparm = data->nvparm = 0;
  data->scalar_parm = 0;
  data->vector_parm = 0;

  /*** set strings ***/
  memset(data->basename,0,MAXNAME);
  memset(data->name,0,MAXLINE);
  memset(data->filename,0,MAXLINE);
  if (nm) vectordata_create_name(data,nm);
  else vectordata_create_name(data,"data");
  if (!data->name[0]) sep[0]='\0';
  if (dir) sprintf(data->filename,"%s/%s%s%s.dat",
		   dir,data->basename,sep,data->name);
  else sprintf(data->filename,"%s%s%s.dat",data->basename,sep,data->name);
  
  return 0;
}

/*** Add scalar and vector parametric (non-configuration) data ***/
int vectordata_add_pars(VectorData* this, int nspar, int nvpar)
{
  const char fnm[MAXNAME] = "vectordata_add_pars";

  this->nsparm = nspar;
  this->nvparm = nvpar;
  this->scalar_parm = malloc(sizeof(Real)*nspar);
  this->vector_parm = arralloc(sizeof(Real),2,nvpar,this->length);
  if ( (nspar && !this->scalar_parm) || (nvpar && !this->vector_parm) ) {
    fprintf(stderr,"%s: cannot allocate space for parameter arrays\n",fnm);
    return NOSPACE;
  }

  return 0;
}

void vectordata_create_name(VectorData* data, const char* nm)
{
  const char fnm[MAXNAME] = "vectordata_create_name";

  strcpy(data->basename,nm);
  if (!data->parm) {
    if (nm[0]=='\0') strcpy(data->name,"data"); else data->name[0]='\0';
    return;
  }

  switch (data->type) {
  case MESON: {
    Propagator_parameters* ppar = data->parm;
    Gauge_parameters* gpar = ppar->gpar;
    sprintf(data->name, "b%03dk%04d",
	    INT(gpar->beta*100),INT(ppar->kappa*10000));
    break;
  }
  case GAUGE_D: {
    Gauge_parameters* par = data->parm;
    Lattice* lat = par->lattice;
    switch (par->nf) {
    case 0:
      sprintf(data->name,"b%03ds%02dt%02d",
	      INT(par->beta*100),lat->length[IX],lat->length[IT]);
      break;
    case 2:
      sprintf(data->name,"b%03dk%04dmu%02dj%02ds%02dt%02d",INT(par->beta*100),
	      INT(par->mass[0]*10000),INT(par->mass[par->nf]*100),
	      INT(par->mass[par->nf+1]*100),
	      lat->length[IX],lat->length[IT]);
      break;
    default:
      fprintf(stderr,"%s: Warning: %d flavours not implemented\n",fnm,par->nf);
      strcpy(data->name,nm);
    }
    break;
  }
  case HIGGS:
  default:
    fprintf(stderr,"%s: Data type %d not implemented\n",fnm,data->type);
    return;
  }

}

int read_vectordata(VectorData* data, int* mask, int nread)
{
  const char fnm[MAXNAME] = "read_vectordata";
  FILE* in;
  int cfg, idat, t, i, j, sweep, iline;
  double rdat;
  char line[MAXLINE];

  /** check mask and translate to "select" array **/
  for (idat=0,i=0; i<nread; i++) {
    if (idat>data->nvector) {
      printf("%s: data overflow -- mask does not match nvector\n",fnm);
      return BADDATA;
    }
  }

  if ((in=fopen(data->filename, "r")) == NULL) {
    printf("%s: Could not open file <%s>\n",fnm,data->filename);
    return BADFILE;
  }

  iline=0;
  for (cfg=0; cfg < data->nconfig; cfg++) {
#ifndef CORRF_NOSWEEP
    fgets(line,MAXLINE,in); iline++;
    if (fgets(line,MAXLINE,in)) {
      if (sscanf(line, "Sweep number %d", &sweep) != 1) {
	printf("%s: Bad format on file <%s> - missing sweep number for config number %d\n",
	       fnm, data->filename, cfg);
	printf("%s: Read line as <%s>\n",fnm, line);
	fclose(in);
	return BADDATA;
      }
      iline++;
    } else {
      printf("%s: Error on file <%s> - premature end of file\n",
	     fnm,data->filename);
      fclose(in);
      return BADFILE;
    }
#endif
    /** TODO: read scalar data here! **/
    for (i=0,j=0; i<nread; i++) {
#ifndef CORRF_NONAME
      /* The data are preceded by a line containing the correlator name */
      // TODO: perform check on this??
      if (!fgets(line,MAXLINE,in)) {
	printf("%s: Error on file <%s> - premature end of file\n",
	       fnm,data->filename);
	fclose(in);
	return BADFILE;
      }
      iline++;
#endif
      for (idat=0; idat<data->length; idat++) {
	if (fgets(line,MAXLINE,in)) {
	  if (sscanf(line,"%d %lg",&t,&rdat)<2) {
	    fprintf(stderr,"error on line %d in file <%s>: corrupted data\n",
		    iline, data->filename);
	    return BADDATA;
	  }
	  if (t!=idat) {
	    fprintf(stderr,"%s: error on line %d in file <%s>: ",
		    fnm,iline,data->filename);
	    fprintf(stderr,"counts do not match %d!=%d\n",t,idat);
	    return BADDATA;
	  }
	  if (mask[i]) data->vector_data[j][cfg][idat] = rdat;
	  iline++;
	} else {
	  printf("%s: Error on file <%s> - premature end of file\n",
		 fnm,data->filename);
	  fclose(in);
	  return BADFILE;
	}
      }
      if (mask[i]) j++;
    }
  }

  fclose(in);
  return 0;
}
