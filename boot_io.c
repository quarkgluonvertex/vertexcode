#include "analysis.h"
#include "filename.h"
#include "xdr.h"

bool_t xdr_boot(XDR* xdrs, Boot* boot);

int write_boot(Boot* boot, int len, char* name)
{
  const char fnm[MAXNAME] = "write_boot";
  int result;
  char  outname[MAX_LENGTH_FILE];
  FILE* out;
  XDR  xdrs;

  /** Generate filename  **/
  get_filename(outname,name,".xdr");

  /** Open file and associate it with an xdr stream **/
  if ( !(out=fopen(outname,"wb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,outname);
    return BADFILE;
  }
  xdrstdio_create(&xdrs,out,XDR_ENCODE);

  result = xdr_array(&xdrs,&boot,&len,len,sizeof(Boot),xdr_boot);

  fclose(out);

  return result ? 0 : BADFILE;
}

int read_boot(Boot** boot, int len, char* filename)
{
  const char fnm[MAXNAME] = "read_boot";
  int n, result;
  FILE* in;
  XDR  xdrs;

  /** Open file and associate it with an xdr stream **/
  if ( !(in=fopen(filename,"rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
    return BADFILE;
  }
  xdrstdio_create(&xdrs,in,XDR_DECODE);

  result = xdr_array(&xdrs,boot,&n,len,sizeof(Boot),xdr_boot);
  if (n != len) {
    fprintf(stderr,"%s: read only %d of %d objects\n",fnm,n,len);
  }

  fclose(in);
  return result ? 0 : BADFILE;
}

bool_t xdr_boot(XDR* xdrs, Boot* boot)
{
  return ( xdr_Real(xdrs,&boot->best) && xdr_int(xdrs,&boot->nboot) &&
	   xdr_array(xdrs,&boot->boot,&boot->nboot,boot->nboot, 
		     sizeof(Real),xdr_Real) );
}
