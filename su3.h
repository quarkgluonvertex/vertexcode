/* $Id: su3.h,v 1.10 2006/12/04 17:30:21 jonivar Exp $ */

#ifndef _SU3_H_
#define _SU3_H_

#include "Complex.h"

/* QCD definitions */
#define	COLOUR	3
#define NALG 8                  /* number of Lie algebra components */

typedef Complex Su3mat[COLOUR][COLOUR];
typedef Complex Su3vec[COLOUR];
typedef Su3mat Su3;
typedef Complex Short_Su3[COLOUR-1][COLOUR];
typedef Complex Short_Trans_Su3[COLOUR][COLOUR-1];

typedef Real Su3alg[NALG];
typedef Complex Su3alg_c[NALG];

/*** function declarations ***/
void su3_mult(Su3,Su3,Su3);
void su3_mult_dag(Su3,Su3,Su3);
void su3_dag_mult(Su3,Su3,Su3);
void su3_dag_mult_dag(Su3,Su3,Su3);
void su3_power(Su3,Su3,int);
void su3_add(Su3,Su3,Su3);
void su3_sub(Su3,Su3,Su3);
void su3_addto(Su3,Su3);
void su3_subto(Su3,Su3);
void su3_by_complex(Su3,Su3,Complex);
void su3_by_real(Su3,Su3,Real);
void su3_make_zero(Su3);
void su3_make_unit(Su3);
void su3_dagger(Su3,Su3);
void su3_transpose(Su3,Su3);
void su3_scale(Su3,Real);
void su3_antihermitean(Su3,Su3);
void su3_make_traceless(Su3);
Real su3_norm(Su3);

/*** functions to restore SU(3) matrices from shorts ***/
void uncompress_Su3(Su3* strip, int length);
void uncompress_Su3_trans(Su3* strip, int length);
void reconstruct_su3(Su3 mat);
void su3_reunitarise(Su3 mat);

/*** SU(3) algebra functions ***/
void su3_to_su3alg(Su3alg res, Su3 u);
void su3alg_make_zero(Su3alg res);
Real su3alg_norm(Su3alg a);
void su3alg_add(Su3alg res, Su3alg a1, Su3alg a2);
void su3alg_sub(Su3alg res, Su3alg a1, Su3alg a2);
void su3alg_addto(Su3alg,Su3alg);
void su3alg_subto(Su3alg,Su3alg);
void su3alg_by_real(Su3alg res, Su3alg a, Real r);

void su3_to_su3algc(Su3alg_c res, Su3 u);
void su3algc_zero(Su3alg_c res);
Real su3algc_norm(Su3alg_c a);
void su3algc_by_real(Su3alg_c res, Su3alg_c a, Real r);
void su3algc_by_complex(Su3alg_c res, Su3alg_c a, Complex c);

/*** Inline functions ***/
/* Add a real number times the identity to a matrix */
static inline void su3_add_real(Su3 u, Real r)
 { u[0][0].re += r; u[1][1].re += r; u[2][2].re += r; }

/* function to calculate trace of a complex matrix */
static inline Complex su3_trace(Su3 u) {
  Complex _trace;
  _trace.re = u[0][0].re + u[1][1].re + u[2][2].re;
  _trace.im = u[0][0].im + u[1][1].im + u[2][2].im;
  return _trace;
}

/* the real part of the trace */
static inline Real su3_rtrace(Su3 u)
 { return u[0][0].re + u[1][1].re + u[2][2].re; }

/* Copy u to res */
static inline void su3_copy(Su3 res, Su3 u) {
  res[0][0]=u[0][0]; res[0][1]=u[0][1]; res[0][2]=u[0][2];
  res[1][0]=u[1][0]; res[1][1]=u[1][1]; res[1][2]=u[1][2];
  res[2][0]=u[2][0]; res[2][1]=u[2][1]; res[2][2]=u[2][2];
}
	
/*** macros to make routines look group-independent ***/
#define grp_mult         su3_mult
#define grp_dag_mult     su3_dag_mult
#define grp_mult_dag     su3_mult_dag
#define grp_dag_mult_dag su3_dag_mult_dag
#define grpmat_mult         su3_mult
#define grpmat_dag_mult     su3_dag_mult
#define grpmat_mult_dag     su3_mult_dag
#define grpmat_dag_mult_dag su3_dag_mult_dag
#define grp_copy   su3_copy
#define grp_add    su3_add
#define grp_addto  su3_addto
#define grp_sub    su3_sub
#define grp_subto  su3_subto
#define grp_scale  su3_scale
#define grp_zero   su3_make_zero
#define grp_unit   su3_make_unit
#define grp_ctrace su3_trace
#define grp_rtrace su3_rtrace
#define grp_reunitarise su3_reunitarise

#define alg_zero su3alg_make_zero
#define alg_norm su3alg_norm
#define alg_add  su3alg_add
#define alg_sub  su3alg_sub
#define grp_to_alg su3_to_su3alg

#define grpmat_to_algc su3_to_su3algc
#define algc_zero su3algc_zero
#define algc_norm su3algc_norm
#define grpmat_scale(a,b) su3_scale(a,b)
#endif
