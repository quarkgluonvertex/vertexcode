/* $Id: Quark.h,v 1.11 2014/08/21 08:19:33 jonivar Exp $ */

/*****************************************************************************
 * Header file containing spinor structures and operations on dirac matrices *
 *****************************************************************************/

#ifndef _QUARK_H_
#define _QUARK_H_
#include "Group.h"          /* includes def of Complex and colour structures */

#define SPIN 4	    /* number of dirac indices */
#define SPINCOL (SPIN*COLOUR)
#define GSPIN (SPIN*2)  /* number of gorkov spin/flavour indices */
#define GSPINCOL (SPIN*COLOUR*2)

/*** Gamma matrix definitions ***/
#ifdef UKQCD_GAMMA
#define HERMITEAN_GAMMA
#define NO_BASIC_GAMMA 16   /* number of independent gamma matrices */
#else
#define CHIRAL_GAMMA
#define NO_BASIC_GAMMA 16   /* number of gamma matrices defined */
#endif
#define NUMBER_INDICES 16

#define IDENTITY 0
#define GAMMA_X  1
#define GAMMA_Y  2
#define GAMMA_Z  3
#define GAMMA_T  4
#define GAMMA_5  5
#ifdef UKQCD_GAMMA
/* the following are defined with an additional factor i but ignore that */
#define GAMMA_5X 6
#define GAMMA_5Y 7
#define GAMMA_5Z 8
#define GAMMA_5T 9
#define SIGMA_XY 10
#define SIGMA_XZ 11
#define SIGMA_XT 12
#define SIGMA_YZ 13
#define SIGMA_YT 14
#define SIGMA_ZT 15
#else
#define GAMMA_X5 6
#define GAMMA_Y5 7
#define GAMMA_Z5 8
#define GAMMA_T5 9
#define SIGMA_XY 10
#define SIGMA_XZ 11
#define SIGMA_XT 12
#define SIGMA_YZ 13
#define SIGMA_YT 14
#define SIGMA_ZT 15
#define CCONJ    (SIGMA_YT) /* C = g2g4 */
#define CCONJG5  (SIGMA_XZ)    /* Cg5   */
#define CCONJ5_X (-GAMMA_Z)    /* Cg5g1 */
#define CCONJ5_Y (GAMMA_T5)    /* Cg5g2 */
#define CCONJ5_Z (GAMMA_X)     /* Cg5g3 */
#define CCONJ5_T (-GAMMA_Y5)   /* Cg5g4 */
#define GAMMA_5X (-GAMMA_X5)
#define GAMMA_5Y (-GAMMA_Y5)
#define GAMMA_5Z (-GAMMA_Z5)
#define GAMMA_5T (-GAMMA_T5)
#endif

typedef Group_vec Spinor[SPIN];
typedef Complex Dirac[SPIN][SPIN];
typedef Spinor Gorkov[2];

/*** Propagator is labelled by                                ***
 *** Prop[sink_spin][source_spin][sink_colour][source_colour] ***/

typedef Group_mat Prop[SPIN][SPIN];
typedef Prop GProp[2][2];

#endif
