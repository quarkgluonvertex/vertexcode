#!/bin/sh -e
############################################################################
Ns=32
Nt=64
beta=5.29
kappas=13632
kappav=$kappas
csw=1.9192
fix=no
unpack=yes
delete=yes
compute_gluon=no
compute_fgauge=yes
compute_prop=no
save_fprop=no
write_prop=no
name=qcdsf.632
start=380
stop=580
skip=20
swap=yes
dprec=yes

############################################################################
# determine derived variables
ss=$(printf "%02d" $Ns)
tt=$(printf "%02d" $Nt)
Np=$((Ns/4))
Ne=$((Nt/4))
pp=$(printf "%02d" $Np)
ee=$(printf "%02d" $Ne)
b=$(echo $beta | awk '{print $1*100}')
bp=$(echo $beta | sed 's/\./p/')

# set directory and progam names
DATA_ROOT=/data/gamow/qg
DATADIR=$DATA_ROOT/b${bp}kp${kappas}-${Ns}x${Nt}
#DATA_ROOT=$LHOME/Data/QCDSF
#DATADIR=$DATA_ROOT/B${b}K${kappas}/V${Ns}X${Nt}
gaugedir=$DATADIR
propdir=$DATADIR/qgv.122
src=src_0_0_0_0  # enters into the propagator filename

gdir=$HOME/Research/quark-gluon/gluon
qdir=$HOME/Research/quark-gluon/quark
logdir=$qdir/LOGS
logroot=$logdir/fquark.b${b}k${kappas}
logfile=${logroot}.out
GPROG=ftrans_gluon
PROG=ftrans_prop

gfix_prec=1.0e-10
gparfile=ftrans.par
parfile=${progroot}.par
sort=2 # for quark at mu=0
nvar=3 # is sufficient at mu=0

############################################################################
# loop over all configurations
for ((cfg=start; cfg<=stop; cfg+=skip))
do
sweep=$(printf "%05d" $cfg)

############################################################################
# first unpack and fourier transform the gauge configuration if required
if [ $compute_gluon == yes -o $compute_fgauge == yes ]
then
echo fourier transforming gauge config $cfg at $(date)
echo fourier transforming gauge config $cfg at $(date) >> $logfile

if [ $unpack == yes ];
then
  cd $gaugedir
  lime_extract_record ${name}.${sweep}.lgfix.lime 2 4 ${name}_${cfg}.cfg
fi

cd $gdir

cat <<EOF > $gparfile
x_size			$Ns
y_size			$Ns
z_size			$Ns
t_size			$Nt
x_momenta		$Np
y_momenta		$Np
z_momenta		$Np
t_momenta		$Ne

action			Wilson
gauge_producer		scidac
gauge_swap_in		$swap
gauge_is_double		$dprec
gauge_dir               $gaugedir
gauge_basename          $name
beta			$beta
nflavor			2	
mass1			0.$kappas
sweep_number		$cfg

gauge_type		2
gauge_param		0.0
do_gauge_transform      no
trans_precision		$gfix_prec
fix_start		0
fix_variable	        0
max_iterations		0
gfix_param_1		0
seed			0

write_trans		no
save_fourier_fields     $compute_fgauge
save_propagator         $compute_gluon
EOF

nice $GPROG $gparfile >> $logfile 

# remove the unpacked gauge field
if [ $delete == yes ];
then
  rm $gaugedir/${name}_${cfg}.cfg
fi
fi #end if compute gluon

############################################################################
# now unpack and fourier transform the quark propagator
if [ $compute_prop == yes ]
then
echo fourier transforming propagator at $(date)
echo fourier transforming propagator $(date) >> $logfile

propname=${name}.${sweep}.prop

if [ $unpack == yes ];
then
  cd $propdir
  lime_extract_record prop.${name}.${sweep}.${src}.lime 2 3 $propname
fi

cd $qdir
cat <<END > $parfile
x_size			$Ns
y_size			$Ns
z_size			$Ns
t_size			$Nt
periodicity_in_x	yes
periodicity_in_y	yes
periodicity_in_z	yes
periodicity_in_t	no
x_momenta		$Np
y_momenta		$Np
z_momenta		$Np
t_momenta		$Ne

gauge_action		Wilson
gauge_producer		scidac
beta			$beta
nflavor			2
mass1			0.$kappas
sweep_number		$sweep

fermion_action		Clover
prop_producer		scidac
prop_swap_in            $swap
prop_basename           $name
prop_dir                $propdir
kappa			0.$kappav
csw                     $csw

do_gauge_transform	no
trans_producer		internal
gauge_type		landau
gauge_param		0.0

#dcut			1.2
#smear_type		binsize
#smearing		0.01
sort_variable		$sort
momentum_variables	$nvar
#o3_symmetrise		yes

save			$save_fprop
save_propagator		$write_prop
END

nice $PROG $parfile $propdir/$propname >> $logfile

# remove the unpacked propagator
if [ $delete == yes ];
then
  rm $propdir/$propname
fi

fi   # if compute_prop
done # loop over configs

echo all done at $(date)
