/* $Id: momenta.h,v 1.8 2009/08/19 10:14:27 jonivar Exp $ */
#ifndef _MOMENTA_H_
#define _MOMENTA_H_

#include "Real.h"
#include "lattice.h"
#include <magic.h>

#define DISCARDED -1
#define PI 3.14159265359
#define NOP 755  /* return code: nothing done */
/* magic numbers for various lattice momenta */
#define MOMQ  1
#define MOMK  2
#define MOMKK 3
#define MOMKDK   4
/* for space and time kept separate */
#define MOMQTOT 0
#define MOMQS 1
#define MOMQT 2
#define MOMKS 3
#define MOMKT 4

typedef struct {
  int nmom_in[DIR];   /* number of positive momenta in input data        */
  int nmom_out[DIR];  /* number of positive momenta in output data       */
  int L[DIR];         /* lattice size           */
  int bc[DIR];        /* boundary conditions    */
  double cutoff;      /* maximum total momentum */
  double dcut;        /* max distance from 4-diagonal (cylinder cut)     */
  double scale;       /* physical scale or similar      */
  int z3;             /* Z(3) symmetrisation on/off     */
  int o3;             /* O(3) symmetrisation on/off     */
  int z4;             /* Z(4) symmetrisation on/off     */
  int nvar;           /* number of momentum variables to return          */
  int sortvar;        /* sort/smear on momentum variable number          */
  int sort;           /* sort on/off */
  int *key;           /* sort index  */
  enum smear_type {NONE=0, BINSIZE, NBIN, NINBIN} smear;
                      /* smear over momentum values     */
  double smearing;    /* smearing distance (bin size)   */
  Real **val;         /* total momentum                 */
  int *index;         /* mapping of (pt,pz,py,px) -> ip */
  int ****itab;       /* interface for easier indexing  */
  int itabexists;
  int *nequiv;        /* number of equivalent/binned momenta for each ip */
  int nval;           /* number of momenta assigned     */
  int initialised;    /* flag for whether the arrays have been set up    */
} Momenta;


void set_default_momenta(Momenta* mom);
int setup_momenta_onaxis(Momenta *mom);
int setup_momenta(Momenta *mom);
int setup_momenta_st(Momenta *mom);
int setup_1d_momenta_list(char *filename, Momenta *mom);
int setup_smeared_momenta(Momenta *mom);
int smear_momenta(Momenta *mom, Real smearing, int sort_key);
int sort_momenta(Momenta *mom, int sort_key);
void reorder_momenta(Real *data_1d, Real ****data_4d, Momenta *mom);
void reorder_momenta_ptslice(Real *data_1d, Real ****data_4d, 
			     Momenta *mom);
void reorder_1d_momenta(Real *data_out, Real *data_in, Momenta *mom);
int re_reorder_momenta(Real *data_out, Real *data_in, 
		       Momenta *mom_in, Momenta *mom_out);

void scale_momenta(Momenta* mom, Real scale);

void free_momenta(Momenta *mom);
int nallmom(Momenta *mom);
int momindex_p(const int x[DIR], Momenta* mom);

static inline int momindex(int x[DIR], Momenta* mom) {
  const int xx[DIR] = {x[0]-mom->nmom_in[0],x[1]-mom->nmom_in[1],
		       x[2]-mom->nmom_in[2],x[3]-mom->nmom_in[3]};
  return momindex_p(xx,mom);
}


#endif
