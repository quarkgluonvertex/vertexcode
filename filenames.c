#include "gauge.h"
#include <producers.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arralloc.h>

/*****************************************************************************
 *                                                                           *
 *  Routines to construct filenames for gauge configs and transformations    *
 *  The filename is put into the Gauge_parameters.                           *
 *  No checks are performed on the parameters (other than producer_id).      *
 *                                                                           *
 *****************************************************************************/

/*** The location of all binary data.                                    ***
 *** We assume a universal directory structure                           ***
 ***  DATA_ROOT/BETAbb/VxxXtt/{gauge|prop|trans|...}/<other_params>/...  ***
 *** This should preferably be set from the environment variable         ***
 *** $DATA_ROOT                                                          ***/

#define DATA_ROOT "/scratch"
#define TRINLAT_ROOT "/data/trinlat"
#define TRINLAT_BASENAME "test";

static char data_root[FILENAME_MAX] = DATA_ROOT;
static char trinlat_root[FILENAME_MAX] = TRINLAT_ROOT;
static int  initialised = FALSE;
static int  trinlat_ini = FALSE;
static char data_dir[FILENAME_MAX] = "";
static char basename[FILENAME_MAX] = TRINLAT_BASENAME;

static int get_data_root(void) {
  char* str = getenv("DATA_ROOT");
  if (str) strcpy(data_root,str); else return 1;
  initialised = TRUE;
  return 0;
}

static int get_trinlat_root(void) {
  char* str = getenv("TRINLAT_ROOT");
  if (str) strcpy(trinlat_root,str); else return 1;
  trinlat_ini = TRUE;
  return 0;
}

static int get_basename(char* info) {
  char* sptr;
  if (sptr=strstr(info,"base:")) {
    if (sscanf(sptr,"base:%s ",basename)!=1) return 1;
  } else return 0;
  return 1;
}

/* get the data directory from the info string      */
static int get_data_dir(char* info) {
  char* sptr;
  if (sptr=strstr(info,"dir:")) {
    if (sscanf(sptr,"dir:%s ",data_dir)!=1) return 0;
  } else return 0;
  return 1;
}
static void _set_data_dir_(char* nm) { strcpy(data_dir,nm); }

#define MAX_SRC_CODE 6
#define MAX_GAU_CODE 6
#define MAX_ACT_CODE 6
#define MAX_TYP_CODE 6

static int construct_fgauge_filename(Gauge* u);

int set_data_dir(Gauge_parameters* parm)
{
  const char fnm[MAXNAME] = "set_data_dir";
  char dirname[FILENAME_MAX];

  if (!initialised) get_data_root();
  if (get_data_dir(parm->info)) return 0;

  switch (parm->producer_id) {
  case GC_OSU: /* Quenched configurations */
    sprintf(dirname,"%s/B%02d/V%02dX%02d/gauge/u_OSU_Q%02da.%03d",
	    data_root, INT(parm->beta*10),
	    parm->lattice->length[IX],parm->lattice->length[IT],
	    INT(parm->beta*10),parm->sweep);
    break;
/*** UKQCD files are timesliced, so this is the filename root ***/
  case SCIDAC: /* just use the volume for now... */
    sprintf(data_dir,"%s/%dx%d",
	    data_root,parm->lattice->length[IX],parm->lattice->length[IT]);
    break;
  case UKQCD: /* Quenched configurations, may generalise to dynamical */
  case PROD_INTERNAL: /* Use UKQCD conventions? */
  case HOT:
  case COLD: /* these two are the same as internal in this case */
    sprintf(data_dir,"%s/B%02d/V%02dX%02d",
	    data_root,INT(parm->beta*10),
	    parm->lattice->length[IX],parm->lattice->length[IT]);
    break;
  case TRINLAT:
    if (!trinlat_ini) get_trinlat_root();
    switch (parm->id) {
    case 0: /* quenched isotropic */
    sprintf(data_dir,"%s/B%02d_%dx%dx%dx%d/CFG_%d",
	    trinlat_root,INT(parm->beta*10),
	    parm->lattice->length[IX],parm->lattice->length[IY],
	    parm->lattice->length[IZ],parm->lattice->length[IT],
	    parm->sweep);
    break;
    case 1: /* quenched anisotropic */
      sprintf(data_dir,"%s/B%03d/X%03d/V%02dX%02d",data_root,
	      INT(parm->beta*100),INT(parm->mass[0]*100),
	      parm->lattice->length[IX],parm->lattice->length[IT]);
      break;
    default:
      fprintf(stderr,"%s: unknown id %d for trinlat config\n",fnm,parm->id);
      return BADPARAM;
    }
    break;
  case KRASNITZ: /* 3-d, real-time SU(2)-Higgs configurations */
    sprintf(data_dir,"%s/V%02dX%03d/B%02d/M%03d/L%03d/Ini%d",
	    data_root, parm->lattice->length[IX],INT(1000*parm->mass[0]),
	    INT(parm->beta*10),
	    INT(100*parm->mass[1]),INT(100*parm->mass[2]),
	    parm->sweep/100);
    break;
  case SJH_HMC: /* Dynamical wilson SU(2) configs */
    sprintf(data_dir,"%s/B%03d/K%04d/V%02dX%02d/MU%04d",data_root, 
	    INT(parm->beta*100),INT(10000*parm->mass[0]),
	    parm->lattice->length[IX],parm->lattice->length[IT],
	    INT(1000*parm->mass[parm->nf]));
    break;
  default:
    fprintf(stderr,"%s: unknown producer_id %d\n",fnm,
	    parm->producer_id);
    return BADPARAM;
  }
  return 0;
}

int construct_gauge_filename(Gauge* u)
{
  const char fnm[MAXNAME] = "construct_gauge_filename";

  set_data_dir(u->parm);
  memset(u->filename,0,MAXLINE);
  if (u->type==FGAUGE || u->type==FALG) return construct_fgauge_filename(u);

  switch (u->parm->producer_id) {
  case GC_OSU: /* Quenched configurations */
    sprintf(u->filename,"%s/gauge/u_OSU_Q%02da.%03d",
	    data_dir,INT(u->parm->beta*10),u->parm->sweep);
    break;
  case SCIDAC: /* Use supplied name with config number, or UKQCD conventions */
    if (get_basename(u->parm->info)) {
      sprintf(u->filename,"%s/%s_%d.cfg",data_dir,basename,u->parm->sweep);
      return 0;
    }
  case PROD_INTERNAL: /* Use supplied name, or UKQCD conventions */
    if (get_basename(u->parm->info)) {
      sprintf(u->filename,"%s/%s.cfg",data_dir,basename);
      return 0;
    }
/*** UKQCD files are timesliced, so this is the filename root ***/
  case UKQCD: /* Quenched configurations, may generalise to dynamical */
    sprintf(u->filename,"%s/gauge/Q%02dU%06d",
	    data_dir,INT(u->parm->beta*10),u->parm->sweep);
    break;
  case TRINLAT:
    if (get_basename(u->parm->info)) {
      sprintf(u->filename,"%s/%s.cfg",data_dir,basename);
      break;
    }
    switch (u->parm->id) {
    case 0:
      sprintf(u->filename,"%s/%s.cfg",data_dir,basename);
      break;
    case 1:
      sprintf(u->filename,"%s/quench.%dx%d.%d",data_dir,
	      u->parm->lattice->length[IX],u->parm->lattice->length[IT],
	      u->parm->sweep);
      break;
    case 2:
    case 3:
    case 4:
      sprintf(u->filename,"%s/%d_%0*d.cfg",data_dir,
	      u->parm->sweep/10000,u->parm->id,u->parm->sweep%10000);
      break;
    default:
      fprintf(stderr,"%s: unknown id %d for trinlat config\n",fnm,u->parm->id);
    }
    break;
  case KRASNITZ: /* 3-d, real-time SU(2)-Higgs configurations */
    sprintf(u->filename,"%s/conf%dt%d",data_dir,
	    u->parm->sweep%100, u->parm->time);
    break;
  case SJH_HMC: /* Dynamical wilson SU(2) configs */
    sprintf(u->filename,"%s/config.b%03dk%04dmu%04dj%02ds%02dt%02d.%06d",
	    data_dir,INT(u->parm->beta*100),INT(10000*u->parm->mass[0]),
	    INT(1000*u->parm->mass[u->parm->nf]),
	    INT(100*u->parm->mass[u->parm->nf+1]),
	    u->parm->lattice->length[IX],u->parm->lattice->length[IT],
	    u->parm->sweep);
    break;
  case APE_TSMB: /* Dynamical wilson SU(2) configs */
    sprintf(u->filename,"%s/binar.gauge.fd.%d",data_dir,u->parm->sweep);
    break;
  case COLD:
  case HOT:
    u->filename[0]='\0';
    printf("%s: Warning: internal start -- no filename\n",fnm);
    break;
  default:
    fprintf(stderr,"%s: unknown producer_id %d\n",fnm,
	    u->parm->producer_id);
    return BADPARAM;
  }

  return 0;
}

int construct_trans_filename(Gauge_transform* g)
{
  char gauge_code[MAX_GAU_CODE] = "\0\0\0\0\0\0";
  const char fnm[MAXNAME] = "construct_trans_filename";

  if (!initialised) get_data_root();
  switch (g->parm->producer_id) {
  case ROMA:
    sprintf(g->filename,"%s/B%02d/V%02dX%02d/gtrans/XI%d/GT_%d.0.%03d",
	    data_root,INT(g->parm->beta*10),
	    g->parm->lattice->length[IX],g->parm->lattice->length[IT],
	    INT(g->parm->gauge_param),
	    INT(g->parm->gauge_param),g->parm->sweep);
    break;
  case UKQCD:
/*** UKQCD files are timesliced, so this is the filename root ***/
    sprintf(g->filename,"%s/B%02d/V%02dX%02d/gtrans/G%02dU%06d",
	    data_root,INT(g->parm->beta*10),
	    g->parm->lattice->length[IX],g->parm->lattice->length[IT],
	    INT(g->parm->beta*10),g->parm->sweep);
    break;
  case PROD_INTERNAL:
/*** code for gauge type and parameter ***/
    switch (g->parm->gauge) {
    case LANDAU: gauge_code[0]='L'; break;
    case COVARIANT: 
      sprintf(gauge_code,"X%02d",INT(10*g->parm->gauge_param+0.01));
      break;
    case COULOMB: gauge_code[0]='C'; break;
    case UNITARY: gauge_code[0]='U'; break;
    default:
      printf("%s: gauge type %d not implemented\n",fnm,g->parm->gauge);
      printf("%s: defaulting to Landau gauge\n",fnm);
      gauge_code[0]='L';
    }
/*** we use UKQCD-type conventions ***/
/*** create filename ***/
    if (strlen(data_dir)) {
      if (g->parm->lattice->length[IT]==1) {
	sprintf(g->filename,"%s/G%02dU%06dT%04d%s", data_dir,
		INT(g->parm->beta*10),g->parm->sweep,g->parm->time,
		gauge_code);
      } else {
	switch(g->parm->nf) {
	case 0:
	sprintf(g->filename,"%s/G%02dU%06d%s", data_dir,
		INT(g->parm->beta*10),g->parm->sweep,gauge_code);
	break;
	case 2: /* assuming dynamical wilson */
	  if (!g->parm->has_chempot) {
	    sprintf(g->filename,"%s/G%02dK%04dU%06d%s", data_dir,
		    INT(g->parm->beta*100), INT(g->parm->mass[0]*10000), 
		    g->parm->sweep,gauge_code);
	  } else { /* with chemical potential and diquark src */
	    sprintf(g->filename,"%s/G%02dK%04dM%04dJ%03dU%06d%s", data_dir,
		    INT(g->parm->beta*100), INT(g->parm->mass[0]*10000), 
		    INT(g->parm->mass[2]*1000),INT(g->parm->mass[3]*1000),
		    g->parm->sweep,gauge_code);
	  }
	  break;
	default:
	  fprintf(stderr,"%s: Wrong Nf %d; only 0 and 2 implemented\n",fnm,
		  g->parm->nf);
	  return BADPARAM;
	}
      }
    } else {
      if (g->parm->lattice->length[IT]==1) {
	sprintf(g->filename,"%s/B%02d/V%02dX%02d/G%02dU%06dT%04d%s",
		data_root,INT(g->parm->beta*10), 
		g->parm->lattice->length[IX],g->parm->lattice->length[IT],
		INT(g->parm->beta*10),g->parm->sweep,g->parm->time,gauge_code);
      } else {
	sprintf(g->filename,"%s/B%02d/V%02dX%02d/G%02dU%06d%s",data_root,
		INT(g->parm->beta*10),
		g->parm->lattice->length[IX],g->parm->lattice->length[IT],
		INT(g->parm->beta*10),g->parm->sweep,gauge_code);
      }
    }
    break;
  default:
    fprintf(stderr,"%s: unknown producer_id %d\n",fnm,g->parm->producer_id);
    return BADPARAM;
  }

  return 0;
}

static int construct_fgauge_filename(Gauge* u)
{
  const char fnm[MAXNAME] = "construct_fgauge_filename";
  char gauge_code[MAX_GAU_CODE] = "\0\0\0\0\0\0";
  char type_code[MAX_TYP_CODE];
  char filename[FILENAME_MAX];
  const int nf = u->parm->nf;

  if (!initialised) get_data_root();

  switch (u->type) {
  case FGAUGE:
    strcpy(type_code,"UK"); break;
  case FALG:
    strcpy(type_code,"AK"); break;
  default:
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u->type);
    return BADPARAM;
  }
/*** code for gauge type and parameter ***/
  switch (u->parm->gauge) {
  case LANDAU: gauge_code[0]='L'; break;
  case COVARIANT: 
    sprintf(gauge_code,"X%02d",INT(10*u->parm->gauge_param+0.01));
    break;
  case COULOMB: gauge_code[0]='C'; break;
  case UNITARY: gauge_code[0]='U'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   u->parm->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='L';
  }
/*** create filename ***/
  switch (nf) {
  case 0:
    sprintf(filename,"%s%03dU%06d%s",
	    type_code,INT(u->parm->beta*100),u->parm->sweep,gauge_code);
    break;
  case 2:
    if (u->parm->has_chempot) {
      sprintf(filename,"%s%03dK%04dMU%04dJ%03dU%06d%s",
	      type_code,INT(u->parm->beta*100),INT(u->parm->mass[0]*10000),
	      INT(u->parm->mass[nf]*1000),INT(u->parm->mass[nf+1]*1000),
	      u->parm->sweep,gauge_code);
    } else {
      sprintf(filename,"%s%03dK%04dU%06d%s",
	      type_code,INT(u->parm->beta*100),INT(u->parm->mass[0]*10000),
	      u->parm->sweep,gauge_code);
    }
    break;
  default:
    fprintf(stderr,"%s: Wrong Nf %d; only 0 and 2 implemented\n",fnm,nf);
    return BADPARAM;
  }
  if (strlen(data_dir)) {
    sprintf(u->filename,"%s/%s",data_dir,filename);
  } else {
    sprintf(u->filename,"%s/B%03d/V%02dX%02d/fgauge/%s",
	    data_root,INT(u->parm->beta*100),
	    u->parm->lattice->length[IX],u->parm->lattice->length[IT],
	    filename);
  }
  return 0;
}

/**********************************************************************
 * setup_configs: function to read a list of config numbers from file *
 *  Each line consists either of a single number (config number)      *
 *  or three numbers  <start> <stop> <skip>                           *
 *  skip may be omitted in which case it is set to 1                  *
 **********************************************************************/
#define MAXCONFIG 50000
int setup_configs(int** config, int* nconfig, char* filename)
{
  const char fnm[MAXNAME] = "setup_configs";
  int i, n, nr, iline=1;
  int start, stop, skip;
  int cfg[MAXCONFIG];
  char line[MAXLINE];
  FILE* in;

  if (!(in=fopen(filename,"r"))) {
    fprintf(stderr,"%s: cannot open <%s>\n",fnm,filename);
    return BADFILE;
  }

  n=0;
  while (fgets(line,MAXLINE,in)) {
    nr = sscanf(line,"%d %d %d",&start,&stop,&skip);
    switch (nr) {
    case 1: cfg[n++] = start; break;
    case 2: skip=1;
    case 3:
      for (i=start;i<=stop;i+=skip) cfg[n++]=i;
      break;
    default:
      fprintf(stderr,"%s: error on line %d on <%s>\n",fnm,iline,filename);
      return BADFILE;
    }
    iline++;
  }

  *nconfig = n;
  *config = vecalloc(sizeof(int),n);
  for (i=0;i<n;i++) (*config)[i] = cfg[i];

  return 0;
}
#undef MAXCONFIG
