/* $Id: swap.c,v 1.1 2002/06/28 06:49:02 jskuller Exp $ */

/* 
 * Routines used to byte-swap data read in from file, if necessary.
 */

#include "swap.h"

char swap_id[] = "$Id: swap.c,v 1.1 2002/06/28 06:49:02 jskuller Exp $";
#define TRUE 1
#define FALSE 0

int swap(int source)
{
  union swapper {
    int integer;
    char pos[4];
  } a, b;
  
  a.integer = source;
  b.pos[0] = a.pos[3];
  b.pos[1] = a.pos[2];
  b.pos[2] = a.pos[1];
  b.pos[3] = a.pos[0];
    
  return b.integer;
}

void block_swap(int *buffer, size_t length)
{
  size_t i;
  register union swapper {
    int integer;
    char pos[4];
  } a, b;
  
  for( i=0 ; i < length ; i++) {
    a.integer = *buffer;
    b.pos[0] = a.pos[3];
    b.pos[1] = a.pos[2];
    b.pos[2] = a.pos[1];
    b.pos[3] = a.pos[0];
    *buffer  = b.integer;
    buffer++;
  }
}

double swap_double(double source)
{
  union double_swapper {
    double double_number;
    char   pos[8];
  } a, b;
  
  a.double_number = source;
  b.pos[0] = a.pos[7];
  b.pos[1] = a.pos[6];
  b.pos[2] = a.pos[5];
  b.pos[3] = a.pos[4];
  b.pos[4] = a.pos[3];
  b.pos[5] = a.pos[2];
  b.pos[6] = a.pos[1];
  b.pos[7] = a.pos[0];
    
  return b.double_number;
}



void block_swap_double(double *buffer, size_t length)
{
  size_t i;
  register union swapper {
    double double_number;
    char pos[8];
  } a, b;
  
  for( i=0 ; i < length ; i++) {
    a.double_number = *buffer;
    b.pos[0] = a.pos[7];
    b.pos[1] = a.pos[6];
    b.pos[2] = a.pos[5];
    b.pos[3] = a.pos[4];
    b.pos[4] = a.pos[3];
    b.pos[5] = a.pos[2];
    b.pos[6] = a.pos[1];
    b.pos[7] = a.pos[0];
    *buffer  = b.double_number;
    buffer++;
  }
}

/*** byte_swap swaps a strip of length/size items size bytes            ***/
/*** this is the only routine that needs to be visible from the outside ***/
int byte_swap(void* strip, size_t size, size_t length)
{
  switch (size) {
  case sizeof(float):   /* = 4 */
    block_swap(strip,length/size);
    break;
  case sizeof(double):  /* = 8 */
    block_swap_double(strip,length/size);
    break;
  case 1:
    break;
  default:  /* ERROR: unknown size!! */
    return -1;
  }

  return 0;
}
