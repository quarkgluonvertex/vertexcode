#include "analysis.h"

static int grace_version = 50110;
static int grace_v_set = FALSE;

const char typestr[MAXERRBAR_T][MAXNAME]
  = {"xy","xydy","xydx","xydxdy","xydydy","xydxdx","xydxdxdydy"};

static void get_grace_version(void) {
  char* str = getenv("GRACE_VERSION");
  if (str) sscanf(str,"%d",&grace_version);
  grace_v_set = TRUE;
}

static int write_preamble_grace(FILE* fp, PlotOptions* opt);

int write_preamble(FILE* fp, PlotOptions* opt)
{
  const char fnm[MAXNAME] = "write_preamble";
  if (!opt) return 0;
  switch (opt->graph_type) {
  case RAW: return 0;
  case GRACE: return write_preamble_grace(fp,opt);
  default:
    fprintf(stderr,"%s: graph type %d not implemented\n",fnm,opt->graph_type);
    fprintf(stderr,"%s: not writing any preamble\n",fnm);
    return BADPARAM;
  }
}

int write_separator(FILE* fp, PlotOptions* opt, int set)
{
  const char fnm[MAXNAME] = "write_separator";

  if (!opt) { fprintf(fp,"\n"); return 0; }
  switch (opt->graph_type) {
  case RAW: fprintf(fp,"\n"); return 0;
  case GRACE: 
    fprintf(fp,"&\n");
    if ( set && set<opt->nset && opt->errorbars[set]!=opt->errorbars[set-1] ) {
      fprintf(fp,"@type %s\n",typestr[opt->errorbars[set]]);
    }
    return 0;
  default:
    fprintf(stderr,"%s: graph type %d not implemented\n",fnm,opt->graph_type);
    fprintf(stderr,"%s: writing blank line as separator\n",fnm);
    fprintf(fp,"\n");
    return BADPARAM;
  }

}

static int write_preamble_grace(FILE* fp, PlotOptions* opt)
{
  const char fnm[MAXNAME] = "write_preamble_grace";
  const char scalestr[MAXSCALE_T][MAXNAME] = {"Normal","Logarithmic"};
  const int maxsymb = 12;
  int i;

  if (!grace_v_set) get_grace_version();
  fprintf(fp,"@version %d\n",grace_version);
  fprintf(fp,"@g0 on\n@with g0\n");
  if (opt->xmin != UNSET_DBL) fprintf(fp,"@ world xmin %g\n",opt->xmin);
  if (opt->xmax != UNSET_DBL) fprintf(fp,"@ world xmax %g\n",opt->xmax);
  if (opt->ymin != UNSET_DBL) fprintf(fp,"@ world ymin %g\n",opt->ymin);
  if (opt->ymax != UNSET_DBL) fprintf(fp,"@ world ymax %g\n",opt->ymax);
  /** the following gives the size of the graph window **/
  fprintf(fp,"@ view xmin 0.15\n@ view xmax 1.15\n");
  fprintf(fp,"@ view ymin 0.15\n@ view ymax 0.85\n");
  fprintf(fp,"@ title \"%s\"\n",opt->title);

  /** x and y axis layout options **/
  fprintf(fp,"@ xaxes scale %s\n@ yaxes scale %s\n",
	  scalestr[opt->xscale],scalestr[opt->yscale]);
  if (opt->xgrid != UNSET_DBL) fprintf(fp,"@ xaxis tick major %g\n",
				       opt->xgrid);
  if (opt->xtick != UNSET_INT) fprintf(fp,"@ xaxis tick minor ticks %d\n",
				       opt->xtick);
  fprintf(fp,"@ xaxis label \"%s\"\n",opt->xaxis);
  if (opt->ygrid != UNSET_DBL) fprintf(fp,"@ yaxis tick major %g\n",
				       opt->ygrid);
  if (opt->xtick != UNSET_INT) fprintf(fp,"@ yaxis tick minor ticks %d\n",
				       opt->ytick);
  fprintf(fp,"@ yaxis label \"%s\"\n",opt->yaxis);
  if (opt->legend) fprintf(fp,"@ legend on\n");

  /** loop over data sets **/
  for (i=0;i<opt->nset;i++) {
    fprintf(fp,"@ s%d type %s\n",i,typestr[opt->errorbars[i]]);
    fprintf(fp,"@ s%d symbol %d\n",i,opt->symbol[i]%maxsymb);
    fprintf(fp,"@ s%d symbol size %f\n",i,opt->size[i]);
    fprintf(fp,"@ s%d symbol color %d\n",i,opt->colour[i]);
    fprintf(fp,"@ s%d symbol fill color %d\n",i,opt->colour[i]);
    fprintf(fp,"@ s%d symbol fill pattern %d\n",i,opt->symbol[i]/maxsymb);
    fprintf(fp,"@ s%d line type 1\n",i);
    fprintf(fp,"@ s%d linestyle %d\n",i,opt->line[i]);
    fprintf(fp,"@ s%d line color %d\n",i,opt->colour[i]);
    if (opt->errorbars[i]) {
      fprintf(fp,"@ s%d errorbar on\n",i);
      fprintf(fp,"@ s%d errorbar color %d\n",i,opt->colour[i]);
      fprintf(fp,"@ s%d errorbar size %f\n",i,opt->size[i]);
    }
    if (opt->legend) fprintf(fp,"@ s%d legend \"%s\"\n",i,opt->legend[i]);
  }
  fprintf(fp,"@type %s\n",typestr[opt->errorbars[0]]);
  return 0;
}
