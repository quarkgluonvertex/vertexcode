#include "fermion.h"
#include <stdio.h>
#include <array.h>

static int gauge_trans_spincpt(Propagator,Gauge_transform);
static int gauge_trans_gorkovprop(Propagator,Gauge_transform);

int init_prop(Propagator* p, Propagator_parameters* ppar, 
	      Space_t sp, Prop_t type)
{
  const char fnm[MAXNAME] = "init_prop";
  Lattice* lat = ppar->gpar->lattice;
  int i;

  p->parm = ppar;
  p->type = type;
  p->space = sp;
  switch (type) {
  case SPINOR: p->elsz = sizeof(Spinor); break;
  case PROP:   p->elsz = sizeof(Prop); break;
  case COLMAT: p->elsz = sizeof(Group_mat); break;
  case GORKOV: p->elsz = sizeof(Gorkov); break;
  case GORKOVPROP:   p->elsz = sizeof(GProp); break;
  default:
    fprintf(stderr,"%s: type %d not implemented\n",fnm,type);
    return BADPARAM;
  }

  if (sp==TSLICE_P || sp==FOURIER || sp==FOURIERSLICE) {
    const int ndir = (sp==TSLICE_P) ? lat->ndim-1 : lat->ndim;
    fprintf(stderr,"%s: Warning: initialising nmom from defaults\n",fnm);
    p->npoint=1;
    for (i=0;i<ndir;i++) {
      p->nmom[i] = lat->length[i]/4;
      if (ppar->bcs[i]==PERIODIC) p->ntotmom[i]=2*p->nmom[i]+1;
      else p->ntotmom[i]=2*p->nmom[i];
      p->npoint *= p->ntotmom[i];
    }
    if (sp==FOURIERSLICE) p->npoint = p->ntotmom[IT];
    for (i=ndir;i<DIR;i++) {
    /* Set dummy values for momenta */
      p->nmom[i] = -1;
      p->ntotmom[i] = 0;
    }
  } else {
    /* Set dummy values for momenta */
    for (i=0;i<DIR;i++) {
      p->nmom[i] = -1;
      p->ntotmom[i] = 0;
    }
    if (sp==TSLICE_X) {
      p->npoint = lat->nspatial;
      p->tslice = 0; /* default initial value */
    } else {
      p->npoint = lat->nsite;
      p->tslice = -1; /* dummy value */
    }
  }

  if (sp==TSLICE_P) {
    p->prop = arralloc(p->elsz,2,lat->length[IT],p->npoint);
  } else {
    p->prop = vecalloc(p->elsz,p->npoint);
  }
  if (!p->prop) {
    fprintf(stderr,"%s: cannot allocate space\n",fnm);
    return NOSPACE;
  }

/* set dummy values for spin, colour and position */
  p->s1 = p->s2 = p->c1 = p->c2 = -1;
  for (i=0;i<DIR;i++) p->x[i] = -1;

/*** Construct the filename and return ***/
  return (construct_prop_filename(p));
}

int init_fpropslice(Propagator* p, Propagator_parameters* ppar,
		    int* nmom, int* mom)
{
  const char fnm[MAXNAME] = "init_fpropslice";
  int i;
  p->parm = ppar;
  p->type = PROP; /* could be generalised... */
  p->elsz = sizeof(Prop);
  p->space = FOURIERSLICE;

  for (i=0;i<DIR;i++) {
    p->nmom[i] = nmom[i];
    p->ntotmom[i] = (ppar->bcs[i]==PERIODIC) ? 2*nmom[i]+1 : 2*nmom[i];
    /* set position: mom is in (-nmom,...,nmom), x in (0,ntotmom) */
    p->x[i] = mom[i]+nmom[i];
  }
  p->npoint = p->ntotmom[IT];
  p->x[IT] = -1;

  p->prop = vecalloc(p->elsz,p->npoint);
  if (!p->prop) {
    fprintf(stderr,"%s: cannot allocate space\n",fnm);
    return NOSPACE;
  }

/* set dummy values for spin and colour */
  p->s1 = p->s2 = p->c1 = p->c2 = -1;

/*** Construct the filename and return ***/
  return (construct_prop_filename(p));
}

void prop_delete(Propagator* s)
{
  int i;
  for (i=0;i<DIR;i++) s->nmom[i] = s->ntotmom[i] = -1;
  s->tslice = -1;
  s->npoint = 0;
  s->filename[0] = '\0';
  free(s->prop);
  s->prop = 0;
}

int gauge_trans_prop(Propagator s, Gauge_transform g)
{
  const char fnm[MAXNAME] = "gauge_trans_prop";
  Lattice* lat = s.parm->gpar->lattice;
  Prop* p = s.prop;
  Group_mat tmp;
  int x, xg, s1, s2;
  const int nsite = (s.space==XSPACE) ? lat->nsite : lat->nspatial;
  const int t = (s.space==XSPACE) ? 0 : s.tslice;

  if (s.space!=XSPACE && s.space!=TSLICE_X) {
    fprintf(stderr,"%s: propagator not in position space\n",fnm);
    return BADDATA;
  }
  if (s.type==COLMAT) return gauge_trans_spincpt(s,g);
  if (s.type==GORKOVPROP) return gauge_trans_gorkovprop(s,g);
  if (s.type!=PROP) {
    fprintf(stderr,"%s: wrong propagator type %d\n",fnm,s.type);
    return BADDATA;
  }
  if (g.parm->lattice != lat) {
    fprintf(stderr,"%s: incompatible lattices\n",fnm);
    return BADDATA;
  }

  xg = t*lat->nspatial; /* offset for tsliced quark prop */
  for (x=0;x<nsite;x++,xg++) {
    for (s1=0;s1<SPIN;s1++)
      for (s2=0;s2<SPIN;s2++) {
#ifdef SU2_GROUP
	su2mat_mult_dagsu2(tmp,p[x][s1][s2],g.g[0]);
	su2mat_su2_mult(p[x][s1][s2],g.g[xg],tmp);
#else
	grpmat_mult_dag(tmp,p[x][s1][s2],g.g[0]);
	grpmat_mult(p[x][s1][s2],g.g[xg],tmp);
#endif
      }
  }
/*** Assign gauge fixing parameters to s ***/
  s.parm->gpar->gauge           = g.parm->gauge;
  s.parm->gpar->gauge_param     = g.parm->gauge_param;
  s.parm->gpar->rtrace          = g.parm->rtrace;
  s.parm->gpar->gfix_iterations = g.parm->gfix_iterations;
  for (x=0; x<MAXPREC; x++)
    s.parm->gpar->gfix_precision[x] = g.parm->gfix_precision[x];
  return 0;
}

static int gauge_trans_gorkovprop(Propagator s, Gauge_transform g)
{
  const char fnm[MAXNAME] = "gauge_trans_prop";
#ifdef SU2_GROUP
  Lattice* lat = s.parm->gpar->lattice;
  GProp* p = s.prop;
  Group_mat tmp;
  int x, xg, s1, s2;
  const int nsite = (s.space==XSPACE) ? lat->nsite : lat->nspatial;
  const int t = (s.space==XSPACE) ? 0 : s.tslice;
  if (g.parm->lattice != lat) {
    fprintf(stderr,"%s: incompatible lattices\n",fnm);
    return BADDATA;
  }

  xg = t*lat->nspatial; /* offset for tsliced quark prop */
  for (x=0;x<nsite;x++,xg++) {
    for (s1=0;s1<SPIN;s1++)
      for (s2=0;s2<SPIN;s2++) {
	/* normal, psi_1 psibar_1 */
	su2mat_mult_dagsu2(tmp,p[x][0][0][s1][s2],g.g[0]);
	su2mat_su2_mult(p[x][0][0][s1][s2],g.g[xg],tmp);
	/* anomalous, psibar_2^T psibar_1 */
	su2mat_mult_dagsu2(tmp,p[x][1][0][s1][s2],g.g[0]);
	su2mat_starsu2_mult(p[x][1][0][s1][s2],g.g[xg],tmp);
	/* anomalous, psi_1 psi_2^T */
	su2mat_mult_transsu2(tmp,p[x][0][1][s1][s2],g.g[0]);
	su2mat_su2_mult(p[x][0][1][s1][s2],g.g[xg],tmp);
	/* normal backward, psibar_2^T psi_2^T */
	su2mat_mult_transsu2(tmp,p[x][1][1][s1][s2],g.g[0]);
	su2mat_starsu2_mult(p[x][1][1][s1][s2],g.g[xg],tmp);
      }
  }
/*** Assign gauge fixing parameters to s ***/
  s.parm->gpar->gauge           = g.parm->gauge;
  s.parm->gpar->gauge_param     = g.parm->gauge_param;
  s.parm->gpar->rtrace          = g.parm->rtrace;
  s.parm->gpar->gfix_iterations = g.parm->gfix_iterations;
  for (x=0; x<MAXPREC; x++)
    s.parm->gpar->gfix_precision[x] = g.parm->gfix_precision[x];
  return 0;
#else
  fprintf(stderr,"%s: only implemented for SU(2)!\n",fnm);
  return EVIL;
#endif
}

static int gauge_trans_spincpt(Propagator s, Gauge_transform g)
{
  const char fnm[MAXNAME] = "gauge_trans_spincpt";
  Lattice* lat = s.parm->gpar->lattice;
  const int nsite = lat->nspatial;
  Group_mat* p = s.prop;
  Group_mat tmp;
  int x;

  if (g.parm->lattice != lat) {
    fprintf(stderr,"%s: incompatible lattices\n",fnm);
    return BADDATA;
  }

  for (x=0;x<nsite;x++) {
#ifdef SU2_GROUP
    su2mat_mult_dagsu2(tmp,p[x],g.g[0]);
    su2mat_su2_mult(p[x],g.g[x],tmp);
#else
    grpmat_mult_dag(tmp,p[x],g.g[0]);
    grpmat_mult(p[x],g.g[x],tmp);
#endif
  }
/*** Assign gauge fixing parameters to s ***/
  s.parm->gpar->gauge           = g.parm->gauge;
  s.parm->gpar->gauge_param     = g.parm->gauge_param;
  s.parm->gpar->rtrace          = g.parm->rtrace;
  s.parm->gpar->gfix_iterations = g.parm->gfix_iterations;
  for (x=0; x<MAXPREC; x++)
    s.parm->gpar->gfix_precision[x] = g.parm->gfix_precision[x];
  return 0;
}

