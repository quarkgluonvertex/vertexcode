/* $Id: uncompress.c,v 1.1 2002/08/07 09:39:59 jskuller Exp $ */

/***********************************************************************
 *                                                                     *
 *  Routines to uncompress gauge configurations from two rows to three *
 *  Adapted from mehegan / UKQCD(maxwell code)                         *
 *                                                                     *
 ***********************************************************************/

#include "su3.h"
void generate_final_vector(Complex final_vec[COLOUR],
			Complex v[COLOUR], Complex w[COLOUR]);

void uncompress_Su3(Su3* strip, int length)
{
  Su3* result;
  Short_Su3* source;
  
  int i,j,k;

  /* We have to perform this loop backwards to make sure
   * we never overwrite any data we will need later.
   */
  result = (Su3*) strip;
  result += length;                /* one greater than what we need */
  source = (Short_Su3*) strip;
  source += length;                /* one greater than what we need */
  for (i=length; i>0; --i) {
    --result; --source;
    for( j=1 ; j >= 0 ; j-- ) {  /* This backwards as well for *
				  * next to last result        */
      for( k=0 ; k < 3 ; k++) {
	(*result)[j][k] = (*source)[j][k];
      }
    }
  }
  /* Now reconstruct the last row */
  result = (Su3 *) strip;
  for (i=0; i<length; i++) {
    generate_final_vector((*result)[2], (*result)[0], (*result)[1]);
    result++;
  }

}
  
void uncompress_Su3_trans(Su3* strip, int length)
{
  Su3* result;
  Short_Trans_Su3* source;
  Su3 tmp;

  int i,j,k;

/*** We have to perform this loop backwards to make sure ***
 *** we never overwrite any data we will need later.     ***/
  result = (Su3 *) strip;
  result += length;
  source = (Short_Trans_Su3 *) strip;
  source += length;
  for( i=length ; i>1; --i ) {   /* last result must be treated separately */
    --result; --source;
    for( j=COLOUR-2 ; j >= 0 ; j-- )  /* This backwards as well for *
				       * next to last result        */
      for( k=COLOUR-1 ; k >=0 ; k--) {
	(*result)[j][k] = (*source)[k][j];
      }
  }
  /* Last result */
  for (j=0; j<COLOUR-1; j++)
    for (k=0; k<COLOUR; k++) {
      tmp[j][k] = (*source)[k][j];
    }
  su3_copy(*result,tmp);

  /* Now reconstruct the last row */
  result = (Su3 *) strip;
  for(i=0 ; i<length; i++) {
    generate_final_vector((*result)[2], (*result)[0], (*result)[1]);
    result++;
  }

}
  
/* ~generate_final_vector: lets final_vec be the cross product of v* and w*
 *	i.e. final_vec[i] = E[ijk] v*[j] w*[k]
 */

void generate_final_vector(Complex final_vec[COLOUR],
			Complex v[COLOUR], Complex w[COLOUR])
{

	final_vec[0].re = + ( (v[1].re * w[2].re) - (v[1].im * w[2].im) ) 
			  - ( (v[2].re * w[1].re) - (v[2].im * w[1].im) );

	final_vec[0].im = - ( (v[1].re * w[2].im) + (v[1].im * w[2].re) ) 
			  + ( (v[2].re * w[1].im) + (v[2].im * w[1].re) );


	final_vec[1].re = - ( (v[0].re * w[2].re) - (v[0].im * w[2].im) ) 
			  + ( (v[2].re * w[0].re) - (v[2].im * w[0].im) );

	final_vec[1].im = + ( (v[0].re * w[2].im) + (v[0].im * w[2].re) ) 
			  - ( (v[2].re * w[0].im) + (v[2].im * w[0].re) );


	final_vec[2].re = + ( (v[0].re * w[1].re) - (v[0].im * w[1].im) ) 
			  - ( (v[1].re * w[0].re) - (v[1].im * w[0].im) );

	final_vec[2].im = - ( (v[0].re * w[1].im) + (v[0].im * w[1].re) ) 
			  + ( (v[1].re * w[0].im) + (v[1].im * w[0].re) );

}
