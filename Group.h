#ifndef _GROUP_H_
#define _GROUP_H_

/************************************************************************
 *                                                                      *
 *  Definition of the gauge group.                                      *
 *  Change this file when changing between SU(2) and SU(3) (or any      *
 *  other group).  The gauge field structures and many of the           *
 *  high-level may then be used unchanged.                              *
 *                                                                      *
 ************************************************************************/

#define SU2 2
#define SU3 3
/** add other groups as needed **/

#if  ! ( defined SU2_GROUP ) && ! ( defined SU3_GROUP )
#if 1  /* set to 1 for SU(2) */
#define SU2_GROUP
#endif
#if 0  /* set to 1 for SU(3) */
#define SU3_GROUP
#endif
#endif

/**********  SU(2)  **********/
#ifdef SU2_GROUP
#ifdef GROUP
#error GROUP already defined!
#else
#define GROUP SU2
#include "su2.h"
typedef Su2      Group_el;
typedef Su2mat   Group_mat;
typedef Su2vec   Group_vec;
typedef Su2alg   Algebra_el;
typedef Su2alg_c Algebra_c;
#endif
#endif

/**********  SU(3)  **********/
#ifdef SU3_GROUP
#ifdef GROUP
#error GROUP already defined!
#else
#define GROUP SU3
#include "su3.h"
typedef Su3      Group_el;
typedef Su3mat   Group_mat;
typedef Su3vec   Group_vec;
typedef Su3alg   Algebra_el;
typedef Su3alg_c Algebra_c;
#endif
#endif

#endif
