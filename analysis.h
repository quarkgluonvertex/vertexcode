#ifndef _ANALYSIS_H_
#define _ANALYSIS_H_
#include "Real.h"
#include <magic.h>
#include <array.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <math.h>

#define FREEZE_ARRAY_CORRUPTED  -1
#define CORRUPTION -1
#define BOOT_CORRUPTED -1
#define PARAM_COVARIANCE_ERROR  2
#define ERROR_DELTA_PARAM 3
#define TRY_AGAIN 4
#define MAX_ITER 1000
#define MASK(arr,i) ( (arr) ? arr[(i)] : TRUE )

#define UNSET_DBL DBL_MAX
#define UNSET_INT INT_MIN

#ifndef __bool_true_false_are_defined
static const int true  = TRUE;
static const int false = FALSE;
#endif

typedef struct {
  int nboot;
  int nconfig;
  int** config_list;
} Bootinfo;

typedef struct {
  Real best;
  int nboot;
  Real* boot;
  Bootinfo* info;
} Boot;

typedef int  Datafunction(Real* y, Real x, Real* arg, int narg);
typedef void Modelfunction(Real x, Real* fitparam,
                           Real* y, Real* dyda, Real** d2yda2, Real* dydx,
			   int nparam);
typedef void Guessfunction(Real* fitparam, int* freeze, int nparam,
                           Real* x, Real* y, int ndata);

/* Structure used for fitting different data (bundled together) to     *
 * the same function(s).  The offsets determine which bundle(s) to fit */
typedef struct {
  Datafunction* func;
  int voffset;
  int soffset;
} OffsetDatafunction;

/* Types of vector and scalar data: This determines what kind of parameters *
 * are associated with the data.  More types can be added as necessary      */
typedef enum {UNKNOWN=0, HIGGS, GAUGE_D, FERMION, MESON} Data_t;

typedef struct {
  Data_t type;
  void* parm;
  int nconfig;
  int length;            /* number of data points, eg time slices            */
  int nscalar;           /* scalar_data: data which vary between configs     */
  double** scalar_data;  /*              but not between data points         */
  int nvector;           /* vector_data: data which vary between configs     */
  double*** vector_data; /*              as well as between data points      */
  int nsparm;
  double* scalar_parm;   /* scalar_parm: constant parameters                 */
  int nvparm;            /* vector_parm: data which are not config-dependent */
  double** vector_parm;  /*              but do vary between data points     */
  double* xdata;
  char basename[MAXNAME];
  char name[MAXLINE];
  char filename[MAXLINE];
} VectorData;

/* structure used in the internal chi-square fitting routines */
typedef struct {
  int nparam;
  Real* param;
  Real** paramcov;
  Real chisq;
  int* freeze;           /* boolean array: which parameters are frozen */
  int correlate;
  Modelfunction* model;
  int ndata;
  Real* xdata;
  Real* ydata;
  Real** xdatacov;       /* covariances */
  Real** ydatacov;
  Real* sigmax;          /* standard deviations */
  Real* sigmay;
  Real** inv_data_corr;  /* inverse correlation matrix */
  Real* yfit;            /* fit values */
  Real** dyda;           /* fit function derivs wrt parameters         */
  Real*** d2yda2;        /* (for use in Levenberg-Marquardt algorithm) */
  Real* dydx;
} FitData;

/* Top-level structure for fitting data */
typedef struct {
  int nparam;   /* number of fit parameters */
  int ndata;    /* number of data points    */
  int* subset;  /* boolean array: subset of points to fit */
  int nboot;
  Bootinfo* bootinfo;
  Boot* param;
  Boot** paramcov;
  Boot chisq;
  long seed;    /* random seed for bootstrap samples */
  int* freeze;  /* boolean array: which parameters are frozen */
  int ndof;     /* degrees of freedom */
  int correlate;
  Datafunction* xfunc;
  Datafunction* yfunc;
  int xoffset;  /* offset values for fitting several data bundles */
  int xsoffset; /* to the same function                           */
  int yoffset;
  int ysoffset;
  Modelfunction* model;
  Guessfunction* guess;
} BootFitFuncs;

typedef struct {
  int  nconfig;
  int  ncorr;
  int  nplot;
  int* plot;
  int  single_plot;
  int  nfit;
  int* fit;
  double* cut;         /* for imposing cuts on data, eg cylinder cut  */
  double* x_fit_min;
  double* x_fit_max;
  bool correlated_fit;
  int  nboot;
  int  seed;           /* random number seed for use in bootstrapping */
  int  write_boot;
  char data_dir[MAXLINE];
  char plotopt_file[MAXLINE];
} Analysis_params;

/*** known plot formats -- add more as required ***/
typedef enum {RAW=0, GRACE, GNU, AXIS} Graph_t;
typedef enum {LINEAR=0, LOG} Scale_t;
typedef enum {NOERR=0, DY, DX, DXY, DYY, DXX, DXXYY} Errorbar_t;
#define MAXGRAPH_T 4
#define MAXSCALE_T 2
#define MAXERRBAR_T 7
#define DEFAULT_SIZE 0.8
#define MAXSUFX 6

static const char suffix[MAXGRAPH_T][MAXSUFX] = {".raw",".agr",".gnu",".ax"};

typedef struct {
  Graph_t graph_type;
  char title[MAXLINE];
  /** options for the layout of the axes **/
  Scale_t xscale;
  Scale_t yscale;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
  double xgrid;           /* distance between 'major ticks' on xaxis */
  double ygrid;           /* distance between 'major ticks' on yaxis */
  int xtick;              /* number of 'minor ticks' on xaxis */
  int ytick;              /* number of 'minor ticks' on yaxis */
  char xaxis[MAXLINE];
  char yaxis[MAXLINE];
  /** options governing the look of the data **/
  int nset;
  Errorbar_t* errorbars;
  int* colour;            /* we assume the same colour for symbols,   *
			   * errorbars, connecting lines, etc         */
  int* symbol;
  float* size;            /* all sizes (symbol, errorbar etc) scale equally */
  int* line;              /* linestyle (if any)  */
  char** legend;
} PlotOptions;

int init_vectordata(VectorData* ths, void* parm, Data_t type, int len,
		    int nconf, int nscal, int nvect, 
		    const char* nm, const char* dir);
int vectordata_add_pars(VectorData* ths, int nspar, int nvpar);
int read_vectordata(VectorData* ths, int* mask, int nread);

/*** parameter assignment functions ***/
int assign_params_analysis(const char* filename, 
			   void* parm, Analysis_params* run);
int assign_params_potential(const char* filename, 
			    void* parm, Analysis_params* run);
/* NOTE KLUDGE FEATURE: physical parameters are passed as void* to keep *
 * it general and since this file may not know about the types defined  *
 * in field_params.h or other places                                    */
int assign_plotoptions(const char* filenam, PlotOptions* opt);

/*** statistics functions ***/
void vector_average(VectorData* this, int* cmask, int* xmask,
		    double** vec_ave, double* scal_ave);
void vector_average_all(VectorData* this, int* mask, int* xmask,
			double* vec_ave, double* scal_ave);
void vector_average_n(VectorData*, int n, int* cmask, int* xmask, double* ave);
void data_average(Real* ave, Real** data, int nset, int len);

int vector_fit(VectorData* ths, BootFitFuncs* fit, int* mask);
int vector_fit_multiensemble(VectorData* ths, BootFitFuncs* fit, int* ncfg);
int fit_bootdata(Boot* x, Boot* y, BootFitFuncs* fit);
int fit_ybootdata(Real* x, Boot* y, BootFitFuncs* fit);

void datafunc_cov_ave(VectorData* ths, Datafunction* func, 
		      double* ave, double** cov, int* cmask, int* xmask);
void offsetfunc_cov_ave(VectorData* this, OffsetDatafunction* func, 
		       double* ave, double** cov, int* cmask, int* xmask);
void datafunc_average(VectorData* ths, Datafunction* func, double* average,
		      int* cmask, int* xmask);
void datafunc_average_offset(VectorData*, Datafunction*, int voff, int soff,
			     double* average, int* cmask, int* xmask);
void find_minmax(Real* data, int ndata, Real* min, Real* max);
int ntrue(int* mask, int len);

/*** graph functions ***/
void vector_graph(VectorData*,Datafunction**,int,
		  PlotOptions*,const char*,int*,int**);
void offset_graph(VectorData*,OffsetDatafunction*,int,
		  PlotOptions*,const char*,int*,int**);
int bootstrap_graph(Boot* xdata, Boot* ydata, int ndata, BootFitFuncs* fit, 
		    PlotOptions* opt, const char* name);
int ybootstrap_graph(Real*,Boot*,int,BootFitFuncs*,PlotOptions*,const char*);
int yboot_2d_graph(Real* xdata, Real* ydata, Boot** yboot, 
		   int nxdata, int nydata, int** mask, int* xkey, int* ykey,
		   char* name);
int write_preamble(FILE* fp, PlotOptions* opt);
int write_separator(FILE* fp, PlotOptions* opt, int n);

/*** bootstrap functions ***/
Boot init_boot(int nboot);
void destroy_boot(Boot bt);
void* init_bootarr(int nboot, int ndim, ...);
int write_boot(Boot*,int,char*);
int read_boot(Boot**,int,char*);

void boot_constant(Boot *res, Real constant);
void boot_copy(Boot *res, Boot src);
void scale_boot(Boot *res, Boot src, Real scale);
void add_boot(Boot *res, Boot bt1, Boot bt2);
int boot_func(Boot* res, Real (*func)(Real*,int), int narg, ...);
int bootarr_func(Boot* res, Real (*func)(Real*,int), int narg, Boot*);
int boot_func_mixed(Boot* res, Real (*func)(Real*,int), int nbt, int nrl, ...);
void restore_bootdata(Boot* bootdata, Real* xdata, int ndata,
		      Boot* bootparam, int nparam, Modelfunction paramfunc);
int bootstrap_model(Boot* ydata, Modelfunction* model,
		    Boot* fitparam, int nparam, Boot xdata);

void set_formal_bootcov(int f);
int  boot_covariance(Real** cov, Boot* data, int ndata, int* mask);
void find_minmax_boot(Boot* data, int ndata, Real* min, Real* max);
void get_boot_stats(Boot* data,	Real* y_average, Real* y_median,
		    Real* y_low, Real* y_high, Real conf_level);
void print_bootstats(Boot);
void print_stats(BootFitFuncs*);

void get_boot_datasetbundle(Real*** vectorbootbundle, 
			    Real*** vectorbundle, int nvector,
			    Real** scalarbootbundle,
			    Real** scalarbundle, int nscalar, int nset);
void get_boot_datasetbundle_multi(Real*** vectorbootbundle, 
				  Real*** vectorbundle, int nvector,
				  Real** scalarbootbundle,
				  Real** scalarbundle, int nscalar, 
				  int* nset, int ndata);
void get_boot_subset(int *subset, int nset);

/*** fitting functions ***/
void init_fitdata(FitData* fit, BootFitFuncs* bfit, int ndata, int xvar);
void free_fitdata(FitData* fit);
int chisq_fit(FitData* fit);
Real get_chisq(FitData* fit);
void get_norm_diff(FitData* fit, Real* delta);
int marquardt_minimise(FitData* fit, Real* lambda);
int get_alpha_beta(FitData *fit, Real** alpha, Real* beta);
int get_fit_param_covariance(FitData* fit);
int find_varying_parameters(int *freeze, int nparams);
void cov_to_corr(Real **cov, Real **corr, Real *sigma,
                 int ndata, int corrflag);
void get_inverse(Real **alpha, int rank, Real **alpha_inv);
void solve_delta_param(Real **alpha_prime, Real *beta, int rank,
		       Real *delta_param);

/* fiddling with the fit algorithm */
void change_delta_chisq_min(Real del_chsq_min);
void change_chisq_max(Real chsq_max);
void change_chisq_maxiter(int imax);
void change_lambda_min(Real l_min);
void change_lambda_max(Real l_max);
void change_lambda_factor(Real factor);
void change_lambda_start(Real l_start);
void change_2nd_derivative(int choice);

/*** miscellaneous functions ***/
int arr_from_int(int* arr, int* len, int i);

/*** prototypes for trivial or widely used data and model functions ***/
Datafunction Df_x;
Datafunction Df_xsqr;
Datafunction Df_y1, Df_y2, Df_y3, Df_y4, Df_y5, Df_y6, Df_y7;
Datafunction Df_ratio;
Datafunction Df_invratio;
Datafunction Df_meff;

Modelfunction polynomial;
Guessfunction polyguess;

#endif
