#include "su3.h"

/*** SU(3) Lie algebra operations, using the conventional representation ***/
#define INVSQRT3 0.57735027

/* Get the algebra-valued A from the group-valued U = exp(it^a A^a) */
void su3_to_su3alg(Su3alg res, Su3 u)
{
  res[0] = u[0][1].im + u[1][0].im;
  res[1] = u[0][1].re - u[1][0].re;
  res[2] = u[0][0].im - u[1][1].im;
  res[3] = u[0][2].im + u[2][0].im;
  res[4] = u[0][2].re - u[2][0].re;
  res[5] = u[1][2].im + u[2][1].im;
  res[6] = u[1][2].re - u[2][1].re;
  res[7] = INVSQRT3*(u[0][0].im + u[1][1].im - 2.0*u[2][2].im);
}

void su3alg_make_zero(Su3alg res)
{
  res[0] = res[1] = res[2] = res[3] = 0.0;
  res[4] = res[5] = res[6] = res[7] = 0.0;
}

Real su3alg_norm(Su3alg a)
{
  int i;
  Real r=0.0;
  for (i=0;i<NALG;i++) r+=a[i]*a[i];
  return r;
}

void su3alg_add(Su3alg res, Su3alg a1, Su3alg a2)
{
  res[0] = a1[0]+a2[0];
  res[1] = a1[1]+a2[1];
  res[2] = a1[2]+a2[2];
  res[3] = a1[3]+a2[3];
  res[4] = a1[4]+a2[4];
  res[5] = a1[5]+a2[5];
  res[6] = a1[6]+a2[6];
  res[7] = a1[7]+a2[7];
}

void su3alg_sub(Su3alg res, Su3alg a1, Su3alg a2)
{
  res[0] = a1[0]-a2[0];
  res[1] = a1[1]-a2[1];
  res[2] = a1[2]-a2[2];
  res[3] = a1[3]-a2[3];
  res[4] = a1[4]-a2[4];
  res[5] = a1[5]-a2[5];
  res[6] = a1[6]-a2[6];
  res[7] = a1[7]-a2[7];
}

void su3alg_addto(Su3alg res, Su3alg a)
{
  res[0]+=a[0]; res[1]+=a[1]; res[2]+=a[2]; res[3]+=a[3];
  res[4]+=a[4]; res[5]+=a[5]; res[6]+=a[6]; res[7]+=a[7];
}

void su3alg_subto(Su3alg res, Su3alg a)
{
  res[0]-=a[0]; res[1]-=a[1]; res[2]-=a[2]; res[3]-=a[3];
  res[4]-=a[4]; res[5]-=a[5]; res[6]-=a[6]; res[7]-=a[7];
}

void su3alg_by_real(Su3alg res, Su3alg a, Real r)
{
  res[0] = r*a[0];  res[1] = r*a[1];  res[2] = r*a[2];  res[3] = r*a[3];
  res[4] = r*a[4];  res[5] = r*a[5];  res[6] = r*a[6];  res[7] = r*a[7];
}

/* Get the complex algebra-valued res from the SL(3,C) matrix u using   *
 *  res[a] = -i tr(lambda^a u) where lambda are Gell-Mann matrices      *
 * Note the factor -i: this is the generalisation of su3_to_su3alg      *
 * which gets A^a from U = exp(it^a A^a)                                */
void su3_to_su3algc(Su3alg_c res, Su3 u)
{
  res[0].re = u[0][1].im + u[1][0].im;
  res[1].re = u[0][1].re - u[1][0].re;
  res[2].re = u[0][0].im - u[1][1].im;
  res[3].re = u[0][2].im + u[2][0].im;
  res[4].re = u[0][2].re - u[2][0].re;
  res[5].re = u[1][2].im + u[2][1].im;
  res[6].re = u[1][2].re - u[2][1].re;
  res[7].re = INVSQRT3*(u[0][0].im + u[1][1].im - 2.0*u[2][2].im);

  res[0].im = -u[0][1].re - u[1][0].re;
  res[1].im =  u[0][1].im - u[1][0].im;
  res[2].im =  u[1][1].re - u[0][0].re;
  res[3].im = -u[0][2].re - u[2][0].re;
  res[4].im =  u[0][2].im - u[2][0].im;
  res[5].im = -u[1][2].re - u[2][1].re;
  res[6].im =  u[1][2].im - u[2][1].im;
  res[7].im = INVSQRT3*(2.0*u[2][2].re - u[0][0].re - u[1][1].re);
}

void su3algc_zero(Su3alg_c a)
{
  int i;
  for (i=0;i<NALG;i++) a[i].re=a[i].im=0.0;
}

Real su3algc_norm(Su3alg_c a)
{
  int i;
  Real r=0.0;
  for (i=0;i<NALG;i++) r+=a[i].re*a[i].re+a[i].im*a[i].im;
  return r;
}

void su3algc_by_real(Su3alg_c res, Su3alg_c a, Real r)
{
  int i;
  for (i=0; i<NALG; i++){
    res[i].re = r*a[i].re;
    res[i].im = r*a[i].im;
  }
}

void su3algc_by_complex(Su3alg_c res, Su3alg_c a, Complex c)
{
  int i;
  for (i=0; i<NALG; i++){
    res[i].re = c.re*a[i].re - c.im*a[i].im;
    res[i].im = c.re*a[i].im + c.im*a[i].re;
  }
}

