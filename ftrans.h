/* $Id: ftrans.h,v 1.11 2015/06/17 13:37:50 jonivar Exp $ */

/************************************************************************
 *                                                                      *
 *  Header file for fourier transformation of gauge and fermion fields  *
 *                                                                      *
 ************************************************************************/

#ifndef _FTRANS_H_
#define _FTRANS_H_

#include "gauge.h"
#include "momenta.h"
#ifndef GAUGE_ONLY
#include "fermion.h"
#endif
#include "gfix.h"

/* flags for storage order and format of momentum space fields */
#define SAVE_TZYX 1
#define SAVE_XYZT 2
#define SAVE_XDR  3

typedef enum { FFT_KEEP=0, FFT_INIT, FFT_END } FFT_flag;

/*
 *  Structure containing all the parameters used in the run
 */

typedef struct {
  int nmomenta[DIR];             /* Number of positive momenta to use     */
  int mu_min;                    /* Parameters indicating whether to      */
  int mu_max;                    /* transform a full propagator /         */
  int spin_min;                  /* gauge field                           */
  int spin_max;                  /* or only some components.              */
  int colour_min;                /*                                       */
  int colour_max;                /*                                       */
  int partial_save;              /*                                       */

  int do_gauge_transform;        /* Gauge fixing on/off                   */
  int gauge_type;                /* Landau, Coulomb, axial, m gauge, ...  */
  Real gauge_param;              /* e.g. xi in covariant gauge            */
  Real gfix_precision;
  int gfix_iterations;
  int trans_origin;              /* code for where the trans came from    */
  int trans_id;                  /* further identification of the trans   */
  char control_dir[MAXLINE];
  int save;                      /* Save fourier field on/off             */
  int write_trans;               /* Save gauge transform on/off           */
  int save_propagator;           /* Save Z3-averaged propagator on/off    */
  int medium_prop;               /* In-medium (el/mag) propagator on/off  */

  int id;                        /* ID number for the run */
} Ftrans_params;

/*** Basic function prototypes (constructors and destructors) ***/

int init_trans_ftrans(Gauge_transform* g, 
		      Gauge_parameters* parm, Ftrans_params* run);
int init_fgauge(Gauge* u,
		Gauge_parameters* gparm,Ftrans_params* run);

int assign_params_gftrans(char* filename, Gauge_parameters* parm, 
			  Gfix_params* gparm, Ftrans_params* fparm, 
			  Momenta* mom);
int set_gauge_momenta(Gauge* u, Ftrans_params* run);

#ifndef GAUGE_ONLY
int assign_params_fprop(char* filename, Propagator_parameters* ppar, 
			Ftrans_params* run, Momenta* mom);
int set_prop_momenta(Propagator* p, Ftrans_params* run);
#endif

/*** fourier transformation functions ***/
int fourier_gauge(Gauge* up, Gauge* u);
#ifndef GAUGE_ONLY
int fourier_tslice_prop(Propagator* sp, Propagator* s, FFT_flag);
int fourier_time_prop(Propagator* sp, Propagator* s);
#endif

#endif
