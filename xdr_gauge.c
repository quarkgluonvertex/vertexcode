#include "xdr_gauge.h"
#define MAXLEN 256

/************************************************************************
 *                                                                      *
 *  Routines to encode/decode gauge structures (and Reals) in/to        *
 *  machine-independent xdr format.                                     *
 *                                                                      *
 ************************************************************************/

/*** encode all Reals as float -- may easily be changed eg to double ***/
bool_t xdr_Real(XDR* xdrs, Real* r)
{
#ifdef REAL_IS_FLOAT
  return xdr_float(xdrs, (float*)r);
#else
  float f = *r; /* for encoding */
  bool_t status = xdr_float(xdrs,&f);
  *r = f;       /* for decoding */
  return status;
#endif
}

bool_t xdr_i4vec(XDR* xdrs, i4vec* v)
{
  int i;
  int status = TRUE;

  for (i=0; i<DIR; i++) status = status && xdr_int(xdrs,&(*v)[i]);
  return status;
}

bool_t xdr_lattice(XDR* xdrs, Lattice* lat)
{
  int maxlen=0, mu;
  bool_t status;

  status = (xdr_i4vec(xdrs,&lat->length) && xdr_int(xdrs,&lat->ndim) &&
	    xdr_int(xdrs,&lat->nsite) && xdr_int(xdrs,&lat->nspatial) &&
	    xdr_int(xdrs,&lat->has_ntab));
  if (!lat->has_ntab) return status;

  for (mu=0; mu<DIR; mu++) {
    if (lat->length[mu]>maxlen) maxlen=lat->length[mu];
  }
  status = status && 
     ( xdr_array(xdrs,&lat->up,&maxlen,MAXLEN,sizeof(i4vec),xdr_i4vec) &&
       xdr_array(xdrs,&lat->dn,&maxlen,MAXLEN,sizeof(i4vec),xdr_i4vec) );
  return status;
}

bool_t xdr_parm(XDR* xdrs, Gauge_parameters* par)
{
  int nm=0, i;
  bool_t status = TRUE;

  if ( !( xdr_reference(xdrs,&par->lattice,sizeof(Lattice),xdr_lattice) &&
	  xdr_int(xdrs,&par->action) && xdr_int(xdrs,&par->start) &&
	  xdr_double(xdrs,&par->beta) ) ) return FALSE;
  for (i=0; i<DIR; i++) status = status && xdr_int(xdrs,&(par->bcs[i]));
  if ( !(status && xdr_int(xdrs,&par->nf)) ) return FALSE;
  switch(par->action) {
  case WILSON: nm=0; break;
  case GAUGEHIGGS: nm=3; break;
  default:
    printf("xdr_parm: warning: unknown action %d\n",par->action);
    nm=par->nf;
  }
  status = status &&
    ( xdr_array(xdrs,&par->mass,&nm,nm,sizeof(Real),xdr_double) &&
      xdr_double(xdrs,&par->plaquette) &&
      xdr_int(xdrs,&par->gauge) && xdr_double(xdrs,&par->gauge_param) &&
      xdr_double(xdrs,&par->rtrace) );
  for (i=0; i< MAXPREC; i++) {
    status = status && xdr_double(xdrs,&par->gfix_precision[i]);
  }
  return ( status && xdr_int(xdrs,&par->gfix_iterations) );
}

#ifdef SU2_GROUP
bool_t xdr_su2(XDR* xdrs, Su2* u)
{
  int i;
  int status = TRUE;

  for (i=0; i<4; i++) status = status && xdr_Real(xdrs,&(*u)[i]);
  return status;
}
#endif

#ifdef SU3_GROUP
bool_t xdr_su3(XDR* xdrs, Su3* u)
{
  int i,j;
  bool_t status = TRUE;

  for (i=0; i<COLOUR; i++)
    for (j=0; j<COLOUR; j++) 
      status = status && xdr_Real(xdrs,&(*u)[i][j].re) 
	&& xdr_Real(xdrs,&(*u)[i][j].im);

  return status;
}

bool_t xdr_su3_short(XDR* xdrs, Short_Su3* u)
{
  int i,j;
  bool_t status = TRUE;

  for (i=0; i<COLOUR-1; i++)
    for (j=0; j<COLOUR; j++) 
      status = status && xdr_Real(xdrs,&(*u)[i][j].re) 
	&& xdr_Real(xdrs,&(*u)[i][j].im);

  return status;
}
#endif

bool_t xdr_algebra(XDR* xdrs, Algebra_el* a)
{
  int i;
  int status = TRUE;

  for (i=0; i<NALG; i++)
    status = status && xdr_Real(xdrs,&(*a)[i]);

  return status;
}

bool_t xdr_algebra_c(XDR* xdrs, Algebra_c* a)
{
  int i;
  int status = TRUE;

  for (i=0; i<NALG; i++)
    status = status && xdr_Real(xdrs,&(*a)[i].re) && xdr_Real(xdrs,&(*a)[i].im);

  return status;
}

