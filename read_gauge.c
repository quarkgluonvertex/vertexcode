#include "gauge.h"
#include "swap.h"
#ifdef SU3_GROUP
#include "su3_precision.h"
#endif
#include <producers.h>
#include <string.h>
#include <array.h>
#include <math.h>
#include <assert.h>

#define TINY 1.0e-7

#ifdef SU2_GROUP
typedef struct {
  double re;
  double im;
} Su2vec_double[COLOUR];
#endif

typedef struct {
  float re;
  float im;
} Single_complex;

static int read_gauge_native(Gauge);
static int read_gauge_scidac(Gauge);
static int read_gauge_gc(Gauge u);
static int read_gauge_ukqcd(Gauge u);
static int read_gauge_trinlat(Gauge u);
static int read_gauge_sjhhmc(Gauge u);
static int read_gauge_sjhhmx(Gauge,int);
int read_gauge_krasnitz(Gauge u);

int read_gauge(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge";

  switch(u.parm->producer_id) {
  case PROD_INTERNAL: return read_gauge_native(u);
  case SCIDAC:  return read_gauge_scidac(u);
  case GC_OSU:  return read_gauge_gc(u);
  case UKQCD:   return read_gauge_ukqcd(u);
  case TRINLAT: return read_gauge_trinlat(u);
  case SJH_HMC: return read_gauge_sjhhmc(u);
#ifdef SU2_GROUP
  case KRASNITZ: return read_gauge_krasnitz(u);
#endif
  case COLD: /* do not read, but initialise to unit gauge */
    unit_gauge(u);
    return 0;
  case HOT:  /* do not read, but initialise to random gauge */
    random_gauge(u, u.parm->beta);
    return 0;
  default:
    fprintf(stderr,"%s: unknown producer_id %d\n",fnm,
	    u.parm->producer_id);
    return BADPARAM;
  }

  return EVIL;
}

int pick_gluon(void* gluon, Gauge_parameters* par, int* len, int* x, 
	       Gauge_t type)
{
  const char fnm[MAXNAME] = "pick_gluon";
  Gauge u;
  size_t elsz;
  size_t offset;
  int i;
  FILE* in;
  int status;

  /** dummy switch statement: use this to branch to different formats **/
  switch (par->producer_id) {
  default: i=0;
  }

  /** determine the size of the element to be read **/
  switch (type) {
  case GAUGE:     elsz = sizeof(Group_el); break;
  case ALGEBRA:   elsz = sizeof(Algebra_el); break;
  case GAUGEMAT:
  case FGAUGE:    elsz = sizeof(Group_mat); break;
  case FALG:      elsz = sizeof(Algebra_c); break;
  default:
    fprintf(stderr,"%s: unknown gauge type %d - exiting\n",fnm,type);
    return BADPARAM;
  }

  /** use Gauge structure to determine filename **/
  u.parm = par;
  u.type = type;
  if (type==FGAUGE || type==FALG) {
    for (i=0;i<DIR;i++) {
      u.nmom[i] = len[i];
      u.ntotmom[i] = (par->bcs[i]==PERIODIC) ? 2*len[i]+1 : 2*len[i];
    }
    offset = (x[IZ]+len[IZ]+(x[IT]+len[IT])*u.ntotmom[IZ])*u.ntotmom[IY];
    offset += x[IY]+len[IY]; offset *= u.ntotmom[IX];
    offset += x[IX]+len[IX]; offset *= DIR*elsz;
  } else {
    for (i=0;i<DIR;i++) {
      if (len[i] != par->lattice->length[i]) {
	fprintf(stderr,"%s: lattice mismatch in direction %d: %d!=%d\n",
		fnm,i,len[i],par->lattice->length[i]);
	return BADDATA;
      }
      u.nmom[i] = u.ntotmom[i] = -1;
    }
    offset = DIR*elsz*(x[IX]+(x[IY]+(x[IZ]+x[IT]*len[IT])*len[IY])*len[IX]);
  }
  if (status=construct_gauge_filename(&u)) return status;
  if ( !(in=fopen(u.filename,"rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,u.filename);
    return BADFILE;
  }
  if (fseek(in,offset,SEEK_SET)) {
    fprintf(stderr,"%s: error on scanning <%s>\n",fnm,u.filename);
    fclose(in);
    return BADFILE;
  }
  if (fread(gluon,elsz,par->lattice->ndim,in)!=par->lattice->ndim) {
    fprintf(stderr,"%s: failed to read gauge site from %s\n",fnm,u.filename);
    fclose(in);
    return BADFILE;
  }
  fclose(in);
  return 0;
}

static int read_gauge_native(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge_native";
  const int size = u.ndir*u.npoint;
  Group_el** uu = u.u;
  const int do_swap = ( (u.parm->id/2)%2 == 1); /* allow big/little-endian */
  int n;
  FILE* in;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: wrong field type - only GAUGE implemented\n",fnm);
    return BADPARAM;
  }

  if ( !(in=fopen(u.filename,"rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,u.filename);
    return BADFILE;
  }

  if ( (n=fread(*uu,sizeof(Group_el),size,in)) != size) {
    fprintf(stderr,"%s: failed to read gauge field from %s\n",fnm,u.filename);
    fclose(in);
    return BADFILE;
  }

  fclose(in);
  if (do_swap) byte_swap(*uu,sizeof(Real),size*sizeof(Group_el));

  return 0;
}

static int read_gauge_scidac(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge_scidac";
#ifdef SU3_GROUP
  const int size = u.ndir*u.npoint;
  const int rsize = sizeof(Real);
  Su3** uu = u.u;
  Real* up;
  void* u_tmp; /* can be forced to float, double, short_su3_double etc */
  int su3_size; /* size of su(3) matrices on file */
  int in_size;  /* size of real number on file (float/double) */
  int dprec;    /* double/single precision        */
  int do_swap;  /* byte swapped or native ordered */
  int n;
  FILE* in;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: wrong field type - only GAUGE implemented\n",fnm);
    return BADPARAM;
  }

/***  work out the precision and byte ordering of the input data    ***/
/***  id  0,2: double precision; 1,3: single precision  ***
 ***      0,1: native ordering;  2,3: byte-swapped      ***/
  do_swap = ( (u.parm->id/2)%2 == 1);
  dprec = ( u.parm->id%2 == 0 );

/*** if the precision on file is the same as the native precision we can ***
 *** read this as native data since the index ordering is the same       ***/
#ifdef REAL_IS_DOUBLE
  if (dprec) return read_gauge_native(u);
#endif
#ifdef REAL_IS_FLOAT
  if (!dprec) return read_gauge_native(u);
#endif

/*** define input sizes (single/double precision) ***/
  if (dprec) {
    in_size = sizeof(double);
    su3_size = sizeof(Su3_double);
  } else {
    in_size = sizeof(float);
    su3_size = sizeof(Su3_single);
  }

/*** if the input size is bigger than the internal size we must read ***
 *** into a temporary array, otherwise we can use the data space     ***/
  if (in_size > rsize) {
    u_tmp = malloc(su3_size*size);
    if (!u_tmp) {
      fprintf(stderr,"%s: cannot alloc temporary array\n",fnm);
      return NOSPACE;
    }
  } else u_tmp = *uu;

  if ( !(in=fopen(u.filename,"rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,u.filename);
    return BADFILE;
  }

  if ( (n=fread(u_tmp,su3_size,size,in)) != size) {
    fprintf(stderr,"%s: failed to read gauge field from %s\n",fnm,u.filename);
    fprintf(stderr,"%s: read only %d of %d records\n",fnm,n,size);
    fclose(in);
    return BADFILE;
  }
  fclose(in);

  if (do_swap) byte_swap(u_tmp,in_size,size*su3_size);

  /*** copy the data into place, starting from end to avoid overwriting   ***
   *** float and double must be treated separately to avoid type conflict ***/
  up = (Real*)(*uu) + size*sizeof(Su3)/rsize;
  if (in_size==sizeof(float)) {
    float* uin = (float*)u_tmp + size*su3_size/in_size;
    while (uin > (float*)u_tmp) *(--up)=*(--uin);
  } else {
    double* uin = (double*)u_tmp + size*su3_size/in_size;
    while (uin > (double*)u_tmp) *(--up)=*(--uin);
  }

  if (in_size > rsize) free(u_tmp);
  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}

/*
 *	Routine to read in and check the header. 
 *	Other important parameters describing the format
 *	are also passed. i.e. do_swap, is_short.
 */

static int read_gauge_header_gc(Gauge u, int *length)
{
  FILE *input_file;      
  char line[MAXLINE];

/***  Open the file   ***/
  if((input_file = fopen(u.filename, "r")) == NULL) {
    fprintf(stderr,"failed to open file %s\n", u.filename);
    return BADFILE;
  }

/***  Read the header  ***/
  *length=0;
  strcpy(line,"");
  while (strcmp(line,"END_HEADER\n")) {
    if (!fgets(line,MAXLINE,input_file)) {
      fprintf(stderr,"error in reading header on file %s\n", u.filename);
      return BADFILE;
    }
    *length += strlen(line);
/* Here we could and should put in various checks */
/* Check dimensions                               */
    if (!strncmp(line,"DIMENSION",9)) {
      int mu,l;
      sscanf(line, "DIMENSION_%d = %d", &mu, &l);
      if ( l != u.parm->lattice->length[mu-1]) {
	fprintf(stderr,"lattice sizes do not agree: L[%d] = %d != %d\n", 
	       mu, l, u.parm->lattice->length[mu-1]);
	return BADDATA;
      }
    }
/* Check sweep number                             */
    if (!strncmp(line,"SEQUENCE_NUMBER",15)) {
      int sweep;
      sscanf(line, "SEQUENCE_NUMBER = %d", &sweep);
      if ( sweep != u.parm->sweep ) {
	fprintf(stderr,"sweep numbers do not match: %d != %d\n",
	       sweep, u.parm->sweep);
	return BADDATA;
      }
    }
/* Get plaquette and link trace                   */
    if (!strncmp(line,"LINK_TRACE",10)) {
      float retr;
      sscanf(line, "LINK_TRACE = %g", &retr);
      printf("ReTrU     = %10.6f\n", retr);
      u.parm->rtrace = retr;
    }
    if (!strncmp(line,"PLAQUETTE",9)) {
      float plaq;
      sscanf(line, "PLAQUETTE = %g", &plaq);
      printf("plaquette = %10.6f\n", plaq);
      u.parm->plaquette = plaq;
    }
  }

  /*
   *  Close the file
   */

  if(fclose(input_file)) {
    fprintf(stderr,"failed to close file %s\n", u.filename);
    return BADFILE;
  }
 
  return 0;
}

static int read_gauge_gc(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge_gc";
#ifdef SU3_GROUP
  const int size = DIR*u.parm->lattice->nsite;
  FILE *input_file;
  int icol,jcol,i;
  int head_len;
  long offset;
  Su3** uu = u.u;
  Short_Su3 *uptr;
  Short_Su3_single *utmp = malloc(sizeof(Short_Su3_single)*size);
  Real r;
  int status;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u.type);
    return BADDATA;
  }

/**  Read the header  **/
  if ( (status=read_gauge_header_gc(u, &head_len)) ) return status;

/**  Open the file  **/
  if ((input_file = fopen(u.filename, "rb")) == NULL) {
    fprintf(stderr,"%s: cannot open %s\n", fnm, u.filename);
    return BADFILE;
  }

/**  Calculate the offset to the start of the binary data  **/
  offset = (long) (sizeof(char)*head_len);

  if (fseek(input_file,offset,SEEK_SET)) {
    fprintf(stderr,"%s:Failed to position past header in config file %s\n",fnm,
	    u.filename);
    return BADFILE;
  }
     
/*** Gauge Connection data are single precision, big-endian   ***
 *** (TODO: check that in header!)                            ***
 *** Must byte-swap on little-endian machines                 ***/
  if (fread(utmp, sizeof(Short_Su3_single), size, input_file) != size) {
    fprintf(stderr,"%s: Failed to read gauge field from %s\n",fnm,u.filename);
    return BADDATA;
  }
#ifdef LITTLE_ENDIAN
  byte_swap(utmp,sizeof(float),size*sizeof(Short_Su3_single));
#endif

  uptr = (Short_Su3*)*uu;
  for (i=0;i<DIR*u.parm->lattice->nsite;i++,uptr++) 
    for (icol=0;icol<COLOUR-1;icol++)
      for (jcol=0;jcol<COLOUR;jcol++) {
	(*uptr)[icol][jcol].re = utmp[i][icol][jcol].re;
	(*uptr)[icol][jcol].im = utmp[i][icol][jcol].im;
      }
      
  uncompress_Su3(*uu,size);

/***  Close the file  ***/
  if(fclose(input_file)) {
    fprintf(stderr,"%s: failed to close file %s\n", fnm, u.filename);
    return BADFILE;
  }
/*** Check average link and plaquette ***/
  r = average_trace(u);
  if ( fabs(r-u.parm->rtrace) > TINY ) {
    fprintf(stderr,"%s: average link does not match %g != %g\n", fnm,
          r, u.parm->rtrace);
    return BADDATA;
  }
  r = average_plaquette(u);
  if ( fabs(r-u.parm->plaquette) > TINY ) {
    fprintf(stderr,"%s:average plaquette does not match %g != %g\n",fnm,
          r, u.parm->plaquette);
    return BADDATA;
  }

  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}

/*** UKQCD files may be single or double precision but may be ***
 *** converted to native byte ordering with ieeeconvert       ***/
int read_gauge_ukqcd(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge_ukqcd";
#ifdef SU3_GROUP
  FILE *in;      
  int n, t, icol, jcol;
  const int Lt = u.parm->lattice->length[IT];
  const int vol = u.parm->lattice->nspatial*DIR;
  const int dsize = vol*sizeof(Short_Su3)/sizeof(Real); /* # Reals in a timeslice */
  const int rsize = sizeof(Real);
  int dprec;    /* double/single precision        */
  int do_swap;  /* byte swapped or native ordered */

  Su3** uu = u.u;
  void* u_tmp; /* can be forced to float, double, short_su3_double etc */
  int ssu3_size;  /* size of short su(3) matrices on file */
  int in_size;    /* size of real number on file (float/double) */

  Short_Su3* ushort;
  char filename[FILENAME_MAX];

/***  work out the precision and byte ordering of the input data    ***/
/***  id  0,2: double precision; 1,3: single precision  ***
 ***      0,1: native ordering;  2,3: byte-swapped      ***/
  do_swap = ( (u.parm->id/2)%2 == 1);
  dprec = ( u.parm->id%2 == 0 );

/*** define input sizes (single/double precision) ***/
  if (dprec) {
    in_size = sizeof(double);
    ssu3_size = sizeof(Short_Trans_Su3_double);
  } else {
    in_size = sizeof(float);
    ssu3_size = sizeof(Short_Trans_Su3_single);
  }
/*** if the input size is bigger than the internal size we must read ***
 *** into a temporary array, otherwise we can use the data space     ***/
  if (in_size > rsize) {
    u_tmp = malloc(ssu3_size*vol);
    if (!u_tmp) {
      fprintf(stderr,"%s: cannot alloc temporary array\n",fnm);
      return NOSPACE;
    }
  }

/***  Loop over timeslices and read from timesliced files  ***/

  for (t=0; t<Lt; t++) {
    void* ut = *uu+t*vol;
    if (in_size <= rsize) u_tmp = ut;
    sprintf(filename, "%sT%02d", u.filename, t);

/*** open the file for the first timeslice, otherwise reopen ***/
    if (t==0) {
    } else {
      in=freopen(filename,"rb",in);
    }
    if ( !in ) {
      fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
      return BADFILE;
    }

/*** read to temporary data space ***/
    if((n=fread(u_tmp, ssu3_size,vol,in)) != vol) {
      printf("Failed to read transformation from %s\n", filename);
      return BADDATA;
    }
/*** byte swap if necessary ***/
    if (do_swap) byte_swap(u_tmp,sizeof(Real),vol*ssu3_size/sizeof(Real));

/*** move from temporary array to data array if necessary ***
 *** and swap indices: Colour indices in cray format are  ***
 *** the opposite of the order here                       ***/
    if ( in_size != rsize ) {
      ushort = (Short_Su3*)ut+vol;
/* float and double must be treated separately to avoid type conflict */
      if (in_size==sizeof(double)) {
	double* ud = (double*)u_tmp+dsize;
	while ( ushort > (Short_Su3*)ut ) {
	  --ushort;
	  for (icol=0; icol < COLOUR; icol++)
	    for (jcol=0; jcol < COLOUR-1; jcol++) {
	      (*ushort)[jcol][icol].re = *(--ud);
	      (*ushort)[jcol][icol].im = *(--ud);
	    }
	}
      } else if (in_size==sizeof(float)) {
	float* us = (float*)u_tmp+dsize;
	Short_Su3 gtmp;
	while ( ushort > (Short_Su3*)(ut)+1 ) {
	  --ushort;
	  for (icol=0; icol < COLOUR; icol++)
	    for (jcol=0; jcol < COLOUR-1; jcol++) {
	      (*ushort)[jcol][icol].re = *(--us);
	      (*ushort)[jcol][icol].im = *(--us);
	    }
	}
/** the last matrix must be done separately to avoid overwriting **
 ** since the tmp data has been read into the data space         **/
	for (icol=0; icol < COLOUR; icol++)
	  for (jcol=0; jcol < COLOUR-1; jcol++) {
	    gtmp[jcol][icol].re = *(--us);
	    gtmp[jcol][icol].im = *(--us);
	  }
	for (icol=0; icol < COLOUR-1; icol++)
	  for (jcol=0; jcol < COLOUR; jcol++) {
	    (*ushort)[jcol][icol] = gtmp[icol][jcol];
	  }
      }
/*** Restore SU(3) matrices ***/
      uncompress_Su3(ut, vol);
    }
    else {
/*** Restore SU(3) and swap colour indices ***/
      uncompress_Su3_trans(ut, vol);
    }
  } /*** end loop over timeslice ***/

/***  Close the file at the end ***/
  if(fclose(in)) {
    fprintf(stderr,"%s: error on closing %s\n",fnm,filename);
    return BADFILE;
  }

/***  Dont forget to free the temporary space!  ***/
  if (in_size > rsize) free(u_tmp);

  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}  

#define TRINLAT_MAGIC_SIZE 8
#define TRINLAT_DATAID_SIZE 8

static int read_gauge_trinlat(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge_trinlat";
#ifdef SU3_GROUP
  Lattice* lat = u.parm->lattice;
  const int size = DIR*lat->nsite;
  struct trinlat_hdr {
    char magic[TRINLAT_MAGIC_SIZE];
    char dataid[TRINLAT_DATAID_SIZE];
    int tagsize;
    char precision;
    int nx, ny, nz, nt;
  } header;
  FILE *in;
  int icol,jcol,i,mu,t;
  int do_swap = ( (u.parm->id/2)%2 == 1);
  Su3** uu = u.u;
  // TODO: check precision!!!
  Su3*** utmp = arralloc(sizeof(Su3),3,lat->length[IT],DIR,lat->nspatial);
  Su3* uptr;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u.type);
    return BADDATA;
  }

  if ((in = fopen(u.filename, "rb")) == NULL) {
    fprintf(stderr,"%s: cannot open %s\n", fnm, u.filename);
    return BADFILE;
  }

/**  Read the header  **/
  if (fread(&header,sizeof(struct trinlat_hdr), 1, in) != 1) {
    fprintf(stderr,"%s: Failed to read header from %szn",fnm,u.filename);
    fclose(in);
    return BADFILE;
  }
  //TODO: Check the header!!

/**  Read config data **/
  if (fread(**utmp, sizeof(Su3), size, in) != size) {
    fprintf(stderr,"%s: Failed to read gauge field from %s\n",fnm,u.filename);
    fclose(in);
    return BADDATA;
  }

  if(fclose(in)) {
    fprintf(stderr,"%s: failed to close file %s\n", fnm, u.filename);
    return BADFILE;
  }

/*** Reorder data to native order ***/
  if (do_swap) byte_swap(**utmp,sizeof(Real),size*sizeof(Su3));

  uptr = *uu;
  for (t=0;t<lat->length[IT];t++) 
    for (i=0;i<lat->nspatial;i++)
      for (mu=0;mu<DIR;mu++,uptr++)
	for (icol=0;icol<COLOUR;icol++)
	  for (jcol=0;jcol<COLOUR;jcol++) {
	    (*uptr)[icol][jcol].re = utmp[t][mu][i][icol][jcol].re;
	    (*uptr)[icol][jcol].im = utmp[t][mu][i][icol][jcol].im;
	  }
      
  free(utmp);
  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}

/*** Simons hmc output is always double precision but may be  ***
 *** in the wrong byte order if we have moved machines        ***/
int read_gauge_sjhhmc(Gauge u)
{
  const char fnm[MAXNAME] = "read_gauge_sjhhmc";
#ifdef SU2_GROUP
  FILE *in;      
  const int size = DIR*u.parm->lattice->nsite;
  int do_swap;  /* byte swapped or native ordered */

  Su2**     uu = u.u;
  Su2mat** uum = u.u;
  Double_complex* u1 = malloc(sizeof(Double_complex)*size);
  Double_complex* u2 = malloc(sizeof(Double_complex)*size);
  int dummy;
  int i,j,mu;

  char filename[FILENAME_MAX];

/***  work out the precision and byte ordering of the input data    ***/
/***  id  0,2: double precision; 1,3: single precision  ***
 ***      0,1: native ordering;  2,3: byte-swapped      ***/
  if (u.parm->id%2) return read_gauge_sjhhmx(u,u.parm->id/4+1);
  do_swap = ( (u.parm->id/2)%2 == 1);
  assert ( u.parm->id%2 == 0 );

/***  Check the gauge type (position space gauge field)  ***/
  if (u.type!=GAUGEMAT && u.type!=GAUGE) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u.type);
    return BADDATA;
  }

/***  Read the fortran integer at the start of the file  ***/
  if ( !(in = fopen(u.filename, "rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n", fnm, u.filename);
    return BADFILE;
  }
  in=fopen(u.filename,"rb");
  if (fread(&dummy,sizeof(int),1,in)!=1) {
    fprintf(stderr,"%s: cannot read start data from %s\n",fnm,u.filename);
    return BADDATA;
  }

  if (fread(u1, sizeof(Double_complex), size, in) != size) {
    fprintf(stderr,"%s: Failed to read gauge field from %s\n",fnm,u.filename);
    return BADDATA;
  }
  if (fread(u2, sizeof(Double_complex), size, in) != size) {
    fprintf(stderr,"%s: Failed to read gauge field from %s\n",fnm,u.filename);
    return BADDATA;
  }

  if (do_swap) {
    byte_swap(u1,sizeof(double),size*sizeof(Double_complex));
    byte_swap(u2,sizeof(double),size*sizeof(Double_complex));
  }

  if (u.type==GAUGE) {
    for (j=0,mu=0;mu<DIR;mu++)
      for (i=0;i<u.parm->lattice->nsite;i++,j++) {
	uu[i][mu][0] =  u1[j].re;
	uu[i][mu][1] =  u2[j].im;
	uu[i][mu][2] =  u2[j].re;
	uu[i][mu][3] =  u1[j].im;
      }
  } else {
    for (j=0,mu=0;mu<DIR;mu++)
      for (i=0;i<u.parm->lattice->nsite;i++,j++) {
	uum[i][mu][0][0].re =  u1[j].re;
	uum[i][mu][0][0].im =  u1[j].im;
	uum[i][mu][1][1].re =  u1[j].re;
	uum[i][mu][1][1].im = -u1[j].im;
	uum[i][mu][0][1].re =  u2[j].re;
	uum[i][mu][0][1].im =  u2[j].im;
	uum[i][mu][1][0].re = -u2[j].re;
	uum[i][mu][1][0].im =  u2[j].im;
      }
  }

  /***  Close the file at the end ***/
  if(fclose(in)) {
    fprintf(stderr,"%s: error on closing %s\n",fnm,filename);
    return BADFILE;
  }

/***  Dont forget to free the temporary space!  ***/
  free(u1);
  free(u2);

  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}  

/*** Single-precision, dual-processor hmc output.              ***
 *** The first Nt/2 time slices come first, then the last Nt/2 ***/
int read_gauge_sjhhmx(Gauge u, int nproc)
{
  const char fnm[MAXNAME] = "read_gauge_sjhhmx";
#ifdef SU2_GROUP
  FILE *in;      
  const int size = DIR*u.parm->lattice->nsite;
  const int sizen = size/nproc;
  const int nsite = u.parm->lattice->nsite;
  int do_swap;  /* byte swapped or native ordered */

  Su2**     uu = u.u;
  Su2mat** uum = u.u;
  Single_complex* u1 = malloc(sizeof(Single_complex)*size);
  Single_complex* u2 = malloc(sizeof(Single_complex)*size);
  int dummy;
  int i,j,n,mu;

  char filename[FILENAME_MAX];

/***  work out the precision and byte ordering of the input data    ***/
/***  id  0,2: double precision; 1,3: single precision  ***
 ***      0,1: native ordering;  2,3: byte-swapped      ***/
  do_swap = ( (u.parm->id/2)%2 == 1);
  assert ( u.parm->id%2 == 1 );

/***  Check the gauge type (position space gauge field)  ***/
  if (u.type!=GAUGEMAT && u.type!=GAUGE) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u.type);
    return BADDATA;
  }

  if ( !(in = fopen(u.filename, "rb")) ) {
    fprintf(stderr,"%s: cannot open %s\n", fnm, u.filename);
    return BADFILE;
  }
  in=fopen(u.filename,"rb");

  /*** read each n-th part of the data ***/
  for (i=0;i<nproc;i++) {
    if (fread(&dummy,sizeof(int),1,in)!=1) {
      fprintf(stderr,"%s: cannot read start data %d from %s\n",
	      fnm,i,u.filename);
      return BADFILE;
    }
    if (fread(&u1[i*sizen],sizeof(Single_complex),sizen,in) != sizen) {
      fprintf(stderr,"%s: Failed to read gauge field part %d from %s\n",
	      fnm,i,u.filename);
      return BADFILE;
    }
    if (fread(&u2[i*sizen],sizeof(Single_complex),sizen,in) != sizen) {
      fprintf(stderr,"%s: Failed to read gauge field part %d from %s\n",
	      fnm,i,u.filename);
      return BADFILE;
    }
    if (fread(&dummy,sizeof(int),1,in)!=1) {
      fprintf(stderr,"%s: cannot read stop data %d from %s\n",
	      fnm,i,u.filename);
      return BADFILE;
    }
  }

  if (do_swap) {
    byte_swap(u1,sizeof(float),size*sizeof(Single_complex));
    byte_swap(u2,sizeof(float),size*sizeof(Single_complex));
  }

  if (u.type==GAUGE) {
    j=0;
    for (n=0;n<nproc;n++) for (mu=0;mu<DIR;mu++)
      for (i=n*nsite/nproc;i<(n+1)*nsite/nproc;i++,j++) {
	uu[i][mu][0] =  u1[j].re;
	uu[i][mu][1] =  u2[j].im;
	uu[i][mu][2] =  u2[j].re;
	uu[i][mu][3] =  u1[j].im;
      }
  } else {
    j=0;
    for (n=0;n<nproc;n++) for (mu=0;mu<DIR;mu++)
      for (i=n*nsite/nproc;i<(n+1)*nsite/nproc;i++,j++) {
	uum[i][mu][0][0].re =  u1[j].re;
	uum[i][mu][0][0].im =  u1[j].im;
	uum[i][mu][1][1].re =  u1[j].re;
	uum[i][mu][1][1].im = -u1[j].im;
	uum[i][mu][0][1].re =  u2[j].re;
	uum[i][mu][0][1].im =  u2[j].im;
	uum[i][mu][1][0].re = -u2[j].re;
	uum[i][mu][1][0].im =  u2[j].im;
      }
  }

  /***  Close the file at the end ***/
  if(fclose(in)) {
    fprintf(stderr,"%s: error on closing %s\n",fnm,filename);
    return BADFILE;
  }

/***  Dont forget to free the temporary space!  ***/
  free(u1);
  free(u2);

  return 0;
#else
  fprintf(stderr,"%s: Wrong gauge group!\n",fnm);
  return EVIL;
#endif
}  
