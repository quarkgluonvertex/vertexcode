#include "fermion.h"
#include <stdio.h>
#include <stdlib.h>
#ifdef XDR
#include <rpc/xdr.h>
#endif
#include <array.h>
#define WARNING 0

static int write_fprop(Propagator s);
static int write_tslice_fprop(Propagator s);
static int write_fourier_spincpt(Propagator s);

/************************************************************************
 * Functions to change spacetime ordering of propagators                *
 *   reorder_prop_xyzt changes from the (default) tzyx ordering         *
 *                     (x fastest changing) to xyzt ordering            *
 *   reorder_prop_tzyx changes back from xyzt ordering to tzyx ordering *
 * Note that many functions (eg the momenta suite) require tzyx order   *
 ************************************************************************/
int reorder_prop_xyzt(Propagator* s)
{
  const char fnm[MAXNAME] = "reorder_prop_xyzt";
  Prop* ss = s->prop;
  int ix, iy, iz, it, i, is, js, ic, jc;
  Prop**** s_tmp;
  Prop* p;
  
  if (s->type != PROP || s->space != FOURIER) {
    fprintf(stderr,"%s: only implemented for fourier prop\n",fnm);
    return BADPARAM;
  }

  s_tmp = arralloc(sizeof(Prop),4,s->ntotmom[IX],s->ntotmom[IY],
		   s->ntotmom[IZ],s->ntotmom[IT]);
  if (!s_tmp) {
    fprintf(stderr,"%s: cannot alloc space for temporary arrays\n",fnm);
    return NOSPACE;
  }

  for (i=it=0;it<s->ntotmom[IT];it++)
    for (iz=0;iz<s->ntotmom[IZ];iz++)
      for (iy=0;iy<s->ntotmom[IY];iy++)
	for (ix=0;ix<s->ntotmom[IX];ix++,i++) {
	  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	      for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
		  s_tmp[ix][iy][iz][it][is][js][ic][jc] = ss[i][is][js][ic][jc];
		}
	    }
	}

  p = ***s_tmp;
  for (i=0;i<s->npoint;i++) {
    for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
	    ss[i][is][js][ic][jc] = p[i][is][js][ic][jc];
	  }
      }
  }
  //  s->prop = ***s_tmp;
  //  free(ss);
  free(s_tmp);
  return 0;
}

int reorder_prop_tzyx(Propagator* s)
{
  const char fnm[MAXNAME] = "reorder_prop_tzyx";
  Prop* ss = s->prop;
  int ix, iy, iz, it, i, is, js, ic, jc;
  Prop**** s_tmp;
  Prop* p;
  
  if (s->type != PROP || s->space != FOURIER) {
    fprintf(stderr,"%s: only implemented for fourier prop\n",fnm);
    return BADPARAM;
  }

  s_tmp = arralloc(sizeof(Prop),4,s->ntotmom[IT],s->ntotmom[IZ],
		   s->ntotmom[IY],s->ntotmom[IX]);
  if (!s_tmp) {
    fprintf(stderr,"%s: cannot alloc space for temporary arrays\n",fnm);
    return NOSPACE;
  }

  for (i=ix=0;ix<s->ntotmom[IX];ix++) {
      for (iy=0;iy<s->ntotmom[IY];iy++)
	for (iz=0;iz<s->ntotmom[IZ];iz++)
	  for (it=0;it<s->ntotmom[IT];it++,i++)
	  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	      for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
		  s_tmp[it][iz][iy][ix][is][js][ic][jc] = ss[i][is][js][ic][jc];
		}
	    }
	}

  p = ***s_tmp;
  for (i=0;i<s->npoint;i++) {
    for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
	    ss[i][is][js][ic][jc] = p[i][is][js][ic][jc];
	  }
      }
  }
  //  s->prop = ***s_tmp;
  //  free(ss);
  free(s_tmp);
  return 0;
}

/*** write_prop: generic write routine for propagators ***/
int write_prop(Propagator s)
{
  const char fnm[MAXNAME] = "write_prop";
  switch (s.type) {
  case SPINOR:
    return write_prop_cpt(s,s.s1,s.c1);
  case PROP:
    switch (s.space) {
    case TSLICE_P:
      return write_tslice_fprop(s);
    case FOURIER:
      return write_fprop(s);
    default:
      ;
    }
  default:
    fprintf(stderr,"%s: type %d in space %d not implemented!\n",
	    fnm,s.type,s.space);
    return BADPARAM;
  }
}

int write_prop_cpt(Propagator p, int spin, int col)
{
  const char fnm[MAXLINE] = "write_prop_cpt";
  Lattice* lat = p.parm->gpar->lattice;
  const int tmin = p.space==TSLICE_X ? p.tslice : 0;
  const int tmax = p.space==TSLICE_X ? p.tslice : lat->length[IT]-1;
  const int ns = lat->nspatial;
  char filename[MAXLINE];
  Spinor* psi = p.prop;
  Spinor* psit;
  FILE* out = 0;
  int n, t;

  if (p.space!=XSPACE && p.space!=TSLICE_P) {
    fprintf(stderr,"%s: illegal space %d - only position space implemented\n",
	    fnm,p.type);
    return BADPARAM;
  }
  if (p.type!=SPINOR) {
    fprintf(stderr,"%s: illegal type %d - only spinor types permitted\n",
	    fnm,p.type);
    return BADPARAM;
  }
  /* we write the propagator per timeslice */
  for (t=tmin,psit=psi; t<=tmax; t++,psit+=ns) {
    sprintf(filename,"%s%d%dT%02d",p.filename,spin,col,t);
    out = (!out) ? fopen(filename,"wb") : freopen(filename,"wb",out);
    if (!out) {
      fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
      return BADFILE;
    }
    /* dump in native format */
    if ( (n=fwrite(psit,sizeof(Spinor),ns,out)) != ns) {
      fprintf(stderr,"%s: failed to write propagator to %s\n",fnm,filename);
      fprintf(stderr,"%s: wrote only %d of %d elements\n",fnm,n,ns);
      fclose(out);
      return BADDATA;
    }
  }

  fclose(out);
  return 0;
}

int write_tslice_fprop(Propagator s)
{
  const char fnm[MAXNAME] = "write_tslice_fprop";

  fprintf(stderr,"%s: Not implemented!\n",fnm);
  return EVIL;
}

#ifdef XDR
/*************************************************************************
 *  We always dump momentum space propagators in xdr format,             *
 *  single precision (this may easily be changed though)                 *
 *************************************************************************/
int write_fprop(Propagator s)
{
  const char fnm[MAXNAME] = "write_fprop";
  const size_t psz = s.elsz;
  const size_t rsz = sizeof(Real);
  const size_t fsz = sizeof(float);
  const size_t ntot = s.npoint*psz/rsz;
  Real* sp = s.prop;
  FILE* out;
  XDR xdrs;
  int n, result;

  if (s.space!=FOURIER) {
    fprintf(stderr,"%s: propagator not in momentum space!\n",fnm);
    return BADPARAM;
  }
  if (s.type!=PROP && s.type!=COLMAT) {
    fprintf(stderr,"%s: wrong propagator type!\n",fnm);
    return BADPARAM;
  }

/*** the filename was automatically created at initialisation ***/
  printf("Dumping fourier propagator to <%s>\n", s.filename);
  if ( !(out = fopen(s.filename,"wb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,s.filename);
    return BADFILE;
  }
  xdrstdio_create(&xdrs, out, XDR_ENCODE);

/*** if Real is double we must create a float array to dump ***
 *** otherwise use the data array itself                    ***/
  if (rsz!=fsz) {
    float* dump = malloc(sizeof(float)*ntot);
    float* pd = dump+ntot;
    Real* pos = (Real*)s.prop+ntot;
    if (!dump) {
      fprintf(stderr,"%s: cannot alloc dump space\n",fnm);
      fclose(out);
      return NOSPACE;
    }
    for (;pd>dump;) *(--pd)=*(--pos);

    n = ntot;
    result = xdr_array(&xdrs, &dump, &n, ntot, sizeof(float), xdr_float);
    free(dump);
  } else {
    n = psz*s.npoint;
    result = xdr_array(&xdrs, sp, &n, ntot, sizeof(float), xdr_float);
  }

  n /= psz/rsz;
  if (n!=s.npoint) {
    fprintf(stderr,"%s: Failed to dump propagator to file <%s>\n",
	    fnm,s.filename);
    fprintf(stderr,"%s: Wrote only %d elements\n",fnm,n);
    fclose(out);
    return BADFILE;
  }
  if (fclose(out)) {
    fprintf(stderr,"%s: error on closing %s\n",fnm,s.filename);
    return WARNING;
  }

  return result ? 0 : BADFILE;
}
#else
int write_fprop(Propagator s)
{
  return dump_prop(s);
}
#endif

static int write_fourier_spincpt(Propagator s)
{
  const char fnm[MAXNAME] = "write_fourier_spincpt";
  fprintf(stderr,"%s: not implemented!\n",fnm);
  return EVIL;
}

/*** dump_prop dumps the propagator in native format ***/
static int dump_tslice_fprop(Propagator s)
{
  const char fnm[MAXNAME] = "dump_tslice_fprop";
  fprintf(stderr,"%s: not implemented!\n",fnm);
  return EVIL;
}

int dump_prop(Propagator s)
{
  const char fnm[MAXNAME] = "dump_prop";
  FILE* out;
  int n;

/*** the filename was automatically created at initialisation ***/
  printf("Dumping propagator to <%s>\n", s.filename);
  if ( !(out = fopen(s.filename,"wb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,s.filename);
    return BADFILE;
  }

  if (s.space==TSLICE_P) return dump_tslice_fprop(s);

  if ( (n=fwrite(s.prop, s.elsz, s.npoint, out)) != s.npoint) {
    fprintf(stderr,"%s: Failed to dump propagator to file <%s>\n",
	    fnm,s.filename);
    fprintf(stderr,"%s: Wrote only %d elements\n",fnm,n);
    fclose(out);
    return BADFILE;
  }

  if (fclose(out)) {
    fprintf(stderr,"%s: error on closing %s\n",fnm,s.filename);
    return WARNING;
  }

  return 0;

}


int write_eigenvectors(Spinor** evec, double* eval, int nev,
		       Propagator_parameters* par, char* name)
{
  const char fnm[MAXNAME] = "write_eigenvectors";
  const int taglen=20; /* sizeof trinlat QuarkFile__0_0_tag */
  const int prec=68;   /* char 'D' converted to int */
  Lattice* lat = par->gpar->lattice;
  int ival, n;
  char filename[MAXLINE];
  FILE* out;

  for (ival=0;ival<nev;ival++) {
    sprintf(filename,"%s.%d.qev",name,ival);
    if ( !(out=fopen(filename,"wb")) ) {
      fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
      return BADFILE;
    }
    /*** write trinlat header ***/
    fwrite("Qrk  0.0xxxxxxxx",sizeof(char),16,out);
    fwrite(&taglen,sizeof(int),1,out);
    fwrite(&prec,sizeof(int),1,out);
    fwrite(lat->length,sizeof(int),4,out);
    if ( (n=fwrite(evec[ival],sizeof(Spinor),lat->nsite,out)) != lat->nsite) {
      fprintf(stderr,"%s: failed to write data to %s\n",fnm,filename);
      fprintf(stderr,"%s: wrote only %d of %d elements\n",fnm,n,lat->nsite);
      fclose(out);
      return BADFILE;
    }
    fclose(out);
  }

  return 0;
}
