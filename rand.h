#ifndef _RAND_H_
#define _RAND_H_
#include <magic.h>

#define READ_SEED 1

double my_drand48(long init_flag);
int rand_isinitialised();
void rand_initialise(long s);
long rand_seed(long s);
int rand_save();

#endif
