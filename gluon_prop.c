
/* /DENEME_SU2/DENEME_gluon klasorunun icindeki gluon_prop.c dosyasinda yaptigim modifikasyon denemeleri*/
#include "mom_fields.h"
#include <arralloc.h>
#include <math.h>

static Real** gluon_prop_g(Gauge* g, Momenta* mom, int* status);

Real** gluon_propagator(Gauge* g, Momenta* mom, int* status)
{
  const char fnm[MAXNAME] = "gluon_propagator";
  Lattice *lat = g->parm->lattice;
  int ii, io, mu, nu, i, k[DIR];
  Real q[DIR];
  Real **aarr;
  Algebra_c** a = g->u;
#ifdef PARANOID
  int* neq;
#endif

  aarr = arralloc(sizeof(Real),2,2,mom->nval);
  if ( !aarr ) {
    fprintf(stderr,"%s: cannot allocate space for propagators\n",fnm);
    *status = NOSPACE; return 0;
  }

  if (g->type == FGAUGE) return gluon_prop_g(g,mom,status);
  if (g->type != FALG) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,g->type);
    *status = BADDATA; return 0;
  }

#ifdef PARANOID
  neq = calloc(sizeof(int),mom->nval);
#endif
  for (io=0; io<mom->nval; io++) {
    aarr[0][io] = aarr[1][io] = 0.0;
  }

  for (ii=0,k[IT]=0; k[IT]<g->ntotmom[IT]; k[IT]++) {
    q[IT] = 2.0*sin(PI*(k[IT]-g->nmom[IT])/lat->length[IT]);
    for (k[IZ]=0; k[IZ]<g->ntotmom[IZ]; k[IZ]++) {
      q[IZ] = 2.0*sin(PI*(k[IZ]-g->nmom[IZ])/lat->length[IZ]);
      for (k[IY]=0; k[IY]<g->ntotmom[IY]; k[IY]++) {
	q[IY] = 2.0*sin(PI*(k[IY]-g->nmom[IY])/lat->length[IY]);
	for (k[IX]=0; k[IX]<g->ntotmom[IX]; k[IX]++,ii++) {
	  q[IX] = 2.0*sin(PI*(k[IX]-g->nmom[IX])/lat->length[IX]);
	  if ( (io=momindex(k,mom)) != DISCARDED ) {
	    Real ao=0.0,ad=0.0;
	    for (mu=0; mu<g->ndir; mu++) {
	      for (nu=0; nu<g->ndir; nu++) {
		Real da=0.0;
		for (i=0; i<NALG; i++)  {
		  da += a[ii][mu][i].re*a[ii][nu][i].re 
		      + a[ii][mu][i].im*a[ii][nu][i].im;
		}
		ao += q[mu]*q[nu]*da;
		if (mu==nu) ad += da;
	      }
	    }
	    if (mom->val[MOMQ][io]==0.0) {
	      aarr[0][io] += ad/g->ndir; /* TODO: components at Q=0? */
	    } else {
	      Real dl = ao/(mom->val[MOMQ][io]*mom->val[MOMQ][io]);
	      aarr[0][io] += (ad-dl)/(g->ndir-1);
	      aarr[1][io] += dl;
#if 0
	      printf("%2d %2d %2d  %12.6f  %12.6f   ",
		     k[IX]-g->nmom[IX],k[IY]-g->nmom[IY],k[IZ]-g->nmom[IZ],
		     dl,(ad-dl)/(g->ndir-1));
#endif
	    }
#ifdef PARANOID
	    neq[io]++;
#endif
	  }
	}
      }
    }
  }

  /*** Normalise everything ***/

  for (io=0; io<mom->nval; io++) {
#ifdef PARANOID
    aarr[0][io] /= neq[io]*NALG;
    aarr[1][io] /= neq[io]*NALG;
#else
    aarr[0][io] /= mom->nequiv[io]*NALG;
    aarr[1][io] /= mom->nequiv[io]*NALG;
#endif
  }

#ifdef PARANOID
  free(neq);
#endif

  return aarr;
}

Real** gluon_prop_elmag(Gauge* g, Momenta* mom, int* status)
{
  const char fnm[MAXNAME] = "gluon_prop_elmag";
  Lattice *lat = g->parm->lattice;
  const int tdir = IT;
  int ii, io, mu, nu, i, k[DIR];
  Real q[DIR];
  Real **aarr;
  Algebra_c** a = g->u;
#ifdef PARANOID
  int* neq;
#endif

  aarr = arralloc(sizeof(Real),2,3,mom->nval);
  if ( !aarr ) {
    fprintf(stderr,"%s: cannot allocate space for propagators\n",fnm);
    *status = NOSPACE; return 0;
  }

  if (g->type == FGAUGE) return gluon_prop_g(g,mom,status);
  if (g->type != FALG) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,g->type);
    *status = BADDATA; return 0;
  }

#ifdef PARANOID
  neq = calloc(sizeof(int),mom->nval);
#endif
  for (io=0; io<mom->nval; io++) {
    aarr[0][io] = aarr[1][io] = aarr[2][io] = 0.0;
  }

  for (ii=0,k[IT]=0; k[IT]<g->ntotmom[IT]; k[IT]++) {
    q[IT] = 2.0*sin(PI*(k[IT]-g->nmom[IT])/lat->length[IT]);
    for (k[IZ]=0; k[IZ]<g->ntotmom[IZ]; k[IZ]++) {
      q[IZ] = 2.0*sin(PI*(k[IZ]-g->nmom[IZ])/lat->length[IZ]);
      for (k[IY]=0; k[IY]<g->ntotmom[IY]; k[IY]++) {
	q[IY] = 2.0*sin(PI*(k[IY]-g->nmom[IY])/lat->length[IY]);
	for (k[IX]=0; k[IX]<g->ntotmom[IX]; k[IX]++,ii++) {
	  q[IX] = 2.0*sin(PI*(k[IX]-g->nmom[IX])/lat->length[IX]);
	  if ( (io=momindex(k,mom)) != DISCARDED ) {
	    Real qorth = q[IX]*q[IY]+q[IX]*q[IZ]+q[IY]*q[IZ];
	    Real qspc = q[IX]*q[IX]+q[IY]*q[IY]+q[IZ]*q[IZ];
	    Real al=0.0, ad=0.0, as=0.0, a0=0.0, ao=0.0;
	    if (qspc==0.0) printf("kt=%2d\n",k[IT]-g->nmom[IT]);
	    /*******************************************************
	     * da = D_munu           (gluon propagator itself)     *
	     * al = q_mu q_nu D_munu (4-d longitudinal)            *
	     * as = q_i q_j D_ij     (3-d longitudinal)            *
	     * ad = D_mumu           (diagonal trace)              *
	     * a0 = D_00                                           *
	     * ao = 2(D_12 + D_13 + D_23)  (spatial, off-diagonal) *
	     *******************************************************/
	    for (mu=0; mu<g->ndir; mu++) {
	      for (nu=0; nu<g->ndir; nu++) {
		Real da=0.0;
		for (i=0; i<NALG; i++)  {
		  da += a[ii][mu][i].re*a[ii][nu][i].re 
		      + a[ii][mu][i].im*a[ii][nu][i].im;
		}
#if 0
		if (qspc==0.0) {
		  printf("%12.6f  ",da);
		  if (nu==g->ndir-1) printf("\n");
		}
#endif
		al += q[mu]*q[nu]*da;
		if (mu!=tdir && nu!=tdir) as += q[mu]*q[nu]*da;
		if (mu==tdir && nu==tdir) a0 += da;
		if (mu==nu) {
		  ad += da;
		} else if (mu!=tdir && nu!=tdir) {
		  ao += da;
		}
	      }
	    }
	    {
	      Real qspc = mom->val[MOMQS][io]*mom->val[MOMQS][io];
	      Real qtim = mom->val[MOMQT][io]*mom->val[MOMQT][io];
	      Real qsqr = qspc+qtim;
	      Real dl = (qsqr==0.0) ? 0.0 : al/qsqr;    /* longitudinal gluon */
#if 0
	      Real de = (qtim==0.0) ? a0 : (qsqr*as-qspc*qspc*dl)/(qtim*qspc);
	      aarr[0][io] += (ad+ao-de)/(g->ndir-2); /* magnetic gluon */
#else
	      // TODO: implement modifications where the 4-d longitudinal component is nonzero
	      Real de, dm;
	      int ns = g->ndir-1;
	      int nmag = (qspc==0.0) ? g->ndir-1 : g->ndir-2; // TODO: adapt to coulomb gauge etc
	      if (qtim==0.0) {
		de = a0; dm = (ad-de-dl)/nmag;
	      } else if (qspc==0.0) {
		dm = (ad-ao/(ns-1))/ns;
		de = (ad+ao)/ns - dl;
	      } else {
		de =  qsqr/(qspc*qtim)*as;
		dm = (ad-de-dl)/nmag;
	      }
	      aarr[0][io] += dm;                 /* magnetic gluon     */
#endif
	      aarr[1][io] += de;                 /* electric gluon     */
	      aarr[2][io] += dl;                 /* longitudinal gluon */
#if 0
	      printf("%2d %2d %2d %2d  %6.4f %6.4f %6.4f %12.6f  %12.6f  %12.6f \n",
		     k[IX]-g->nmom[IX],k[IY]-g->nmom[IY],k[IZ]-g->nmom[IZ],
		     k[IT]-g->nmom[IT], qspc, qtim, qsqr, 
		     dl,de,(ad-de-dl)/(g->ndir-2));
#endif
	    }
#ifdef PARANOID
	    neq[io]++;
#endif
	  }
	}
      }
    }
  }

  /*** Normalise everything ***/

  for (io=0; io<mom->nval; io++) {
#ifdef PARANOID
    aarr[0][io] /= neq[io]*NALG;
    aarr[1][io] /= neq[io]*NALG;
    aarr[2][io] /= neq[io]*NALG;
#else
    aarr[0][io] /= mom->nequiv[io]*NALG;
    aarr[1][io] /= mom->nequiv[io]*NALG;
    aarr[2][io] /= mom->nequiv[io]*NALG;
#endif
  }

#ifdef PARANOID
  free(neq);
#endif

  return aarr;
}

/************************************************************************
 * compute_one_gluon_prop_diag: computes diagonal components of the     *
 * gluon propagator for a given momentum q.                             *
 * if nprop=1, only the average of all 4 components is computed.        *
 * if nprop=2, the average of the spatial (i,i) and the time (3,3)      *
 *             components are computed separately.  In this case,       *
 *             d[0]=spatial and d[1]=time.                              *
 * if nprop=4, all 4 diagonal components are computed separately.       *
 * The lattice sizes L is a dummy argument, included for consistency    *
 * with compute_one_gluon_prop_tl.                                      *
 ************************************************************************/
int compute_one_gluon_prop_diag(Real* d, Algebra_c* g, int nprop)
{
  const char fnm[MAXNAME] = "compute_one_gluon_prop_diag";
  int i, mu;
  int ndir;

  for (i=0;i<nprop;i++) d[i]=0.0;

  switch (nprop) {
  case 1:
    ndir=DIR;
    for (mu=0;mu<DIR;mu++) for (i=0;i<NALG;i++) {
      d[0] += g[mu][i].re*g[mu][i].re + g[mu][i].im*g[mu][i].im;
    }
    break;
  case 2:
    ndir=SPATIAL_DIR;
    for (mu=0;mu<SPATIAL_DIR;mu++) for (i=0; i<NALG; i++) {
      d[0] += g[mu][i].re*g[mu][i].re + g[mu][i].im*g[mu][i].im;
    }
    for (i=0;i<NALG;i++) {
      d[1] += g[IT][i].re*g[IT][i].re + g[IT][i].im*g[IT][i].im;
    }
    break;
  case DIR:
    ndir=1;
    for (mu=0;mu<DIR;mu++) for (i=0;i<NALG;i++) {
      d[mu] += g[mu][i].re*g[mu][i].re + g[mu][i].im*g[mu][i].im;
    }
    break;
  default:
    fprintf(stderr,"%s: illegal number of propagator components %d\n",
	    fnm,nprop);
    return BADPARAM;
  }

  d[0] /= ndir*NALG;
  for (i=1;i<nprop;i++) d[i] /= NALG;

  return 0;
}

/************************************************************************
 * compute_one_gluon_prop_tl: computes the transverse and longitudinal  *
 * gluon propagator for a given momentum q.                             *
 * return values: d[0] is the transverse component                      *
 *                d[1] is the longitudinal component                    *
 *   unless we are at zero momentum, in which case                      *
 *                d[0] is the spatial diagonal                          *
 *                d[1] is the time-time component                       *
 ************************************************************************/
int compute_one_gluon_prop_tl(Real* d, Algebra_c* g, int q[DIR], int L[DIR])
{
  const char fnm[MAXNAME] = "compute_one_gluon_prop_tl";
  Real ao=0.0, ad=0.0;
  Real qq[DIR];
  Real qsqr=0.0;
  int i, mu, nu;

  if (q[IX]==0 && q[IY]==0 && q[IZ]==0 && q[IT]==0) {
    d[0]=d[1]=0.0;
    for (mu=0;mu<SPATIAL_DIR;mu++) for (i=0;i<NALG;i++) {
      d[0] += g[mu][i].re*g[mu][i].re + g[mu][i].im*g[mu][i].im;
    }
    for (i=0;i<NALG;i++) {
      d[1] += g[IT][i].re*g[IT][i].re + g[IT][i].im*g[IT][i].im;
    }
    d[0] /= DIR*NALG;
    d[1] /= NALG;
    return 0;
  }

  for (mu=0;mu<DIR;mu++) {
    qq[mu] = 2.0*sin(PI*q[mu]/L[mu]);
    qsqr += qq[mu]*qq[mu];
  }
  for (mu=0;mu<DIR;mu++) {
    for (nu=0;nu<DIR;nu++) {
      Real da=0.0;
      for (i=0;i<NALG;i++) {
	da += g[mu][i].re*g[nu][i].re + g[mu][i].im*g[nu][i].im;
      }
      ao += qq[mu]*qq[nu]*da;
      if (mu==nu) ad += da;
    }
  }
  ao /= qsqr;
  d[0] = (ad-ao)/((DIR-1)*NALG);
  d[1] = ao/NALG;

  return 0;
}

static Real** gluon_prop_g(Gauge* g, Momenta* mom, int* status)
{
  const char fnm[MAXNAME] = "gluon_prop_g";
  fprintf(stderr,"%s: Not implemented!\n",fnm);

  *status = EVIL;
  return 0;
}

