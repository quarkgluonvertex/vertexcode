#ifndef _GAUGE_H_
#define _GAUGE_H_
#include "Real.h"
#include "Group.h"
#include "lattice.h"
#include "field_params.h"
#include <magic.h>
#include <stdio.h>

/* Gauge field types */
typedef enum {GAUGE,GAUGEMAT,ALGEBRA,FGAUGE,FALG} Gauge_t;

/** Gauge field structures **/

typedef struct {
  Gauge_parameters* parm;
  Gauge_t type;
  int nmom[DIR];       /* for momentum-space fields */
  int ntotmom[DIR];    /* in position space these are dummy members */
  int ndir;            /* number of components of vector fields */
  int npoint;          /* total number of points (lattice sites or momenta) */
  void* u;             /* link field (U) or gauge potential (A) */
  char filename[MAX_FILENAME];
} Gauge;

typedef struct {
  Gauge_parameters* parm;
  Group_el*  g;
  char filename[MAX_FILENAME];
} Gauge_transform;

typedef struct {
  Gauge_parameters* parm;
  int ndir;     /*  Number of Lorentz directions          *
                 *  (6 for F_munu, 3 for E_i, 4 for A_mu) */
  Algebra_el** e;
  char filename[MAX_FILENAME];
} Algebra_field;

/** Container for Wilson loops **/
typedef struct {
  int Rmax[3];
  Real**** wl;   /* wilson loop values */
  Real* R;       /* spatial separation */
  Real* d;       /* greatest dist of the lattice path from straight line */
  int nR;        /* number of different (non-equiv) spatial separations  */
  int nT;        /* number of time separations */
  int nsmr;      /* number of operators (smearing levels) */
  int* neqv;     /* counter, for averaging     */
} WilsonLoops;

/*** Basic function prototypes (constructors and destructors) ***/
int assign_params_gauge(char* filename, Gauge_parameters* parm);
int set_data_dir(Gauge_parameters*);

int init_trans(Gauge_transform*,Gauge_parameters*,int);
void trans_delete(Gauge_transform g);

int init_gauge(Gauge* u,Gauge_parameters* parm,Gauge_t type);
void gauge_delete(Gauge u);

/*** gauge transformation functions ***/
void gauge_trans(Gauge u, Gauge_transform g);
void gauge_trans_field(Algebra_field e, Gauge_transform g);
void gauge_trans_temporal(Gauge u);
void get_trans_temporal(Gauge u, Gauge_transform g);

/*** cold and hot start    ***/
void unit_gauge(Gauge u);
void unit_trans(Gauge_transform g);
void random_gauge(Gauge u, float peak);
void random_trans(Gauge_transform g, float peak);

/*** average field (simple observable) functions ***/
Real average_trace(Gauge u);
Real average_plaquette(Gauge u);
int  average_plaquettes(Gauge u, Real* plaq);
int  average_stplaq(Gauge u, Real* splaq, Real* tplaq);
Real average_divergence(Gauge u);
Real average_polyakov(Gauge u);
Complex average_cpolyakov(Gauge u);

void compute_staple(Group_el res, Gauge* u, int* x, int ix, int mu, int ndir);
int compute_gluon(Gauge* gluon, Gauge* u);
int wilsonloop(Gauge* u, WilsonLoops* wl);

int smear_gauge(Gauge* usmr, Gauge* u, Real c, int n, int spatial);

/*** filenames and i/o routines ***/
int construct_gauge_filename(Gauge* u);
int construct_trans_filename(Gauge_transform* g);
void gauge_set_filename(Gauge* u, char* name);
int read_gauge(Gauge u);
int pick_gluon(void*,Gauge_parameters*,int*,int*,Gauge_t);
int write_gauge(Gauge u);
int read_trans(Gauge_transform g);
int write_trans(Gauge_transform g);

int print_wilsonloops(WilsonLoops* wl, char* filename, int sweep);
#endif
