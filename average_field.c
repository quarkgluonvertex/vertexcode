#include "gauge.h"
#include <math.h>
#include <float.h>
#define PI 3.14159265359

Real average_trace(Gauge u)
{
  const char fnm[MAXNAME] = "average_trace";
  const int size = u.ndir*u.parm->lattice->nsite;
  Group_el* up = *((Group_el**)u.u);
  Real r=0.0;
  int i;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return -FLT_MAX;
  }

  for (i=0;i<size;i++,up++) r += grp_rtrace(*up);

  return  r/(size*COLOUR);
}

Real average_plaquette(Gauge u)
{
  const char fnm[MAXNAME] = "average_plaquette";
  Lattice *lat = u.parm->lattice;
  int x[DIR],mu,nu,i;
  Real p=0.0;
  Group_el** uu = u.u;
  Group_el u1, u2;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return -FLT_MAX;
  }

  for (i=0,x[IT]=0; x[IT]<lat->length[IT]; x[IT]++)
    for (x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
      for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
	for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++)
	  for (mu=0; mu<u.ndir; mu++) {
	    for (nu=mu+1; nu<u.ndir; nu++) {
	      int im = i + lat->up[x[mu]][mu];
	      int in = i + lat->up[x[nu]][nu];
	      grp_mult(u1,uu[i][mu],uu[im][nu]);
	      grp_mult_dag(u2,u1,uu[in][mu]);
	      grp_mult_dag(u1,u2,uu[i][nu]);
	      p += grp_rtrace(u1);
	    }
	  }

  return p/(lat->nsite*COLOUR*u.ndir*(u.ndir-1)/2);
}

int average_plaquettes(Gauge u, Real* plaq)
{
  const char fnm[MAXNAME] = "average_plaquette";
  Lattice *lat = u.parm->lattice;
  int x[DIR],mu,nu,i,j;
  Group_el** uu = u.u;
  Group_el u1, u2;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return BADDATA;
  }
  for (j=0;j<u.ndir*(u.ndir-1)/2;j++) plaq[j]=0.0;

  for (i=0,x[IT]=0; x[IT]<lat->length[IT]; x[IT]++)
    for (x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
      for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
	for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++)
	  for (j=mu=0; mu<u.ndir; mu++) {
	    for (nu=mu+1; nu<u.ndir; nu++,j++) {
	      int im = i + lat->up[x[mu]][mu];
	      int in = i + lat->up[x[nu]][nu];
	      grp_mult(u1,uu[i][mu],uu[im][nu]);
	      grp_mult_dag(u2,u1,uu[in][mu]);
	      grp_mult_dag(u1,u2,uu[i][nu]);
	      plaq[j] += grp_rtrace(u1);
	    }
	  }

  for (j=0;j<u.ndir*(u.ndir-1)/2;j++) 
    plaq[j]/=lat->nsite*COLOUR;

  return 0;
}

int average_stplaq(Gauge u, Real* splaq, Real* tplaq)
{
  const char fnm[MAXNAME] = "average_plaquette";
  Lattice *lat = u.parm->lattice;
  int x[DIR],mu,nu,i;
  Real ps=0.0, pt=0.0;
  Group_el** uu = u.u;
  Group_el u1, u2;

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return BADDATA;
  }
  if (u.ndir!=DIR) {
    fprintf(stderr,"%s: wrong dimension %d != %d\n",fnm,u.ndir,DIR);
    return BADDATA;
  }
  for (i=0,x[IT]=0; x[IT]<lat->length[IT]; x[IT]++)
    for (x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
      for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
	for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++)
	  for (mu=0; mu<SPATIAL_DIR; mu++) {
	    for (nu=mu+1; nu<u.ndir; nu++) {
	      int im = i + lat->up[x[mu]][mu];
	      int in = i + lat->up[x[nu]][nu];
	      grp_mult(u1,uu[i][mu],uu[im][nu]);
	      grp_mult_dag(u2,u1,uu[in][mu]);
	      grp_mult_dag(u1,u2,uu[i][nu]);
	      if (nu==IT) pt += grp_rtrace(u1); else ps += grp_rtrace(u1);
	    }
	  }


    *splaq = ps/(lat->nsite*COLOUR*u.ndir*(u.ndir-1)/4);
    *tplaq = pt/(lat->nsite*COLOUR*u.ndir*(u.ndir-1)/4);

    return 0;
}

Real average_polyakov(Gauge u)
{
  const char fnm[MAXNAME] = "average_polyakov";
#ifdef SU2_GROUP
  Lattice* lat = u.parm->lattice;
  Group_el** uu = u.u;
  Group_el plnx;
  int i, x[DIR];
  Real p=0;
#else
  Complex p;
#endif
  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return -FLT_MAX;
  }
#ifdef SU2_GROUP
  for (i=0,x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
    for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
      for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++) {
	int site=i;
	grp_unit(plnx);
	for (x[IT]=0; x[IT]<lat->length[IT]; x[IT]++) {
	  Group_el tmp;
	  grp_mult(tmp,plnx,uu[site][IT]);
	  grp_copy(plnx,tmp);
	  site += lat->up[x[IT]][IT];
	}
	p += grp_rtrace(plnx);
      }

  return p/lat->nspatial;
#else
  p = average_cpolyakov(u);
  return sqrt(p.re*p.re+p.im*p.im);
#endif
}

Complex average_cpolyakov(Gauge u)
{
#ifdef SU2_GROUP
  Complex p = {average_polyakov(u),0.0};
  return p;
#else
  const char fnm[MAXNAME] = "average_cpolyakov";
  Lattice* lat = u.parm->lattice;
  Group_el** uu = u.u;
  Group_el plnx;
  int i, x[DIR];
  Complex tr, p={0,0};

  if (u.type!=GAUGE) {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return ILLEGAL_CPLX;
  }

  for (i=0,x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
    for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
      for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++) {
	int site=i;
	grp_unit(plnx);
	for (x[IT]=0; x[IT]<lat->length[IT]; x[IT]++) {
	  Group_el tmp;
	  grp_mult(tmp,plnx,uu[site][IT]);
	  grp_copy(plnx,tmp);
	  site += lat->up[x[IT]][IT];
	}
	tr = grp_ctrace(plnx);
	p.re += tr.re; p.im += tr.im;
      }

  p.re /= lat->nspatial;
  p.im /= lat->nspatial;
  return p;
#endif
}
Real average_divergence(Gauge u)
{
  const char fnm[MAXNAME] = "average_divergence";
  Lattice *lat = u.parm->lattice;
  int x[DIR],mu,i;
  Real p=0.0;
  Algebra_el a1, a2, adiff;

  if (u.type==GAUGE) {
    Group_el** uu = u.u;
    for (i=0,x[IT]=0; x[IT]<lat->length[IT]; x[IT]++)
      for (x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
	for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
	  for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++) {
	    alg_zero(adiff);
	    for (mu=0; mu<u.ndir; mu++) {
	      int idn = i + lat->dn[x[mu]][mu];
	      grp_to_alg(a1,uu[i][mu]);
	      grp_to_alg(a2,uu[idn][mu]);
	      alg_add(adiff,adiff,a1);
	      alg_sub(adiff,adiff,a2);
	    }
	    p += alg_norm(adiff);
	  }
  } else if (u.type==ALGEBRA) {
    Algebra_el** a = u.u;
    for (i=0,x[IT]=0; x[IT]<lat->length[IT]; x[IT]++)
      for (x[IZ]=0; x[IZ]<lat->length[IZ]; x[IZ]++)
	for (x[IY]=0; x[IY]<lat->length[IY]; x[IY]++)
	  for (x[IX]=0; x[IX]<lat->length[IX]; x[IX]++,i++) {
	    alg_zero(adiff);
	    for (mu=0; mu<u.ndir; mu++) {
	      int idn = i + lat->dn[x[mu]][mu];
	      alg_add(adiff,adiff,a[i][mu]);
	      alg_sub(adiff,adiff,a[idn][mu]);
	    }
	    p += alg_norm(adiff);
	  }
  } else if (u.type==FALG) {
    Algebra_c** a = u.u;
    Real q[DIR];
    for (i=0,x[IT]=0; x[IT]<u.ntotmom[IT]; x[IT]++) {
      q[IT] = 2.0*sin(PI*(x[IT]-u.nmom[IT])/lat->length[IT]);
      for (x[IZ]=0; x[IZ]<u.ntotmom[IZ]; x[IZ]++) {
	q[IZ] = 2.0*sin(PI*(x[IZ]-u.nmom[IZ])/lat->length[IZ]);
	for (x[IY]=0; x[IY]<u.ntotmom[IY]; x[IY]++) {
	  q[IY] = 2.0*sin(PI*(x[IY]-u.nmom[IY])/lat->length[IY]);
	  for (x[IX]=0; x[IX]<u.ntotmom[IX]; x[IX]++,i++) {
	    Algebra_c qa;
	    algc_zero(qa);
	    q[IX] = 2.0*sin(PI*(x[IX]-u.nmom[IX])/lat->length[IX]);
	    for (mu=0; mu<u.ndir; mu++) {
	      int ialg;
	      for (ialg=0;ialg<NALG;ialg++) {
		qa[ialg].re += q[mu]*a[i][mu][ialg].re;
		qa[ialg].im += q[mu]*a[i][mu][ialg].im;
	      }
	    }
	    p += algc_norm(qa);
	  }
	}
      }
    }
  } else {
    fprintf(stderr,"%s: illegal gauge type %d\n",fnm,u.type);
    return -FLT_MAX;
  }

  return p/(u.npoint*NALG*DIR);
}

void compute_staple(Group_el res, Gauge* u, int* x, int site, int mu, int ndir)
{
  Lattice* lat = u->parm->lattice;
  Group_el** uu = u->u;
  Group_el u1,u2;
  int im = site + lat->up[x[mu]][mu];
  int i, nu;

  grp_zero(res);

  for (nu=0;nu<ndir;nu++) {
    int in = site + lat->up[x[nu]][nu];
    int jn = site + lat->dn[x[nu]][nu];
    int jnm = jn + lat->up[x[mu]][mu];
    if (nu==mu) continue;
    grp_mult(u1,uu[site][nu],uu[in][mu]);
    grp_mult_dag(u2,u1,uu[im][nu]);
    grp_addto(res,u2);
    grp_dag_mult(u1,uu[jn][nu],uu[jn][mu]);
    grp_mult(u2,u1,uu[jnm][nu]);
    grp_addto(res,u2);
  }
}
