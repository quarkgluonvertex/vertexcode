#include "sort.h"

/*  keysort: creates an index array from the array val, so that              *
 *   val[key[i]] is ordered in i.  Uses quicksort.                           *
 *  May be made into a general-purpose routine like qsort                    */

#define SWAP(a,b) { int tmp=(a);(a)=(b);(b)=tmp; }
#define NSTACK 50
#define M 7

void keysort(Real *val, int length, int *key)
{
  int left=0, right=length-1, i, j, l;
  int istack=0;
  int stack[NSTACK];
  Real a;

  /* Initialise key sequence */
  for (i=0; i<length; i++) key[i]=i;

  while (1) {
    if (right-left<M) {
      /* sort remaining element(s) directly */
       for (i=left+1; i<=right; i++) {
 	a=val[key[i]]; l=key[i];
 	for (j=i-1;j>=left;j--) {
 	  if (val[key[j]]<=a) break;
 	  key[j+1]=key[j];
 	}
 	key[j+1]=l;
       }
      if (right>left && val[key[left]]>val[key[right]])
	SWAP(key[left],key[right]);
      /* check stack */
      if (istack) { right= stack[--istack]; left=stack[--istack]; }
      else return;  // finished sorting??
    } else {
      /* choose partitioning element as median of left, right, centre */
      SWAP(key[(left+right)/2],key[left+1]);
      if (val[key[left]]  >val[key[right ]]) SWAP(key[left],key[right]);
      if (val[key[left]]  >val[key[left+1]]) SWAP(key[left],key[left+1]);
      if (val[key[left+1]]>val[key[right ]]) SWAP(key[left+1],key[right]);
      a = val[key[l=left+1]];
      /* partition into subarray < a (left) and > a (right) */
      i=l; j=right;
      while (i<j) {
	do i++; while (val[key[i]] < a);
	do j--; while (val[key[j]] > a);
	if (i<j) SWAP(key[i],key[j]);
      }
      /* put partitioning element back in place */
      SWAP(key[l],key[j]);
      /* partitioning complete. push larger subarray onto stack  *
       * and proceed with smaller                                */
      if (istack>NSTACK-2) {
	printf("Error: stack too small on %d, %s\n", __LINE__, __FILE__);
	exit(-1);
      }
      if ( j-left < right-i) {
	stack[istack++] = i; stack[istack++] = right;
	right=j;
      } else {
	stack[istack++] = left; stack[istack++] = j;
	left = i;
      }
    }      
  }
  
}

#undef SWAP
