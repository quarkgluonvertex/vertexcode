#include "gauge.h"
#include "swap.h"
#ifdef XDR
#include "xdr_gauge.h"
#endif
#include <producers.h>
#ifdef SU3_GROUP
#include "su3_precision.h"
#endif

int read_trans_roma(Gauge_transform g);
int read_trans_ukqcd(Gauge_transform g);
#ifdef XDR
int read_trans_xdr(Gauge_transform g);
#endif

int read_trans(Gauge_transform g)
{
  int status;

  switch(g.parm->producer_id) {
  case ROMA:
    status = read_trans_roma(g);
    break;
  case UKQCD:
    status = read_trans_ukqcd(g);
    break;
#ifdef XDR
  case PROD_INTERNAL:
    status = read_trans_xdr(g);
    break;
#endif
  default:
    fprintf(stderr,"read_trans: unknown producer_id %d\n",
	    g.parm->producer_id);
    status = BADPARAM;
  }

  return status;
}

#ifdef XDR
int read_trans_xdr(Gauge_transform g)
{
  const int size=g.parm->lattice->nsite;
  int n=size;
  XDR xdrs;
  FILE* in;
  Gauge_parameters par;
  Lattice lat;
  lat.up = lat.dn = 0;
  lat.has_ntab = FALSE;
  par.lattice = &lat;

  in = fopen(g.filename,"rb");
  if (!in) {
    printf("cannot open %s\n",g.filename);
    return BADFILE;
  }
  xdrstdio_create(&xdrs,in,XDR_DECODE);

  xdr_parm(&xdrs,&par);

  // TODO: check parameters and copy dynamical ones here!!

  xdr_array(&xdrs, &(g.g), &n, size, sizeof(Group_el), xdr_grpel);

  if (n != size) {
    printf("error in reading %s: read only %d of %d items\n",g.filename,n,size);
    fclose(in);
    return BADDATA;
  }
  if (fclose(in)) {
    printf("error on closing %s\n",g.filename);
    return BADFILE;
  }

  return 0;
}
#endif

int read_trans_roma(Gauge_transform g)
{
#ifdef SU3_GROUP
  FILE* in;
  const int size = g.parm->lattice->nsite;
  int n;

/*** Check that the gauge type is correct (covariant only) ***/
  if (g.parm->gauge != COVARIANT) {
    printf("read_trans_roma: invalid gauge type %d\n",g.parm->gauge);
    printf("read_trans_roma: only covariant gauge (%d) available from Roma\n",
	   COVARIANT);
    return BADPARAM;
  }

/*** We assume the filename has already been constructed by the constructor ***/
  in = fopen(g.filename,"rb");
  if (!in) {
    printf("cannot open %s\n",g.filename);
    return BADFILE;
  }

  if (fread(&n,sizeof(int),1,in)!=1) {
    printf("error in reading dummy int from %s\n",g.filename);
    fclose(in);
    return BADFILE;
  }

/*** Here we must put in information about the precision and byte-ordering ***/
/*** The Roma files are double precision, big-endian ***/
  if (sizeof(Real)==sizeof(double)) { /* read directly to data array */
    if ( (n=fread((Short_Su3*)g.g,sizeof(Short_Su3),size,in)) != size ) {
      printf("error in reading %s: read only %d of %d items\n",g.filename,n,size);
      fclose(in);
      return BADDATA;
    }
/* byte swap here if necessary */
#ifdef LITTLE_ENDIAN
    byte_swap(g.g,sizeof(double),size*sizeof(Short_Su3));
#endif
  } else { /* read to temporary array and convert */
    const int dsize = size*COLOUR*(COLOUR-1)*2;
    double* tmp = malloc(sizeof(double)*dsize);
    Real* gp = (Real*)g.g;

    if (!tmp) {
      fprintf(stderr,"read_trans_roma: cannot alloc temporary array\n");
      fclose(in);
      return NOSPACE;
    }
    if ( (n=fread(tmp,sizeof(double),dsize,in)) != dsize ) {
      fprintf(stderr,"read_trans_roma: error in reading %s: read only %d of %d items\n",
	      g.filename,n,dsize);
      fclose(in);
      return BADDATA;
    }
#ifdef LITTLE_ENDIAN
    byte_swap(tmp,sizeof(double),dsize*sizeof(double));
#endif
    for (n=0;n<dsize;n++) { *(gp++) = (Real)*(tmp++); }
    free(tmp);
  }
  if (fclose(in)) {
    printf("error on closing %s\n",g.filename);
    return BADFILE;
  }
  uncompress_Su3(g.g,size);

  return 0;
#else
  fprintf(stderr,"read_trans_roma: wrong gauge group!\n");
  return EVIL;
#endif
}

/*** UKQCD files may be single or double precision but may be ***
 *** converted to native byte ordering with ieeeconvert       ***/
int read_trans_ukqcd(Gauge_transform u)
{
#ifdef SU3_GROUP
/* TODO: implement single/double precision in id flag */
  FILE *in;      
  int n, t, icol, jcol;
  const int Lt = u.parm->lattice->length[IT];
  const int vol = u.parm->lattice->nspatial;
  const int dsize = vol*sizeof(Short_Su3)/sizeof(Real); /* # Reals in a timeslice */
  const int rsize = sizeof(Real);
  int dprec;    /* double/single precision        */
  int do_swap;  /* byte swapped or native ordered */

  void* u_tmp; /* can be forced to float, double, short_su3_double etc */
  int ssu3_size;  /* size of short su(3) matrices on file */
  int in_size;    /* size of real number on file (float/double) */

  Short_Su3* ushort;
  char filename[FILENAME_MAX];

/***  work out the precision and byte ordering of the input data    ***/
/***  id  0,2: double precision; 1,3: single precision  ***
 ***      0,1: native ordering;  2,3: byte-swapped      ***/
  do_swap = ( (u.parm->id/2)%2 == 1);
  dprec = ( u.parm->id%2 == 0 );

/*** define input sizes (single/double precision) ***/
  if (dprec) {
    in_size = sizeof(double);
    ssu3_size = sizeof(Short_Trans_Su3_double);
  } else {
    in_size = sizeof(float);
    ssu3_size = sizeof(Short_Trans_Su3_single);
  }
/*** if the input size is bigger than the internal size we must read ***
 *** into a temporary array, otherwise we can use the data space     ***/
  if (in_size > rsize) {
    u_tmp = malloc(ssu3_size*vol);
    if (!u_tmp) {
      fprintf(stderr,"read_trans_ukqcd: cannot alloc temporary array\n");
      return NOSPACE;
    }
  }
 
/***  Loop over timeslices and read from timesliced files  ***/

  for (t=0; t<Lt; t++) {
    void *gt = u.g+t*vol;
    if (in_size <= rsize) u_tmp = gt;
    sprintf(filename, "%sT%02d", u.filename, t);

/*** open the file for the first timeslice, otherwise reopen ***/
    if (t==0) {
      in=fopen(filename,"rb");
    } else {
      in=freopen(filename,"rb",in);
    }
    if ( !in ) {
      fprintf(stderr,"read_trans_ukqcd: cannot open %s\n", filename);
      return BADFILE;
    }

/*** read to temporary data space ***/
    if((n=fread(u_tmp, ssu3_size,vol,in)) != vol) {
      printf("Failed to read transformation from %s\n", filename);
      return BADDATA;
    }
/*** byte swap if necessary ***/
    if (do_swap) byte_swap(u_tmp,sizeof(Real),vol*ssu3_size/sizeof(Real));

/*** move from temporary array to data array if necessary ***
 *** and swap indices: Colour indices in cray format are  ***
 *** the opposite of the order here                       ***/
    if ( in_size != rsize ) {
      ushort = (Short_Su3*)gt+vol;
/* float and double must be treated separately to avoid type conflict */
      if (in_size==sizeof(double)) {
	double* ud = (double*)u_tmp+dsize;
	while ( ushort > (Short_Su3*)gt ) {
	  --ushort;
	  for (icol=0; icol < COLOUR; icol++)
	    for (jcol=0; jcol < COLOUR-1; jcol++) {
	      (*ushort)[jcol][icol].re = *(--ud);
	      (*ushort)[jcol][icol].im = *(--ud);
	    }
	}
      } else if (in_size==sizeof(float)) {
	float* us = (float*)u_tmp+dsize;
	Short_Su3 gtmp;
	while ( ushort > (Short_Su3*)gt+1 ) {
	  --ushort;
	  for (icol=0; icol < COLOUR; icol++)
	    for (jcol=0; jcol < COLOUR-1; jcol++) {
	      (*ushort)[jcol][icol].re = *(--us);
	      (*ushort)[jcol][icol].im = *(--us);
	    }
	}
/** the last matrix must be done separately to avoid overwriting **
 ** since the tmp data has been read into the data space         **/
	for (icol=0; icol < COLOUR; icol++)
	  for (jcol=0; jcol < COLOUR-1; jcol++) {
	    gtmp[jcol][icol].re = *(--us);
	    gtmp[jcol][icol].im = *(--us);
	  }
	for (icol=0; icol < COLOUR-1; icol++)
	  for (jcol=0; jcol < COLOUR; jcol++) {
	    (*ushort)[jcol][icol] = gtmp[icol][jcol];
	  }
      }
/*** Restore SU(3) matrices ***/
      uncompress_Su3(gt, vol);
    }
    else {
/*** Restore SU(3) and swap colour indices ***/
      uncompress_Su3_trans(gt, vol);
    }
  } /*** end loop over timeslice ***/

/***  Close the file at the end ***/
  if(fclose(in)) {
    fprintf(stderr,"read_trans_ukqcd: error on closing %s\n", filename);
    return BADFILE;
  }

/***  Dont forget to free the temporary space!  ***/
  if (in_size > rsize) free(u_tmp);

  return 0;
#else
  fprintf(stderr,"read_trans_ukqcd: wrong gauge group!\n");
  return EVIL;
#endif
}  
