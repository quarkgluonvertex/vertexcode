#include "analysis.h"
#include "rand.h"
#include <stdarg.h>
/* ALIGN returns the next b byte aligned address after a */
#define ALIGN(a,b)	(int*)( (((long)(a) + (b) - 1)/(b))*(b) )

/************************************************************************
 *                                                                      *
 * Routines to generate and analyse bootstrap samples of data           *
 * plus operations on Boot structures                                   *
 *                                                                      *
 ************************************************************************/

static int formal_cov = FALSE;
void set_formal_bootcov(int f) { formal_cov = f; }

static int compare(const void* xv, const void* yv);
static Real find_value_for_position(Real position, Real* ordered);

Boot init_boot(int nboot)
{
  Boot res;

  res.best  = 0.0;
  res.nboot = nboot;
  res.boot  = vecalloc(sizeof(Real),nboot);

  return res;
}

void destroy_boot(Boot bt)
{
  free(bt.boot);
}

void* init_bootarr(int nboot, int ndim, ...)
{
  const int size = sizeof(Boot);
  int* dims = malloc(sizeof(int)*ndim);
  void** arr;
  void** start;
  Boot* bp;
  Real* rp;
  int i, ndat=1, nptr=0;
  va_list ap;

  if (!dims) return 0;

  va_start(ap,ndim);
  for (i=0;i<ndim;i++) {
    dims[i] = va_arg(ap,int);
    ndat *= dims[i];
    if (i<ndim-1) nptr += ndat;
  }
  va_end(ap);

  /*** allocate space for pointers and data, including boot samples ***/
  if ( !(start = malloc(size*(ndat+1)+nptr*sizeof(void**)
			+ (ndat*nboot+1)*sizeof(Real)) ) ) return 0;

  /*** set up pointer array ***/
  if (ndim>1) subarray(size,size,ndim-1,1,&arr,start,dims,0);
  else arr = start;
  free(dims);

  /*** finally initialise all the Boot structures ***/
  bp = ndim>1 ? (Boot*)ALIGN(arr+nptr,sizeof(Boot)) : (Boot*)(arr+nptr);
  rp = (Real*)ALIGN((bp+ndat),sizeof(Real));
  for (i=0; i<ndat; i++) {
    bp->best = 0.0;
    bp->nboot = nboot;
    bp->boot = nboot ? rp : 0;
    bp++;
    rp += nboot;
  }
  return arr;
}

#undef ALIGN

void boot_constant(Boot *res, Real constant)
{
  int i;

  res->best = constant;
  for (i=0;i<res->nboot;i++) res->boot[i]=constant;
}

void boot_copy(Boot *res, Boot src)
{
  const char fnm[MAXNAME] = "boot_copy";
  int i;

  if (res->nboot != src.nboot) {
    fprintf(stderr, "%s: incompatible arguments\n",fnm);
    fprintf(stderr, " nboot(res)=%d  nboot(src)=%d\n", 
            res->nboot, src.nboot);
    return;
  }

  res->best = src.best;
  for (i=0;i<res->nboot;i++) res->boot[i] = src.boot[i];
}

void scale_boot(Boot *res, Boot src, Real scale)
{
  const char fnm[MAXNAME] = "scale_boot";
  int i;

  if (res->nboot != src.nboot) {
    fprintf(stderr, "%s: incompatible arguments\n",fnm);
    fprintf(stderr, " nboot(res)=%d  nboot(src)=%d\n", 
            res->nboot, src.nboot);
    return;
  }

  res->best = scale*src.best;
  for (i=0;i<res->nboot;i++) res->boot[i] = scale*src.boot[i];
}

void add_boot(Boot *res, Boot bt1, Boot bt2)
{
  const char fnm[MAXNAME] = "scale_boot";
  int i;

  if (res->nboot != bt1.nboot || res->nboot != bt2.nboot) {
    fprintf(stderr,"%s: incompatible arguments\n",fnm);
    fprintf(stderr," nboot(res)=%d  nboot(src)=%d %d\n", 
            res->nboot,bt1.nboot,bt2.nboot);
    return;
  }

  res->best = bt1.best + bt2.best;
  for (i=0;i<res->nboot;i++) res->boot[i] = bt1.boot[i] + bt2.boot[i];
}

/*** boot_func takes a list of Boots and applies the narg-param function ***
 *** func to all the bootstrap samples, putting the result in res        ***/
int boot_func(Boot* res, Real (*func)(Real*,int), int narg, ...)
{
  const char fnm[MAXNAME] = "boot_func";
  Real* args = vecalloc(sizeof(Real),narg);
  Boot* bootlist = vecalloc(sizeof(Boot),narg);
  va_list ap;
  int i, iboot, nboot;

  va_start(ap,narg);
  for (i=0;i<narg;i++) bootlist[i] = va_arg(ap,Boot);
  va_end(ap);

  nboot = bootlist[0].nboot;
  for (i=0;i<narg;i++) {
    if (bootlist[i].nboot != nboot) {
      if (nboot==0) nboot=bootlist[i].nboot;
      else {
	fprintf(stderr,"%s: incompatible nboot %d!=%d for argument %d\n",
		fnm, bootlist[i].nboot,nboot,i);
	return BADDATA;
      }
    }
    args[i]=bootlist[i].best;
  }
  if (res->nboot != nboot) {
    fprintf(stderr,"%s: incompatible nboot %d!=%d for result pointer\n",
	    fnm, res->nboot,nboot);
    return BADDATA;
  }
  res->best = func(args,narg);

  for (iboot=0;iboot<nboot;iboot++) {
    for (i=0;i<narg;i++) {
      if (bootlist[i].nboot==0) args[i]=bootlist[i].best; 
      else args[i]=bootlist[i].boot[iboot];
    }
    res->boot[iboot] = func(args,narg);
  }
  free(bootlist);
  free(args);

  return 0;
}

/*** bootarr_func is like boot_func but takes an array of Boots ***
 *** as argument rather than a va_list                          ***/
int bootarr_func(Boot* res, Real (*func)(Real*,int), int narg, Boot* arg)
{
  const char fnm[MAXNAME] = "boot_func";
  Real* args = vecalloc(sizeof(Real),narg);
  int i, iboot, nboot;

  nboot = arg[0].nboot;
  for (i=0;i<narg;i++) {
    if (arg[i].nboot != nboot) {
      if (nboot==0) nboot=arg[i].nboot;
      else {
	fprintf(stderr,"%s: incompatible nboot %d!=%d for argument %d\n",
		fnm, arg[i].nboot,nboot,i);
	return BADDATA;
      }
    }
    args[i]=arg[i].best;
  }
  if (res->nboot != nboot) {
    fprintf(stderr,"%s: incompatible nboot %d!=%d for result pointer\n",
	    fnm, res->nboot,nboot);
    return BADDATA;
  }
  res->best = func(args,narg);

  for (iboot=0;iboot<nboot;iboot++) {
    for (i=0;i<narg;i++) {
      if (arg[i].nboot==0) args[i]=arg[i].best; 
      else args[i]=arg[i].boot[iboot];
    }
    res->boot[iboot] = func(args,narg);
  }
  free(args);

  return 0;
}

/*** boot_func_mixed is like boot_func but takes Real (scalar) as well ***
 *** as bootstrapped arguments                                         ***/
int boot_func_mixed(Boot* res, Real (*func)(Real*,int), int nbt, int nrl, ...)
{
  const char fnm[MAXNAME] = "boot_func_mixed";
  const int narg = nbt+nrl;
  Real* args = vecalloc(sizeof(Real),narg);
  Boot* bootlist = vecalloc(sizeof(Boot),nbt);
  va_list ap;
  int i, iboot, nboot;

  va_start(ap,nrl);
  for (i=0;i<nbt;i++) bootlist[i] = va_arg(ap,Boot);
  for (i=nbt;i<narg;i++)  args[i] = va_arg(ap,Real);
  va_end(ap);

  nboot = bootlist[0].nboot;
  for (i=0;i<nbt;i++) {
    if (bootlist[i].nboot != nboot) {
      if (nboot==0) nboot=bootlist[i].nboot;
      else {
	fprintf(stderr,"%s: incompatible nboot %d!=%d for argument %d\n",
		fnm, bootlist[i].nboot,nboot,i);
	return BADDATA;
      }
    }
    args[i]=bootlist[i].best;
  }
  if (res->nboot != nboot) {
    fprintf(stderr,"%s: incompatible nboot %d!=%d for result pointer\n",
	    fnm, res->nboot,nboot);
    return BADDATA;
  }
  res->best = func(args,narg);

  for (iboot=0;iboot<nboot;iboot++) {
    for (i=0;i<nbt;i++) {
      if (bootlist[i].nboot==0) args[i]=bootlist[i].best; 
      else args[i]=bootlist[i].boot[iboot];
    }
    res->boot[iboot] = func(args,narg);
  }
  free(bootlist);
  free(args);

  return 0;
}

/***  restore_bootdata: creates bootstrap samples of data (NOT datasets) ***
 ***  from bootstrapped parameters bootparam read from bootfile(s).      ***
 ***  The data are returned in the array bootdata.                       ***/
void restore_bootdata(Boot* bootdata, Real* xdata, int ndata,
		      Boot* bootparam, int nparam, Modelfunction paramfunc)
{
  const int nboot = bootparam[0].nboot;
  Real *params = vecalloc(sizeof(Real),nparam);
  int i, j, k;

  for (i=0; i<ndata; i++) {
    bootdata[i].nboot = nboot; // TODO: check compatibility!
    for (k=0; k<nparam; k++) {
      params[k] = bootparam[k].best;
    }
    (*paramfunc)(xdata[i], params, &bootdata[i].best, 0, 0, 0, nparam);

    for (j=0; j<nboot; j++) {
      for (k=0; k<nparam; k++) {
	if (bootparam[k].nboot != 0) {
	  params[k] = bootparam[k].boot[j];
	}
      }
      (*paramfunc)(xdata[i], params, &bootdata[i].boot[j], 0, 0, 0, nparam);
    }
  }

  free(params);
}

/***  bootstrap_model: as restore_bootdata, but for a single point ***
 ***  and with bootstrapped xdata                                  ***/
int bootstrap_model(Boot* ydata, Modelfunction* model,
		    Boot* fitparam, int nparam, Boot xdata)
{
  const char fnm[MAXNAME] = "bootstrap_model";
  const int nboot = ydata->nboot;
  int i, j;
  Real *a = vecalloc(sizeof(Real),nparam);
  Real x;

  if (xdata.nboot != nboot && xdata.nboot != 0) {
    fprintf(stderr,"%s: incompatible nboots %d!=%d\n",fnm,xdata.nboot,nboot);
    return BADDATA;
  }

  x = xdata.best;
  for (j=0;j<nparam;j++) {
    if (fitparam[j].nboot != nboot && fitparam[j].nboot != 0) {
      fprintf(stderr,"%s: incompatible nboots %d!=%d for param %d\n",
	      fnm,xdata.nboot,nboot,j);
      return BADDATA;
    }
    a[j] = fitparam[j].best;
  }
  model(x,a,&ydata->best,0,0,0,nparam);


  for (i=0;i<nboot;i++) {
    if (xdata.nboot) x=xdata.boot[i];
    for (j=0;j<nparam;j++) {
      if (fitparam[j].nboot) a[j]=fitparam[j].boot[i];
    }
    model(x,a,&ydata->boot[i],0,0,0,nparam);
  }

  free(a);
  return 0;
}

/** get_boot_datasetbundle: Make a bootstrap resampling of the data **
 ** contained in vectorbundle and scalarbundle                      **
 ** The bootstrap samples are returned in (vector|scalar)bootbundle **/
void get_boot_datasetbundle(Real*** vectorbootbundle,
			    Real*** vectorbundle, int nvector,
			    Real** scalarbootbundle,
			    Real** scalarbundle, int nscalar, int nset)
{
  int *bootlist = vecalloc(sizeof(int), nset);
  int bundle, set;

  get_boot_subset(bootlist, nset);

  for (bundle=0; bundle<nvector; bundle++) {
    for (set=0; set<nset; set++) {
      vectorbootbundle[bundle][set] = vectorbundle[bundle][bootlist[set]];
    }
  }

  for (bundle=0; bundle<nscalar; bundle++) {
    for (set=0; set<nset; set++) {
      scalarbootbundle[bundle][set] = scalarbundle[bundle][bootlist[set]];
    }
  }

  free(bootlist);
}

void resample_datasetbundle(Real*** vectorbootbundle,
			    Real*** vectorbundle, int nvector,
			    Real** scalarbootbundle,
			    Real** scalarbundle, int nscalar, int nset,
			    int* bootlist)
{
  int bundle, set;

  for (bundle=0; bundle<nvector; bundle++) {
    for (set=0; set<nset; set++) {
      vectorbootbundle[bundle][set] = vectorbundle[bundle][bootlist[set]];
    }
  }

  for (bundle=0; bundle<nscalar; bundle++) {
    for (set=0; set<nset; set++) {
      scalarbootbundle[bundle][set] = scalarbundle[bundle][bootlist[set]];
    }
  }
}

/** get_boot_datasetbundle_multi: Make a bootstrap resampling of data     **
 ** from different ensembles with different number of configurations nset ** 
 ** The data are contained in vectorbundle and scalarbundle               **
 ** The bootstrap samples are returned in (vector|scalar)bootbundle       **/
void get_boot_datasetbundle_multi(Real*** vectorbootbundle,
				  Real*** vectorbundle, int nvector,
				  Real** scalarbootbundle,
				  Real** scalarbundle, int nscalar, 
				  int* nset, int ndata)
{
  int* bootlist;
  int bundle, set, data;
  int maxset, dmaxset;

  /* find the ensemble dmaxset with the largest number maxset of configs */
  maxset=0; dmaxset=0;
  for (data=0;data<ndata;data++) 
    if (nset[data]>maxset) {
      maxset=nset[data];
      dmaxset = data;
    }
  bootlist = vecalloc(sizeof(int),maxset);

  /* Each 'data' corresponds to a different ensemble so we must    *
   * resample them separately.                                     *
   * In the absence of more information we take the scalar data as *
   * coming from the ensemble with the largest number of configs   */
  for (data=0;data<ndata;data++) {
    get_boot_subset(bootlist, nset[data]);
  
    for (bundle=0; bundle<nvector; bundle++) {
      for (set=0; set<nset[data]; set++) {
      vectorbootbundle[bundle][set][data]
	= vectorbundle[bundle][bootlist[set]][data];
      }
    }
    if (data==dmaxset) {
      for (bundle=0;bundle<nscalar;bundle++) {
	for (set=0;set<maxset; set++) {
	  scalarbootbundle[bundle][set] = scalarbundle[bundle][bootlist[set]];
	}
      }
    }
  }

  free(bootlist);
}

void get_boot_subset(int *subset, int nset)
{
  int set;

  for (set=0; set<nset; set++) {
    subset[set] = (int)(nset*my_drand48(0));
  }
}

/** write the bootstrap sample information as a plain text file **/
int write_bootinfo(Bootinfo* info, char* filename)
{
  const char fnm[MAXNAME] = "write_bootinfo";
  int bt, cf;
  FILE* out;

  if ( !(out=fopen(filename,"w")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
    return BADFILE;
  }
  fprintf(out,"nboot=%d\nnconfig=%d\n",info->nboot,info->nconfig);
  for (bt=0;bt<info->nboot;bt++) {
    for (cf=0;cf<info->nconfig;cf++) {
      fprintf(out,"%d ",info->config_list[bt][cf]);
    }
    fprintf(out,"\n");
  }
  fclose(out);
  return 0;
}

/** read the bootstrap sample information from a plain text file **/
int read_bootinfo(Bootinfo* info, char* filename)
{
  const char fnm[MAXNAME] = "read_bootinfo";
  int bt, cf;
  FILE* in;

  if ( !(in=fopen(filename,"r")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,filename);
    return BADFILE;
  }
  fscanf(in,"nboot=%d\nnconfig=%d\n",&info->nboot,&info->nconfig);
  for (bt=0;bt<info->nboot;bt++) {
    for (cf=0;cf<info->nconfig;cf++) {
      if (fscanf(in,"%d ",&info->config_list[bt][cf]) != 1) {
	fprintf(stderr,"%s: error reading %s\n",fnm,filename);
	fclose(in);
	return BADFILE;
      }
    }
    fscanf(in,"\n");
  }
  fclose(in);
  return 0;
}

void find_minmax_boot(Boot* data, int ndata, Real* min, Real* max)
{
  Real hi=data->best, lo=data->best;
  int i;

  for (i=0;i<data->nboot;i++) {
    if (data->boot[i]>hi) hi=data->boot[i];
    if (data->boot[i]<lo) lo=data->boot[i];
  }
  *min = lo;
  *max = hi;
}

int boot_covariance(Real** cov, Boot* data, int ndata, int* mask)
{
  const char fnm[MAXNAME] = "boot_covariance";
  const int nboot = data->nboot;
  const int len = ntrue(mask,ndata);
  Real* ave = vecalloc(sizeof(Real),len);
  int* ii = vecalloc(sizeof(int),len);
  Real* scale = formal_cov ? 0 : vecalloc(sizeof(Real),len);
  int i, j, ibt;

  if (!ave || !ii || (!formal_cov && !scale)) {
    fprintf(stderr,"%s: not enough space!\n",fnm);
    return NOSPACE;
  }
  for (i=j=0;i<ndata;i++) {
    if (MASK(mask,i)) {
      Real med, lo, hi;
      if (data[i].nboot!=nboot) {
	fprintf(stderr,"%s: incompatible boots\n",fnm);
	free(ave); free(ii);
	if (scale) free(scale);
	return BADDATA;
      }
      get_boot_stats(&data[i],&ave[j],&med,&lo,&hi,0.68);
      if (!formal_cov) scale[j] = 0.5*(hi-lo);
      ii[j++] = i;
    }
  }

  for (i=0;i<len;i++) {
    for (j=i;j<len;j++) {
      Real cij = 0.0;
      for (ibt=0;ibt<nboot;ibt++) {
	cij += (data[ii[i]].boot[ibt]-ave[i])*(data[ii[j]].boot[ibt]-ave[j]);
      }
      cov[i][j] = cov[j][i] = cij/nboot;
    }
  }
  free(ii);

  /* rescale with the bootstrap errors if appropriate */
  if (!formal_cov) {
    for (i=0;i<len;i++) scale[i] /= sqrt(cov[i][i]);
    for (i=0;i<len;i++) for (j=0;j<len;j++) cov[i][j] *= scale[i]*scale[j];
    free(scale);
  }

  free(ave);
  return 0;
}

void get_boot_stats(Boot* data,	Real* y_average, Real* y_median,
		    Real* y_low, Real* y_high, Real conf_level )
{
  const int nboot = data->nboot;
  Real pos;
   int i;

  if (nboot) {
    Real* ordered = vecalloc(sizeof(Real),nboot);
   /**	Start with getting y_average **/
    *y_average = 0.0;
    for (i=0;i<nboot;i++) *y_average += data->boot[i];
    *y_average /= nboot;

    for (i=0; i<nboot; i++) ordered[i] = data->boot[i];
    qsort((char *)&ordered[0],nboot,sizeof(Real),compare);

    /** Get low confidence bound **/
    pos = (1.0-conf_level)*0.5*nboot - 1.0;
    if (pos<0) pos=0;
    *y_low = find_value_for_position(pos,ordered);

    /** Get high confidence bound **/
    pos = (1.0+conf_level)*0.5*nboot;
    if (pos>nboot-1) pos=nboot-1;
    *y_high = find_value_for_position(pos,ordered);

    /** Get median **/
    pos = 0.5*nboot - 1.0;
    *y_median = find_value_for_position(pos,ordered);

    free(ordered);
  } else {
    /** No bootstraps, object is a pure number with no distribution **
     ** all quantities are just the best number                     **/
    *y_average = *y_median = *y_high = *y_low = data->best;
  }

}

void print_bootstats(Boot data)
{
  Real ave, med, low, high;
  get_boot_stats(&data, &ave, &med, &low, &high, 0.68);
  printf("%-10.6g (+ %-10.6g - %-10.6g)\n",
	 data.best, high-data.best, data.best-low);
}

void print_stats(BootFitFuncs* data)
{
  Real ave, med, low, high;
  int i;

  for (i=0; i<data->nparam; i++) {
    get_boot_stats(&data->param[i], &ave, &med, &low, &high, 0.68);
    printf("%-10.6g (+ %-10.6g - %-10.6g)\n", data->param[i].best,
	   high-data->param[i].best, data->param[i].best-low);
  }

  get_boot_stats(&data->chisq, &ave,&med,&low,&high,0.99);
  printf("chi^2 = %-10.6g  ( %g -- %g )\n", data->chisq.best,low,high);
  printf("chi^2/dof = %-10.6g\n",data->chisq.best/data->ndof);
  fflush(stdout);
}

static Real find_value_for_position(Real position, Real* ordered)
{
  int low_pos, high_pos;
  Real low, high;

  /** find closest integers to pos **/
  low = floor(position);
  high = ceil(position);
  low_pos  = (int)(low);
  high_pos = (int)(high);

  if (low_pos == high_pos)
    return ordered[low_pos];
  else
    return (high - position)*ordered[low_pos]
          + (position - low)*ordered[high_pos];

}

static int compare(const void* xv, const void* yv)
{
  const Real* x=xv;
  const Real* y=yv;
  if (*x > *y) return 1;
  if (*x < *y) return -1;
  return 0;
}
