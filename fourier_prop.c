
/*
 * Routines to perform fast fourier transforms on spin correlators 
 * and propagators
 */
#include "fermion.h"
#include "ftrans.h"
#ifdef FFTW
#include <fftw3.h>
#else
#include <fft.h>
#endif
#include <array.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265359

typedef struct {
  double re;
  double im;
} double_complex;


static int fourier_tslice_cpt(Propagator* sk, Propagator* s, FFT_flag);
static int fourier_tslice_gorkov(Propagator*,Propagator*,FFT_flag);
static int fourier_tslice_spincpt(Propagator*,Propagator*,FFT_flag);
static int fourier_time_gorkov(Propagator*,Propagator*);
static int fourier_time_spincpt_sgl(Propagator*,Propagator*);

static inline int fourier_time_spincpt(Propagator* sk, Propagator* skt)
{ return fourier_time_spincpt_sgl(sk,skt); }

int set_prop_momenta(Propagator* s, Ftrans_params* fpar)
{
  const char fnm[MAXNAME] = "set_prop_momenta";
  Lattice* lat = s->parm->gpar->lattice;
  int i;

  for (i=0; i<DIR; i++) {
    if (fpar->nmomenta[i]<0 || fpar->nmomenta[i]>=lat->length[i]) {
      fprintf(stderr,"%s: illegal nmomenta[%d]=%d\n",fnm,i,fpar->nmomenta[i]);
      return BADDATA;
    }
    if (fpar->nmomenta[i]>lat->length[i]/2) {
      fprintf(stderr,"%s: Warning: nmom>L/2 in direction %d\n",fnm,i);
    }
    s->nmom[i] = fpar->nmomenta[i];
    if (s->parm->bcs[i]==ANTIPERIODIC) s->ntotmom[i]=2*s->nmom[i];
    else s->ntotmom[i]=2*s->nmom[i]+1;
  }

  return 0;
}

/*
 * fourier_tslice_prop assumes the lattice sizes are the same in all
 *    the three spatial directions.  This can be easily generalised
 *    if necessary.
 */

int fourier_tslice_prop(Propagator* sk, Propagator* s, FFT_flag flag)
{
  const char fnm[MAXNAME] = "fourier_tslice_prop";
  Lattice* lat = s->parm->gpar->lattice;
  const int ndim = 3;
  const int Lx = lat->length[IX];
  const int Ly = lat->length[IY];
  const int Lz = lat->length[IZ];
#ifdef __GNUC__
  const int L[3] = {Lz,Ly,Lx};
#else
  int len[3];
#endif
  unsigned int i, x[3], k[3], ic, jc, is, js;
  int src_offset = FALSE;
  static Complex* epz=0;  /* phase factors exp(ipz), z=source location */
  const int t = s->tslice;
#ifdef FFTW
  int j;
  static fftw_complex* tmp=0;
  static fftw_plan plan=0;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#else
  static double_complex*** tmp=0;
#endif
  Prop* ss = s->prop;
  Prop** sp;

  if (s->space!=TSLICE_X) {
    fprintf(stderr,"%s: input prop is not in timesliced x-space\n",fnm);
    return BADDATA;
  }

/* check boundary conditions (must be periodic) */
  for (i=0; i<ndim; i++) if (s->parm->bcs[i] != PERIODIC) {
    fprintf(stderr, 
	    "%s: illegal (non-periodic) boundary condition in direction %d\n",
	    fnm,i);
    return BADDATA;
  }

  if (sk==s) {
    fprintf(stderr,"%s: illegal attempt to overwrite input data\n",fnm);
    return BADDATA;
  }

/*** check the validity and compatibility of sk ***/
  if (sk->parm != s->parm) {
    fprintf(stderr, "%s: parameters do not match\n",fnm);
    return BADDATA;
  }
  if (sk->type!=s->type || sk->space!=TSLICE_P) {
    fprintf(stderr,"%s: incompatible type for fourier prop\n",fnm);
    return BADDATA;
  }
  for (i=0; i<ndim; i++) {
    if (sk->nmom[i]<0) {
      fprintf(stderr,"%s: uninitialised momenta for fourier prop\n",fnm);
      return BADDATA;
    }
  }
  if (s->type==SPINOR) return fourier_tslice_cpt(sk,s,flag);
  if (s->type==COLMAT) return fourier_tslice_spincpt(sk,s,flag);
  if (s->type==GORKOVPROP) return fourier_tslice_gorkov(sk,s,flag);

  if (!sk->prop) {
    sk->prop = sp = arralloc(sizeof(Prop),2,lat->length[IT],sk->npoint);
  } else sp = sk->prop;
  if (!sp) {
    fprintf(stderr,"%s: cannot allocate space for fourier prop\n",fnm);
    return NOSPACE;
  }

  if (!tmp || flag==FFT_INIT) {
#ifdef FFTW
    tmp = fftw_malloc(sizeof(Prop)*s->npoint);
#else
    tmp = arralloc(sizeof(double_complex), 3, Lz, Ly, Lx);
#endif
  }
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

  src_offset = (s->parm->source_address[IX]!=0
		|| s->parm->source_address[IY]!=0
		|| s->parm->source_address[IZ]!=0);
  /* we assume that successive calls to fourier_tslice_prop have *
   * the same source address, otherwise we need to call with     *
   * FFT_END before reinitialising                               */
  if (src_offset && (!epz || flag==FFT_INIT)) {
    int maxmom = sk->nmom[IX];
    int mu;
    Complex** expp;
    Complex phase, phase1;
    Real sin1, cos1;
    
    if (sk->nmom[IY]>maxmom) maxmom = sk->nmom[IY];
    if (sk->nmom[IZ]>maxmom) maxmom = sk->nmom[IZ];
    expp = arralloc(sizeof(Complex),2,3,maxmom+1);

    epz = vecalloc(sizeof(Complex),sk->npoint);
    if (!epz) {
      fprintf(stderr,"%s: Warning: cannot alloc space for phase factors\n",fnm);
      fprintf(stderr,"%s: Warning: proceeding without phase factors\n",fnm);
      src_offset = FALSE;
    }
    /* first set up phase factors in each direction   */
    /* we are only calculating factors for positive k *
     * since -k is the complex conjugate              */
    expp[IX][0].re = expp[IY][0].re = expp[IZ][0].re = 1.0;
    expp[IX][0].im = expp[IY][0].im = expp[IZ][0].im = 0.0;
    for (mu=0;mu<SPATIAL_DIR;mu++) {
      /* note that exp(i*pi*x0[mu]) can be in any quadrant  *
       * so sin and cos must be computed separately         */
      sin1 = sin(2.0*PI*s->parm->source_address[mu]/lat->length[mu]);
      cos1 = cos(2.0*PI*s->parm->source_address[mu]/lat->length[mu]);
      for (i=1;i<=sk->nmom[mu];i++) {
	expp[mu][i].re = expp[mu][i-1].re*cos1 - expp[mu][i-1].im*sin1;
	expp[mu][i].im = expp[mu][i-1].re*sin1 + expp[mu][i-1].im*cos1;
      }
    }
    /* now combine these to set up total phase factors */
    /* note that we need to branch for pos/neg momenta */
    for (i=0,k[IZ]=0;k[IZ]<sk->ntotmom[IZ];k[IZ]++) {
      int kz = abs(k[IZ]-sk->nmom[IZ]);
      phase = expp[IZ][kz];
      if (k[IZ]<sk->nmom[IZ]) phase.im = -phase.im;
      for (k[IY]=0;k[IY]<sk->ntotmom[IY];k[IY]++) {
	int ky = abs(k[IY]-sk->nmom[IY]);
	if (k[IY]<sk->nmom[IY]) {
	  phase1.re = phase.re*expp[IY][ky].re + phase.im*expp[IY][ky].im;
	  phase1.im = phase.im*expp[IY][ky].re - phase.re*expp[IY][ky].im;
	} else {
	  phase1.re = phase.re*expp[IY][ky].re - phase.im*expp[IY][ky].im;
	  phase1.im = phase.im*expp[IY][ky].re + phase.re*expp[IY][ky].im;
	}
	for (k[IX]=0;k[IX]<sk->ntotmom[IX];k[IX]++,i++) {
	  int kx = abs(k[IX]-sk->nmom[IX]);
	  if (k[IX]<sk->nmom[IX]) {
	    epz[i].re = phase1.re*expp[IX][kx].re + phase1.im*expp[IX][kx].im;
	    epz[i].im = phase1.im*expp[IX][kx].re - phase1.re*expp[IX][kx].im;
	  } else {
	    epz[i].re = phase1.re*expp[IX][kx].re - phase1.im*expp[IX][kx].im;
	    epz[i].im = phase1.im*expp[IX][kx].re + phase1.re*expp[IX][kx].im;
	  }
	}
      }
    }
  }
#ifndef __GNUC__
/** cc does not understand initialisation of const arrays **
 **         so it must be done here                       **/
  L[0] = Lz; L[1] = Ly; L[2] = Lx;
#endif

#ifdef FFTW
  /*** initialise the fftw on the first pass                    ***/
  if (!plan || flag==FFT_INIT) {
    if (wisdom=fopen(wisdom_name,"r")) {
      fftw_import_wisdom_from_file(wisdom);
      fclose(wisdom);
    }
    plan = fftw_plan_many_dft(ndim,L,SPINCOL*SPINCOL,tmp,0,SPINCOL*SPINCOL,1,
			      tmp,0,SPINCOL*SPINCOL,1,FFTW_FORWARD,flags);
  }
  memcpy(tmp,ss,sizeof(Prop)*s->npoint);
  fftw_execute(plan);
#if 0 // guru interface - requires plan created with different in and out
  fftw_execute_dft(plan,(double*)ss,tmp);
#endif
  for (i=0,k[IZ]=0; k[IZ] < sk->ntotmom[IZ]; k[IZ]++) {
    x[IZ] = (k[IZ]+Lz-sk->nmom[IZ])%Lz;
    for (k[IY]=0; k[IY] < sk->ntotmom[IY]; k[IY]++) {
      x[IY] = (k[IY]+Ly-sk->nmom[IY])%Ly;
      for (k[IX]=0; k[IX] < sk->ntotmom[IX]; k[IX]++,i++) {
	x[IX] = (k[IX]+Lx-sk->nmom[IX])%Lx;
	j = (x[IX]+Lx*(x[IY]+Ly*x[IZ]))*SPINCOL*SPINCOL;
	for (is=0; is<SPIN; is++) for (js=0; js<SPIN; js++)
	  for (ic=0; ic<COLOUR; ic++) for (jc=0; jc<COLOUR; jc++,j++) {
	    sp[t][i][is][js][ic][jc].re = tmp[j][0];
	    sp[t][i][is][js][ic][jc].im = tmp[j][1];
	  }
	}
      }
    }
  /*** destroy plan at the very end ***/
  if (flag==FFT_END) {
    free(tmp); tmp=0;
    fftw_destroy_plan(plan); plan=0;
    if (src_offset) {
      free(epz); epz=0;
    }
  }
#else /*** fourier transform one (spin,col) cpt at a time   ***/
  for (is=0; is<SPIN; is++) {
    for (js=0; js<SPIN; js++) {
      for (ic=0; ic<COLOUR; ic++) {
	for (jc=0; jc<COLOUR; jc++) {

/***  Assign temporary array for fourier transform ***/
	  for (i=0,x[IZ]=0; x[IZ] < Lz; x[IZ]++) {
	    for (x[IY]=0; x[IY] < Ly; x[IY]++) {
	      for (x[IX]=0; x[IX] < Lx; x[IX]++,i++) {
		tmp[x[IZ]][x[IY]][x[IX]].re = ss[i][is][js][ic][jc].re;
		tmp[x[IZ]][x[IY]][x[IX]].im = ss[i][is][js][ic][jc].im;
	      } 
	    } 
	  } 

          /*** Compute fourier transform ***/
	  nfft((double *)**tmp, L, ndim, -1);
	  
      /***  Pick out the momenta we want to keep, and reorder so that the ***
       ***  values will be -nmom...nmom (instead of 0...L-1).             ***/
	  for (i=0,k[IZ]=0; k[IZ] < sk->ntotmom[IZ]; k[IZ]++) {
	    x[IZ] = (k[IZ]+Lz-sk->nmom[IZ])%Lz;
	    for (k[IY]=0; k[IY] < sk->ntotmom[IY]; k[IY]++) {
	      x[IY] = (k[IY]+Ly-sk->nmom[IY])%Ly;
	      for (k[IX]=0; k[IX] < sk->ntotmom[IX]; k[IX]++,i++) {
		x[IX] = (k[IX]+Lx-sk->nmom[IX])%Lx;
		sp[t][i][is][js][ic][jc].re = tmp[x[IZ]][x[IY]][x[IX]].re;
		sp[t][i][is][js][ic][jc].im = tmp[x[IZ]][x[IY]][x[IX]].im;
#ifdef DEBUG
		fprintf(stderr,"%d %d %d --> %d %d %d\n",z,y,x,kz,ky,kx);
#endif
	      }
	    }
	  }

	} /* end loop over jc */
      } /* end loop over ic */
    } /* end loop over js */
  } /* end loop over is */

  if (flag==FFT_END) {
    free(tmp); tmp=0;
    free_nfft();
  }
#endif
  /* Put in phase factors for source location if required */
  if (src_offset) {
    for (i=0;i<sk->npoint;i++) {
      for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	  for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
	      Complex tmp = sp[t][i][is][js][ic][jc];
	      sp[t][i][is][js][ic][jc].re = tmp.re*epz[i].re - tmp.im*epz[i].im;
	      sp[t][i][is][js][ic][jc].im = tmp.re*epz[i].im + tmp.im*epz[i].re;
	    }
	}
    }
    if (flag==FFT_END) {
      free(epz); epz=0;
    }
  }
  return 0;
}
  
static int fourier_tslice_gorkov(Propagator* sk, Propagator* s, FFT_flag flag)
{
  const char fnm[MAXNAME] = "fourier_tslice_gorkov";
  Lattice* lat = s->parm->gpar->lattice;
  const int ndim = 3;
  const int Lx = lat->length[IX];
  const int Ly = lat->length[IY];
  const int Lz = lat->length[IZ];
#ifdef __GNUC__
  const int L[3] = {Lz,Ly,Lx};
#else
  int len[3];
#endif
  unsigned int i, x[3], k[3], ic, jc, is, js, ifl, jfl;
  const int t = s->tslice;
#ifdef FFTW
  int j;
  static fftw_complex* tmp = 0;
  static fftw_plan plan = 0;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#else
  static double_complex*** tmp=0;
#endif
  GProp* ss = s->prop;
  GProp** sp;

  if (!sk->prop) {
    sk->prop = sp = arralloc(sizeof(GProp),2,lat->length[IT],sk->npoint);
  } else sp = sk->prop;
  if (!sp) {
    fprintf(stderr,"%s: cannot allocate space for fourier prop\n",fnm);
    return NOSPACE;
  }

  if (!tmp || flag==FFT_INIT) {
#ifdef FFTW
    tmp = fftw_malloc(sizeof(Prop)*s->npoint);
#else
    tmp = arralloc(sizeof(double_complex), 3, Lz, Ly, Lx);
#endif
  }
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

#ifndef __GNUC__
/** cc does not understand initialisation of const arrays **
 **         so it must be done here                       **/
  L[0] = Lz; L[1] = Ly; L[2] = Lx;
#endif

#ifdef FFTW
  if (!plan || flag==FFT_INIT) {
    if (wisdom=fopen(wisdom_name,"r")) {
      fftw_import_wisdom_from_file(wisdom);
      fclose(wisdom);
    }
    plan = fftw_plan_many_dft(ndim,L,4*SPINCOL*SPINCOL,
			      tmp,0,4*SPINCOL*SPINCOL,1,
			      tmp,0,4*SPINCOL*SPINCOL,1,FFTW_FORWARD,flags);
  }
  memcpy(tmp,ss,sizeof(Prop)*s->npoint);
  fftw_execute(plan);
#if 0 // guru interface
  fftw_execute_dft(plan,ss,tmp);
#endif
  /***  Pick out the momenta we want to keep, and reorder so that the ***
   ***  values will be -nmom...nmom (instead of 0...L-1).             ***/
  for (i=0,k[IZ]=0; k[IZ] < sk->ntotmom[IZ]; k[IZ]++) {
    x[IZ] = (k[IZ]+Lz-sk->nmom[IZ])%Lz;
    for (k[IY]=0; k[IY] < sk->ntotmom[IY]; k[IY]++) {
      x[IY] = (k[IY]+Ly-sk->nmom[IY])%Ly;
      for (k[IX]=0; k[IX] < sk->ntotmom[IX]; k[IX]++,i++) {
	x[IX] = (k[IX]+Lx-sk->nmom[IX])%Lx;
	j = (x[IX]+Lx*(x[IY]+Ly*x[IZ]))*4*SPINCOL*SPINCOL;
	for (ifl=0;ifl<2;ifl++) for (jfl=0;jfl<2;jfl++)
	  for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
	    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++,j++) {
	      sp[t][i][ifl][jfl][is][js][ic][jc].re = tmp[j][0];
	      sp[t][i][ifl][jfl][is][js][ic][jc].im = tmp[j][1];
	  }
	}
      }
    }
  if (flag==FFT_END) {
    fftw_destroy_plan(plan);
    fftw_free(tmp);
  }
#else /*** fourier transform one (flav,spin,col) cpt at a time   ***/
  for (ifl=0;ifl<2;ifl++) for (jfl=0;jfl<2;jfl++)
    for (is=0; is<SPIN; is++) for (js=0; js<SPIN; js++)
      for (ic=0; ic<COLOUR; ic++) for (jc=0; jc<COLOUR; jc++) {

/***  Assign temporary array for fourier transform ***/
	for (i=0,x[IZ]=0; x[IZ] < Lz; x[IZ]++) {
	  for (x[IY]=0; x[IY] < Ly; x[IY]++) {
	    for (x[IX]=0; x[IX] < Lx; x[IX]++,i++) {
	      tmp[x[IZ]][x[IY]][x[IX]].re = ss[i][ifl][jfl][is][js][ic][jc].re;
	      tmp[x[IZ]][x[IY]][x[IX]].im = ss[i][ifl][jfl][is][js][ic][jc].im;
	    } 
	  } 
	} 

	/*** Compute fourier transform ***/
	nfft((double *)**tmp, L, ndim, -1);
	  
      /***  Pick out the momenta we want to keep, and reorder so that the ***
       ***  values will be -nmom...nmom (instead of 0...L-1).             ***/
	for (i=0,k[IZ]=0; k[IZ] < sk->ntotmom[IZ]; k[IZ]++) {
	  x[IZ] = (k[IZ]+Lz-sk->nmom[IZ])%Lz;
	  for (k[IY]=0; k[IY] < sk->ntotmom[IY]; k[IY]++) {
	    x[IY] = (k[IY]+Ly-sk->nmom[IY])%Ly;
	    for (k[IX]=0; k[IX] < sk->ntotmom[IX]; k[IX]++,i++) {
	      x[IX] = (k[IX]+Lx-sk->nmom[IX])%Lx;
	      sp[t][i][ifl][jfl][is][js][ic][jc].re = tmp[x[IZ]][x[IY]][x[IX]].re;
	      sp[t][i][ifl][jfl][is][js][ic][jc].im = tmp[x[IZ]][x[IY]][x[IX]].im;
#ifdef DEBUG
	      fprintf(stderr,"%d %d %d --> %d %d %d\n",z,y,x,kz,ky,kx);
#endif
	    }
	  }
	}

      } /* end loop over internal indices */

  if (flag==FFT_END) {
    free(tmp);
    free_nfft();
  }
#endif
  return 0;
}

static int fourier_tslice_cpt(Propagator* sk, Propagator* s, FFT_flag f)
{
  const char fnm[MAXNAME] = "fourier_tslice_cpt";
  fprintf(stderr,"%s: i am not defined!\n",fnm);
  return EVIL;
}

static int fourier_tslice_spincpt(Propagator* sk, Propagator* s, FFT_flag flag)
{
  const char fnm[MAXNAME] = "fourier_tslice_spincpt";
  Lattice* lat = s->parm->gpar->lattice;
  const int ndim = 3;
  const int Lx = lat->length[IX];
  const int Ly = lat->length[IY];
  const int Lz = lat->length[IZ];
#ifdef __GNUC__
  const int L[3] = {Lz,Ly,Lx};
#else
  int L[3];
#endif
  const int t = s->tslice;
  int x, y, z, kx, ky, kz, i, ic, jc;
  Group_mat*  ss = s->prop;
  Group_mat** sp = sk->prop;
#ifdef FFTW
  int j;
  static fftw_complex* tmp = 0;
  static fftw_plan plan = 0;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#else
  static double_complex*** tmp=0;
#endif

  if (s->type!=COLMAT || s->space!=TSLICE_X) {
    fprintf(stderr,"%s: illegal prop type\n",fnm);
    return BADDATA;
  }

  if (!sk->prop) {
    sk->prop = sp = arralloc(sizeof(Prop),2,lat->length[IT],sk->npoint);
  } else sp = sk->prop;
  if (!sp) {
    fprintf(stderr,"%s: cannot allocate space for fourier prop\n",fnm);
    return NOSPACE;
  }

  if (!tmp || flag==FFT_INIT) {
#ifdef FFTW
    tmp = fftw_malloc(sizeof(Prop)*s->npoint);
#else
    tmp = arralloc(sizeof(double_complex),3,Lz,Ly,Lx);
#endif
  }
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

#ifndef __GNUC__
/** cc does not understand initialisation of const arrays **
 **         so it must be done here                       **/
  L[0] = Lz; L[1] = Ly; L[2] = Lx;
#endif
#ifdef FFTW
  if (!plan || flag==FFT_INIT) {
    if (wisdom=fopen(wisdom_name,"r")) {
      fftw_import_wisdom_from_file(wisdom);
      fclose(wisdom);
    }
    plan = fftw_plan_many_dft(ndim,L,COLOUR*COLOUR,tmp,0,COLOUR*COLOUR,1,
			      tmp,0,4*COLOUR*COLOUR,1,FFTW_FORWARD,flags);
  }
  memcpy(tmp,ss,sizeof(Prop)*s->npoint);
  fftw_execute(plan);
#if 0 // guru interface
  fftw_execute_dft(plan,ss,tmp);
#endif
  /***  Pick out the momenta we want to keep, and reorder so that the ***
   ***  values will be -nmom...nmom (instead of 0...L-1).             ***/
  for (i=0,kz=0; kz<sk->ntotmom[IZ]; kz++) {
    z = (kz+Lz-sk->nmom[IZ])%Lz;
    for (ky=0; ky<sk->ntotmom[IY]; ky++) {
      y = (ky+Ly-sk->nmom[IY])%Ly;
      for (kx=0; kx<sk->ntotmom[IX]; kx++,i++) {
	x = (kx+Lx-sk->nmom[IX])%Lx;
	j = (x+Lx*(y+Ly*z))*COLOUR*COLOUR;
	for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++,j++) {
	  sp[t][i][ic][jc].re = tmp[j][0];
	  sp[t][i][ic][jc].im = tmp[j][1];
	}
      }
    }
  }
  if (flag==FFT_END) {
    fftw_destroy_plan(plan);
    fftw_free(tmp);
  }
#else
  for (ic=0; ic<COLOUR; ic++) {
    for (jc=0; jc<COLOUR; jc++) {
      for (i=0,x=0; x<Lx; x++) {
	for (y=0; y<Ly; y++) {
	  for (z=0; z<Lz; z++,i++) {
	    tmp[z][y][x].re = ss[i][ic][jc].re;
	    tmp[z][y][x].im = ss[i][ic][jc].im;
	  } 
	} 
      } 

/*** Compute fourier transform ***/

      nfft((double *)**tmp, L, ndim, -1);
	  
/***  Pick out the momenta we want to keep, and reorder so that the ***
 ***  values will be -nmom...nmom (instead of 0...L-1).             ***/

      for (i=0,kz=0; kz<sk->ntotmom[IZ]; kz++) {
	z = (kz-sk->nmom[IZ])%Lz;
	for (ky=0; ky<sk->ntotmom[IY]; ky++) {
	  y = (ky-sk->nmom[IY])%Ly;
	  for (kx=0; kx<sk->ntotmom[IX]; kx++,i++) {
	    x = (kx-sk->nmom[IX])%Lx;
	    sp[t][i][ic][jc].re = tmp[z][y][x].re;
	    sp[t][i][ic][jc].im = tmp[z][y][x].im;
	  }
	}
      }
    }
  }
  if (flag==FFT_END) {
    free(tmp); tmp=0;
    free_nfft();
  }
#endif
  return 0;
}

/*** Fourier transformation of a fermion propagator in the time direction ***
 *** with half-integer momentum values (antiperiodic boundary conditions) ***
 *** Uses the trick of doubling the lattice size and performing a normal  ***
 *** fourier transformation on the double lattice                         ***/
int fourier_time_prop(Propagator* sk, Propagator* skt)
{
  const char fnm[MAXNAME] = "fourier_time_prop";
  Lattice* lat = skt->parm->gpar->lattice;
  const int nt = lat->length[IT];
  const int twice_nt = 2*nt;
  int L = twice_nt;
  const int nsmom = skt->npoint;
  int nmom = sk->nmom[IT];
  int is, js, ic, jc, ip, t;
  const int src_offset = (skt->parm->source_address[IT]!=0);
#ifdef FFTW
  const int ntot = nsmom*SPINCOL*SPINCOL;
  int i, j;
  fftw_complex* tmp;
  static fftw_plan plan;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#else
  double_complex* tmp;
#endif
  Prop** spt = skt->prop;
  Prop*  sp  = sk->prop;

  if (skt->space!=TSLICE_P) {
    fprintf(stderr,"%s: input data not in timesliced momentum space\n",fnm);
    return BADDATA;
  }
  if (skt->type==GORKOVPROP) return fourier_time_gorkov(sk,skt);
  if (skt->type==COLMAT) return fourier_time_spincpt(sk,skt);
  if (skt->type!=PROP) {
    fprintf(stderr,"%s: wrong type for timesliced prop\n",fnm);
    return BADDATA;
  }

/*** check boundary conditions ***/
  if (skt->parm->bcs[IT] != ANTIPERIODIC) {
    fprintf(stderr,"%s: illegal (periodic) boundary conditions in time\n",fnm);
    return BADPARAM;
  }
  if (sk==skt) {
    if (sk->nmom[IT]<0) {
      fprintf(stderr,"%s: Warning: setting nmom[%d] to default (L/4)\n",
	      fnm,IT);
      nmom = sk->nmom[IT] = lat->length[IT]/4;
      sk->ntotmom[IT] = 2*sk->nmom[IT];
    }
    sp = vecalloc(sizeof(Prop),sk->ntotmom[IT]*nsmom);
  } else {
/* check validity and compatibility of sk */
    if (sk->parm != skt->parm) {
      fprintf(stderr, "%s: parameters do not match\n",fnm);
      return BADDATA;
    }
    if (sk->type!=skt->type) {
      fprintf(stderr,"%s: incompatible type for fourier prop\n",fnm);
      return BADDATA;
    }
    if (sk->nmom[IT]<0) {
      fprintf(stderr,"%s: uninitialised momenta for fourier prop\n",fnm);
      return BADDATA;
    }
    if (!sk->prop) {
      sk->prop = sp = vecalloc(sizeof(Prop),nmom*nsmom);
    }
  }

  if (!sp) {
    fprintf(stderr,"%s: cannot alloc space for fourier prop\n",fnm);
    return NOSPACE;
  }

  if (nmom>nt/2) {
    fprintf(stderr,"%s: Illegal nmom %d > %d/2\n",fnm,nmom,nt);
    return BADPARAM;
  }

#ifdef FFTW
  tmp = fftw_malloc(sizeof(Prop)*nsmom*twice_nt);
  if (wisdom=fopen(wisdom_name,"r")) {
    fftw_import_wisdom_from_file(wisdom);
    fclose(wisdom);
  }
  plan = fftw_plan_many_dft(1,&L,SPINCOL*SPINCOL*nsmom,
			    tmp,0,nsmom*SPINCOL*SPINCOL,1,
			    tmp,0,nsmom*SPINCOL*SPINCOL,1,FFTW_FORWARD,flags);
  /*** replicate the data with opposite sign for the second copy ***/
  memcpy(tmp,spt,sizeof(Prop)*nsmom);
  memcpy(tmp+nt*ntot,spt,sizeof(Prop)*nsmom);
  for (i=nt*ntot;i<2*nt*ntot;i++) {
    tmp[i][0] = -tmp[i][0]; tmp[i][1] = -tmp[i][1];
  }
  fftw_execute(plan);
  for (i=nmom,t=0;t<nmom;t++) {
    int ki=(2*t+1)*ntot;
    int kj=(2*nt-2*t-1)*ntot;
    j=(nmom-t-1)*nsmom;
    for (ip=0;ip<nsmom;ip++,i++,j++) {
      for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
	for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++,ki++,kj++) {
	  sp[i][is][js][ic][jc].re = tmp[ki][0]/2.0;
	  sp[i][is][js][ic][jc].im = tmp[ki][0]/2.0;
	  sp[j][is][js][ic][jc].re = tmp[kj][1]/2.0;
	  sp[j][is][js][ic][jc].im = tmp[kj][1]/2.0;
	}
      }
  }
#else
/*** allocate array for the fft ***/
  tmp = vecalloc(sizeof(double_complex),twice_nt);
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

  for (ip=0;ip<nsmom;ip++) {
    for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
      for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
	for (t=0;t<nt;t++) {
	  tmp[t].re    =  spt[t][ip][is][js][ic][jc].re;
	  tmp[t].im    =  spt[t][ip][is][js][ic][jc].im;
	  tmp[t+nt].re = -spt[t][ip][is][js][ic][jc].re;
	  tmp[t+nt].im = -spt[t][ip][is][js][ic][jc].im;
	}

/* 		fft1(tmp, tmp, twice_nt, -1); */
	nfft((double*)tmp, &L, 1, -1);

	for (t=0;t<nmom;t++) {
	  sp[(nmom+t)*nsmom+ip][is][js][ic][jc].re = tmp[2*t+1].re/2.0;
	  sp[(nmom+t)*nsmom+ip][is][js][ic][jc].im = tmp[2*t+1].im/2.0;
	  sp[(nmom-t-1)*nsmom+ip][is][js][ic][jc].re = tmp[2*nt-2*t-1].re/2.0;
	  sp[(nmom-t-1)*nsmom+ip][is][js][ic][jc].im = tmp[2*nt-2*t-1].im/2.0;
	}
      }
  }
  free_nfft();
/*   free_fft1(); */
#endif
  free(tmp);

  /* Compute and add in phase factors from source location if required */
  if (src_offset) {
    Complex* epz = vecalloc(sizeof(Complex),nmom);
    Real sin1, cos1, rtmp, itmp;
    int i;
    Complex phase;

    /* first compute exp(i*pi*t0/Lt) ie the half-momentum */
    /* note that it can be in any quadrant so sin and cos *
     * must be computed separately                        */
    sin1 = sin(PI*skt->parm->source_address[IT]/lat->length[IT]);
    cos1 = cos(PI*skt->parm->source_address[IT]/lat->length[IT]);
    epz[0].re = cos1; epz[0].im = sin1;
    /* now compute the integer-momentum factor exp(2i*pi*t0/Lt); */
    rtmp = cos1*cos1 - sin1*sin1;
    sin1 = 2.0*cos1*sin1;
    cos1 = rtmp;
    for (t=1;t<nmom;t++) {
      epz[t].re = epz[t-1].re*cos1 - epz[t-1].im*sin1;
      epz[t].im = epz[t-1].im*cos1 + epz[t-1].re*sin1;
    }
    /* multiply in phase factors: first negative then positive mom values */
    for (i=0,t=nmom-1;t>=0;t--) {
      for (ip=0;ip<nsmom;ip++,i++) {
	for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
		rtmp = sp[i][is][js][ic][jc].re;
		itmp = sp[i][is][js][ic][jc].im;
		sp[i][is][js][ic][jc].re = rtmp*epz[t].re + itmp*epz[t].im;
		sp[i][is][js][ic][jc].im = itmp*epz[t].re - rtmp*epz[t].im;
	      }
	  }
      }
    }
    for (t=0;t<nmom;t++) {
      for (ip=0;ip<nsmom;ip++,i++) {
	for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++) {
	    for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
		rtmp = sp[i][is][js][ic][jc].re;
		itmp = sp[i][is][js][ic][jc].im;
		sp[i][is][js][ic][jc].re = rtmp*epz[t].re - itmp*epz[t].im;
		sp[i][is][js][ic][jc].im = itmp*epz[t].re + rtmp*epz[t].im;
	      }
	  }
      }
    }
    free(epz);
  }
  if (sk==skt) {
    free(skt->prop);
    sk->prop = sp;
    sk->npoint *= sk->ntotmom[IT];
    sk->space = FOURIER;
    return construct_prop_filename(sk);
  }
  return 0;
}

/*** This is an alternative way of fourier transforming in time with ***
 *** half-integer momentum values, using a table of phase factors    ***/
int fourier_time_prop_sgl(Propagator* sk, Propagator* skt)
{
  const char fnm[MAXNAME] = "fourier_time_prop_sgl";
  Lattice* lat = skt->parm->gpar->lattice;
  const int nt = lat->length[IT];
  const int nsmom = skt->npoint;
  int nmom = sk->nmom[IT];
  int is, js, ic, jc, ip, t;
  Prop** spt = skt->prop;
  Prop*  sp  = sk->prop;
  double_complex *tmp = vecalloc(sizeof(double_complex), nt);
  double_complex *expit = vecalloc(sizeof(double_complex), nt);
#ifdef FFTW
  fftw_complex* tp;
  fftw_plan plan;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#endif
  if (sk==skt) {
    if (sk->nmom[IT]<0) {
      fprintf(stderr,"%s: Warning: setting nmom[%d] to default (L/4)\n",
	      fnm,IT);
      nmom = sk->nmom[IT] = lat->length[IT]/4;
      sk->ntotmom[IT] = 2*sk->nmom[IT];
    }
    sp = vecalloc(sizeof(Prop),sk->ntotmom[IT]*nsmom);
  } else {
/* check validity and compatibility of sk */
    if (sk->parm != skt->parm) {
      fprintf(stderr, "%s: parameters do not match\n",fnm);
      return BADDATA;
    }
    if (sk->type!=skt->type) {
      fprintf(stderr,"%s: incompatible type for fourier prop\n",fnm);
      return BADDATA;
    }
    if (sk->space!=FOURIER) {
      fprintf(stderr,"%s: fourier prop not in momentum space!\n",fnm);
      return BADDATA;
    }
    if (sk->nmom[IT]<0) {
      fprintf(stderr,"%s: uninitialised momenta for fourier prop\n",fnm);
      return BADDATA;
    }
    if (!sk->prop) {
      sk->prop = sp = vecalloc(sizeof(Prop),nmom*nsmom);
    }
  }

  if (!sp) {
    fprintf(stderr,"%s: cannot alloc space for fourier prop\n",fnm);
    return NOSPACE;
  }

  if (nmom > nt/2) {
    printf("%s: Illegal nmom %d > %d/2\n",fnm,nmom,nt);
    return BADPARAM;
  }

/*** Initialise the fft ***/
#ifdef FFTW
  if (wisdom=fopen(wisdom_name,"r")) {
    fftw_import_wisdom_from_file(wisdom);
    fclose(wisdom);
  }
  tp = (fftw_complex*) tmp;
  plan = fftw_plan_dft_1d(nt,tp,tp,FFTW_FORWARD,flags);
#else
  free_nfft();
#endif
/*** Initialise table of factors [ exp(-i PI t/nt) ] ***/
  expit[0].re = 1.0;
  expit[0].im = 0.0;
  expit[1].re =  cos(PI/nt);
  expit[1].im = -sin(PI/nt);
  for (t=2; t<nt; t++) {
    expit[t].re = expit[t-1].re*expit[1].re - expit[t-1].im*expit[1].im;
    expit[t].im = expit[t-1].im*expit[1].re + expit[t-1].re*expit[1].im;
  }

  for (ip=0; ip<nsmom; ip++) {
    for (is=0; is<SPIN; is++) {
      for (js=0; js<SPIN; js++) {
	for (ic=0; ic<COLOUR; ic++) {
	  for (jc=0; jc<COLOUR; jc++) {
	    for (t=0; t<nt; t++) {
	      tmp[t].re = spt[t][ip][is][js][ic][jc].re*expit[t].re
		        - spt[t][ip][is][js][ic][jc].im*expit[t].im;
	      tmp[t].im = spt[t][ip][is][js][ic][jc].im*expit[t].re
		        + spt[t][ip][is][js][ic][jc].re*expit[t].im;
#ifdef DEBUG
	      if ((is==js) && (ic==0) && (jc==0))
		printf("p=%d t=%d prop(%d %d %d %d)=%g %g\n",
		       ip, t, is, js, ic, jc,
		       creal(tmp[t]), cimag(tmp[t])); 
#endif
	    }
#ifdef FFTW
	    fftw_execute(plan);
#else
	    fft1((double*)tmp, (double*)tmp, nt, -1);
#endif
	    for (t=0; t<nmom; t++) {
	      sp[(nmom+t)*nsmom+ip][is][js][ic][jc].re = tmp[t].re;
	      sp[(nmom+t)*nsmom+ip][is][js][ic][jc].im = tmp[t].im;
	      sp[(nmom-t-1)*nsmom+ip][is][js][ic][jc].re = tmp[nt-t-1].re;
	      sp[(nmom-t-1)*nsmom+ip][is][js][ic][jc].im = tmp[nt-t-1].im;

	    }
	  }
#ifdef DEBUG
	  for (t=0; t<2*nmom; t++) {
	    printf("p=(%d,%d,%d,%d) prop(%d %d 0 0)=%g %g\n",
		   t, px, py, pz, is, is,
		   creal(sp[t][pz][py][px][is][is][0][0]),
		   cimag(sp[t][pz][py][px][is][is][0][0])); 
	  }
#endif
	}
      }
    }
  }
  
  free(tmp);
  free(expit);
#ifdef FFTW
  fftw_destroy_plan(plan);
#endif
  if (sk==skt) {
    free(skt->prop);
    sk->prop = sp;
    sk->npoint *= sk->ntotmom[IT];
    sk->space = FOURIER;
    return construct_prop_filename(sk);
  }
  return 0;
}

static int fourier_time_gorkov(Propagator* sk, Propagator* skt)
{
  const char fnm[MAXNAME] = "fourier_time_gorkov";
  Lattice* lat = skt->parm->gpar->lattice;
  const int nt = lat->length[IT];
  const int twice_nt = 2*nt;
  int L = twice_nt;
  const int nsmom = skt->npoint;
  int nmom = sk->nmom[IT];
  int ifl, jfl, is, js, ic, jc, ip, t;
#ifdef FFTW
  const int ntot = nsmom*4*SPINCOL*SPINCOL;
  int i, j;
  fftw_complex* tmp;
  static fftw_plan plan;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#else
  double_complex* tmp;
#endif
  GProp** spt = skt->prop;
  GProp*  sp  = sk->prop;

  if (skt->space!=TSLICE_P || skt->type!=GORKOVPROP) {
    fprintf(stderr,"%s: wrong prop type!\n",fnm);
    return BADDATA;
  }

/*** check boundary conditions ***/
  if (skt->parm->bcs[IT] != ANTIPERIODIC) {
    fprintf(stderr,"%s: illegal (periodic) boundary conditions in time\n",fnm);
    return BADPARAM;
  }
  if (sk==skt) {
    if (sk->nmom[IT]<0) {
      fprintf(stderr,"%s: Warning: setting nmom[%d] to default (L/4)\n",
	      fnm,IT);
      nmom = sk->nmom[IT] = lat->length[IT]/4;
      sk->ntotmom[IT] = 2*sk->nmom[IT];
    }
    sp = vecalloc(sizeof(GProp),sk->ntotmom[IT]*nsmom);
  } else {
/* check validity and compatibility of sk */
    if (sk->parm != skt->parm) {
      fprintf(stderr, "%s: parameters do not match\n",fnm);
      return BADDATA;
    }
    if (sk->type!=skt->type) {
      fprintf(stderr,"%s: incompatible type for fourier prop\n",fnm);
      return BADDATA;
    }
    if (sk->nmom[IT]<0) {
      fprintf(stderr,"%s: uninitialised momenta for fourier prop\n",fnm);
      return BADDATA;
    }
    if (!sk->prop) {
      sk->prop = sp = vecalloc(sizeof(Prop),nmom*nsmom);
    }
  }

  if (!sp) {
    fprintf(stderr,"%s: cannot alloc space for fourier prop\n",fnm);
    return NOSPACE;
  }

  if (nmom>nt/2) {
    fprintf(stderr,"%s: Illegal nmom %d > %d/2\n",fnm,nmom,nt);
    return BADPARAM;
  }

/*** allocate array for the fft ***/
#ifdef FFTW
  tmp = fftw_malloc(sizeof(GProp)*nsmom*twice_nt);
#else
  tmp = vecalloc(sizeof(double_complex),twice_nt);
#endif
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }
#ifdef FFTW
  if (wisdom=fopen(wisdom_name,"r")) {
    fftw_import_wisdom_from_file(wisdom);
    fclose(wisdom);
  }
  plan = fftw_plan_many_dft(1,&L,4*SPINCOL*SPINCOL*nsmom,
			    tmp,0,nsmom*4*SPINCOL*SPINCOL,1,
			    tmp,0,nsmom*4*SPINCOL*SPINCOL,1,
			    FFTW_FORWARD,flags);
  /*** replicate the data with opposite sign for the second copy ***/
  memcpy(tmp,spt,sizeof(GProp)*nsmom);
  memcpy(tmp+nt*ntot,spt,sizeof(GProp)*nsmom);
  for (i=nt*ntot;i<2*nt*ntot;i++) {
    tmp[i][0] = -tmp[i][0]; tmp[i][1] = -tmp[i][1];
  }
  fftw_execute(plan);
  for (i=nmom,t=0;t<nmom;t++) {
    int ki=(2*t+1)*ntot;
    int kj=(2*nt-2*t-1)*ntot;
    j=(nmom-t-1)*nsmom;
    for (ip=0;ip<nsmom;ip++,i++,j++) {
      for (ifl=0;ifl<2;ifl++) for (jfl=0;jfl<2;jfl++)
	for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
	  for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++,ki++,kj++) {
	    sp[i][ifl][jfl][is][js][ic][jc].re = tmp[ki][0]/2.0;
	    sp[i][ifl][jfl][is][js][ic][jc].im = tmp[ki][0]/2.0;
	    sp[j][ifl][jfl][is][js][ic][jc].re = tmp[kj][1]/2.0;
	    sp[j][ifl][jfl][is][js][ic][jc].im = tmp[kj][1]/2.0;
	  }
    }
  }
  fftw_destroy_plan(plan);
  fftw_free(tmp);
#else
  for (ip=0;ip<nsmom;ip++) {
    for (ifl=0;ifl<2;ifl++) for (jfl=0;jfl<2;jfl++)
      for (is=0;is<SPIN;is++) for (js=0;js<SPIN;js++)
	for (ic=0;ic<COLOUR;ic++) for (jc=0;jc<COLOUR;jc++) {
	  for (t=0;t<nt;t++) {
	    tmp[t].re    =  spt[t][ip][ifl][jfl][is][js][ic][jc].re;
	    tmp[t].im    =  spt[t][ip][ifl][jfl][is][js][ic][jc].im;
	    tmp[t+nt].re = -spt[t][ip][ifl][jfl][is][js][ic][jc].re;
	    tmp[t+nt].im = -spt[t][ip][ifl][jfl][is][js][ic][jc].im;
	  }

/* 		fft1(tmp, tmp, twice_nt, -1); */
	  nfft((double*)tmp, &L, 1, -1);

	  for (t=0;t<nmom;t++) {
	    sp[(nmom+t)*nsmom+ip][ifl][jfl][is][js][ic][jc].re = tmp[2*t+1].re/2.0;
	    sp[(nmom+t)*nsmom+ip][ifl][jfl][is][js][ic][jc].im = tmp[2*t+1].im/2.0;
	    sp[(nmom-t-1)*nsmom+ip][ifl][jfl][is][js][ic][jc].re = tmp[2*nt-2*t-1].re/2.0;
	    sp[(nmom-t-1)*nsmom+ip][ifl][jfl][is][js][ic][jc].im = tmp[2*nt-2*t-1].im/2.0;
	  }
	}
  }
  free_nfft();
/*   free_fft1(); */
#endif
  free(tmp);

  if (sk==skt) {
    free(skt->prop);
    sk->prop = sp;
    sk->npoint *= sk->ntotmom[IT];
    sk->space = FOURIER;
    return construct_prop_filename(sk);
  }
  return 0;
}


static int fourier_time_spincpt_sgl(Propagator* sk, Propagator* skt)
{
  const char fnm[MAXNAME] = "fourier_time_spincpt_sgl";
  Lattice* lat = skt->parm->gpar->lattice;
  const int nt = lat->length[IT];
  const int nsmom = skt->npoint;
  int nmom = sk->nmom[IT];
  int ic, jc, ip, t;
  Group_mat** spt = skt->prop;
  Group_mat*  sp  = sk->prop;
#ifdef FFTW
  fftw_complex* tp;
  fftw_plan plan;
  unsigned flags = FFTW_PATIENT;
  FILE* wisdom;
  char wisdom_name[MAXLINE] = "wisdom";
#endif
  double_complex *tmp = vecalloc(sizeof(double_complex),nt);
  double_complex *expit = vecalloc(sizeof(double_complex),nt);

  if (skt->space!=TSLICE_P) {
    fprintf(stderr,"%s: input data not in timesliced momentum space\n",fnm);
    return BADDATA;
  }
  if (skt->type!=COLMAT) {
    fprintf(stderr,"%s: Wrong input type!\n",fnm);
    return BADDATA;
  }

  if (nmom > nt/2) {
    fprintf(stderr,"%s: Illegal nmom %d > %d/2\n",fnm,nmom,nt);
    return BADPARAM;
  }

/*** Initialise the fft ***/
#ifdef FFTW
  if (wisdom=fopen(wisdom_name,"r")) {
    fftw_import_wisdom_from_file(wisdom);
    fclose(wisdom);
  }
  tp = (fftw_complex*) tmp;
  plan = fftw_plan_dft_1d(nt,tp,tp,FFTW_FORWARD,flags);
#else
  free_nfft();
#endif
/*** Initialise table of factors ***/
  expit[0].re = 1.0;
  expit[0].im = 0.0;
  expit[1].re =  cos(PI/nt);
  expit[1].im = -sin(PI/nt);
  for (t=2; t<nt; t++) {
    expit[t].re = expit[t-1].re*expit[1].re - expit[t-1].im*expit[1].im;
    expit[t].im = expit[t-1].im*expit[1].re + expit[t-1].re*expit[1].im;
  }

/*** Loop over spatial momenta and colour indices ***/
  for (ip=0; ip<nsmom; ip++) {
    for (ic=0; ic<COLOUR; ic++) {
      for (jc=0; jc<COLOUR; jc++) {
/*** assign temporary array for fft ***/
	for (t=0; t<nt; t++) {
	  tmp[t].re = spt[t][ip][ic][jc].re*expit[t].re
        	    - spt[t][ip][ic][jc].im*expit[t].im;
	  tmp[t].im = spt[t][ip][ic][jc].im*expit[t].re
		    + spt[t][ip][ic][jc].re*expit[t].im;
#ifdef DEBUG
	  if ((ic == 0) && (jc == 0))
	    printf("p=%d t=%d prop(%d %d)=%g %g\n",
		   ip, t, ic, jc, creal(tmp[t]), cimag(tmp[t])); 
#endif
	}
/*** perform fourier transform ***/
#ifdef FFTW
	fftw_execute(plan);
#else
	nfft((double*)tmp, &nt, 1, -1);
#endif
	for (t=0; t<nmom; t++) {
	  sp[(nmom+t)*nsmom+ip][ic][jc].re = tmp[t].re;
	  sp[(nmom+t)*nsmom+ip][ic][jc].im = tmp[t].im;
	  sp[(nmom-t-1)*nsmom+ip][ic][jc].re = tmp[nt-t-1].re;
	  sp[(nmom-t-1)*nsmom+ip][ic][jc].im = tmp[nt-t-1].im;
	}
      }
#ifdef DEBUG
      for (t=0; t<2*nmom; t++) {
	printf("p=(%d,%d,%d,%d) prop(0 0)=%g %g\n",
		   t, px, py, pz,
		   creal(sp[t][px][py][pz][0][0]),
		   cimag(sp[t][px][py][pz][0][0])); 
	  }
#endif
	}
  }
  
  free(tmp);
  free(expit);
#ifdef FFTW
  fftw_destroy_plan(plan);
#endif

  if (sk==skt) {
    free(skt->prop);
    sk->prop = sp;
    sk->npoint *= sk->ntotmom[IT];
    sk->space = FOURIER;
    return construct_prop_filename(sk);
  }
  return 0;
}

/*** fourier transform a dirac spinor in time, using integer momenta (!) ***/
int fourier_dirac(Dirac *corr_p, Dirac *corr, int nmom, int nt)
{
  const char fnm[MAXNAME] = "fourier_dirac";
#ifdef FFTW
  fprintf(stderr,"%s: FFTW not implemented!\n",fnm);
  return EVIL;
#else
  int is, js, t;
  double_complex *tmp = arralloc(sizeof(double_complex), 1, nt);
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

  if (nmom > nt/2) {
    fprintf(stderr,"%s: Illegal nmom %d > %d/2\n",fnm,nmom,nt);
    return BADPARAM;
  }

  for (is=0; is<SPIN; is++) {
    for (js=0; js<SPIN; js++) {
      for (t=0; t<nt; t++) {
	tmp[t].re = corr[t][is][js].re;
	tmp[t].im = corr[t][is][js].im;
      }
      fft1((double*)tmp, (double*)tmp, nt, -1);

      corr_p[nmom][is][js].re = tmp[0].re;
      corr_p[nmom][is][js].im = tmp[0].im;
      for (t=1; t <= nmom; t++) {
	corr_p[nmom+t][is][js].re = tmp[t].re;
	corr_p[nmom+t][is][js].im = tmp[t].im;
	corr_p[nmom-t][is][js].re = tmp[nt-t].re;
	corr_p[nmom-t][is][js].im = tmp[nt-t].im;
      }
    }
  }
  
  free(tmp);

  return 0;
#endif
}

