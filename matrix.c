#include "matrix.h"

void matrix_unit(Complex** res, int sz)
{
  int i,j;
  for (i=0;i<sz;i++) {
    res[i][i].re = 1.0;
    res[i][i].im = 0.0;
    for (j=i+1;j<sz;j++)
      res[i][j].re=res[i][j].im=res[j][i].re=res[j][i].im=0.0;
  }
}

void matrix_mult(Complex** res, Complex** m1, Complex** m2, int sz)
{
  int i,j,k;
  for (i=0;i<sz;i++) for (j=0;j<sz;j++) {
    res[i][j].re=res[i][j].im=0.0;
    for (k=0;k<sz;k++) {
      res[i][j].re += m1[i][k].re*m2[k][j].re - m1[i][k].im*m2[k][j].im;
      res[i][j].im += m1[i][k].re*m2[k][j].im + m1[i][k].im*m2[k][j].re;
    }
  }
}

void matrix_scale(Complex** m, Real r, int sz)
{
  int i, j;
  for (i=0;i<sz;i++) for (j=0;j<sz;j++) {
      m[i][j].re *= r;
      m[i][j].im *= r;
    }
}

void matrix_add_real(Complex** m, Real r, int sz)
{
  int i;
  for (i=0;i<sz;i++) m[i][i].re += r;
}

/*** complex matrix inversion ***/
#ifdef M_INVERSE
#include <matrix_inverse.h>
static double** remat = 0;
static double** immat = 0;
static double** reinv = 0;
static double** iminv = 0;

void matrix_inverse(Complex** res, Complex** mat, int size)
{
  int i, j;
  if (!remat) {
    remat = arralloc(sizeof(double),2,size,size);
    immat = arralloc(sizeof(double),2,size,size);
    reinv = arralloc(sizeof(double),2,size,size);
    iminv = arralloc(sizeof(double),2,size,size);
  }

  for (i=0;i<size;i++) for (j=0;j<size;j++) {
    remat[i][j] = mat[i][j].re;
    immat[i][j] = mat[i][j].im;
  }
  svd_inverse_complex(reinv,iminv,remat,immat,size);
  for (i=0;i<size;i++) for (j=0;j<size;j++) {
    res[i][j].re = reinv[i][j];
    res[i][j].im = iminv[i][j];
  }
}
#endif

