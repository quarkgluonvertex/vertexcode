#include <stdio.h>
#include <malloc.h>
#include <magic.h>

typedef double Real;  /* assume data are stored in double precision */

int main(int argc, char** argv)
{
  int i, j, n, len;
  FILE* in;
  FILE* out;
  Real* data;
  Real* tmpdata;

  if (argc<3) {
    printf("Usage: %s outfile infile1 infile2 ...\n",argv[0]);
    return BADPARAM;
  }

  /*** check for length of input file 1 ***/
  if (!(in = fopen(argv[2],"rb"))) {
    fprintf(stderr,"Cannot open %s\n",argv[2]);
    return BADFILE;
  }
  fseek(in,0,SEEK_END);
  len = ftell(in)/sizeof(Real);
  rewind(in);

  /*** allocate space for data ***/
  data    = malloc(sizeof(double)*len);
  tmpdata = malloc(sizeof(double)*len);
  if ( !data || !tmpdata ) {
    fprintf(stderr,"Cannot allocate space for data\n");
    return NOSPACE;
  }

  /*** read data from first input file ***/
  if ( (n=fread(data,sizeof(Real),len,in)) != len) {
    fprintf(stderr,"Failed to read data from %s: read only %d of %d items\n",
	    argv[2],n,len);
    return BADFILE;
  }

  /*** loop over remaining input files, read and add to data ***/
  for (i=3;i<argc;i++) {
    if (!(in = freopen(argv[i],"rb",in))) {
      fprintf(stderr,"Cannot open %s\n",argv[i]);
      return BADFILE;
    }
    if ( (n=fread(tmpdata,sizeof(Real),len,in)) != len) {
      fprintf(stderr,"Failed to read data from %s: read only %d of %d items\n",
	      argv[i],n,len);
      fclose(in);
      return BADFILE;
    }
    for (j=0;j<len;j++) data[j]+=tmpdata[j];
  }
  fclose(in);

  /*** normalise data and write to output file ***/
  n = argc-2;  /* number of input files */
  for (j=0;j<len;j++) data[j] /= n; 

  if (!(out = fopen(argv[1],"wb"))) {
    fprintf(stderr,"Cannot open %s for writing\n",argv[1]);
    return BADFILE;
  }

  if ( (n=fwrite(data,sizeof(Real),len,out)) != len ) {
    fprintf(stderr,"Failed to write data to %s: wrote only %d of %d items\n",
	    argv[1],n,len);
    fclose(out);
    return BADFILE;
  }
  
  fclose(out);
  return 0;
}
