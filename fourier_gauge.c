#include "gauge.h"
#include "ftrans.h"
#include <fft.h>
#define PI 3.14159265359

static int fourier_4d_gluon(Gauge* up, Gauge* u);
static int fourier_4d_gauge(Gauge* up, Gauge* u);

int fourier_gauge(Gauge* up, Gauge* u)
{
  const char fnm[MAXLINE] = "fourier_gauge";
  const int ndim = u->parm->lattice->ndim;
  if (ndim != 4) {
    fprintf(stderr,"%s: wrong number of dimensions %d\n",fnm,ndim);
    return BADDATA;
  }
  switch (u->type) {
  case GAUGE:   return fourier_4d_gauge(up,u);
  case ALGEBRA: return fourier_4d_gluon(up,u);
  case FGAUGE:
  case FALG:
    printf("%s: input field is already in momentum space",fnm);
    printf(" -- copying to output\n");
    if (up!=u) *up=*u;
    return 0;
  default:
    printf("%s: unknown gauge type %d\n",fnm,u->type);
    return BADDATA;
  }
}

static int fourier_4d_gauge(Gauge* up, Gauge* u)
{
  const char fnm[MAXLINE] = "fourier_4d_gauge";
  Lattice* lat = u->parm->lattice;
  const int Lx = lat->length[IX];
  const int Ly = lat->length[IY];
  const int Lz = lat->length[IZ];
  const int Lt = lat->length[IT];
  const int ndim = lat->ndim;
  const Real volnorm = 1.0/sqrt((Real)lat->nsite);

#ifdef __GNUC__
  const int len[DIR] = {Lt,Lz,Ly,Lx};
#else
  int len[DIR];
#endif
  int i, x, y, z, t, kx, ky, kz, kt, icol, jcol, mu;
  int ntot;

  fft_complex ****tmp;
  Group_el**  uu = u->u;
  Group_mat** fu;

  if (u->type==ALGEBRA) return fourier_4d_gluon(up,u);

/*** check dimensions ***/
  if (ndim != 4) {
    fprintf(stderr,"%s: wrong number of dimensions %d != 4\n",fnm,ndim);
    return BADDATA;
  }

/*** check boundary conditions (must be periodic) ***/
  for (mu=0; mu<DIR; mu++) {
    if (u->parm->bcs[mu] != PERIODIC) {
      fprintf(stderr, 
	      "%s: illegal (non-periodic) boundary condition in direction %d\n",
	      fnm,mu);
      return BADDATA;
    }
  }

  ntot=1;
  if (up==u) {
    for (mu=0; mu<ndim; mu++) {
      if (up->nmom[mu]<0) {
	fprintf(stderr,"%s: Warning: setting nmom[%d] to default (L/4)\n",
		fnm,mu);
	up->nmom[mu]=lat->length[mu]/4;
	up->ntotmom[mu] = 2*up->nmom[mu]+1;
      }
      ntot *= up->ntotmom[mu];
    }
    fu = arralloc(sizeof(Group_mat),2,ntot,u->ndir);
  } else {
    /*** check the validity and compatibility of up ***/
    if (u->parm != up->parm) {
      fprintf(stderr, "%s: parameters do not match\n",fnm);
      return BADDATA;
    }
    if (up->type!=FGAUGE) {
      fprintf(stderr,"%s: incompatible type for fourier gauge\n",fnm);
      return BADDATA;
    }
    for (mu=0; mu<ndim; mu++) {
      if (up->nmom[mu]<0) {
	fprintf(stderr,"%s: uninitialised momenta for fourier gauge\n",fnm);
	return BADDATA;
      }
    }
    ntot = up->npoint;
    if (!up->u) {
      up->u = fu = arralloc(sizeof(Group_mat),2,ntot,u->ndir);
    } else fu = up->u;
  }
  if (!fu) {
    fprintf(stderr,"%s: cannot allocate space for fourier gauge\n",fnm);
    return NOSPACE;
  }

  tmp = arralloc(sizeof(fft_complex), 4, Lt, Lz, Ly, Lx);
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

#ifndef __GNUC__
/** cc does not understand initialisation of const arrays **
 **         so it must be done here                       **/
  len[0]=Lt;  len[1]=Lz;  len[2]=Ly;  len[3]=Lx;
#endif

  init_nfft(len, DIR, -1);

  for (icol=0; icol < COLOUR; icol++) {
    for (jcol=0; jcol < COLOUR; jcol++) {
      for (mu=0; mu < DIR; mu++) {

/***  Allocate temporary array for fourier transform ***/
	for (i=0,t=0; t < Lt; t++) {
	  for (z=0; z < Lz; z++) {
	    for (y=0; y < Ly; y++) {
	      for (x=0; x < Lx; x++,i++) {
#ifdef SU2_GROUP
		tmp[t][z][y][x].re = (icol==jcol) ? uu[i][mu][0]
		  :  (icol==0) ? uu[i][mu][2] : -uu[i][mu][2];
		tmp[t][z][y][x].im = (icol==jcol) ? 
		  (icol==0) ? uu[i][mu][3] : - uu[i][mu][3]
		  : uu[i][mu][1];
#else
		tmp[t][z][y][x].re = uu[i][mu][icol][jcol].re;
		tmp[t][z][y][x].im = uu[i][mu][icol][jcol].im;
#endif
	      }
	    }
	  }
	}
	
/*** Compute fourier transform ***/

  	nfft((double *) ***tmp, len, DIR, -1);

/***  Pick out the momenta we want to keep, and reorder so that the ***
 ***  values will be -nmom...nmom (instead of 0...L-1).             ***/
	for (i=0,kt=0; kt < up->ntotmom[IT]; kt++) {
	  t = (kt+Lt-up->nmom[IT])%Lt;
	  for (kz=0; kz < up->ntotmom[IZ]; kz++) {
	    z = (kz+Lz-up->nmom[IZ])%Lz;
	    for (ky=0; ky < up->ntotmom[IY]; ky++) {
	      y = (ky+Ly-up->nmom[IY])%Ly;
	      for (kx=0; kx < up->ntotmom[IX]; kx++,i++) {
		x = (kx+Lx-up->nmom[IX])%Lx;
		fu[i][mu][icol][jcol].re = tmp[t][z][y][x].re*volnorm;
		fu[i][mu][icol][jcol].im = tmp[t][z][y][x].im*volnorm;
#ifdef DEBUG
		fprintf(stderr,"%d %d %d %d --> %d %d %d %d\n",
			t,z,y,x,kt,kz,ky,kx);
#endif
	      }
	    }
	  }
	}
      } /* end loop over mu */
    } /* end loop over jcol */
  } /* end loop over icol */

  free_nfft();

  if (up==u) {
    free(u->u);
    up->u = fu;
    up->npoint = ntot;
    up->type = FGAUGE;
  }
  return 0;
}

static int fourier_4d_gluon(Gauge* up, Gauge* u)
{
  const char fnm[MAXLINE] = "fourier_gluon";
  Lattice* lat = u->parm->lattice;
  const int Lx = lat->length[IX];
  const int Ly = lat->length[IY];
  const int Lz = lat->length[IZ];
  const int Lt = lat->length[IT];
  const int maxlen = (Lx<Ly) ? (Ly<Lz) ? (Lz<Lt) ? Lt : Lz
    : (Ly<Lt) ? Lt : Ly 
    : (Lx<Lz) ? (Lz<Lt) ? Lt : Lz : (Lx<Lt) ? Lt : Lx;
  const int ndim = lat->ndim;
  const Real sqrtvol = sqrt((Real)lat->nsite);
#ifdef __GNUC__
  const int len[4] = {Lt,Lz,Ly,Lx};
  Complex expp[maxlen+1];
#else
  int len[4];
  Complex* expp = vecalloc(sizeof(Complex),maxlen+1);
#endif
  int i, j, x[4], k[4], ialg, mu;
  int ntot;

  Real cos1, sin1;
  fft_complex ****tmp;
  Algebra_el** a = u->u;
  Algebra_c** ap;

/*** check dimensions ***/
  if (ndim != 4) {
    fprintf(stderr,"%s: wrong number of dimensions %d != 4\n",fnm,ndim);
    return BADDATA;
  }

/*** check boundary conditions (must be periodic) ***/
  for (mu=0; mu<ndim; mu++) {
    if (u->parm->bcs[mu] != PERIODIC) {
      fprintf(stderr, 
	      "%s: illegal (non-periodic) boundary condition in direction %d\n",
	      fnm,mu);
      return BADDATA;
    }
  }

  ntot=1;
  if (up==u) {
    for (mu=0; mu<ndim; mu++) {
      if (up->nmom[mu]<0) {
	fprintf(stderr,"%s: Warning: setting nmom[%d] to default (L/4)\n",
		fnm,mu);
	up->nmom[mu]=lat->length[mu]/4;
	up->ntotmom[mu] = 2*up->nmom[mu]+1;
      }
      ntot *= up->ntotmom[mu];
    }
    ap = arralloc(sizeof(Algebra_c),2,ntot,u->ndir);
  } else {
    /*** check the validity and compatibility of up ***/
    if (u->parm != up->parm) {
      fprintf(stderr, "%s: parameters do not match\n",fnm);
      return BADDATA;
    }
    if (up->type!=FALG) {
      fprintf(stderr,"%s: incompatible type for fourier gauge\n",fnm);
      return BADDATA;
    }
    for (mu=0; mu<ndim; mu++) {
      if (up->nmom[mu]<0) {
	fprintf(stderr,"%s: uninitialised momenta for fourier gauge\n",fnm);
	return BADDATA;
      }
    }
    ntot = up->npoint;
    if (!up->u) {
      up->u = ap = arralloc(sizeof(Algebra_c),2,ntot,u->ndir);
    } else ap = up->u;
  }
  if (!ap) {
    fprintf(stderr,"%s: cannot allocate space for fourier doublet\n",fnm);
    return NOSPACE;
  }

  /*** fft takes double-precision input ***/
  tmp = arralloc(sizeof(fft_complex), 4, Lt, Lz, Ly, Lx);
  if (!tmp) {
    fprintf(stderr,"%s: cannot alloc space for tmp array\n",fnm);
    return NOSPACE;
  }

#ifndef __GNUC__
/** cc does not understand initialisation of const arrays **
 **         so it must be done here                       **/
  len[0]=Lt;  len[1]=Lz;  len[2]=Ly;  len[3]=Lx;
#endif

  /*** set up tables of phase factors ***/
  init_nfft(len, ndim, -1);

  /*** Phase factors of 'half-momenta'.  Note that the second half ***
   *** of the momentum lattice corresponds to negative momenta     ***/
  for (mu=0; mu<u->ndir; mu++) {
    expp[0].re = expp[lat->length[mu]].re = 1.0;
    expp[0].im = expp[lat->length[mu]].im = 0.0;
    sin1 = -sin(PI/lat->length[mu]);
    cos1 = sqrt(1.0-sin1*sin1);
    for (i=1,j=lat->length[mu]-1;i<lat->length[mu]/2;i++,j--) {
      expp[i].re = expp[i-1].re*cos1 - expp[i-1].im*sin1;
      expp[i].im = expp[i-1].re*sin1 + expp[i-1].im*cos1;
      expp[j].re =  expp[j+1].re*cos1 + expp[j+1].im*sin1;
      expp[j].im = -expp[j+1].re*sin1 + expp[j+1].im*cos1;
    }
    /*** the edge of the brillouin zone is a dreadful place to be ***
     *** but we assign a value to it nonetheless                  ***/
    if (lat->length[mu]%2==0) {
      i=lat->length[mu]/2;
      expp[i].re = expp[i-1].re*cos1 - expp[i-1].im*sin1;
      expp[i].im = expp[i-1].re*sin1 + expp[i-1].im*cos1;
    }
    /*** First we fourier transform the gauge potentials a           ***
     *** Note that the ftrans of a real is complex                   ***/
    for (ialg=0; ialg<NALG; ialg++) {

      /***  Allocate temporary array for fourier transform ***/
      for (i=0,x[IT]=0; x[IT] < Lt; x[IT]++) {
	for (x[IZ]=0; x[IZ] < Lz; x[IZ]++) {
	  for (x[IY]=0; x[IY] < Ly; x[IY]++) {
	    for (x[IX]=0; x[IX] < Lx; x[IX]++,i++) {
	      tmp[x[IT]][x[IZ]][x[IY]][x[IX]].re = a[i][mu][ialg];
	      tmp[x[IT]][x[IZ]][x[IY]][x[IX]].im = 0.0;
	    }
	  }
	}
      }

      /*** Compute fourier transform ***/
      nfft((double *) ***tmp, len, ndim, -1);

      /***  Pick out the momenta we want to keep, and reorder so that the ***
       ***  values will be -nmom...nmom (instead of 0...L-1).             ***/
      for (i=0,k[IT]=0; k[IT] < up->ntotmom[IT]; k[IT]++) {
	x[IT] = (k[IT]+Lt-up->nmom[IT])%Lt;
	for (k[IZ]=0; k[IZ] < up->ntotmom[IZ]; k[IZ]++) {
	  x[IZ] = (k[IZ]+Lz-up->nmom[IZ])%Lz;
	  for (k[IY]=0; k[IY] < up->ntotmom[IY]; k[IY]++) {
	    x[IY] = (k[IY]+Ly-up->nmom[IY])%Ly;
	    for (k[IX]=0; k[IX] < up->ntotmom[IX]; k[IX]++,i++) {
	      x[IX] = (k[IX]+Lx-up->nmom[IX])%Lx;
	      /** we define the fields on the midpoint of the link so they **
	       ** get a phase factor exp(ip[mu])                           **/
	      ap[i][mu][ialg].re 
		= tmp[x[IT]][x[IZ]][x[IY]][x[IX]].re*expp[x[mu]].re
		- tmp[x[IT]][x[IZ]][x[IY]][x[IX]].im*expp[x[mu]].im;
	      ap[i][mu][ialg].im 
		= tmp[x[IT]][x[IZ]][x[IY]][x[IX]].im*expp[x[mu]].re
		+ tmp[x[IT]][x[IZ]][x[IY]][x[IX]].re*expp[x[mu]].im;
#ifdef DEBUG
	      fprintf(stderr,"%d %d %d %d --> %d %d %d %d (%d)\n",
		      x[IT],x[IZ],x[IY],x[IX],k[IT],k[IZ],k[IY],k[IX], i);
#endif
	    }
	  }
	}
      }

    } /* end loop over ialg */
  } /* end loop over mu */

  free_nfft();

#ifndef __GNUC__
  free(expp);
#endif

  /*** Normalise with sqrt(volume) ***/
  for (i=0; i<ntot; i++) {
    for (mu=0; mu<u->ndir; mu++)
      for (ialg=0; ialg<NALG; ialg++) {
	ap[i][mu][ialg].re /= sqrtvol;
	ap[i][mu][ialg].im /= sqrtvol;
      }
  }
  if (up==u) {
    free(u->u);
    up->u = ap;
    up->npoint = ntot;
    up->type = FALG;
  }

  return 0;
}

