#ifndef _GFIX_H_
#define _GFIX_H_

#include "gauge.h"

#define MAX_GFIXPAR 4

/* magic numbers */
#define OVERRELAX        0
#define CORNELL          1
#define STEEPEST_DESCENT CORNELL
#define FACC             2
#define ANNEAL           3

#define DMUAMU 0
#define Q0     1
#define QALL   2
#define FDIFF  3

typedef struct {
  int gauge;                   /* landau, coulomb       */
  double param;                /* may be useful later   */
  int start;                   /* random, cold, load    */
  double startval;              /* eg. inverse temp for random start */
  int producer_id;             /* for reading externally-produced transforms */
  int id;                      /* ditto                 */
  int algorithm;               /* overrelax, fourier acc, ...       */
  int criterion;               /* (d_mu a_mu)^2, Q, ... */
  double precision;
  double algparm[MAX_GFIXPAR]; /* tunable parameters    */
  int maxiter;
  long seed;
} Gfix_params;

int assign_params_gfix(char* filename, Gauge_parameters* parm, 
		       Gfix_params* gparm);
int init_trans_gfix(Gauge_transform* g, Gauge_parameters* parm, 
		    Gfix_params* run);
int gauge_fix(Gauge* u, Gauge_transform* g, Gfix_params* par);


/************************************************************************
 *** Internal implementation defs and routines -- should in principle ***
 *** not be visible from the outside...                               ***
 ************************************************************************/

/** Gauge_Grp should be exactly the same as Gauge, except that          **
 ** the void* u is explicitly Group_el** so that it can be dereferenced **/
typedef struct {
  Gauge_parameters* parm;
  Gauge_t type;
  int nmom[DIR]; int ntotmom[DIR]; /* dummy members */
  int ndir;
  int npoint;
  Group_el** u;
  char filename[MAX_FILENAME];
} Gauge_Grp;

int gfix_overrel_grp(Gauge_Grp* u, Gauge_transform* g, Gfix_params* par);
int gfix_sd_grp(Gauge_Grp* u, Gauge_transform* g, Gfix_params* par);
int gfix_facc_grp(Gauge_Grp* u, Gauge_transform* g, Gfix_params* par);
int gfix_anneal_grp(Gauge_Grp* u, Gauge_transform* g, Gfix_params* par);

#endif
