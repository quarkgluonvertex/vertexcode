#ifndef _FIELD_PARAMS_H_
#define _FIELD_PARAMS_H_
#include <magic.h>
#include "lattice.h"

/** The following codes are logically enums **/

/* Codes for initialisation of fields */
#define COLD 0
#define HOT  1
#define LOAD 2
#define UNIT_GAUGE COLD
#define RANDOM     HOT
#define READ       LOAD

/* Codes for action types */
#define WILSON 0          /* Gauge and fermion!! */
#define CLOVER 1
#define STAGGERED      2
#define GAUGEHIGGS     3
#define SYMANZIK       4  /* Symanzik improved gauge action */
#define TSI31          5
#define FWCHW31        6
/* Add further types as required */

/* Codes for gauge types */
#define RANDOM_G  0
#define COULOMB   1
#define LANDAU    2
#define COVARIANT 3
#define AXIAL     4
#define UNITARY   5

/* Codes for source types */
#define POINT   0
#define ROTATED 1
#define FUZZED  2
#define SMEARED 3

/** Parameters for a gauge field **/
#define MAXPREC 4
typedef struct {
  Lattice*  lattice;
  int       action;              /* Wilson, improved, dynamical...          */
  int       start;               /* disordered, ordered,... (for updating)  */
  double    beta;                /* Could in principle be part of Lattice   */
  int       bcs[DIR];            /* Boundary conditions                     */
  int       nf;                  /* number of flavours for dynamical        */
  double*   mass;                /* For dynamical configs (or improvement)  */
  int       has_chempot;         /* Flag for nonzero chemical potential     */
  double    plaquette;           /* Average plaquette                       */
  int       gauge;               /* Landau, Coulomb, covariant etc          */
  double    gauge_param;         /* eg. xi in covariant gauge               */
  double    rtrace;              /* function maximised in gauge fixing      */
  double    gfix_precision[MAXPREC];
  int       gfix_iterations;

  int       producer_id;         /* code for origin of the config: Gauge connection, */
                                 /* UKQCD, Roma, Swansea etc                */
  int       id;                  /* additional identification               */
  int       sweep;
  int       time;
  unsigned int checksum;         /* checksum on the configuration */
  char      info[MAXLINE];       /* various other info            */
} Gauge_parameters;

/** Parameters for a higgs or gauge-higgs field **/
typedef struct {
  Lattice* lattice;
  double mu;
  double lambda;
  double vev;
  double beta;   /* HACK for consistency */
  double T;      /* input temperature */
  double dt;     /* time-step */
  int sweep;
  int time;
  int producer_id;
  int id;
} Higgs_parameters;

/** Parameters for a fermion field or quark propagator **/
typedef struct {
  Gauge_parameters* gpar;
/*** These are only the parameters exclusive to quark propagators ***/
  int action;                   /* Wilson, clover, staggered etc           */
  double kappa;			/* hopping parameter                       */
  double kappat;		/* temporal hopping parameter (anisotropic)*/
  double xi;    		/* anisotropy = kappat/kappa               */
  double kappa_c;               /* critical hopping parameter (if known)   */
  double mass;                  /* subtracted bare mass (if known)         */
  int ncoeff;                   /* number of improvement coefficients      */
                                /* (usually given by the action            */
  double* imp;                  /* improvement coefficients                */
  double chempot;               /* chemical potential                      */
  double j, jbar;               /* diquark sources or similar              */
  int bcs[DIR];			/* flags for boundary conditions           */
  int source_type;		/* Point, smeared etc..                    */
  double kappa_scalar;		/* determines the amount of smearing       */
  int source_address[DIR];	/* address of source - or time slice       */
  int spin, colour;             /* dilution parameters etc                 */
  int nev;                      /* number of eigenvectors (spectral repr)  */
  int producer_id;              /* code for where the propagator came from */
                                /* UKQCD, Roma, Swansea etc                */
  int id;                       /* additional identification               */
  char info[MAXLINE];           /* various other info                      */
} Propagator_parameters;

int setup_configs(int**,int*,char*);
#endif
