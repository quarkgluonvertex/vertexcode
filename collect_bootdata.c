#include "analysis.h"
#include "vertex.h"
#include "momenta.h"
#include <string.h>
#include <assert.h>

#define ILLEGAL_VALUE -12345.6789
#define ILLEGAL_INT -12345
#define VECTOR -1

#define AM_MAXCODE 20

#define TINY 1.0e-6
#define INT(a) ((int)((a)+TINY))
#define SWAP(a,b) { int tmp=(a);(a)=(b);(b)=tmp; }

typedef int i4vec[DIR];

static void setup_qequiv(i4vec q, i4vec** qarr, int* nval);
static int nequiv(int x, int y, int z);
static int create_datastring(char* datastr,
			     Analysis_params* run, Action_par* act);

int main(int argc, char **argv)
{
  Boot *data;
  Boot *datatmp;
  Boot tmp;

  int npt, nalldata, nqeqv, nspace, qt0, qt1;
  int mu, gamma, i, j, jq, pt, qt;
  i4vec p, k;
  i4vec* q=0;
  int zerokin;

  int status;      // exit code

  char filename[MAXLINE];
  char datastr[MAXLINE], cfgstr[MAXLINE];
  char kinstr[AM_MAXCODE]="", qstr[AM_MAXCODE];

  Vertex_params run;

  if (argc != 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename>\n", argv[0]);
    return BADPARAM;
  }

  sprintf(filename, "%s",argv[1]);
  if (assign_params_vertex(filename,&run)) return BADPARAM;
  if ((status=create_datastring(datastr,&run))) return status;

/***  Set up momentum parameters  **/
  npt = 2*run.nmom[IT];
  zerokin = (run.kinematics==ZEROGLUON); // KLUDGE
  if (zerokin) strcpy(kinstr,"z");
  nspace = (2*run.nmom[IX]+1)*(2*run.nmom[IY]+1)*(2*run.nmom[IZ]+1);
  nalldata = npt*nspace;
  qt0 = run.p[IT];
  qt1 = run.q[IT]+1;

  sprintf(cfgstr, "U%d_%dcfg_%dbt", run.start_cfg, run.nconfig, run.nboot);
  sprintf(qstr,"q%d%d%d%d_",run.q[IX],run.q[IY],run.q[IZ],run.q[IT]);

  tmp = init_boot(run.nboot);
  datatmp = init_bootarr(run.nboot,1,npt);
  data = init_bootarr(run.nboot,1,nalldata);

  for (qt=qt0; qt<qt1; qt++) {
    run.q[IT] = qt;
    npt = 2*run.nmom[IT] - qt;
    setup_qequiv(run.q, &q, &nqeqv);

    for (mu=0; mu<=run.mu; mu++) {
      const int nu = mu+1;
      if (run.gamma==VECTOR) gamma=nu;
      /*** loop through equivalent q-values ***/
      for (jq=0; jq<nqeqv; jq++) {
	int ptoff = (q[jq][IT]<0) ? -q[jq][IT] : 0;  /* offset in pt-values */
	if (zerokin && q[jq][mu]!=0) continue;
	sprintf(qstr,"q%d%d%d%d_",
		q[jq][IX],q[jq][IY],q[jq][IZ],q[jq][IT]);
	/*** loop over all p-values ***/
	j=0;
	for (p[IX]=-run.nmom[IX]; p[IX]<=run.nmom[IX]; p[IX]++)
	  for (p[IY]=-run.nmom[IY]; p[IY]<=run.nmom[IY]; p[IY]++)
	    for (p[IZ]=-run.nmom[IZ]; p[IZ]<=run.nmom[IZ]; p[IZ]++) {
	      int valid=TRUE;
	      for (i=0; i<SPATIAL_DIR; i++) {
		k[i] = p[i]+q[jq][i];
		if (k[i]<-run.nmom[i] || k[i]>run.nmom[i]) valid=FALSE;
	      }
	      if (!valid) continue;

	      sprintf(filename,"%s/data_vtx_%s_mu%dg%d%s_%sp%d%d%dt_%s_00.xdr",
		      run.boot_dir,datastr,nu,qstr,p[IX],p[IY],p[IZ],cfgstr);
	      if ((status=read_boot(&datatmp,npt,filename))) return status;
	      for (pt=0; pt<npt; pt++) {
		p[IT] = pt-run.nmom[IT]+ptoff;
		k[IT] = p[IT]+q[jq][IT];
		if (k[IT]<-run.nmom[IT] || k[IT]>=run.nmom[IT]) continue;
		boot_copy(&data[j++],datatmp[pt]);
	      }
	    }
	nalldata=j;
	printf("Writing %d data items for mu=%d q=(%d,%d,%d,%d)\n",
	       nalldata, mu, q[jq][IX], q[jq][IY], q[jq][IZ], q[jq][IT]);
	sprintf(filename,"%s/data_vtx_%s_mu%dg%d%s_%snp%d%d%d%02d%s_%s_",
		run.boot_dir, datastr, nu, gamma, reim[run.realpart],
		qstr, run.nmom[IX], run.nmom[IY], run.nmom[IZ], run.nmom[IT],
		kinstr, cfgstr);
	if ( (status=xdr_write_boot(data,nalldata,filename)) ) return status;
      }
    }
  }
  return 0;
}

static void setup_qequiv(i4vec q, i4vec** qarr, int *nval)
{
  int i, j, k, n, p, nswap, nperm;
  int ix=IX, iy=IY, iz=IZ;
  i4vec* qa;
/*** First sort the components in canonical order ***/
  for (i=0; i<DIR; i++) if (q[i]<0) q[i]=-q[i];
  if (q[IX]>q[IZ]) SWAP(q[IX],q[IZ]);
  if (q[IY]>q[IZ]) SWAP(q[IY],q[IZ]);
  if (q[IX]>q[IY]) SWAP(q[IX],q[IY]);

  n = nequiv(q[IX], q[IY], q[IZ]);
  if (q[IT]) n *= 2;
  nswap = q[IX] ? 8 : q[IY] ? 4 : q[IZ] ? 2 : 1;
  nperm = q[IX]==q[IZ] ? 1 : (q[IX]==q[IY]||q[IY]==q[IZ]) ? 3 : 6;

  if (*qarr) free(*qarr);
  qa = vecalloc(sizeof(i4vec),n+1);

  for (i=0;i<DIR;i++) qa[0][i]=q[i];

  j=1;
  for (p=0;p<nperm;p++) {
/** All possible combinations of +/- the current permutation **/
    for (k=1; k<=nswap; k++,j++) {
      for (i=0;i<DIR;i++) qa[j][i]=qa[j-1][i];
      if (q[IZ]) qa[j][iz] = -qa[j-1][iz];
      if (q[IY] && j%2==0) qa[j][iy] = -qa[j-1][iy];
      if (q[IX] && j%4==0) qa[j][ix] = -qa[j-1][ix];
    }
    if (q[IX]!=q[IZ]) {
/** Permute the momenta **/
      j--;
      if (q[IY]==q[IZ]) {
	if (p%2) {
	  SWAP(qa[j][ix],qa[j][iz]);
	  SWAP(ix,iz);
	} else {
	  SWAP(qa[j][ix],qa[j][iy]);
	  SWAP(ix,iy);
	}
      } else {
	if (p%2) {
	  SWAP(qa[j][ix],qa[j][iz]);
	  SWAP(ix,iz);
	} else {
	  SWAP(qa[j][iy],qa[j][iz]);
	  SWAP(iy,iz);
	}
      }
      j++;
    }
  }

/** Duplicate this with negative qt if appropriate **/
  if (q[IT]) {
    for (j=n/2; j<n; j++) {
      for (i=0;i<DIR;i++) qa[j][i]=qa[j-n/2][i];
      qa[j][IT]=-qa[j][IT];
    }
  }

  *nval = n;
  *qarr = qa;
}

/*  ~nequiv returns the number of vectors which are Z3-equivalent to *
 *     (x,y,z).  It is assumed that x<=y<=z.                         */
static int nequiv(int x, int y, int z)
{
  if (z==0) return 1;
  if (y) {
    if (x==z) {
      return 8;
    }
    else if ((x == y) || (y == z)) {
      if (x) return 24;
      else return 12;
    }
    else {
      if (x) return 48;
      else return 24;
    }
  }
  else return 6;
}

static int create_datastring(char* datastr, Vertex_params* run)
{
  const int action = run->prop->action;
  char actstr[AM_MAXCODE], gaugestr[AM_MAXCODE];
  char impstr[AM_MAXCODE];

  switch (run->gauge->gauge) {
  case LANDAU:
    strcpy(gaugestr,"L"); break;
  case COVARIANT:
    sprintf(gaugestr,"X%02d",INT(10*run->->gauge->gauge_param));
    break;
  default:
    printf("Unknown gauge type %d\n",run->gauge->gauge);
    return BADPARAM;
  }

  /*** Set up strings describing parameters (for filenames) ***/
  switch (action) {
  case WILSON:
    strcpy(actstr,"W"); break;
  case CLOVER:
    sprintf(actstr,"C%03d",INT(100*act->csw)); break;
  default:
    printf("Unknown action type %d\n",action);
    return BADPARAM;
  }

  /** string code for improvement terms: only for clover (improved) action **/
  if (action == CLOVER) {
    if (run->rot==0.0) {
      sprintf(impstr,"l%02db%03d",INT(100*run->lambda),INT(100*run->bm));
    } else {
      if (run->lambda==0.0) {
	sprintf(impstr,"r%03db%03d",INT(1000*run->rot),INT(100*run->bm));
      } else {
	sprintf(impstr,"r%03db%03dl%02d",
		INT(1000*run->rot),INT(100*run->bm),INT(100*run->lambda));
      }
    }
  }
  sprintf(datastr,"B%d%s%sK%04d%s",
	  run->beta_code, gaugestr, actstr, run->kappa, impstr);

  return 0;
}

