#include "gauge.h"
#include <assert.h>

void unit_gauge(Gauge u)
{
  int i, mu;
  Group_el** uu = u.u;
  assert(u.type==GAUGE);
#ifdef SU2_GROUP
  for (i=0; i<u.npoint; i++) {
    for (mu=0; mu<u.ndir; mu++) {
      su2_make_unit(uu[i][mu]);
    }
  }
#elif defined SU3_GROUP
  for (i=0; i<u.npoint; i++) {
    for (mu=0; mu<u.ndir; mu++) {
      su3_make_unit(uu[i][mu]);
    }
  }
#else
  fprintf(stderr,"unit_gauge: wrong group!\n");
  return;
#endif
}

void unit_trans(Gauge_transform g)
{
  int i;

#ifdef SU2_GROUP
  for (i=0; i<g.parm->lattice->nsite; i++) {
    su2_make_unit(g.g[i]);
  }
#elif defined SU3_GROUP
  for (i=0; i<g.parm->lattice->nsite; i++) {
    su3_make_unit(g.g[i]);
  }
#else
  fprintf(stderr,"unit_trans: wrong group!\n");
  return;
#endif
}
