#include "gauge.h"
#include "fermion.h"
#include "ftrans.h"
#include "momenta.h"
#include "mom_fields.h"
#include <array.h>
#include <string.h>

static int construct_quark_filename(char* name, Propagator* s, char* dir);
static int print_quark_prop(Propagator* prop, Momenta* mom, char* filename);

int main(int argc, char** argv) {
  Propagator_parameters parm;
  Propagator prop_in, prop_out;
  Momenta mom;
  Ftrans_params run;
  Real* p_in;
  Real* p_out;
  int i, x;
  int nprop, ndata;
  char filename[MAXLINE];
  int status;

/*** Read run parameters from file ***/
  if (argc < 4) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: %s <param_filename> prop_filename1 prop_filename2 ... out_filename\n", argv[0]);
    return BADPARAM;
  }
  printf("Starting run\n");
  sprintf(filename, "%s",argv[1]);
  if (assign_params_fprop(filename,&parm,&run,&mom)) {
    return BADPARAM;
  }
  nprop = argc-3;
    
  printf("Initialising\n");
/*** Initialise and allocate space ***/
  if (status=set_data_dir(parm.gpar)) return status;
  /* dont crash if filename is not constructed */
  status=init_prop(&prop_in,&parm,FOURIER,PROP);
  if (status && status!=NOCODE) return status;
  status=init_prop(&prop_out,&parm,FOURIER,PROP);
  if (status && status!=NOCODE) return status;
  /* replace auto-filename with input */
  strcpy(prop_out.filename,argv[argc-1]);
  arrf_set_zero(prop_out.prop,sizeof(Prop),sizeof(Real),prop_out.npoint);
  p_out = prop_out.prop;

  ndata = prop_out.npoint*COLOUR*COLOUR*SPIN*SPIN*2;
  
/*** Loop over input files, read propagator and add to output ***/
  for (i=0; i<nprop; i++) {
    strcpy(prop_in.filename,argv[2+i]);
    printf("Reading <%s>\n",prop_in.filename);
    if (status=read_prop(prop_in)) return status;
    p_in = prop_in.prop;
    for (x=0;x<ndata;x++) p_out[x] += p_in[x];
  }    
  for (x=0;x<ndata;x++) p_out[x] /= nprop;

/*** Dump averaged propagator to file ***/
  if ( (status = write_prop(prop_out)) ) return status; 

/*** Now we can compute the two scalar functions if we want to ***/
  if (run.save_propagator) {
    Real** ab;
    int i;
    FILE* out;
    if (status=setup_momenta(&mom)) return status;
    if ( !(ab = arralloc(sizeof(Real),2,2,mom.nval)) ) {
      fprintf(stderr,"Cannot alloc space for propagator functions!\n");
      return NOSPACE;
    }
    /* Propagator needs to be reordered back to tzyx order, assuming xyzt */
    if (status=reorder_prop_tzyx(&prop_out)) return status;
    if (status=quark_propagator(ab[0],ab[1],&prop_out,&mom)) return status;

    status = construct_quark_filename(filename,&prop_out,run.control_dir);
    if (status) return status;
    if ( !(out=fopen(filename,"a")) ) {
      fprintf(stderr,"Cannot open <%s>\n",filename);
      return BADFILE;
    }
    fprintf(out,"Sweep number %d\n",prop_out.parm->gpar->sweep);
    for (i=0;i<mom.nval;i++) {
      fprintf(out," %20.16f\t%20.16f\n",ab[0][i],ab[1][i]);
    }
  }

  return 0;

}

static int construct_quark_filename(char* name, Propagator* s, char* dir)
{
  const char fnm[MAXNAME] = "construct_quark_filename";
  Lattice* lat = s->parm->gpar->lattice;
  Gauge_parameters* gpar = s->parm->gpar;
  char gauge_code[6] = "\0\0\0\0\0\0";
  char cutstr[12] = "";

  if (s->space != FOURIER) {
    fprintf(stderr,"%s: propagator not in momentum space!\n",fnm);
    return BADPARAM;
  }
  if (s->type != PROP) {
    fprintf(stderr,"%s: wrong data type for propagator\n",fnm);
    return BADPARAM;
  }

/*** code for gauge type and parameter ***/
  switch (s->parm->gpar->gauge) {
  case LANDAU: gauge_code[0]='l'; break;
  case COVARIANT: 
    sprintf(gauge_code,"x%02d",(int)(10*s->parm->gpar->gauge_param+0.01));
    break;
  case COULOMB: gauge_code[0]='c'; break;
  default:
    printf("%s: gauge type %d not implemented\n",fnm,
	   s->parm->gpar->gauge);
    printf("%s: defaulting to Landau gauge\n",fnm);
    gauge_code[0]='l';
  }
/*** create filename ***/
  switch (s->parm->gpar->nf) {
  case 0:
    sprintf(name,"%s/quark_b%03ds%02dt%02d%s.k%04d_p%02d%02d%02d%02d%s.ave.dat",
	    dir,INT(s->parm->gpar->beta*100),lat->length[IX],lat->length[IT],
	    gauge_code,INT(s->parm->kappa*10000),
	    s->nmom[IX],s->nmom[IY],s->nmom[IZ],s->nmom[IT],cutstr);
    break;
  case 2:
    if (gpar->has_chempot) {
      sprintf(name,
	      "quark_b%03dks%04dmu%04dj%03ds%02dt%02d%s.kv%04dmu%04dj%03d.ave.dat",
	      INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      INT(gpar->mass[2]*1000),INT(gpar->mass[3]*1000),
	      lat->length[IX],lat->length[IT],
	      gauge_code,INT(s->parm->kappa*10000),INT(s->parm->chempot*1000),
	      INT(s->parm->j*1000));
    } else { /* assume that valence chempot is zero */
      sprintf(name,"quark_b%03dks%04ds%02dt%02d%s.kv%04d.ave.dat",
	      INT(gpar->beta*100),INT(gpar->mass[0]*10000),
	      gpar->lattice->length[IX],gpar->lattice->length[IT],
	      gauge_code,INT(s->parm->kappa*10000));
    }
    break;
  default:
    fprintf(stderr,"%s: Warning: %d flavours not implemented\n",fnm,gpar->nf);
    fprintf(stderr,"%s: Creating 0-flavour name\n",fnm);
    sprintf(name,"%s/quark_b%03ds%02dt%02d%s.k%04d_p%02d%02d%02d%02d%s.ave.dat",
	    dir,INT(s->parm->gpar->beta*100),lat->length[IX],lat->length[IT],
	    gauge_code,INT(s->parm->kappa*10000),
	    s->nmom[IX],s->nmom[IY],s->nmom[IZ],s->nmom[IT],cutstr);
  }


  return 0;
}

static int print_quark_prop(Propagator* prop, Momenta* mom, char* filename)
{
  Real** ab;
  int i;
  int status;
  FILE* out;
  if (!mom->initialised) if (status=setup_momenta(mom)) return status;
  if ( !(ab = arralloc(sizeof(Real),2,2,mom->nval)) ) {
    fprintf(stderr,"Cannot alloc space for propagator functions!\n");
    return NOSPACE;
  }
  if (status=reorder_prop_tzyx(prop)) return status;
  if (status=quark_propagator(ab[0],ab[1],prop,mom)) return status;
  if ( !(out=fopen(filename,"a")) ) {
    fprintf(stderr,"Cannot open <%s>\n",filename);
    return BADFILE;
  }
  fprintf(out,"Sweep number %d\n",prop->parm->gpar->sweep);
  for (i=0;i<mom->nval;i++) {
    fprintf(out," %20.16f\t%20.16f\n",ab[0][i],ab[1][i]);
  }
  free(ab);
  fclose(out);
  if (status=reorder_prop_xyzt(prop)) return status;
  return 0;
}
