#include "gauge.h"
#ifdef XDR
#include "xdr_gauge.h"
#endif
#define WARNING 0

/*************************************************************************
 *                                                                       *
 *  Routines to dump gauge fields to files.                              *
 *                                                                       *
 *  We always dump momentum space gauge fields in xdr format,            *
 *  single precision (this may easily be changed though)                 *
 *                                                                       *
 *************************************************************************/

#ifdef XDR
static int write_fgauge_xdr(Gauge u);
#endif
int dump_gauge(Gauge u);

int write_gauge(Gauge u)
{
  const char fnm[MAXNAME] = "write_gauge";
  switch (u.type) {
  case GAUGE:
    return dump_gauge(u);
  case FGAUGE:
  case FALG:
#ifdef XDR
    return write_fgauge_xdr(u);
#else
    return dump_gauge(u);
#endif
  default:
    fprintf(stderr,"%s: wrong gauge type %d -- only fourier types implemented\n",
	    fnm, u.type);
    return BADPARAM;
  }
}

int dump_gauge(Gauge u)
{
  const char fnm[MAXNAME] = "dump_gauge";
  FILE* out;
  Group_el**   uel = u.u;
  Group_mat** umat = u.u;
  Algebra_c** ualg = u.u;
  const int n = u.ndir*u.npoint;
  int elsz;
  int m;

  if ( !(out = fopen(u.filename,"wb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,u.filename);
    return BADFILE;
  }
  switch (u.type) {
  case GAUGE: 
    m = fwrite(*uel,sizeof(Group_el),n,out);
    break;
  case GAUGEMAT:
  case FGAUGE:
    m = fwrite(*umat,sizeof(Group_mat),n,out);
    break;
  case FALG:
    m = fwrite(*ualg,sizeof(Algebra_c),n,out);
    break;
  default:
    fprintf(stderr,"%s: gauge type %d not implemented\n",fnm,u.type);
    fclose(out);
    return BADDATA;
  }
  if ( m != n ) {
    fprintf(stderr,"%s: failed to write gauge field to %s\n",fnm,u.filename);
    fprintf(stderr,"%s: wrote only %d of %d records\n",fnm,m,n);
    fclose(out);
    return BADFILE;
  }
  fclose(out);

  return 0;
}

#ifdef XDR
static int write_fgauge_xdr(Gauge u)
{
  const char fnm[MAXNAME] = "write_fgauge";
  const u_int size = DIR*u.npoint;
  FILE* out;
  XDR xdrs;
  u_int n=size;
  bool_t result;

/*** check the type ***/
  if (u.type!=FGAUGE && u.type!=FALG) {
    fprintf(stderr,"%s: wrong gauge type %d\n",fnm,u.type);
    return BADDATA;
  }

#ifdef SU3_GROUP
/*** the filename was automatically created at initialisation ***/
  printf("Dumping fourier gauge to <%s>\n", u.filename);
  if ( !(out = fopen(u.filename,"wb")) ) {
    fprintf(stderr,"%s: cannot open %s\n",fnm,u.filename);
    return BADFILE;
  }
  xdrstdio_create(&xdrs, out, XDR_ENCODE);

  if (u.type==FGAUGE) {
  //  result = xdr_array(&xdrs, &dummy, &n, 
  //		     size*sizeof(Su3)/sizeof(Real), sizeof(Real),
  //		     xdr_Real);
    Su3** uu = u.u;
    result = xdr_su3_arr(&xdrs, uu, &n, size);
  } else {
    Su3alg_c** uu = u.u;
    result = xdr_array(&xdrs, uu, &n, size, 
		       sizeof(Su3alg_c), xdr_algebra_c);
  }

  if ( n != size ) {
    fprintf(stderr,"%s: Failed to dump gauge field to file <%s>\n",fnm,
	    u.filename);
    fprintf(stderr,"%s: Wrote only %d elements\n",fnm,n);
    fclose(out);
    return BADFILE;
  }
  fclose(out);
  return result ? 0 : BADFILE;
#else
  fprintf(stderr,"%s: Wrong group! only SU(3) implemented!\n",fnm);
  return 0;
#endif
}
#endif
